//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SeeIT_Data.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class DBOARD_DATA_MASTER_PRIORITY
    {
        public string PRIORITY_ID { get; set; }
        public string REQUEST_PRIORITY { get; set; }
        public Nullable<int> PRIORITY_ORDER { get; set; }
    }
}
