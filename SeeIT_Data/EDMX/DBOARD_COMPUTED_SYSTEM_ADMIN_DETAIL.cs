//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SeeIT_Data.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class DBOARD_COMPUTED_SYSTEM_ADMIN_DETAIL
    {
        public int ADMIN_ID { get; set; }
        public string ADMIN_USER_ID { get; set; }
        public string ADMIN_USER_NAME { get; set; }
        public Nullable<int> IS_ADMIN { get; set; }
    }
}
