﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeIT_Data.SpModel
{
    class USP_CREATE_PARETO_CHART
    {
        public string REQUEST_PRIORITY { get; set; }
        public Double LINE_CHART_VALUE { get; set; }
        public Double BACKLOG { get; set; }
        public Double INCOMING { get; set; }
        public string COLUMN_NAME { get; set; }
        public Int32 EXCEEDING{get;set;}
        public Int32 NON_EXCEEDING{get;set;}
        public string SHOW_FIRST_SPINNER_RANGE { get; set; }
        public string SHOW_SECOND_SPINNER_RANGE { get; set; }
        public Double COLUMN_VALUE { get; set; }

      }
}
