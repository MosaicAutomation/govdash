﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeIT_Data.SpModel
{
    class USP_CREATE_RESPONSE_EXCEEDING
    {
        public string REQUEST_PRIORITY { get; set; }
        public Int32 EXCEEDING_COUNT { get; set; }
        public Int32 NON_EXCEEDING_COUNT { get; set; }
        public Double EXCEEDING_PERC { get; set; }
        public string MONTH_WEEK_MAPPING { get; set; }
    }
}
