﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeIT_Data.SpModel
{
    class USP_CREATE_HEATMAP_DAY
    {
        public string PRIORITY { get; set; }
        public int? date { get; set; }
        public int? value { get; set; }
        public string Category { get; set; }
    }
}
