﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeIT_Data.SpModel
{
    class USP_CREATE_TIME_BOOK
    {
        public Int32 REQUEST_COUNT { get; set; }
        public Double TOTAL_TIME { get; set; }
        public string MONTH_WEEK_MAPPING { get; set; }

    }
}
