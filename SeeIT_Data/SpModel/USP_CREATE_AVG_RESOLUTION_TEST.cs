﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeIT_Data.SpModel
{
    class USP_CREATE_AVG_RESOLUTION_TEST
    {
        public String CHART_TYPE { get; set; }
        public Double REQUEST_ID_COUNT { get; set; }
        public String REQUEST_PRIORITY { get; set; }
        public String MONTH_WEEK_MAPPING { get; set; }
        public String IS_Exceeding { get; set; }
        public Double? DIFF { get; set; }
        public String COLOUR { get; set; }
        public Int32? SLI { get; set; }
        public String SLI_UNIT { get; set; }
        public Int32? ORDERWEEK { get; set; }
    }
}
