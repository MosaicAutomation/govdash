﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeIT_Data.SpModel
{
    class USP_CREATE_AGING_CHART
    {
        public double EXCEEDS_SLA { get; set; }
        public double NOT_EXCEED_SLA { get; set; }
        public double MEET_SLA { get; set; }
        public string REQUEST_PRIORITY { get; set; }
        public Int32? SLI { get; set; }
        public string SLIUnit { get; set; }
    }
}
