﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeIT_Data.SpModel
{
    class USP_CREATE_HEATMAP_HOUR
    {
        public virtual string PRIORITY { get; set; }
        public virtual int? day { get; set; }
        public virtual int? hour { get; set; }
        public virtual int? value { get; set; }
        public virtual string Category { get; set; }
    }
}
