﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeIT_Data.SpModel
{
     class USP_CREATE_INCOMING_COMPLETED
    {
        public string REQUEST_PRIORITY { get; set; }
        public string MONTH_WEEK_MAPPING { get; set; }
        public Double? INCOMING { get; set; }
        public Double? BACKLOG { get; set; }
        public Double? RESOLVED { get; set; }
    }
}
