﻿using System;
using System.Collections.Generic;
using System.Linq;
using SeeIT_Data.EDMX;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.IO;
using System.Text.RegularExpressions;
using System.Data.Entity;
using SeeIT_Data.SpModel;
using SeeIT_Common.Model;
using SeeIT_Common.Constant;
using System.DirectoryServices;
using SeeIT_Common.Helper;
using System.Security.Principal;
using System.Web;

namespace SeeIT_Data.Repository
{
    public class DashboardRepository
    {
        HelperFunctions helper = new HelperFunctions();
        UserSettingRepository UserSettingRepository = new UserSettingRepository();

        public List<Dashboard> GetDashBoardCollections()
        {
            List<Dashboard> UserDashboardData = new List<Dashboard>();
            try
            {

                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = UserSettingRepository.IsLoginForm();
                    string UserID = helper.GetUserID(IsLogin);
                    //vivek 
                    //if (HttpContext.Current.Request.Cookies["UserId"].Value != null)
                    //{
                    //    UserID = HttpContext.Current.Request.Cookies["UserId"].Value.ToString();
                    //    string[] ArrayUserID = UserID.Split('=');
                    //    UserID = ArrayUserID[1];
                    //}

                    if (UserID != "" || UserID != null)
                    {
                        UserDashboardData = (from y in context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL
                                             join x in context.DBOARD_COMPUTED_SYSTEM_MAPPING_DASHBOARD_USER
                                             on y.DASHBOARD_ID equals x.DASHBOARD_ID
                                             where x.USER_ID.Contains(UserID) || x.IS_PRIVACY_SETTING == 1
                                             select new Dashboard
                                             {
                                                 DashBoardIID = y.DASHBOARD_ID,
                                                 DashBoardName = y.DASHBOARD_NAME,
                                                 DashboardIcon = y.DASHBOARD_ICON,
                                                 PrivacySetting = x.IS_PRIVACY_SETTING
                                             }).ToList<Dashboard>();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return UserDashboardData;
        }

        public UserDashboard GetDashboardSettings(string DashboardID, int ISSaved)
        {
            UserDashboard UserDashboardData = new UserDashboard();
            DataSet dsUserSetting = new DataSet(); 
            string Query;
            
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                     var IsLogin = UserSettingRepository.IsLoginForm();
                     string UserID = helper.GetUserID(IsLogin);
                    //Vivek
                    //if (HttpContext.Current.Request.Cookies["UserId"].Value != null)
                    //{
                    //    UserID = HttpContext.Current.Request.Cookies["UserId"].Value.ToString();
                    //    string[] ArrayUserID = UserID.Split('=');
                    //    UserID = ArrayUserID[1];
                    //}

                    Query = "EXEC USP_GET_DASHBOARD_SETTING '" + UserID + "','" + DashboardID + "','" + ISSaved + "'";
                    dsUserSetting = helper.GetResultReport(Query);
                    if (ISSaved == 1)
                    {
                        UserDashboardData = (from userdashboard in dsUserSetting.Tables[5].AsEnumerable()
                                             select new UserDashboard
                                             {
                                                 DashBoardIID = userdashboard.Field<string>("DASHBOARD_ID"),
                                                 DashBoardName = userdashboard.Field<string>("DASHBOARD_NAME"),
                                                 DashboardIcon = userdashboard.Field<string>("DASHBOARD_ICON"),
                                                 DashboardStartDate = userdashboard.Field<DateTime>("START_DATE").Year.ToString() + '-' + userdashboard.Field<DateTime>("START_DATE").Month.ToString("D2") + '-' + userdashboard.Field<DateTime>("START_DATE").Day.ToString("D2"),
                                                 DashboardEndDate = userdashboard.Field<DateTime>("END_DATE").Year.ToString() + '-' + userdashboard.Field<DateTime>("END_DATE").Month.ToString("D2") + '-' + userdashboard.Field<DateTime>("END_DATE").Day.ToString("D2"),
                                                 UserID = userdashboard.Field<string>("CREATED_BY"),
                                                 Request = userdashboard.Field<string>("REQUEST_TYPE_CHOOSEN"),
                                                 Request_Priority = userdashboard.Field<string>("PRIORITY_CHOOSEN"),
                                                 Request_Status = userdashboard.Field<string>("STATUS_CHOOSEN"),
                                                 Criticality = userdashboard.Field<string>("CRITICALITY_CHOOSEN"),
                                                 UserName = userdashboard.Field<string>("CREATED_NAME"),
                                                 IsCreator=userdashboard.Field<int>("IS_CREATED"),
                                                 IsInDays = userdashboard.Field<Int32>("IS_INDAYS"),
                                                 IsPrivacySetting = userdashboard.Field<Int32>("IS_PRIVACY_SETTING"),
                                                 InDays = userdashboard.Field<Int32>("INDAYS"),
                                                 SharedUserId=userdashboard.Field<string>("USER_ID"),
                                                 SharedUserName = userdashboard.Field<string>("USER_NAME_MAP"),
                                                 //vivek groupfilter
                                                 IsCursol = userdashboard.Field<string>("IS_CURSOL"),
                                                 IsScoreCard = userdashboard.Field<string>("IS_SCORECARD"),
                                                 IsWithSLA=userdashboard.Field<Int32?>("IS_WITH_SLA"),
                                                 Filter = (from filtermaster in dsUserSetting.Tables[0].AsEnumerable()
                                                           select new CommonData
                                                           {
                                                               id = filtermaster.Field<string>("ID"),
                                                               name = filtermaster.Field<string>("NAME"),
                                                               ticked = Convert.ToBoolean(filtermaster.Field<string>("ticked"))
                                                           }).ToList<CommonData>(),
                                                 AllRequest = (from prioritymaster in dsUserSetting.Tables[1].AsEnumerable()
                                                               select new CommonData
                                                               {
                                                                   name = prioritymaster.Field<string>("NAME"),
                                                                   ticked = Convert.ToBoolean(prioritymaster.Field<string>("ticked"))
                                                               }).ToList<CommonData>(),
                                                 AllPriority = (from prioritymaster in dsUserSetting.Tables[2].AsEnumerable()
                                                                select new CommonData
                                                                {
                                                                    name = prioritymaster.Field<string>("NAME"),
                                                                    ticked = Convert.ToBoolean(prioritymaster.Field<string>("ticked"))
                                                                }).ToList<CommonData>(),
                                                 AllStatus = (from prioritymaster in dsUserSetting.Tables[3].AsEnumerable()
                                                              select new CommonData
                                                              {
                                                                  name = prioritymaster.Field<string>("NAME"),
                                                                  ticked = Convert.ToBoolean(prioritymaster.Field<string>("ticked"))
                                                              }).ToList<CommonData>(),
                                                 AllCriticality = (from prioritymaster in dsUserSetting.Tables[4].AsEnumerable()
                                                                   select new CommonData
                                                                   {
                                                                       name = prioritymaster.Field<string>("NAME"),
                                                                       ticked = Convert.ToBoolean(prioritymaster.Field<string>("ticked"))
                                                                   }).Distinct().ToList<CommonData>(),
                                                 HiercharchyFilter = (from Filtermaster in dsUserSetting.Tables[6].AsEnumerable()
                                                                      select new Hiercharchy
                                                                      {
                                                                          GenericKey = Filtermaster.Field<string>("GENERICKEY"),
                                                                          FilterName = Filtermaster.Field<string>("FILTERNAME"),
                                                                          GenericValue = Filtermaster.Field<string>("GENERICVALUE")
                                                                      }).ToList<Hiercharchy>(),

                                                //vivek Groupfilter
                                                 GroupDetailFilter = (from Filtermaster in dsUserSetting.Tables[7].AsEnumerable()
                                                                      select new GroupDetail
                                                                      {
                                                                          GroupID = Filtermaster.Field<Int32?>("GROUPID"),
                                                                          GroupName = Filtermaster.Field<string>("GROUPNAME"),
                                                                          GroupType = Filtermaster.Field<Int32?>("GROUPTYPE"),
                                                                          GroupView = Filtermaster.Field<Int32?>("GROUPVIEW"),
                                                                          GroupIndex = Filtermaster.Field<Int32?>("GROUPINDEX"),
                                                                          GroupFilter = Filtermaster.Field<string>("GROUPFILTER"),
                                                                          GroupFilterValue=Filtermaster.Field<string>("GROUPFILTERVALUE"),
                                                                          IsFilter = Filtermaster.Field<string>("IsFilter"),
                                                                          GroupKey = Filtermaster.Field<string>("GroupKey")
                                                                      }).ToList<GroupDetail>()
                                             }).FirstOrDefault();
                    }
                    else
                    {
                        UserDashboardData = (from userdashboard in dsUserSetting.Tables[0].AsEnumerable()
                                             select new UserDashboard
                                             {
                                                 IsInDays = 0,
                                                 IsCreator=1,
                                                 IsPrivacySetting=1,
                                                 Filter = (from filtermaster in dsUserSetting.Tables[0].AsEnumerable()
                                                           select new CommonData
                                                           {
                                                               id = filtermaster.Field<string>("ID"),
                                                               name = filtermaster.Field<string>("NAME"),
                                                               ticked = Convert.ToBoolean(filtermaster.Field<string>("ticked"))
                                                           }).ToList<CommonData>(),
                                                 AllRequest = (from prioritymaster in dsUserSetting.Tables[1].AsEnumerable()
                                                               select new CommonData
                                                               {
                                                                   name = prioritymaster.Field<string>("NAME"),
                                                                   ticked = Convert.ToBoolean(prioritymaster.Field<string>("ticked"))
                                                               }).ToList<CommonData>(),
                                                 AllPriority = (from prioritymaster in dsUserSetting.Tables[2].AsEnumerable()
                                                                select new CommonData
                                                                {
                                                                    name = prioritymaster.Field<string>("NAME"),
                                                                    ticked = Convert.ToBoolean(prioritymaster.Field<string>("ticked"))
                                                                }).ToList<CommonData>(),
                                                 AllStatus = (from prioritymaster in dsUserSetting.Tables[3].AsEnumerable()
                                                              select new CommonData
                                                              {
                                                                  name = prioritymaster.Field<string>("NAME"),
                                                                  ticked = Convert.ToBoolean(prioritymaster.Field<string>("ticked"))
                                                              }).ToList<CommonData>(),
                                                 AllCriticality = (from prioritymaster in dsUserSetting.Tables[4].AsEnumerable()
                                                                   select new CommonData
                                                                   {
                                                                       name = prioritymaster.Field<string>("NAME"),
                                                                       ticked = Convert.ToBoolean(prioritymaster.Field<string>("ticked"))
                                                                   }).Distinct().ToList<CommonData>()
                                             }).FirstOrDefault();
                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }
            return UserDashboardData;
        }

        public List<Hiercharchy> GetDashboardFilterValues(string filterType, string dashboardID, string filterValues)
        {
            List<Hiercharchy> FilterHierarchy = new List<Hiercharchy>();
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    string Query = "EXEC USP_GET_FILTER_VALUES '" + filterType + "','" + filterValues + "'";
                    DataSet dsFilterValues = helper.GetResultReport(Query);

                    FilterHierarchy = (from filterVal in dsFilterValues.Tables[0].AsEnumerable()
                                       select new Hiercharchy
                                       {
                                           FilterName = filterType,
                                           GenericKey = filterVal.Field<string>("ID"),
                                           GenericValue = filterVal.Field<string>("NAME"),
                                           IsTicked = filterVal.Field<string>("TICKED"),
                                           TickedVal = Convert.ToBoolean(filterVal.Field<string>("TICKED"))
                                       }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return FilterHierarchy;
        }
                
        public virtual List<Search> GetRequestIDCollections(string keyword)
        {
            List<Search> GetSearchData = new List<Search>();
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    GetSearchData = (from customermaster in context.DBOARD_DATA_DETAILS_REQUEST
                                     where customermaster.REQUEST_ID.Contains(keyword)
                                     select new Search
                                     {
                                         SearchValue = customermaster.REQUEST_ID
                                     }).Distinct().Take(10).ToList<Search>();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return GetSearchData;
        }

        public DataSet GetRequestDeepDive(string RequestId)
        {
            DataSet dsPivot = new DataSet();
            string Query;
            try
            {
                Query = "EXEC USP_REQUEST_INFO '" + RequestId + "'";
                dsPivot = helper.GetResultReport(Query);
            }
            catch (Exception ex)
            {
                throw;
            }
            return dsPivot;
        }

        public string SaveDashboardSettings(string DashboardID, UserDashboard UserDashboardData)
        {
            string returnDBID = "";
            string DBId = "";
            string UserID = "";
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = UserSettingRepository.IsLoginForm();
                    UserID = helper.GetUserID(IsLogin);
                    //if (HttpContext.Current.Request.Cookies["UserId"].Value != null)
                    //{
                    //    UserID = HttpContext.Current.Request.Cookies["UserId"].Value.ToString();
                    //    string[] ArrayUserID = UserID.Split('=');
                    //    UserID = ArrayUserID[1];
                    //}

                    DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL dashboardmas = context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL.SingleOrDefault(x=>x.DASHBOARD_ID == DashboardID);
                    if (dashboardmas == null)
                    {
                        DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL dashboardmasinsert = new DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL();
                        dashboardmasinsert.DASHBOARD_ICON = UserDashboardData.DashboardIcon;
                        dashboardmasinsert.DASHBOARD_NAME = UserDashboardData.DashBoardName;
                        dashboardmasinsert.START_DATE = Convert.ToDateTime(UserDashboardData.DashboardStartDate);
                        dashboardmasinsert.END_DATE = Convert.ToDateTime(UserDashboardData.DashboardEndDate);
                        dashboardmasinsert.CREATED_DATE = DateTime.Now;
                        dashboardmasinsert.CREATED_BY = UserID;
                        dashboardmasinsert.CREATED_NAME = UserDashboardData.UserName;
                        dashboardmasinsert.PRIORITY_CHOOSEN = UserDashboardData.Request_Priority;
                        dashboardmasinsert.REQUEST_TYPE_CHOOSEN = UserDashboardData.Request;
                        dashboardmasinsert.STATUS_CHOOSEN = UserDashboardData.Request_Status;
                        dashboardmasinsert.CRITICALITY_CHOOSEN = UserDashboardData.Criticality;
                        dashboardmasinsert.IS_INDAYS = UserDashboardData.IsInDays;
                        dashboardmasinsert.INDAYS = UserDashboardData.InDays;
                        //vivek
                        dashboardmasinsert.IS_CURSOL = UserDashboardData.IsCursol;
                        dashboardmasinsert.IS_SCORECARD = UserDashboardData.IsScoreCard;
                        dashboardmasinsert.IS_WITH_SLA = UserDashboardData.IsWithSLA;
                        
                        //
                        var DashBoardID = (from GenerateID in context.DBOARD_CORE_GENERATE_ID
                                           select new
                                           {
                                               DID = GenerateID.NEXT_DASHBOARD_ID
                                           }).FirstOrDefault();

                        //vivek Updating DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL table
                        if (UserDashboardData.GroupDetailFilter !=null)
                        {
                            foreach (var groupfilter in UserDashboardData.GroupDetailFilter)
                            {
                                DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL groupdetailsetting = new DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL();
                                groupdetailsetting.GroupType = groupfilter.GroupType;
                                groupdetailsetting.GroupName = groupfilter.GroupName;
                                groupdetailsetting.GroupView = groupfilter.GroupView;
                                groupdetailsetting.DashBoardID = DashBoardID.DID;
                                groupdetailsetting.IS_ACTIVE = "Y";
                                groupdetailsetting.GroupIndex = groupfilter.GroupIndex;
                                groupdetailsetting.GroupFilter = groupfilter.GroupFilter;
                                groupdetailsetting.GroupFilterValue = groupfilter.GroupFilterValue;
                                groupdetailsetting.IS_FILTER = groupfilter.IsFilter;
                                context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL.Add(groupdetailsetting);
                                context.SaveChanges();
                            }
                        }

                        var Groupid = (from groupdetail in context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL
                                       where groupdetail.DashBoardID == DashBoardID.DID && groupdetail.IS_ACTIVE=="Y"
                                       select groupdetail.GroupID).ToList();

                        var AllGroupIDs = string.Join(",", Groupid.Select(x => x.ToString()).ToArray());

                        dashboardmasinsert.GROUP_ID = AllGroupIDs;
                       //end vivek

                        dashboardmasinsert.DASHBOARD_ID = DashBoardID.DID;
                        context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL.Add(dashboardmasinsert);
                        context.SaveChanges();
                        returnDBID = "{\"DashBoardIcon\": \"" + dashboardmasinsert.DASHBOARD_ICON + "\",\"sizeX\":2,\"sizeY\":2,\"Dashboard\":{\"Id\": \"" + DashBoardID.DID + "\",\"DashboardName\": \"" + dashboardmasinsert.DASHBOARD_NAME + "\",\"idNum\": \"" + "1" + "\"}}";
                        DBId = DashBoardID.DID;

                       
                        foreach (var elementfilter in UserDashboardData.HiercharchyFilter)
                        {
                            DBOARD_COMPUTED_SYSTEM_DASHBOARD_FILTER userpersetting = new DBOARD_COMPUTED_SYSTEM_DASHBOARD_FILTER();
                            userpersetting.DASHBOARD_ID = DashBoardID.DID;
                            userpersetting.CUSTOMIZED_COLUMN = elementfilter.GenericKey;
                            userpersetting.CUSTOMIZED_VALUE = elementfilter.GenericValue;
                            userpersetting.ISACTIVE = "Y";
                            context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_FILTER.Add(userpersetting);
                            context.SaveChanges();
                        }
                    }
                    else
                    {
                        dashboardmas.DASHBOARD_ICON = UserDashboardData.DashboardIcon;
                        dashboardmas.DASHBOARD_NAME = UserDashboardData.DashBoardName;
                        dashboardmas.START_DATE = Convert.ToDateTime(UserDashboardData.DashboardStartDate);
                        dashboardmas.END_DATE = Convert.ToDateTime(UserDashboardData.DashboardEndDate);
                        dashboardmas.CRITICALITY_CHOOSEN = UserDashboardData.Criticality;
                        dashboardmas.STATUS_CHOOSEN = UserDashboardData.Request_Status;
                        dashboardmas.REQUEST_TYPE_CHOOSEN = UserDashboardData.Request;
                        dashboardmas.PRIORITY_CHOOSEN = UserDashboardData.Request_Priority;
                        dashboardmas.MODIFIED_DATE = DateTime.Now;
                        dashboardmas.IS_INDAYS = UserDashboardData.IsInDays;
                        dashboardmas.INDAYS = UserDashboardData.InDays;
                        //vivek
                        dashboardmas.IS_CURSOL = UserDashboardData.IsCursol;
                        dashboardmas.IS_SCORECARD = UserDashboardData.IsScoreCard;
                        dashboardmas.IS_WITH_SLA = UserDashboardData.IsWithSLA;
                       
                        //vivek edited group detail
                        var GroupTableID1 = (from usergroupdetailfilter in context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL
                                            where usergroupdetailfilter.DashBoardID == DashboardID && usergroupdetailfilter.IS_ACTIVE == "Y"
                                            select new GroupDetail
                                            {
                                                GroupID = usergroupdetailfilter.GroupID,
                                                GroupName=usergroupdetailfilter.GroupName,
                                                GroupType=usergroupdetailfilter.GroupType,
                                                GroupView=usergroupdetailfilter.GroupView,
                                                GroupIndex= usergroupdetailfilter.GroupIndex,
                                                GroupFilter=usergroupdetailfilter.GroupFilter,
                                                GroupFilterValue=usergroupdetailfilter.GroupFilterValue,
                                                IsFilter=usergroupdetailfilter.IS_FILTER
                                            }).ToList();

                        var grupdetail = UserDashboardData.GroupDetailFilter;

                        var list1 = grupdetail.Except(GroupTableID1).Where(x=>x.GroupID==null).ToList();

                        var list2 = grupdetail.Where(x => x.GroupID != null).ToList();

                        var GroupTableID = (from usergroupdetailfilter in context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL
                                           where usergroupdetailfilter.DashBoardID == DashboardID && usergroupdetailfilter.IS_ACTIVE == "Y"
                                           select new
                                           {
                                               GroupFilterID = usergroupdetailfilter.GroupID
                                           }).ToList();

                        if (GroupTableID.Count() > 0)
                        {
                            foreach (var Groupfilter in GroupTableID)
                            {
                                DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL userGroupDetail = context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL.SingleOrDefault(x => x.GroupID == Groupfilter.GroupFilterID);
                                userGroupDetail.IS_ACTIVE = "N";
                                context.SaveChanges();
                            }
                        }

                        if (list1 != null)
                        {
                            foreach (var groupfilter in list1)
                            {
                                DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL groupdetailsetting = new DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL();
                                groupdetailsetting.GroupType = groupfilter.GroupType;
                                groupdetailsetting.GroupName = groupfilter.GroupName;
                                groupdetailsetting.GroupView = groupfilter.GroupView;
                                groupdetailsetting.DashBoardID = DashboardID;
                                groupdetailsetting.IS_ACTIVE = "Y";
                                groupdetailsetting.GroupIndex = groupfilter.GroupIndex;
                                groupdetailsetting.GroupFilter = groupfilter.GroupFilter;
                                groupdetailsetting.GroupFilterValue = groupfilter.GroupFilterValue;
                                groupdetailsetting.IS_FILTER = groupfilter.IsFilter;
                                context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL.Add(groupdetailsetting);
                                context.SaveChanges();
                            }
                        }

                        if (list2 != null)
                        {
                            foreach (var groupfilter in list2)
                            {
                                DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL groupdetailsetting = context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL.SingleOrDefault(x => x.GroupID == groupfilter.GroupID && x.DashBoardID == DashboardID);
                             
                                groupdetailsetting.GroupType = groupfilter.GroupType;
                                groupdetailsetting.GroupName = groupfilter.GroupName;
                                groupdetailsetting.GroupView = groupfilter.GroupView;
                                groupdetailsetting.DashBoardID = DashboardID;
                                groupdetailsetting.IS_ACTIVE = "Y";
                                groupdetailsetting.GroupIndex = groupfilter.GroupIndex;
                                groupdetailsetting.GroupFilterValue = groupfilter.GroupFilterValue;
                                groupdetailsetting.GroupFilter = groupfilter.GroupFilter;
                                groupdetailsetting.IS_FILTER = groupfilter.IsFilter;
                                context.SaveChanges();
                            }
                        }



                        //if (UserDashboardData.GroupDetailFilter.Count() > 0 && GroupTableID.Count()>0)
                        //{
                        //    foreach (var elementfilter in UserDashboardData.GroupDetailFilter)
                        //    {
                        //        DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL usergroupsetting = context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL.SingleOrDefault(x => x.GroupName.ToLower().Trim() == elementfilter.GroupName.ToLower().Trim() && x.DashBoardID == DashboardID);
                        //        if (usergroupsetting == null)
                        //        {
                        //            DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL groupsettinginsert = new DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL();
                        //            groupsettinginsert.DashBoardID = DashboardID;
                        //            groupsettinginsert.GroupName = elementfilter.GroupName;
                        //            groupsettinginsert.GroupType = elementfilter.GroupType;
                        //            groupsettinginsert.GroupView = elementfilter.GroupView;
                        //            groupsettinginsert.IS_ACTIVE = "Y";
                        //            context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL.Add(groupsettinginsert);
                        //            context.SaveChanges();
                        //        }
                        //        else
                        //        {
                        //            usergroupsetting.GroupName = elementfilter.GroupName;
                        //            usergroupsetting.GroupType = elementfilter.GroupType;
                        //            usergroupsetting.GroupView = elementfilter.GroupView;
                        //            usergroupsetting.IS_ACTIVE = "Y";
                        //            context.SaveChanges();
                        //        }
                        //    }
                        //}
                        

                        var UpdateGroupid = (from Updategroupdetail in context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL
                                             where Updategroupdetail.DashBoardID == DashboardID && Updategroupdetail.IS_ACTIVE == "Y"
                                             select Updategroupdetail.GroupID).ToList();

                        if (UpdateGroupid != null)
                        {
                            var AllGroupIDs = string.Join(",", UpdateGroupid.Select(x => x.ToString()).ToArray());

                            dashboardmas.GROUP_ID = AllGroupIDs;
                        }
                        //end vivek


                        context.SaveChanges();
                        returnDBID = "{\"DashBoardIcon\": \"" + dashboardmas.DASHBOARD_ICON + "\",\"Dashboard\":{\"Id\": \"" + DashboardID + "\",\"DashboardName\": \"" + dashboardmas.DASHBOARD_NAME + "\",\"idNum\": \"" + "1" + "\"}}";
                       

                        var userdashboardfilter = (from userpersonalizationdet in context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_FILTER
                                                   where userpersonalizationdet.DASHBOARD_ID == DashboardID
                                                   select new
                                                   {
                                                       UserFilterID = userpersonalizationdet.DASHBOARD_FILTER_ID
                                                   }).ToList();
                        foreach (var filterID in userdashboardfilter)
                        {
                            DBOARD_COMPUTED_SYSTEM_DASHBOARD_FILTER userpersettingstatus = context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_FILTER.SingleOrDefault(x => x.DASHBOARD_FILTER_ID == filterID.UserFilterID);
                            userpersettingstatus.ISACTIVE = "N";
                            context.SaveChanges();
                        }
                        DBId = DashboardID;
                        foreach (var elementfilter in UserDashboardData.HiercharchyFilter)
                        {
                            DBOARD_COMPUTED_SYSTEM_DASHBOARD_FILTER userpersetting = context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_FILTER.SingleOrDefault(x => x.CUSTOMIZED_COLUMN.ToLower().Trim() == elementfilter.GenericKey.ToLower().Trim() && x.DASHBOARD_ID == DashboardID);
                            if (userpersetting == null)
                            {
                                DBOARD_COMPUTED_SYSTEM_DASHBOARD_FILTER userpersettinginsert = new DBOARD_COMPUTED_SYSTEM_DASHBOARD_FILTER();
                                userpersettinginsert.DASHBOARD_ID = DashboardID;
                                userpersettinginsert.CUSTOMIZED_COLUMN = elementfilter.GenericKey;
                                userpersettinginsert.CUSTOMIZED_VALUE = elementfilter.GenericValue;
                                userpersettinginsert.ISACTIVE = "Y";
                                context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_FILTER.Add(userpersettinginsert);
                                context.SaveChanges();
                            }
                            else
                            {
                                userpersetting.CUSTOMIZED_VALUE = elementfilter.GenericValue;
                                userpersetting.ISACTIVE = "Y";
                                context.SaveChanges();
                            }
                        }
                    }
                    var UserMap = (from x in context.DBOARD_COMPUTED_SYSTEM_MAPPING_DASHBOARD_USER
                                   where x.DASHBOARD_ID == DBId
                                   select x).FirstOrDefault();
                    if (UserMap != null)
                    {
                        UserMap.DASHBOARD_ID = DBId;
                        UserMap.USER_ID = UserDashboardData.SharedUserId;
                        UserMap.USER_NAME_MAP = UserDashboardData.SharedUserName;
                        UserMap.IS_PRIVACY_SETTING = UserDashboardData.IsPrivacySetting;
                        context.SaveChanges();
                    }
                    else
                    {
                        DBOARD_COMPUTED_SYSTEM_MAPPING_DASHBOARD_USER UserMaps = new DBOARD_COMPUTED_SYSTEM_MAPPING_DASHBOARD_USER();
                        UserMaps.DASHBOARD_ID = DBId;
                        UserMaps.USER_ID = UserDashboardData.SharedUserId;
                        UserMaps.USER_NAME_MAP = UserDashboardData.SharedUserName;
                        UserMaps.IS_PRIVACY_SETTING = UserDashboardData.IsPrivacySetting;
                        context.DBOARD_COMPUTED_SYSTEM_MAPPING_DASHBOARD_USER.Add(UserMaps);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return returnDBID;
        }

        public string DeleteDashboardOrWidgetKPI(string DashboardId, string IsDelDash, string KPIID = null, string GroupKey=null)
        {
            string returnValue = "";
            UserDetail user = new UserDetail();
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = UserSettingRepository.IsLoginForm();
                    string UserId = helper.GetUserID(IsLogin);
                   
                    //vivek 
                    //if (HttpContext.Current.Request.Cookies["UserId"].Value != null)
                    //{
                    //    UserId = HttpContext.Current.Request.Cookies["UserId"].Value.ToString();
                    //    string[] ArrayUserID = UserId.Split('=');
                    //    UserId = ArrayUserID[1];
                    //}
                    if (IsDelDash == "1")
                    {
                        DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL dash = context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL.SingleOrDefault(t => t.DASHBOARD_ID == DashboardId);
                        if (dash != null)
                        {
                            var q = (from x in context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL
                                     where x.DASHBOARD_ID == DashboardId
                                     select x).ToList();
                            foreach (var item in q)
                            {
                                var z = (from y in context.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE
                                         where y.KPIID == item.KPI_ID
                                         select y).ToList();
                                context.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE.RemoveRange(z);
                                context.SaveChanges();
                            }

                            var grp = (from x in context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL
                                       where x.DashBoardID == DashboardId
                                       select x).ToList();

                            context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL.RemoveRange(grp);
                            context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL.RemoveRange(q);
                            context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL.Remove(dash);
                            context.SaveChanges();
                            returnValue = "Deleted Successfully";
                        }
                    }
                    else if(IsDelDash == "0")
                    {
                        DBOARD_COMPUTED_SYSTEM_KPI_DETAIL userKPI = context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL.SingleOrDefault(d => d.KPI_ID == KPIID);
                        if (userKPI != null)
                        {
                            var z = (from y in context.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE
                                     where y.KPIID == KPIID
                                     select y).ToList();
                            context.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE.RemoveRange(z);
                            context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL.Remove(userKPI);
                            context.SaveChanges();
                            returnValue = "Deleted Successfully";
                        }
                    }
                    else
                    {
                        DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL groupdetailinfo = context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL.SingleOrDefault(x => x.GROUP_KEY == GroupKey && x.DashBoardID == DashboardId);
                        if (groupdetailinfo != null)
                        {
                            var kpi = (from x in context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL
                                     where x.GROUP_KEY == GroupKey
                                     select x).ToList();
                            foreach (var item in kpi)
                            {
                                var z = (from y in context.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE
                                         where y.KPIID == item.KPI_ID
                                         select y).ToList();
                                context.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE.RemoveRange(z);
                                context.SaveChanges();
                            }
                            context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL.RemoveRange(kpi);
                            context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL.Remove(groupdetailinfo);
                            context.SaveChanges();
                            returnValue = "Deleted Successfully";
                        }
                
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return returnValue;
        }

        public string CloneDashboardSetting(string CurrentDashboardID, CloneDashboarddetail CloneDBDetail)
        {
            string returnDBID = "";
            string UserID = "";
            string DBId = "";
            string KpiDetail = "";
            string grpDetail = "";
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = UserSettingRepository.IsLoginForm();
                    UserID = helper.GetUserID(IsLogin);
                   // UserID = helper.GetCurrentUserId();
                   
                    UserDetails userdetail= helper.GetUserDetailsWithID(UserID);
                   
                    DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL dashboardmas = context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL.SingleOrDefault(x => x.DASHBOARD_ID == CurrentDashboardID);
                    if (dashboardmas != null)
                    {
                        DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL dashboardmasinsert = new DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL();
                        dashboardmasinsert.DASHBOARD_ICON = CloneDBDetail.dashBoardIcon;
                        dashboardmasinsert.DASHBOARD_NAME = CloneDBDetail.dashBoardName;
                        dashboardmasinsert.START_DATE = Convert.ToDateTime(dashboardmas.START_DATE);
                        dashboardmasinsert.END_DATE = Convert.ToDateTime(dashboardmas.END_DATE);
                        dashboardmasinsert.CREATED_DATE = DateTime.Now;
                        dashboardmasinsert.CREATED_BY = UserID;
                        dashboardmasinsert.CREATED_NAME = userdetail.UserName;
                        dashboardmasinsert.PRIORITY_CHOOSEN = dashboardmas.PRIORITY_CHOOSEN;
                        dashboardmasinsert.REQUEST_TYPE_CHOOSEN = dashboardmas.REQUEST_TYPE_CHOOSEN;
                        dashboardmasinsert.STATUS_CHOOSEN = dashboardmas.STATUS_CHOOSEN;
                        dashboardmasinsert.CRITICALITY_CHOOSEN = dashboardmas.CRITICALITY_CHOOSEN;
                        dashboardmasinsert.IS_INDAYS = dashboardmas.IS_INDAYS;
                        dashboardmasinsert.INDAYS = dashboardmas.INDAYS;
                        dashboardmasinsert.IS_CURSOL = dashboardmas.IS_CURSOL;
                        dashboardmasinsert.IS_SCORECARD = dashboardmas.IS_SCORECARD;
                        dashboardmasinsert.IS_WITH_SLA = dashboardmas.IS_WITH_SLA;

                        //
                        var DashBoardID = (from GenerateID in context.DBOARD_CORE_GENERATE_ID
                                           select new
                                           {
                                               DID = GenerateID.NEXT_DASHBOARD_ID
                                           }).FirstOrDefault();

                        //vivek Updating DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL table
                        var Groupid = (from groupdetail in context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL
                                       where groupdetail.DashBoardID == CurrentDashboardID && groupdetail.IS_ACTIVE == "Y"
                                       select groupdetail.GroupID).ToList();

                        if (Groupid != null)
                        {
                            foreach (var groupID in Groupid)
                            {
                                DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL newgroupdetailsetting = new DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL();
                                DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL groupdetailsetting = context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL.SingleOrDefault(x => x.GroupID == groupID);
                                newgroupdetailsetting.GroupType = groupdetailsetting.GroupType;
                                newgroupdetailsetting.GroupName = groupdetailsetting.GroupName;
                                newgroupdetailsetting.GroupView = groupdetailsetting.GroupView;
                                newgroupdetailsetting.DashBoardID = DashBoardID.DID;
                                newgroupdetailsetting.IS_ACTIVE = "Y";
                                newgroupdetailsetting.GroupIndex = groupdetailsetting.GroupIndex;
                                newgroupdetailsetting.GroupFilter = groupdetailsetting.GroupFilter;
                                newgroupdetailsetting.IS_FILTER = groupdetailsetting.IS_FILTER;
                                newgroupdetailsetting.GroupFilterValue = groupdetailsetting.GroupFilterValue;

                                var grpID = groupdetailsetting.GROUP_KEY;
                                if (grpID != null)
                                {
                                    string[] ArrayUserID = grpID.Split('_');
                                    grpID = ArrayUserID[0];
                                    int IDGrp = Convert.ToInt32(grpID);
                                    if (IDGrp != groupdetailsetting.GroupID)
                                    {
                                        newgroupdetailsetting.GROUP_KEY = IDGrp + "_" + DashBoardID.DID;
                                    }
                                    else
                                    {
                                        newgroupdetailsetting.GROUP_KEY = groupdetailsetting.GroupID + "_" + DashBoardID.DID;
                                    }
                                    
                                }
                                context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL.Add(newgroupdetailsetting);
                                context.SaveChanges();

                            }
                        }

                        var NewGroupid = (from groupdetail in context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL
                                       where groupdetail.DashBoardID == DashBoardID.DID && groupdetail.IS_ACTIVE == "Y"
                                       select groupdetail.GroupID).ToList();

                        var AllGroupIDs = string.Join(",", NewGroupid.Select(x => x.ToString()).ToArray());

                        dashboardmasinsert.GROUP_ID = AllGroupIDs;
                        //end vivek

                        dashboardmasinsert.DASHBOARD_ID = DashBoardID.DID;
                        context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL.Add(dashboardmasinsert);
                        context.SaveChanges();
                        returnDBID = "{\"DashBoardIcon\": \"" + dashboardmasinsert.DASHBOARD_ICON + "\",\"sizeX\":2,\"sizeY\":2,\"Dashboard\":{\"Id\": \"" + DashBoardID.DID + "\",\"DashboardName\": \"" + dashboardmasinsert.DASHBOARD_NAME + "\",\"idNum\": \"" + "1" + "\"}}";
                        DBId = DashBoardID.DID;


                        var HiercharchyFilterID = (from groupdetail in context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_FILTER
                                       where groupdetail.DASHBOARD_ID == CurrentDashboardID && groupdetail.ISACTIVE == "Y"
                                       select groupdetail.DASHBOARD_FILTER_ID).ToList();

                        if (HiercharchyFilterID != null)
                        {
                            foreach (var elementfilter in HiercharchyFilterID)
                            {
                                DBOARD_COMPUTED_SYSTEM_DASHBOARD_FILTER newuserpersetting = new DBOARD_COMPUTED_SYSTEM_DASHBOARD_FILTER();
                                DBOARD_COMPUTED_SYSTEM_DASHBOARD_FILTER userpersetting = context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_FILTER.SingleOrDefault(x => x.DASHBOARD_FILTER_ID == elementfilter);
                                newuserpersetting.DASHBOARD_ID = DashBoardID.DID;
                                newuserpersetting.CUSTOMIZED_COLUMN = userpersetting.CUSTOMIZED_COLUMN;
                                newuserpersetting.CUSTOMIZED_VALUE = userpersetting.CUSTOMIZED_VALUE;
                                newuserpersetting.ISACTIVE = userpersetting.ISACTIVE;
                                context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_FILTER.Add(newuserpersetting);
                                context.SaveChanges();
                            }
                        }

                        var UserMap = (from x in context.DBOARD_COMPUTED_SYSTEM_MAPPING_DASHBOARD_USER
                                       where x.DASHBOARD_ID == CurrentDashboardID
                                       select x).FirstOrDefault();
                        if (UserMap != null)
                        {
                            DBOARD_COMPUTED_SYSTEM_MAPPING_DASHBOARD_USER NewUserMaps = new DBOARD_COMPUTED_SYSTEM_MAPPING_DASHBOARD_USER();
                            DBOARD_COMPUTED_SYSTEM_MAPPING_DASHBOARD_USER UserMaps = context.DBOARD_COMPUTED_SYSTEM_MAPPING_DASHBOARD_USER.SingleOrDefault(x => x.DASHBOARD_ID == UserMap.DASHBOARD_ID);
                            NewUserMaps.DASHBOARD_ID = DBId;
                            NewUserMaps.USER_ID = UserID;
                            NewUserMaps.USER_NAME_MAP = userdetail.UserName + "("+userdetail.UserID+")";
                            NewUserMaps.IS_PRIVACY_SETTING = UserMaps.IS_PRIVACY_SETTING;
                            context.DBOARD_COMPUTED_SYSTEM_MAPPING_DASHBOARD_USER.Add(NewUserMaps);
                            context.SaveChanges();
                        }

                        KpiDetail = CloneKPIdetail(CurrentDashboardID, DBId);

                       // grpDetail = groupdetailid(CurrentDashboardID, DBId);

                    }
                }
            }
            catch
            {

            }

            return returnDBID;
        }


        public string CloneKPIdetail(string CurrentDashboardID, string NewDascboardID)
        {
            string str = "";
            string UserID = "";

            UserID = helper.GetCurrentUserId();

            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var KPIDetailList = (from x in context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL
                                         where x.DASHBOARD_ID == CurrentDashboardID
                                         select x.KPI_ID).ToList();


                    if (KPIDetailList != null)
                    {
                        foreach (var kpiid in KPIDetailList)
                        {

                            var KPIinsert = (from GenerateID in context.DBOARD_CORE_GENERATE_ID
                                               select new
                                               {
                                                   KPIID = GenerateID.NEXT_KPI_ID
                                               }).FirstOrDefault();

                            DBOARD_COMPUTED_SYSTEM_KPI_DETAIL NewKPIDetail = new DBOARD_COMPUTED_SYSTEM_KPI_DETAIL();
                            DBOARD_COMPUTED_SYSTEM_KPI_DETAIL KPIDetail = context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL.SingleOrDefault(x => x.KPI_ID == kpiid);
                            NewKPIDetail.KPI_ID = KPIinsert.KPIID;
                            NewKPIDetail.BASE_KPI_ID = KPIDetail.BASE_KPI_ID;
                            NewKPIDetail.DASHBOARD_ID = NewDascboardID;
                            NewKPIDetail.KPI_NAME = KPIDetail.KPI_NAME;
                            NewKPIDetail.CREATED_BY = UserID;
                            NewKPIDetail.CREATED_DATE = DateTime.Now;
                            NewKPIDetail.PRIORITY_CHOOSEN = KPIDetail.PRIORITY_CHOOSEN;
                            NewKPIDetail.REQUEST_TYPE_CHOOSEN = KPIDetail.REQUEST_TYPE_CHOOSEN;
                            NewKPIDetail.STATUS_CHOOSEN = KPIDetail.STATUS_CHOOSEN;
                            NewKPIDetail.CRITICALITY = KPIDetail.CRITICALITY;
                            NewKPIDetail.SLI_CRITICALITY = KPIDetail.CRITICALITY;
                            NewKPIDetail.PARETO_BAR_COLUMN = KPIDetail.PARETO_BAR_COLUMN;
                            NewKPIDetail.PARETO_BAR_COLUMN_NAME = KPIDetail.PARETO_BAR_COLUMN_NAME;
                            NewKPIDetail.PARETO_BAR_UNIT = KPIDetail.PARETO_BAR_UNIT;
                            NewKPIDetail.IS_PARETO_LINE_CHART = KPIDetail.IS_PARETO_LINE_CHART;
                            NewKPIDetail.SPLIT_DATA = KPIDetail.SPLIT_DATA;
                            NewKPIDetail.GROUP_DATA = KPIDetail.GROUP_DATA;
                            NewKPIDetail.CHART_BY_TYPE = KPIDetail.CHART_BY_TYPE;
                            NewKPIDetail.CHART_BY_VALUE = KPIDetail.CHART_BY_VALUE;
                            NewKPIDetail.IS_PARENT = KPIDetail.IS_PARENT;
                            NewKPIDetail.IS_ACTIVE = "Y";
                            NewKPIDetail.Chart_Color = KPIDetail.Chart_Color;
                            NewKPIDetail.IS_GROUPED = KPIDetail.IS_GROUPED;
                            NewKPIDetail.IS_USETHEME = KPIDetail.IS_USETHEME;
                            NewKPIDetail.KPI_TYPE = KPIDetail.KPI_TYPE;
                            NewKPIDetail.GROUP_NAME = KPIDetail.GROUP_NAME;

                            var grpID = KPIDetail.GROUP_KEY;
                            if (grpID!=null)
                            {
                                string[] ArrayUserID = grpID.Split('_');
                                grpID = ArrayUserID[0];
                                NewKPIDetail.GROUP_KEY = grpID +"_"+NewDascboardID;
                            }

                            context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL.Add(NewKPIDetail);
                            context.SaveChanges();


                            if (NewKPIDetail.BASE_KPI_ID == "BKPI0009")
                            {
                                DBOARD_COMPUTED_SYSTEM_AGING_KPI NewagingKPI = new DBOARD_COMPUTED_SYSTEM_AGING_KPI();
                                DBOARD_COMPUTED_SYSTEM_AGING_KPI agingKPI = context.DBOARD_COMPUTED_SYSTEM_AGING_KPI.SingleOrDefault(x => x.KPI_ID == kpiid);
                                NewagingKPI.AGING_CONDITION = agingKPI.AGING_CONDITION;
                                NewagingKPI.AGING_UNIT = agingKPI.AGING_UNIT;
                                NewagingKPI.AGING_VALUE = agingKPI.AGING_VALUE;
                                NewagingKPI.KPI_ID = KPIinsert.KPIID;
                                NewagingKPI.AGING_COMMON_VALUES = agingKPI.AGING_COMMON_VALUES;
                                NewagingKPI.QUERY_JSON = agingKPI.QUERY_JSON;
                                context.DBOARD_COMPUTED_SYSTEM_AGING_KPI.Add(NewagingKPI);
                                context.SaveChanges();
                            }
                            if (NewKPIDetail.BASE_KPI_ID == "BKPI0007")
                            {
                                DBOARD_COMPUTED_SYSTEM_SCOREBOARD_KPI genericdatainssert = new DBOARD_COMPUTED_SYSTEM_SCOREBOARD_KPI();
                                DBOARD_COMPUTED_SYSTEM_SCOREBOARD_KPI genericdata = context.DBOARD_COMPUTED_SYSTEM_SCOREBOARD_KPI.SingleOrDefault(x => x.KPI_ID == kpiid);
                                genericdatainssert.KPI_ID = KPIinsert.KPIID;
                                genericdatainssert.KPI_ICON = genericdata.KPI_ICON;
                                genericdatainssert.CONDITION = genericdata.CONDITION;
                                genericdatainssert.QUERY_JSON = genericdata.QUERY_JSON;
                                genericdatainssert.KPI_ICON = genericdata.KPI_ICON;
                                genericdatainssert.SCOREBOARD_TYPE = genericdata.SCOREBOARD_TYPE;
                                genericdatainssert.IS_ACTIVE = "Y";
                                context.DBOARD_COMPUTED_SYSTEM_SCOREBOARD_KPI.Add(genericdatainssert);
                                context.SaveChanges();
                            }

                            var kpiPosID = (from x in context.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE
                                         where x.KPIID == kpiid
                                         select x.POSITIONID).ToList();


                            if (kpiPosID != null)
                            {
                                foreach (var kpivmode in kpiPosID)
                                {
                                    DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE usersettingviewmode = new DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE();
                                    DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE userviewmode = context.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE.SingleOrDefault(x => x.POSITIONID == kpivmode);
                                    usersettingviewmode.POSITION_X = userviewmode.POSITION_X;
                                    usersettingviewmode.POSITION_Y = userviewmode.POSITION_Y;
                                    usersettingviewmode.SIZE_XAXIS = userviewmode.SIZE_XAXIS;
                                    usersettingviewmode.SIZE_YAXIS = userviewmode.SIZE_YAXIS;
                                    usersettingviewmode.VIEW_MODE = userviewmode.VIEW_MODE;
                                    usersettingviewmode.KPIID = KPIinsert.KPIID;
                                    context.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE.Add(usersettingviewmode);
                                    context.SaveChanges(); 
                                }
                            }  
                        }
                    }
                }
            }
            catch
            {

            }
            return str;
        }

        //public string groupdetailid(string CurrentDashboardID, string DBId)
        
        //{
        //    string id = "";
        //    List<Grpdetail> KPIDetailList = new List<Grpdetail>();
        //    using (SeeITEntities context = new SeeITEntities())
        //    {

        //        KPIDetailList = (from x in context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL
        //                         where x.DASHBOARD_ID == CurrentDashboardID
        //                         select new Grpdetail
        //                         {
        //                             GroupID = (from y in context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL
        //                                        where y.GroupName == x.GROUP_NAME && y.DashBoardID == DBId && y.IS_ACTIVE=="Y"
        //                                        select y.GroupID).FirstOrDefault(),
        //                             KPIID = x.KPI_ID,
        //                             NewKPIID=(from z in context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL
        //                                       where z.DASHBOARD_ID == DBId && x.KPI_NAME==z.KPI_NAME
        //                                       select z.KPI_ID).FirstOrDefault(),
        //                             OldGroupID = (from y in context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL
        //                                           where y.GroupName == x.GROUP_NAME && y.DashBoardID == CurrentDashboardID && y.IS_ACTIVE == "Y"
        //                                           select y.GroupID).FirstOrDefault()
        //                         }).ToList<Grpdetail>();



        //        if (KPIDetailList != null)
        //        {
        //            foreach (var kpiid in KPIDetailList)
        //            {
        //                if (kpiid.OldGroupID != 0 && kpiid.KPIID != null)
        //                {
        //                    DBOARD_COMPUTED_SYSTEM_KPI_DETAIL KPIDetail = context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL.SingleOrDefault(x =>x.KPI_ID==kpiid.NewKPIID);
        //                    KPIDetail.GROUP_ID = kpiid.GroupID;
        //                    context.SaveChanges();
        //                }

        //            }
        //        }
        //    }
        //    return id;
        //}


        public AdminDetailCollection AppInfoDetail()
        {
            string returnValue = "";
            string UserID = "277070";
            AdminDetailCollection AdminDetail = new AdminDetailCollection();

            try
            {
                using( SeeITEntities context = new SeeITEntities())
                {
                    //UserID = helper.GetUserID();

                    if(UserID!= null)
                    {
                        AdminDetail.AdminInfo = (from admininfo in context.DBOARD_COMPUTED_ADMIN_INFO_SETTING
                                         select new AdminInfoDetail
                                         {
                                             Slno = admininfo.SL_No,
                                             AppLogo = admininfo.APPLICATION_LOGO,
                                             AppName = admininfo.APPLICATION_NAME,
                                             Status = admininfo.RESOLVED_STATUS
                                         }).ToList<AdminInfoDetail>();

                        AdminDetail.Request_Priority = (from priority in context.DBOARD_DATA_MASTER_PRIORITY
                                                               select new CommonData
                                                               {
                                                                   id=priority.PRIORITY_ID,
                                                                   name=priority.REQUEST_PRIORITY
                                                                }).ToList<CommonData>();
                        AdminDetail.Request_Status = (from status in context.DBOARD_DATA_MASTER_STATUS
                                                      select new CommonData
                                                      {
                                                          id = status.STATUS_ID,
                                                          name = status.REQUEST_STATUS
                                                      }).ToList<CommonData>();

                        AdminDetail.Criticality = (from criticality in context.DBOARD_DATA_MASTER_CRITICALITY
                                                   select new CommonData
                                                   {
                                                       id = criticality.CRITICALITY_ID,
                                                       name = criticality.REQUEST_CRITICALITY
                                                   }).ToList<CommonData>();
                        AdminDetail.Request = (from type in context.DBOARD_DATA_MASTER_REQUEST
                                               select new CommonData
                                               {
                                                   id = type.REQUEST_ID,
                                                   name = type.REQUEST_TYPE
                                               }).ToList<CommonData>();
                    }
                }

            }
            catch
            {

            }

            return AdminDetail;
        }

    }
}
