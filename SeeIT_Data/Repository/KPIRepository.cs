﻿using SeeIT_Common.Helper;
using SeeIT_Common.Model;
using SeeIT_Data.EDMX;
using SeeIT_Data.SpModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace SeeIT_Data.Repository
{
    public class KPIRepository
    {
        HelperFunctions helper = new HelperFunctions();
        UserSettingRepository UserSettingRepository = new UserSettingRepository();

        public DataSet GetDataKPIAgingReport(string UserID, string DashboardID, string KPIID)
        {
            DataSet dsPivot = new DataSet();
            string Query;
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    string PrivacySet = GetDashboardPrivacySetting(context, DashboardID);
                    //Query = "EXEC USP_CREATE_PIVOT_DATA '" + UserID + "','" + DashboardID + "','" + KPIID + "','" + GroupData + "','" + SplitData + "'";
                    Query = "EXEC USP_CREATE_AGING_CHART '" + UserID + "','" + PrivacySet + "','" + DashboardID + "','" + KPIID + "'";
                    dsPivot = helper.GetResultReport(Query);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dsPivot;
        }

        public KPIAvgResponseResolution GetDataAvgResponseResolution( string DashboardID, string KPIID, string isResp)
        {
            KPIAvgResponseResolution AvgResponseResolution = new KPIAvgResponseResolution();
            UserKPIDashboard UserKPIDashboardDetails = new UserKPIDashboard();
            string Query = string.Empty;
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = UserSettingRepository.IsLoginForm();
                    string UserId = helper.GetUserID(IsLogin);
                    //Vivek
                    //if (HttpContext.Current.Request.Cookies["UserId"].Value != null)
                    //{
                    //    UserId = HttpContext.Current.Request.Cookies["UserId"].Value.ToString();
                    //    string[] ArrayUserID = UserId.Split('=');
                    //    UserId = ArrayUserID[1];
                    //}
                    string PrivacySet = GetDashboardPrivacySetting(context, DashboardID);
                    UserKPIDashboardDetails = GetKPIDashboardBasicDetails(context, DashboardID, KPIID);
                    if (UserKPIDashboardDetails == null)
                    {
                        return AvgResponseResolution;
                    }
                    if (isResp == "Y")
                    {
                        Query = "EXEC USP_CREATE_AVG_RESPONSE_TREND '" + UserId + "','" + PrivacySet + "','" + KPIID + "','" + DashboardID + "'";
                    }
                    else
                    {
                        Query = "EXEC USP_CREATE_AVG_RESOLUTION_TREND '" + UserId + "','" + PrivacySet + "','" + KPIID + "','" + DashboardID + "'";
                    }
                    List<USP_CREATE_AVG_RESOLUTION_TEST> returnData = context.Database.SqlQuery<USP_CREATE_AVG_RESOLUTION_TEST>(Query).DefaultIfEmpty<USP_CREATE_AVG_RESOLUTION_TEST>().ToList();
                    AvgResponseResolution = new KPIAvgResponseResolution()
                    {
                        ChartColor = UserKPIDashboardDetails.ChartColor,
                        IsWithSLA=UserKPIDashboardDetails.IsWithSLA,
                        IsUseTheme = UserKPIDashboardDetails.IsUseTheme,
                        KPIName = UserKPIDashboardDetails.KPIName,
                        MonthExceeding = returnData.Where(t => t.CHART_TYPE.ToLower().Equals("month") && t.IS_Exceeding.ToLower().Equals("yes")).Select(t => new KPIPriorityMonthCount() { Colour = t.COLOUR, Request_Priority = t.REQUEST_PRIORITY, PriorityAvg = (Double)t.DIFF, PriorityCount = t.REQUEST_ID_COUNT, PriorityMonthDate = t.MONTH_WEEK_MAPPING }).ToList<KPIPriorityMonthCount>(),
                        MonthExclExceeding = returnData.Where(t => t.CHART_TYPE.ToLower().Equals("month") && t.IS_Exceeding.ToLower().Equals("no")).Select(t => new KPIPriorityMonthCount() { Colour = t.COLOUR, Request_Priority = t.REQUEST_PRIORITY, PriorityAvg = (Double)t.DIFF, PriorityCount = t.REQUEST_ID_COUNT, PriorityMonthDate = t.MONTH_WEEK_MAPPING }).ToList<KPIPriorityMonthCount>(),
                        MonthTotal = returnData.Where(t => t.CHART_TYPE.ToLower().Equals("month") && t.IS_Exceeding.ToLower().Equals("total")).Select(t => new KPIPriorityMonthCount() { Colour = t.COLOUR, Request_Priority = t.REQUEST_PRIORITY, PriorityAvg = (Double)t.DIFF, PriorityCount = t.REQUEST_ID_COUNT, PriorityMonthDate = t.MONTH_WEEK_MAPPING }).ToList<KPIPriorityMonthCount>(),
                        Request_Priority = UserKPIDashboardDetails.Request_Priority,
                        SumExceeding = returnData.Where(t => t.CHART_TYPE.ToLower().Equals("count") && t.IS_Exceeding.ToLower().Equals("yes")).Select(t => new KPIPriorityCount() { Colour = t.COLOUR, Request_Priority = t.REQUEST_PRIORITY, PriorityAvg = (Double)t.DIFF, PriorityCount = t.REQUEST_ID_COUNT, SLI =(t.SLI!=null)? (Double)t.SLI:0, SLIUnit = (t.SLI_UNIT!=null)?t.SLI_UNIT:"" }).ToList<KPIPriorityCount>(),
                        SumExclExceeding = returnData.Where(t => t.CHART_TYPE.ToLower().Equals("count") && t.IS_Exceeding.ToLower().Equals("no")).Select(t => new KPIPriorityCount() { Colour = t.COLOUR, Request_Priority = t.REQUEST_PRIORITY, PriorityAvg = (Double)t.DIFF, PriorityCount = t.REQUEST_ID_COUNT, SLI = (t.SLI != null) ? (Double)t.SLI : 0, SLIUnit = (t.SLI_UNIT != null) ? t.SLI_UNIT : "" }).ToList<KPIPriorityCount>(),
                        SumTotal = returnData.Where(t => t.CHART_TYPE.ToLower().Equals("count") && t.IS_Exceeding.ToLower().Equals("total")).Select(t => new KPIPriorityCount() { Colour = t.COLOUR, Request_Priority = t.REQUEST_PRIORITY, PriorityAvg = (Double)t.DIFF, PriorityCount = t.REQUEST_ID_COUNT, SLI = (t.SLI != null) ? (Double)t.SLI : 0, SLIUnit = (t.SLI_UNIT != null) ? t.SLI_UNIT : "" }).ToList<KPIPriorityCount>()
                    };
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return AvgResponseResolution;
        }

        public KPIIncomingVSCompleted GetDataKPIIncomingVSCompleted( string DashboardID, string KPIID)
        {
            KPIIncomingVSCompleted IncomingVSCompleted = new KPIIncomingVSCompleted();
            UserKPIDashboard UserKPIDashboardDetails = new UserKPIDashboard();
            string Query = string.Empty;
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = UserSettingRepository.IsLoginForm();
                    string UserId = helper.GetUserID(IsLogin);
                   // UserId = "277070";
                    //Vivek
                    //if (HttpContext.Current.Request.Cookies["UserId"].Value != null)
                    //{
                    //    UserId = HttpContext.Current.Request.Cookies["UserId"].Value.ToString();
                    //    string[] ArrayUserID = UserId.Split('=');
                    //    UserId = ArrayUserID[1];
                    //}
                    string PrivacySet = GetDashboardPrivacySetting(context, DashboardID);
                    Query = "EXEC USP_CREATE_INCOMING_COMPLETED '" + UserId + "','" + PrivacySet + "','" + KPIID + "','" + DashboardID + "'";
                    List<USP_CREATE_INCOMING_COMPLETED> CreateIncomingData = context.Database.SqlQuery<USP_CREATE_INCOMING_COMPLETED>(Query).DefaultIfEmpty<USP_CREATE_INCOMING_COMPLETED>().ToList();
                    context.Database.CommandTimeout = 900000;
                    UserKPIDashboardDetails = GetKPIDashboardBasicDetails(context, DashboardID, KPIID);
                    if (UserKPIDashboardDetails == null)
                    {
                        return IncomingVSCompleted;
                    }
                    IncomingVSCompleted = new KPIIncomingVSCompleted()
                    {
                        Request_Priority = UserKPIDashboardDetails.Request_Priority,
                        KPIName = UserKPIDashboardDetails.KPIName,
                        ChartColor = UserKPIDashboardDetails.ChartColor,
                        IsGrouped = UserKPIDashboardDetails.IsGrouped,
                        IsUseTheme = UserKPIDashboardDetails.IsUseTheme,
                        IncomingVSCompleted = CreateIncomingData.Select(t => new IncomingVSCompletedPriorityCount()
                        {
                            Request_Priority = t.REQUEST_PRIORITY.ToString(),
                            Incoming = (Double)t.INCOMING,
                            Completed = (Double)t.RESOLVED,
                            Backlog = (Double)t.BACKLOG,
                            PriorityMonthDate = t.MONTH_WEEK_MAPPING
                        }).ToList<IncomingVSCompletedPriorityCount>(),
                        Error = "False"
                    };
                    string[] arr = IncomingVSCompleted.Request_Priority.Split(',');
                    var arrlength = arr.Length;
                    if (arrlength != 1)
                    {
                        IncomingVSCompleted.Request_Priority = "TOTAL," + IncomingVSCompleted.Request_Priority;
                        IncomingVSCompleted.IncomingVSCompleted.AddRange(IncomingVSCompleted.IncomingVSCompleted
                        .GroupBy(c => c.PriorityMonthDate)
                        .Select(g => new IncomingVSCompletedPriorityCount
                        {
                            Request_Priority = "Total",
                            PriorityMonthDate = g.Key,
                            Completed = g.Sum(x => x.Completed),
                            Incoming = g.Sum(x => x.Incoming),
                            Backlog = g.Sum(x => x.Backlog)
                        }).ToList<IncomingVSCompletedPriorityCount>()
                        );
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return IncomingVSCompleted;
        }

        public KPIExceeding GetDataKPIResponseExceeding(string DashboardID, string KPIID)
        {
            KPIExceeding ResponseExceedingCount = new KPIExceeding();
            UserKPIDashboard UserKPIDashboardDetails = new UserKPIDashboard();
            string Query = string.Empty;
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = UserSettingRepository.IsLoginForm();
                    string UserId = helper.GetUserID(IsLogin);
                 
                    string PrivacySet = GetDashboardPrivacySetting(context, DashboardID);
                    Query = "EXEC USP_CREATE_RESPONSE_EXCEEDING '" + UserId + "','" + PrivacySet + "','" + KPIID + "','" + DashboardID + "'";
                    List<USP_CREATE_RESPONSE_EXCEEDING> CreateRespExceedData = context.Database.SqlQuery<USP_CREATE_RESPONSE_EXCEEDING>(Query).DefaultIfEmpty<USP_CREATE_RESPONSE_EXCEEDING>().ToList();
                    context.Database.CommandTimeout = 400000;
                    UserKPIDashboardDetails = GetKPIDashboardBasicDetails(context,DashboardID, KPIID);
                    if (UserKPIDashboardDetails == null)
                    {
                        return ResponseExceedingCount;
                    }
                    ResponseExceedingCount = new KPIExceeding()
                    {
                        Request_Priority = UserKPIDashboardDetails.Request_Priority,
                        KPIName = UserKPIDashboardDetails.KPIName,
                        ChartColor = UserKPIDashboardDetails.ChartColor,
                        IsGrouped = UserKPIDashboardDetails.IsGrouped,
                        IsUseTheme = UserKPIDashboardDetails.IsUseTheme,
                        IncomingVSCompleted = CreateRespExceedData.Select(t => new ExceedingCount()
                        {
                            Request_Priority = t.REQUEST_PRIORITY,
                            Exceeding = t.EXCEEDING_COUNT,
                            NonExceeding = t.NON_EXCEEDING_COUNT,
                            ExceedingPerc = t.EXCEEDING_PERC,
                            PriorityMonthDate = t.MONTH_WEEK_MAPPING
                        }).ToList<ExceedingCount>()
                    };
                    string[] arr = ResponseExceedingCount.Request_Priority.Split(',');
                    var arrlength = arr.Length;
                    if (arrlength != 1)
                    {
                        ResponseExceedingCount.Request_Priority = "TOTAL," + ResponseExceedingCount.Request_Priority;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return ResponseExceedingCount;
        }

        public KPIExceeding GetDataKPIResolveExceeding( string DashboardID, string KPIID)
        {
            KPIExceeding ResolveExceedingCount = new KPIExceeding();
            UserKPIDashboard UserKPIDashboardDetails = new UserKPIDashboard();
            string Query = string.Empty;
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = UserSettingRepository.IsLoginForm();
                    string UserId = helper.GetUserID(IsLogin);
                    //Vivek
                    //if (HttpContext.Current.Request.Cookies["UserId"].Value != null)
                    //{
                    //    UserId = HttpContext.Current.Request.Cookies["UserId"].Value.ToString();
                    //    string[] ArrayUserID = UserId.Split('=');
                    //    UserId = ArrayUserID[1];
                    //}
                    string PrivacySet = GetDashboardPrivacySetting(context, DashboardID);
                    Query = "EXEC USP_CREATE_RESOLVE_EXCEEDING '" + UserId + "','" + PrivacySet + "','" + KPIID + "','" + DashboardID + "'";
                    List<USP_CREATE_RESPONSE_EXCEEDING> CreateResolveExceedData = context.Database.SqlQuery<USP_CREATE_RESPONSE_EXCEEDING>(Query).DefaultIfEmpty<USP_CREATE_RESPONSE_EXCEEDING>().ToList();
                    context.Database.CommandTimeout = 400000;
                    UserKPIDashboardDetails = GetKPIDashboardBasicDetails(context, DashboardID, KPIID);
                    if (UserKPIDashboardDetails == null)
                    {
                        return ResolveExceedingCount;
                    }
                    ResolveExceedingCount = new KPIExceeding()
                    {
                        Request_Priority = UserKPIDashboardDetails.Request_Priority,
                        KPIName = UserKPIDashboardDetails.KPIName,
                        ChartColor = UserKPIDashboardDetails.ChartColor,
                        IsGrouped = UserKPIDashboardDetails.IsGrouped,
                        IsUseTheme = UserKPIDashboardDetails.IsUseTheme,
                        IncomingVSCompleted = CreateResolveExceedData.Select(t => new ExceedingCount()
                        {
                            Request_Priority = t.REQUEST_PRIORITY,
                            Exceeding = t.EXCEEDING_COUNT,
                            NonExceeding = t.NON_EXCEEDING_COUNT,
                            ExceedingPerc = t.EXCEEDING_PERC,
                            PriorityMonthDate = t.MONTH_WEEK_MAPPING
                        }).ToList<ExceedingCount>()
                    };
                    string[] arr = ResolveExceedingCount.Request_Priority.Split(',');
                    var arrlength = arr.Length;
                    if (arrlength != 1)
                    {
                        ResolveExceedingCount.Request_Priority = "TOTAL," + ResolveExceedingCount.Request_Priority;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return ResolveExceedingCount;
        }

        public KPIGeneric GetDataKPIScoreboard(string DashboardID, string KPIID)
        {
            KPIGeneric GenericDataCount = new KPIGeneric();
            UserKPIDashboard UserKPIDashboardDetails = new UserKPIDashboard();
            string Query = string.Empty;
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = UserSettingRepository.IsLoginForm();
                    string UserId = helper.GetUserID(IsLogin);
                 
                    var IsPeriodic = (from y in context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL
                                      where y.KPI_ID == KPIID && y.IS_ACTIVE == "Y"
                                      select y).FirstOrDefault();

                    if (IsPeriodic.IS_PERIODIC == "Y")
                    {
                        var KPIDATA = (from y in context.DBOARD_COMPUTED_SYSTEM_SCOREBOARD_KPI
                                       where y.KPI_ID == KPIID && y.IS_ACTIVE == "Y"
                                       select new KPIGeneric
                                       {
                                           GenericValueIcon = y.KPI_ICON,
                                           PlotValue = y.SCOREBOARD_TYPE,
                                           ParetoBarColumn = y.CONDITION
                                       }).FirstOrDefault();


                 

                        string ColumnData = KPIDATA.ParetoBarColumn;
                        string stringValue = ColumnData.Substring(1);

                        string[] ColumnName = stringValue.Split(' ', '\t');
                        string columnname = ColumnName[0];
                        string columnnameValue = "";
                        string ColumnCondition = "";

                        var ColumnMappingData = (from y in context.DBOARD_COMPUTED_ADMIN_COLUMN_SETTING
                                                 where y.COLUMN_NAME == columnname
                                                 select new Fields
                                                 {
                                                     Column = y.REMEDY_COLUMN_NAME,
                                                     name = y.REMEDY_TABLE_NAME,
                                                 }).FirstOrDefault();

                        int index = 3;
                        if (index < ColumnName.Length && ColumnName[1]!="between")
                        {
                            ColumnCondition = ColumnName[1] + " " + ColumnName[2];
                            columnnameValue = ColumnName[3];
                        }
                        else if(ColumnName[1]=="between")
                        {
                            ColumnCondition = ColumnName[1];
                            columnnameValue =   ColumnName[2] + " and " + ColumnName[4];
                        }
                        else 
                        {
                            ColumnCondition = ColumnName[1];
                            columnnameValue = ColumnName[2];
                        }
                        columnnameValue = Regex.Replace(columnnameValue, "[^0-9A-Za-z]+", " ");

                        ColumnCondition = ColumnMappingData.Column + " " + ColumnCondition + " " + columnnameValue;


                        string datacount = GetDataFrmRemedy(DashboardID, KPIID, ColumnMappingData.name, ColumnCondition).ToString();
                        UserKPIDashboardDetails = GetKPIDashboardBasicDetails(context, DashboardID, KPIID);
                        if (UserKPIDashboardDetails == null)
                        {
                            return GenericDataCount;
                        }
                        GenericDataCount = new KPIGeneric()
                        {
                            Request_Priority = UserKPIDashboardDetails.Request_Priority,
                            KPIName = UserKPIDashboardDetails.KPIName,
                            ChartColor = UserKPIDashboardDetails.ChartColor,
                            IsGrouped = UserKPIDashboardDetails.IsGrouped,
                            IsUseTheme = UserKPIDashboardDetails.IsUseTheme,
                            ScoreBoardType = KPIDATA.PlotValue.ToString(),
                            GenericValue = datacount,
                            GenericValueIcon = KPIDATA.GenericValueIcon.ToString(),
                            IsPeriodic = IsPeriodic.IS_PERIODIC,
                            PeriodicTime=IsPeriodic.PERIODIC_TIME,
                            PeriodicTimeUnit=IsPeriodic.PERIODIC_TIME_UNIT

                        };
                    }
                    else
                    {
                        string PrivacySet = GetDashboardPrivacySetting(context, DashboardID);
                        Query = "EXEC USP_CREATE_SCOREBOARD_DATA '" + UserId + "','" + PrivacySet + "','" + KPIID + "','" + DashboardID + "'";
                        List<USP_CREATE_SCOREBOARD_DATA> returnGenericData = context.Database.SqlQuery<USP_CREATE_SCOREBOARD_DATA>(Query).DefaultIfEmpty<USP_CREATE_SCOREBOARD_DATA>().ToList();
                        UserKPIDashboardDetails = GetKPIDashboardBasicDetails(context, DashboardID, KPIID);
                        if (UserKPIDashboardDetails == null)
                        {
                            return GenericDataCount;
                        }
                        GenericDataCount = new KPIGeneric()
                        {
                            Request_Priority = UserKPIDashboardDetails.Request_Priority,
                            KPIName = UserKPIDashboardDetails.KPIName,
                            ChartColor = UserKPIDashboardDetails.ChartColor,
                            IsGrouped = UserKPIDashboardDetails.IsGrouped,
                            IsUseTheme = UserKPIDashboardDetails.IsUseTheme,
                            ScoreBoardType = returnGenericData.Select(t => t.TYPE).FirstOrDefault().ToString(),
                            GenericValue = returnGenericData.Select(t => t.COUNT_TOTAL).FirstOrDefault().ToString(),
                            GenericValueIcon = returnGenericData.Select(t => t.KPI_ICON).FirstOrDefault(),
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return GenericDataCount;
        }

        public KPIIncomingVSCompleted GetDataKPIParetoInc(string DashboardID, string KPIID, string SpinnerData, string PrioritySelected = null)
        {
            KPIIncomingVSCompleted IncomingVSCompleted = new KPIIncomingVSCompleted();
            UserKPIDashboard UserKPIDashboardDetails = new UserKPIDashboard();
            string Query = string.Empty;
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = UserSettingRepository.IsLoginForm();
                    string UserId = helper.GetUserID(IsLogin);
            
                    string PrivacySet = GetDashboardPrivacySetting(context, DashboardID);
                    Query = "EXEC USP_CREATE_PARETO_CHART '" + UserId + "','" + PrivacySet + "','" + KPIID + "','" + DashboardID + "','" + SpinnerData + "','" + PrioritySelected + "'";
                    List<USP_CREATE_PARETO_CHART> returnparetodata = context.Database.SqlQuery<USP_CREATE_PARETO_CHART>(Query).DefaultIfEmpty<USP_CREATE_PARETO_CHART>().ToList();
                    context.Database.CommandTimeout = 400000;
                    UserKPIDashboardDetails = GetKPIDashboardBasicDetails(context, DashboardID, KPIID);
                    if (UserKPIDashboardDetails == null)
                    {
                        return IncomingVSCompleted;
                    }
                    IncomingVSCompleted = new KPIIncomingVSCompleted()
                    {
                        Request_Priority = "Total," + UserKPIDashboardDetails.Request_Priority,
                        KPIName = UserKPIDashboardDetails.KPIName,
                        IsLine = UserKPIDashboardDetails.IsLine,
                        ParetoBarColumn = UserKPIDashboardDetails.ParetoBarColumn,
                        ParetoBarColumnName = UserKPIDashboardDetails.ParetoBarColumnName,
                        ParetoBarUnit = UserKPIDashboardDetails.ParetoBarColumn,
                        ParetoLineUnit = UserKPIDashboardDetails.ParetoLineUnit,
                        ChartColor = UserKPIDashboardDetails.ChartColor,
                        IsUseTheme = UserKPIDashboardDetails.IsUseTheme,
                        IsGrouped = UserKPIDashboardDetails.IsGrouped,
                        IncomingVSCompleted = returnparetodata.Select(t => new IncomingVSCompletedPriorityCount()
                        {
                            Request_Priority = t.REQUEST_PRIORITY,
                            Incoming = t.INCOMING,
                            Completed = t.LINE_CHART_VALUE,
                            Backlog = t.BACKLOG,
                            PriorityMonthDate = t.COLUMN_NAME
                        }).ToList<IncomingVSCompletedPriorityCount>()
                    };
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return IncomingVSCompleted;
        }
        public KPIReOpenCount GetDataKPIParetoReOpen(string DashboardID, string KPIID, string SpinnerData, string PrioritySelected = null)
        {
            KPIReOpenCount IncomingVSCompleted = new KPIReOpenCount();
            UserKPIDashboard UserKPIDashboardDetails = new UserKPIDashboard();
            string Query = string.Empty;
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = UserSettingRepository.IsLoginForm();
                    string UserId = helper.GetUserID(IsLogin);
                 
                    string PrivacySet = GetDashboardPrivacySetting(context, DashboardID);
                    Query = "EXEC USP_CREATE_PARETO_CHART '" + UserId + "','" + PrivacySet + "','" + KPIID + "','" + DashboardID + "','" + SpinnerData + "','" + PrioritySelected + "'";
                    List<USP_CREATE_PARETO_CHART> returnparetodata = context.Database.SqlQuery<USP_CREATE_PARETO_CHART>(Query).DefaultIfEmpty<USP_CREATE_PARETO_CHART>().ToList();
                    context.Database.CommandTimeout = 400000;
                    UserKPIDashboardDetails = GetKPIDashboardBasicDetails(context, DashboardID, KPIID);
                    if (UserKPIDashboardDetails == null)
                    {
                        return IncomingVSCompleted;
                    }
                    IncomingVSCompleted = new KPIReOpenCount()
                    {
                        Request_Priority = "Total," + UserKPIDashboardDetails.Request_Priority,
                        KPIName = UserKPIDashboardDetails.KPIName,
                        IsLine = UserKPIDashboardDetails.IsLine,
                        ParetoBarColumn = UserKPIDashboardDetails.ParetoBarColumn,
                        ParetoBarColumnName = UserKPIDashboardDetails.ParetoBarColumnName,
                        ParetoBarUnit = UserKPIDashboardDetails.ParetoBarColumn,
                        ParetoLineUnit = UserKPIDashboardDetails.ParetoLineUnit,
                        ChartColor = UserKPIDashboardDetails.ChartColor,
                        IsUseTheme = UserKPIDashboardDetails.IsUseTheme,
                        IsGrouped = UserKPIDashboardDetails.IsGrouped,
                        IncomingVSCompleted = returnparetodata.Select(t => new ReOpen_Count()
                        {
                            Request_Priority = t.REQUEST_PRIORITY,
                            ReOpenCount = t.INCOMING,
                            Pareto_Cat=t.COLUMN_NAME
                        }).ToList<ReOpen_Count>()
                    };
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return IncomingVSCompleted;
        }

        public KPIExceeding GetDataKPIParetoResolveResolveExceeding(string DashboardID, string KPIID, string SpinnerData, string PrioritySelected = null)
        {
            KPIExceeding ParetoExceedingCount = new KPIExceeding();
            UserKPIDashboard UserKPIDashboardDetails = new UserKPIDashboard();
            string Query = string.Empty;
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = UserSettingRepository.IsLoginForm();
                    string UserId = helper.GetUserID(IsLogin);
                    //Vivek
                    //if (HttpContext.Current.Request.Cookies["UserId"].Value != null)
                    //{
                    //    UserId = HttpContext.Current.Request.Cookies["UserId"].Value.ToString();
                    //    string[] ArrayUserID = UserId.Split('=');
                    //    UserId = ArrayUserID[1];
                    //}
                    string PrivacySet = GetDashboardPrivacySetting(context, DashboardID);
                    Query = "EXEC USP_CREATE_PARETO_CHART '" + UserId + "','" + PrivacySet + "','" + KPIID + "','" + DashboardID + "','" + SpinnerData + "','" + PrioritySelected + "'";
                    List<USP_CREATE_PARETO_CHART> returnparetodata = context.Database.SqlQuery<USP_CREATE_PARETO_CHART>(Query).DefaultIfEmpty<USP_CREATE_PARETO_CHART>().ToList();
                    context.Database.CommandTimeout = 400000;
                    UserKPIDashboardDetails = GetKPIDashboardBasicDetails(context, DashboardID, KPIID);
                    if (UserKPIDashboardDetails == null)
                    {
                        return ParetoExceedingCount;
                    }
                    if ((returnparetodata).Count > 1)
                    {
                        ParetoExceedingCount = new KPIExceeding()
                        {
                            Request_Priority = "Total," + UserKPIDashboardDetails.Request_Priority,
                            KPIName = UserKPIDashboardDetails.KPIName,
                            IsLine = UserKPIDashboardDetails.IsLine,
                            ParetoBarColumn = UserKPIDashboardDetails.ParetoBarColumn,
                            ParetoBarColumnName = UserKPIDashboardDetails.ParetoBarColumnName,
                            ParetoBarUnit = UserKPIDashboardDetails.ParetoBarColumn,
                            ParetoLineUnit = UserKPIDashboardDetails.ParetoLineUnit,
                            ChartColor = UserKPIDashboardDetails.ChartColor,
                            IsUseTheme = UserKPIDashboardDetails.IsUseTheme,
                            IsGrouped = UserKPIDashboardDetails.IsGrouped,
                            IncomingVSCompleted = returnparetodata.Select(t => new ExceedingCount()
                            {
                                Request_Priority = t.REQUEST_PRIORITY,
                                Exceeding = t.EXCEEDING,
                                NonExceeding = t.NON_EXCEEDING,
                                ExceedingPerc = t.LINE_CHART_VALUE,
                                PriorityMonthDate = t.COLUMN_NAME
                            }).ToList<ExceedingCount>()
                        };
                    }
                    else
                    {
                        ParetoExceedingCount.Error = "No Data";

                        return ParetoExceedingCount;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return ParetoExceedingCount;
        }

        public KPIPareto GetDataKPIPareto(string DashboardID, string KPIID, string SpinnerData, string PrioritySelected = null)
        {
            KPIPareto ParetoData = new KPIPareto();
            UserKPIDashboard UserKPIDashboardDetails = new UserKPIDashboard();
            DataSet dsPareto = new DataSet();
            string Query = string.Empty;
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = UserSettingRepository.IsLoginForm();
                    string UserId = helper.GetUserID(IsLogin);
             
                    string PrivacySet = GetDashboardPrivacySetting(context, DashboardID);
                    Query = "EXEC USP_CREATE_PARETO_CHART '" + UserId + "','" + PrivacySet + "','" + KPIID + "','" + DashboardID + "','" + SpinnerData + "','" + PrioritySelected + "'";
                    dsPareto = helper.GetResultReport(Query);
                    context.Database.CommandTimeout = 400000;
                    UserKPIDashboardDetails = GetKPIDashboardBasicDetails(context, DashboardID, KPIID);
                    if (UserKPIDashboardDetails == null)
                    {
                        return ParetoData;
                    }
                    UserKPIDashboardDetails = (from userkpimapping in context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL
                                               join userdashboard in context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL
                                               on userkpimapping.DASHBOARD_ID equals userdashboard.DASHBOARD_ID
                                               where userkpimapping.KPI_ID == KPIID && userkpimapping.DASHBOARD_ID == DashboardID
                                               select new UserKPIDashboard
                                               {
                                                   Request_Priority = userkpimapping.PRIORITY_CHOOSEN,
                                                   KPIName = userkpimapping.KPI_NAME,
                                                   IsLine = userkpimapping.IS_PARETO_LINE_CHART,
                                                   ParetoBarColumn = userkpimapping.PARETO_BAR_COLUMN,
                                                   ParetoBarColumnName = userkpimapping.PARETO_BAR_COLUMN_NAME,
                                                   ParetoBarUnit = userkpimapping.PARETO_BAR_UNIT,
                                                   ParetoLineUnit = userkpimapping.PARETO_BAR_UNIT.ToUpper(),
                                                   ChartColor = userkpimapping.Chart_Color,
                                                   IsGrouped = userkpimapping.IS_GROUPED,
                                                   IsUseTheme = userkpimapping.IS_USETHEME
                                               }).FirstOrDefault();
                    if (UserKPIDashboardDetails.ParetoBarUnit == "AVG_RESP_TIME" || UserKPIDashboardDetails.ParetoBarUnit == "AVG_RESLN_TIME")
                    {
                        ParetoData = (from sdrequest in dsPareto.Tables[1].AsEnumerable()
                                      select new KPIPareto
                                      {
                                          Request_Priority = "Total," + UserKPIDashboardDetails.Request_Priority,
                                          KPIName = UserKPIDashboardDetails.KPIName,
                                          IsLine = UserKPIDashboardDetails.IsLine,
                                          ParetoBarColumn = UserKPIDashboardDetails.ParetoBarColumn,
                                          ParetoBarColumnName = UserKPIDashboardDetails.ParetoBarColumnName,
                                          ParetoBarUnit = UserKPIDashboardDetails.ParetoBarColumn,
                                          ParetoLineUnit = UserKPIDashboardDetails.ParetoLineUnit,
                                          FirstSpinner = sdrequest.Field<string>("SHOW_FIRST_SPINNER_RANGE"),
                                          SecondSpinner = sdrequest.Field<string>("SHOW_SECOND_SPINNER_RANGE"),
                                          ChartColor = UserKPIDashboardDetails.ChartColor,
                                          IsGrouped = UserKPIDashboardDetails.IsGrouped,
                                          IsUseTheme = UserKPIDashboardDetails.IsUseTheme,
                                          Pareto = (from paretorequest in dsPareto.Tables[0].AsEnumerable()
                                                    select new ParetoCount
                                                    {
                                                        BarName = paretorequest.Field<string>("COLUMN_NAME"),
                                                        BarValue = paretorequest.Field<double>("COLUMN_VALUE"),
                                                        LineValue = paretorequest.Field<double>("LINE_CHART_VALUE"),
                                                        Request_Priority = paretorequest.Field<string>("REQUEST_PRIORITY"),
                                                    }).ToList<ParetoCount>()
                                      }).FirstOrDefault();
                    }
                    else
                    {
                        ParetoData = (from sdrequest in dsPareto.Tables[1].AsEnumerable()
                                      select new KPIPareto
                                      {
                                          Request_Priority = UserKPIDashboardDetails.Request_Priority,
                                          KPIName = UserKPIDashboardDetails.KPIName,
                                          IsLine = UserKPIDashboardDetails.IsLine,
                                          ParetoBarColumn = UserKPIDashboardDetails.ParetoBarColumn,
                                          ParetoBarColumnName = UserKPIDashboardDetails.ParetoBarColumnName,
                                          ParetoBarUnit = UserKPIDashboardDetails.ParetoBarColumn,
                                          ParetoLineUnit = UserKPIDashboardDetails.ParetoLineUnit,
                                          FirstSpinner = sdrequest.Field<string>("SHOW_FIRST_SPINNER_RANGE"),
                                          SecondSpinner = sdrequest.Field<string>("SHOW_SECOND_SPINNER_RANGE"),
                                          ChartColor = UserKPIDashboardDetails.ChartColor,
                                          IsGrouped = UserKPIDashboardDetails.IsGrouped,
                                          IsUseTheme = UserKPIDashboardDetails.IsUseTheme,
                                          Pareto = (from paretorequest in dsPareto.Tables[0].AsEnumerable()
                                                    select new ParetoCount
                                                    {
                                                        BarName = paretorequest.Field<string>("COLUMN_NAME"),
                                                        BarValue = paretorequest.Field<double>("COLUMN_VALUE"),
                                                        LineValue = paretorequest.Field<double>("LINE_CHART_VALUE")
                                                    }).ToList<ParetoCount>()
                                      }).FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return ParetoData;
        }

        public DataSet GetDataKPIPivot(string DashboardID, string KPIID)
        {
            DataSet dsPivot = new DataSet();
            string Query;
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = UserSettingRepository.IsLoginForm();
                    string UserId = helper.GetUserID(IsLogin);
                 
                    string PrivacySet = GetDashboardPrivacySetting(context, DashboardID);
                    Query = "EXEC USP_CREATE_PIVOT_DATA '" + UserId + "','" + PrivacySet + "','" + DashboardID + "','" + KPIID + "'";
                    dsPivot = helper.GetResultReport(Query);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dsPivot;
        }

        public DataSet GetTreeMapping(string DashboardID, string KPIID)
        {
            DataSet dsPivot = new DataSet();
            string Query;
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = UserSettingRepository.IsLoginForm();
                    string UserId = helper.GetUserID(IsLogin);
              
                    string PrivacySet = GetDashboardPrivacySetting(context, DashboardID);
                    Query = "EXEC USP_CREATE_TREEMAP_DATA '" + UserId + "','" + PrivacySet + "','" + DashboardID + "','" + KPIID + "'";
                    dsPivot = helper.GetResultReport(Query);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dsPivot;
        }

        public DataSet GetWordCloud(string DashboardID, string KPIID)
        {
            DataSet dsPivot = new DataSet();
            string Query;
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = UserSettingRepository.IsLoginForm();
                    string UserId = helper.GetUserID(IsLogin);
               
                    string PrivacySet = GetDashboardPrivacySetting(context, DashboardID);
                    Query = "EXEC USP_CREATE_WORDCLOUD '" + UserId + "','" + PrivacySet + "','" + DashboardID + "','" + KPIID + "'";
                    dsPivot = helper.GetResultReport(Query);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dsPivot;
        }

        public DataSet GetDrillDownData(string DashboardID, string KPIID, string Request_Priority=null, int? ISMonth=null, string YearMonthWeek=null, string Exceeding=null, string FirstGenericData=null, string SecondGenericData=null, string ThirdGenericData=null)
        {
            DataSet dsKPIData = new DataSet();
            string Query;
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = UserSettingRepository.IsLoginForm();
                    string UserId = helper.GetUserID(IsLogin);
              
                    string PrivacySet = GetDashboardPrivacySetting(context, DashboardID);
                    Query = "EXEC USP_CREATE_DRILLDOWN_DATA '" + UserId + "','" + KPIID + "','" + PrivacySet + "','" + DashboardID + "','" + Request_Priority + "','" + ISMonth + "','" + YearMonthWeek + "','" + Exceeding + "','" + FirstGenericData + "','" + SecondGenericData + "','" + ThirdGenericData + "'";
                    dsKPIData = helper.GetResultReport(Query);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dsKPIData;
        }

        //Function called by all the above functions to get the PRIVACY details of the Dashboard
        public string GetDashboardPrivacySetting(SeeITEntities context, string DashboardID)
        {
            string PrivacySetting = "";
            var DashBoardDetails = (from x in context.DBOARD_COMPUTED_SYSTEM_MAPPING_DASHBOARD_USER
                                    where x.DASHBOARD_ID == DashboardID
                                    select new Dashboard
                                    {
                                        PrivacySetting=x.IS_PRIVACY_SETTING
                                    }).FirstOrDefault();
            PrivacySetting = DashBoardDetails.PrivacySetting.ToString();
            return PrivacySetting;
        }

        //Function called by all the above functions to get the basic details of the Dashboard and KPI
        public UserKPIDashboard GetKPIDashboardBasicDetails(SeeITEntities context, string DashboardID, string KPIID)
        {
            var UserKPIDashboarddata = (from userkpimapping in context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL
                                        join userdashboard in context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL
                                        on userkpimapping.DASHBOARD_ID equals userdashboard.DASHBOARD_ID
                                        where userkpimapping.KPI_ID == KPIID && userkpimapping.DASHBOARD_ID == DashboardID
                                        select new UserKPIDashboard
                                        {
                                            Request_Priority = userkpimapping.PRIORITY_CHOOSEN,
                                            KPIName = userkpimapping.KPI_NAME,
                                            IsLine = userkpimapping.IS_PARETO_LINE_CHART,
                                            ParetoBarColumn = userkpimapping.PARETO_BAR_COLUMN,
                                            ParetoBarColumnName = userkpimapping.PARETO_BAR_COLUMN_NAME,
                                            ParetoBarUnit = userkpimapping.PARETO_BAR_UNIT,
                                            ParetoLineUnit = userkpimapping.PARETO_BAR_UNIT.ToUpper(),
                                            ChartColor = userkpimapping.Chart_Color,
                                            IsUseTheme = userkpimapping.IS_USETHEME,
                                            IsGrouped = userkpimapping.IS_GROUPED,
                                            IsWithSLA = userdashboard.IS_WITH_SLA
                                        }).FirstOrDefault<UserKPIDashboard>();

            return UserKPIDashboarddata;
        }

        //need to verify
        public KPITimeBookRequest TimeBookRequest(string UserID, string DashboardID, string KPIID)
        {
            KPITimeBookRequest KPITimeBookRequestData = new KPITimeBookRequest();
            UserKPIDashboard UserKPIDashboardDetails = new UserKPIDashboard();
            string Query = string.Empty;
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {

                    Query = "EXEC USP_CREATE_TIME_BOOK '" + UserID + "','" + KPIID + "','" + DashboardID + "'";
                    List<USP_CREATE_TIME_BOOK> returtimebookdata = context.Database.SqlQuery<USP_CREATE_TIME_BOOK>(Query).DefaultIfEmpty<USP_CREATE_TIME_BOOK>().ToList();
                    //  dsKPIKPITimeBookRequest = helper.GetResultReport(Query);
                    UserKPIDashboardDetails = GetKPIDashboardBasicDetails(context, DashboardID, KPIID);
                    if (UserKPIDashboardDetails == null)
                    {
                        return KPITimeBookRequestData;
                    }
                    var KPITimeRequestData = new KPITimeBookRequest()
                    {
                        Request_Priority = UserKPIDashboardDetails.Request_Priority,
                        KPIName = UserKPIDashboardDetails.KPIName,
                        ChartColor = UserKPIDashboardDetails.ChartColor,
                        IsGrouped = UserKPIDashboardDetails.IsGrouped,
                        TimeBook = returtimebookdata.Select(t => new TimeBookRequestCount()
                        {
                            RequestCount = t.REQUEST_COUNT,
                            TimeBook = t.TOTAL_TIME,
                            PriorityMonthDate = t.MONTH_WEEK_MAPPING
                        }).ToList<TimeBookRequestCount>()
                    };
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return KPITimeBookRequestData;
        }

        public KPIHeatMapHour GetHourHeatmap( string DashboardID, string KPIID)
        {
            DataSet dsHeatMap = new DataSet();
            string Query;
            UserKPIDashboard UserKPIDashboardDetails = new UserKPIDashboard();
            KPIHeatMapHour HeatMapHour = new KPIHeatMapHour();
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = UserSettingRepository.IsLoginForm();
                    string UserId = helper.GetUserID(IsLogin);
                    //Vivek
                    //if (HttpContext.Current.Request.Cookies["UserId"].Value != null)
                    //{
                    //    UserId = HttpContext.Current.Request.Cookies["UserId"].Value.ToString();
                    //    string[] ArrayUserID = UserId.Split('=');
                    //    UserId = ArrayUserID[1];
                    //}
                    string PrivacySet = GetDashboardPrivacySetting(context, DashboardID);
                    Query = "EXEC USP_CREATE_HEATMAP_HOUR '" + UserId + "','" + PrivacySet + "','" + DashboardID + "','" + KPIID + "'";
                    dsHeatMap = helper.GetResultReport(Query);
                    UserKPIDashboardDetails = GetKPIDashboardBasicDetails(context, DashboardID, KPIID);
                    if (UserKPIDashboardDetails == null)
                    {
                        return HeatMapHour;
                    }
                    HeatMapHour.Request_Priority = "Total," + UserKPIDashboardDetails.Request_Priority;
                    HeatMapHour.KPIName = UserKPIDashboardDetails.KPIName;
                    HeatMapHour.Category = (from y in dsHeatMap.Tables[1].AsEnumerable()
                                           select new Category
                                           {
                                               name = y.Field<string>("Category"),
                                               ticked = false
                                           }).ToList<Category>();
                    HeatMapHour.HeatMapDay = (from k in dsHeatMap.Tables[0].AsEnumerable()
                                             select new HeatMapHour
                                             {
                                                 Request_Priority = k.Field<string>("PRIORITY"),
                                                 day = k.Field<int?>("day"),
                                                 hour= k.Field<int?>("hour"),
                                                 value = k.Field<int?>("value"),
                                                 Category = k.Field<string>("Category")
                                             }).ToList<HeatMapHour>();
                    HeatMapHour.Category[0].ticked = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return HeatMapHour;
        }

        public KPIHeatMapDay GetDayHeatmap(string DashboardID, string KPIID)
        {
            DataSet dsHeatMap = new DataSet();
            string Query;
            UserKPIDashboard UserKPIDashboardDetails = new UserKPIDashboard();
            KPIHeatMapDay HeatMapDay = new KPIHeatMapDay();
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = UserSettingRepository.IsLoginForm();
                    string UserId = helper.GetUserID(IsLogin);
                 
                    string PrivacySet = GetDashboardPrivacySetting(context, DashboardID);
                    Query = "EXEC USP_CREATE_HEATMAP_DAY '" + UserId + "','" + PrivacySet + "','" + DashboardID + "','" + KPIID + "'";
                    dsHeatMap = helper.GetResultReport(Query);
                    UserKPIDashboardDetails = GetKPIDashboardBasicDetails(context, DashboardID, KPIID);
                    if (UserKPIDashboardDetails == null)
                    {
                        return HeatMapDay;
                    }
                    HeatMapDay.Request_Priority = "Total," + UserKPIDashboardDetails.Request_Priority;
                    HeatMapDay.KPIName = UserKPIDashboardDetails.KPIName;
                    HeatMapDay.Category = (from y in dsHeatMap.Tables[1].AsEnumerable()
                                           select new Category
                                           {
                                               name = y.Field<string>("Category"),
                                               ticked = false
                                           }).ToList<Category>();
                    HeatMapDay.HeatMapDay = (from k in dsHeatMap.Tables[0].AsEnumerable()
                                             select new HeatMapDay
                                             {
                                                 Request_Priority = k.Field<string>("PRIORITY"),
                                                 date = k.Field<int?>("date"),
                                                 value = k.Field<int?>("value"),
                                                 Category = k.Field<string>("Category")
                                             }).ToList<HeatMapDay>();
                    HeatMapDay.Category[0].ticked = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return HeatMapDay;
        }

        public string GetDataFrmRemedy(string DashboardID, string KPIID, string TableName, string ColumnCondition)
        {

            string QueryHPDHelpDeskClassic = string.Empty;
            var conString = System.Configuration.ConfigurationManager.ConnectionStrings["RemedyConnectionString"];
            string connectionStringRemedy = conString.ConnectionString;
            string count = "0";
            DateTime dateAsOfNow = (Convert.ToDateTime("2017-01-01 00:00:00"));
            string initiallastmodifieddate = dateAsOfNow.ToString("yyyy-MM-dd HH:mm:ss");
            //string connectionStringRemedy = "Data Source=172.21.103.63;Initial Catalog=ARSystem;User ID=sa;Password=Admin@123456";
            DataTable dt = new DataTable();
            DataTable lastmodifieddt = new DataTable();
            try
            {
                using (SqlConnection conRemedyDBConnection = new SqlConnection(connectionStringRemedy))
                {
                    conRemedyDBConnection.Open();
                   //QueryHPDHelpDeskClassic = "SELECT Incident_Number,Priority,Assigned_Group ,DATEADD(s, Reported_Date, '19700101 00:00') as Reported_Date, Assigned_Support_Company, Assigned_Support_Organization,Assignee,Assignee_Groups,DATEADD(s, Closed_Date, '19700101 00:00') as Closed_Date,InstanceId,DATEADD(s, Last_Date_Duration_Calculated, '19700101 00:00') as Last_Date_Duration_Calculated,DATEADD(s, Submit_Date, '19700101 00:00') as Submit_Date FROM HPD_Help_Desk WHERE Assignee is null and (Status='1' or status='2' or status='3') and cast(DATEADD(s, Reported_Date, '19700101 00:00') as datetime)>{ts '" + initiallastmodifieddate + "'}";
                                                   //" WHERE Assignee is null and (Status='Assigned' or status='In Progress' or status='Pending')";
                   // QueryHPDHelpDeskClassic = "SELECT Incident_Number,Priority,Assigned_Group ,DATEADD(s, Reported_Date, '19700101 00:00') as Reported_Date, Assigned_Support_Company, Assigned_Support_Organization,Assignee,Assignee_Groups,DATEADD(s, Closed_Date, '19700101 00:00') as Closed_Date,InstanceId,DATEADD(s, Last_Date_Duration_Calculated, '19700101 00:00') as Last_Date_Duration_Calculated,DATEADD(s, Submit_Date, '19700101 00:00') as Submit_Date," + ColumnName+ " FROM " + TableName + " WHERE (Status='1' or status='2' or status='3') and cast(DATEADD(s, Reported_Date, '19700101 00:00') as datetime)>{ts '" + initiallastmodifieddate + "'} and " + ColumnCondition;
                    QueryHPDHelpDeskClassic = "SELECT Count(Incident_Number) as TOTAL_COUNT FROM " + TableName + " WHERE (Status='1' or status='2' or status='3') and cast(DATEADD(s, Reported_Date, '19700101 00:00') as datetime)>{ts '" + initiallastmodifieddate + "'} and " + ColumnCondition;
                        try
                        {
                            SqlDataAdapter adptrHPDHelpDeskClassic = new SqlDataAdapter(QueryHPDHelpDeskClassic, conRemedyDBConnection);
                            DataTable dtHPDHelpDeskClassic = new DataTable();
                            adptrHPDHelpDeskClassic.Fill(dtHPDHelpDeskClassic);
                            string TotalCount = dtHPDHelpDeskClassic.Rows[0]["TOTAL_COUNT"].ToString();
                            int count6 = dtHPDHelpDeskClassic.Rows.Count;

                           // dtHPDHelpDeskClassic.Columns.Add(new DataColumn("MINUTES_OPEN", typeof(Int32)));
                           // dtHPDHelpDeskClassic.Columns.Add(new DataColumn("TYPE", typeof(Int32)));
                           // PerformBulkCopy(dtHPDHelpDeskClassic, columnname, columnnameValue);
                            count = TotalCount;
                        }

                        catch
                        {

                        }
                    
                    //else if (TableName == "MINUTES_OPEN" && KPIID == "KPI000041")
                    //{
                    //    QueryHPDHelpDeskClassic = "SELECT Incident_Number,Priority,Assigned_Group ,DATEADD(s, Reported_Date, '19700101 00:00') as Reported_Date, Assigned_Support_Company, Assigned_Support_Organization,Assignee,Assignee_Groups,DATEADD(s, Closed_Date, '19700101 00:00') as Closed_Date,InstanceId,DATEADD(s, Last_Date_Duration_Calculated, '19700101 00:00') as Last_Date_Duration_Calculated,DATEADD(s, Submit_Date, '19700101 00:00') as Submit_Date FROM HPD_Help_Desk WHERE (Priority='0' or Priority='1') and (Status='1' or status='2' or status='3') ";
                    //    //" WHERE (Priority='High' or Priority='Critical') and (Status='Assigned' or status='In Progress' or status='Pending')";
                    //    try
                    //    {
                    //        SqlDataAdapter adptrHPDHelpDeskClassic = new SqlDataAdapter(QueryHPDHelpDeskClassic, conRemedyDBConnection);
                    //        DataTable dtHPDHelpDeskClassic = new DataTable();
                    //        adptrHPDHelpDeskClassic.Fill(dtHPDHelpDeskClassic);
                    //        int count6 = dtHPDHelpDeskClassic.Rows.Count;
                    //        dtHPDHelpDeskClassic.Columns.Add(new DataColumn("MINUTES_OPEN", typeof(Int32)));
                    //        dtHPDHelpDeskClassic.Columns.Add(new DataColumn("TYPE", typeof(Int32)));

                    //        foreach (DataRow row in dtHPDHelpDeskClassic.Rows)
                    //        {
                    //            string Createddate = row["Reported_Date"].ToString();
                    //            DateTime time = Convert.ToDateTime(Createddate);
                    //            DateTime startTime = DateTime.Now;
                    //            TimeSpan span = startTime.Subtract(time);
                    //            double minutes = span.TotalMinutes;
                    //            int minutesRounded = (int)Math.Round(span.TotalMinutes);

                    //            row["MINUTES_OPEN"] = minutesRounded.ToString();
                    //            row["TYPE"] = columnnameValue;
                    //        }

                    //        int datavalue = int.Parse(columnnameValue);
                    //        var results = from myRow in dtHPDHelpDeskClassic.AsEnumerable()
                    //                      where myRow.Field<int>("MINUTES_OPEN") > datavalue
                    //                      select myRow;

                    //        // var result = dtHPDHelpDeskClassic.AsEnumerable().Where(myRow => myRow.Field<int>("MINUTES_OPEN") > 1);
                    //        PerformBulkCopy(dtHPDHelpDeskClassic, TableName, columnnameValue);
                    //        count = results.Count().ToString();
                    //    }

                    //    catch
                    //    {

                    //    }
                    //}
           
                }

            }
            catch
            {

            }
            return count.ToString();
        }

        private static void PerformBulkCopy(DataTable dt, string columnname, string columnnameValue)
        {
            //string connectionString = "Data Source=172.25.13.28;Initial Catalog=MOSAIC_GOVERNANCEDB_INFINEON_DEV;User ID=Supply;Password=Yourock123#";
            string connectionString = "Data Source=172.25.13.28;Initial Catalog=MOSAIC_GOVERNANCEDB_INFINEON_DEV ;User ID=Supply;Password=Yourock123#";
            // open the destination data
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                // open the connection
                sqlConnection.Open();
                String st = "";
                if (columnname == "ASSIGNEE")
                {
                    st = "DELETE FROM Remedy_Data WHERE MINUTES_OPEN is Null";
                }
                else
                {
                    st = "DELETE FROM Remedy_Data WHERE TYPE=" + columnnameValue;
                }
                SqlCommand sqlcom = new SqlCommand(st, sqlConnection);
                sqlcom.ExecuteNonQuery();

                if (dt.Rows.Count > 0)
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection))
                    {
                        sqlBulkCopy.BulkCopyTimeout = 3600;
                        sqlBulkCopy.DestinationTableName = "dbo.Remedy_Data";
                        sqlBulkCopy.WriteToServer(dt);
                    }
                }
            }
        }


        public string GetKPISettingInfo(string KPIID, string DashboardID)
        {
            string PrivacySetting = "";
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                   
                    var DashBoardDetails = (from x in context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL
                                            where x.DASHBOARD_ID == DashboardID && x.KPI_ID== KPIID
                                            select new Fields
                                            {
                                                Column = x.IS_PERIODIC
                                            }).FirstOrDefault();
                    PrivacySetting = DashBoardDetails.Column.ToString();
                    
                }
            }
            catch
            {

            }
            return PrivacySetting;
          }

        public DataSet GetRemedyData(string KPIID, string DashboardID)
        {
            DataSet dsKPIData = new DataSet();
            DataTable dt = new DataTable();

            using (SeeITEntities context = new SeeITEntities())
            {
                var KPIDATA = (from y in context.DBOARD_COMPUTED_SYSTEM_SCOREBOARD_KPI
                               where y.KPI_ID == KPIID && y.IS_ACTIVE == "Y"
                               select new KPIGeneric
                               {
                                   GenericValueIcon = y.KPI_ICON,
                                   PlotValue = y.SCOREBOARD_TYPE,
                                   ParetoBarColumn = y.CONDITION
                               }).FirstOrDefault();

                string ColumnData = KPIDATA.ParetoBarColumn;
                string stringValue = ColumnData.Substring(1);

                string[] ColumnName = stringValue.Split(' ', '\t');
                string columnname = ColumnName[0];
                string columnnameValue = "";
                string ColumnCondition = "";

                var ColumnMappingData = (from y in context.DBOARD_COMPUTED_ADMIN_COLUMN_SETTING
                                         where y.COLUMN_NAME == columnname
                                         select new Fields
                                         {
                                             Column = y.REMEDY_COLUMN_NAME,
                                             name = y.REMEDY_TABLE_NAME,
                                         }).FirstOrDefault();

                int index = 3;
                if (index < ColumnName.Length && ColumnName[1] != "between")
                {
                    ColumnCondition = ColumnName[1] + " " + ColumnName[2];
                    columnnameValue = ColumnName[3];
                }
                else if (ColumnName[1] == "between")
                {
                    ColumnCondition = ColumnName[1];
                    columnnameValue = ColumnName[2] + " and " + ColumnName[4];
                }
                else
                {
                    ColumnCondition = ColumnName[1];
                    columnnameValue = ColumnName[2];
                }
                columnnameValue = Regex.Replace(columnnameValue, "[^0-9A-Za-z]+", " ");

                ColumnCondition = ColumnMappingData.Column + " " + ColumnCondition + " " + columnnameValue;


                DataTable DataTableRemedy = GetDrillDownRemedyData(DashboardID, KPIID, ColumnMappingData.name, ColumnCondition);
                dsKPIData.Tables.Add(DataTableRemedy); 

            }

            return dsKPIData;
        }

        public DataTable GetDrillDownRemedyData(string DashboardID, string KPIID, string TableName, string ColumnCondition)
        {
            DataSet dsKPIData = new DataSet();
            DataTable dtHPDHelpDeskClassic = new DataTable();
            string QueryHPDHelpDeskClassic = string.Empty;
            var conString = System.Configuration.ConfigurationManager.ConnectionStrings["RemedyConnectionString"];
            string connectionStringRemedy = conString.ConnectionString;
            string count = "0";
            DateTime dateAsOfNow = (Convert.ToDateTime("2017-01-01 00:00:00"));
            string initiallastmodifieddate = dateAsOfNow.ToString("yyyy-MM-dd HH:mm:ss");
            string Query;
            try
            {
                using (SqlConnection conRemedyDBConnection = new SqlConnection(connectionStringRemedy))
                {
                    conRemedyDBConnection.Open();
                    //QueryHPDHelpDeskClassic = "SELECT Incident_Number,Priority,Assigned_Group ,DATEADD(s, Reported_Date, '19700101 00:00') as Reported_Date, Assigned_Support_Company, Assigned_Support_Organization,Assignee,Assignee_Groups,DATEADD(s, Closed_Date, '19700101 00:00') as Closed_Date,InstanceId,DATEADD(s, Last_Date_Duration_Calculated, '19700101 00:00') as Last_Date_Duration_Calculated,DATEADD(s, Submit_Date, '19700101 00:00') as Submit_Date FROM HPD_Help_Desk WHERE Assignee is null and (Status='1' or status='2' or status='3') and cast(DATEADD(s, Reported_Date, '19700101 00:00') as datetime)>{ts '" + initiallastmodifieddate + "'}";
                    //" WHERE Assignee is null and (Status='Assigned' or status='In Progress' or status='Pending')";
                     QueryHPDHelpDeskClassic = "SELECT Incident_Number,Priority,Assigned_Group ,DATEADD(s, Reported_Date, '19700101 00:00') as Reported_Date, Assigned_Support_Company, Assigned_Support_Organization,Assignee,Assignee_Groups,DATEADD(s, Closed_Date, '19700101 00:00') as Closed_Date,InstanceId,DATEADD(s, Last_Date_Duration_Calculated, '19700101 00:00') as Last_Date_Duration_Calculated,DATEADD(s, Submit_Date, '19700101 00:00') as Submit_Date FROM " + TableName + " WHERE (Status='1' or status='2' or status='3') and cast(DATEADD(s, Reported_Date, '19700101 00:00') as datetime)>{ts '" + initiallastmodifieddate + "'} and " + ColumnCondition;
                    //QueryHPDHelpDeskClassic = "SELECT Count(Incident_Number) as TOTAL_COUNT FROM " + TableName + " WHERE (Status='1' or status='2' or status='3') and cast(DATEADD(s, Reported_Date, '19700101 00:00') as datetime)>{ts '" + initiallastmodifieddate + "'} and " + ColumnCondition;
                    try
                    {
                        SqlDataAdapter adptrHPDHelpDeskClassic = new SqlDataAdapter(QueryHPDHelpDeskClassic, conRemedyDBConnection);
                        adptrHPDHelpDeskClassic.Fill(dtHPDHelpDeskClassic);
                        int count6 = dtHPDHelpDeskClassic.Rows.Count;
                    }
                    catch
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return dtHPDHelpDeskClassic;
        }
    }
}
