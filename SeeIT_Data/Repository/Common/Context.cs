﻿using SeeIT_Data.EDMX;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeIT_Data.Repository.Common
{
    public class Context : IContext
    {
        public DbContext _DbContext { get; private set; }
        public Context()
        {
            _DbContext = new SeeITEntities();

        }
        public Context(DbContext context)
        {
            // For unit testing
            _DbContext = context;

        }
        public void Dispose()
        {
            _DbContext.Dispose();
        }

    }
}
