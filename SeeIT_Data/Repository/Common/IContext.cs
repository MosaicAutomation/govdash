﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeIT_Data.Repository.Common
{
    public interface IContext : IDisposable
    {
        DbContext _DbContext { get; }
    }
}
