﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeIT_Data.Repository.Common
{
    public interface IUnitOfWork : IDisposable
    {
        void SaveDetails();
    }
}
