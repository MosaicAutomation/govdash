﻿using SeeIT_Data.EDMX;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SeeIT_Data.Repository.Common
{
    public class Repository<T> : IRepository<T>, IUnitOfWork where T : class
    {
        DbContext dbContext = null;

        //By Default taking it own Db context
        public Repository()
        {
            dbContext = new Context()._DbContext;
        }

        // To Specify the own context for Unit testing.
        public Repository(IContext context)
        {
            dbContext = context._DbContext;
        }

        public IQueryable<T> GetDetails(Expression<Func<T, bool>> Predicate)
        {
            try
            {
                var tList = dbContext.Set<T>();
                return tList.Where(Predicate);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void Add(T entity)
        {
            try {
                var tEntity = dbContext.Set<T>();
                dbContext.Set<T>().Add(entity);
                dbContext.Entry<T>(entity).State = EntityState.Added;
                SaveDetails();
            }
            catch (Exception ex)
            {

            }
        }

        public void Update(T entity)
        {
            dbContext.Set<T>().Attach(entity);
            dbContext.Entry<T>(entity).State = EntityState.Modified;
            SaveDetails();
        }

        public void SaveDetails()
        {
            if (dbContext != null)
                dbContext.SaveChanges();
        }

        public void Dispose()
        {
            this.SaveDetails();
            dbContext = null;
            GC.SuppressFinalize(dbContext);
        }

    }
}
