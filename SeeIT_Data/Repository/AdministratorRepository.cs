﻿using SeeIT_Common.Helper;
using SeeIT_Common.Model;
using SeeIT_Data.EDMX;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeIT_Data.Repository
{
    public class AdministratorRepository
    {
        HelperFunctions helper = new HelperFunctions();
        public virtual List<Search> GetBaseKPICollection(string keyword)
        {
            List<Search> BaseKPIs = new List<Search>();
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    BaseKPIs = (from customermaster in context.DBOARD_CORE_BASE_KPIS
                                where customermaster.BASE_KPI_ID.Contains(keyword) || customermaster.BASE_KPI_NAME.Contains(keyword)
                                select new Search
                                {
                                    SearchID = customermaster.BASE_KPI_ID.ToString(),
                                    SearchValue = customermaster.BASE_KPI_NAME
                                }).Distinct().Take(5).ToList<Search>();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return BaseKPIs;
        }

        public virtual List<Admin> GetBaseKPIColumnMapping(string keyword)
        {
            List<Admin> GetSearchData = new List<Admin>();
            List<Admin> getVal = new List<Admin>();
            List<string> AddItems = new List<string>();

            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    GetSearchData = (from colmap in context.DBOARD_COMPUTED_ADMIN_MAPPING_KPI_COLUMN
                                     join kpimaster in context.DBOARD_CORE_BASE_KPIS
                                     on colmap.BASE_KPI_ID equals kpimaster.BASE_KPI_ID
                                     where kpimaster.BASE_KPI_ID == keyword || kpimaster.BASE_KPI_NAME == keyword
                                     join conName in context.DBOARD_COMPUTED_ADMIN_COLUMN_SETTING
                                     on new { colmap.COLUMN_ID } equals
                                     new { COLUMN_ID = conName.COLUMN_ID.ToString() }
                                     orderby colmap.SORT_ORDER
                                     select new Admin
                                     {
                                         ColID = conName.COLUMN_ID.ToString(),
                                         ColName = conName.COLUMN_NAME,
                                         IsActive = colmap.IS_ACTIVE,
                                         DispColName = conName.DISPLAY_COLUMN_NAME
                                     }).ToList<Admin>();



                    var myList = new List<string>();
                    foreach (var item in GetSearchData)
                    {
                        myList.Add(item.DispColName);
                    }
                    getVal = (from colmaster in context.DBOARD_COMPUTED_ADMIN_COLUMN_SETTING
                              where !myList.Contains(colmaster.DISPLAY_COLUMN_NAME)
                              select new Admin
                              {
                                  ColID = colmaster.COLUMN_ID.ToString(),
                                  ColName = colmaster.COLUMN_NAME,
                                  IsActive = "N",
                                  DispColName = colmaster.DISPLAY_COLUMN_NAME
                              }).ToList<Admin>();

                    getVal.AddRange(GetSearchData);


                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return getVal;

        }

        public virtual List<Admin> GetMasterColumnMapping()
        {
            List<Admin> GetMasterData = new List<Admin>();
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    GetMasterData = (from x in context.DBOARD_COMPUTED_ADMIN_COLUMN_SETTING
                                     select new Admin
                                     {
                                         ColID = x.COLUMN_ID.ToString(),
                                         ColName = x.COLUMN_NAME,
                                         DispColName = x.DISPLAY_COLUMN_NAME,
                                         IsInRequestMaster = x.IS_IN_DISPLAYTABLE,
                                         IsInFilter = x.IS_IN_FILTER,
                                         IsInPivot = x.IS_IN_PIVOT,
                                         IsInScoreCard = x.IS_IN_SCOREBOARD
                                     }).ToList<Admin>();
                }
            }
            catch (Exception e)
            {
                throw;
            }
            return GetMasterData;
        }

        public virtual string SaveBaseKPIColumnMapping(string name, AdminList YesArray, AdminList NoArray)
        {
            string returnValue = "";
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var kpiID = (from bse in context.DBOARD_CORE_BASE_KPIS

                                 where bse.BASE_KPI_NAME == name || bse.BASE_KPI_ID == name
                                 select bse.BASE_KPI_ID).FirstOrDefault();
                    int i = 0;
                    foreach (var item in YesArray.Admin)
                    {
                        var colname = (from colarr in context.DBOARD_COMPUTED_ADMIN_MAPPING_KPI_COLUMN
                                       where colarr.BASE_KPI_ID == kpiID && colarr.COLUMN_ID == item.ColID
                                       select colarr).ToList();
                        if (colname.Count != 0)
                        {
                            DBOARD_COMPUTED_ADMIN_MAPPING_KPI_COLUMN addcol = new DBOARD_COMPUTED_ADMIN_MAPPING_KPI_COLUMN();
                            addcol = (from update in context.DBOARD_COMPUTED_ADMIN_MAPPING_KPI_COLUMN
                                      where update.BASE_KPI_ID == kpiID && update.COLUMN_ID == item.ColID
                                      select update).FirstOrDefault();
                            addcol.SORT_ORDER = i + 1;
                            addcol.IS_ACTIVE = "Y";
                            context.SaveChanges();
                        }
                        if (colname.Count == 0)
                        {
                            var colid = (from colmaster in context.DBOARD_COMPUTED_ADMIN_COLUMN_SETTING
                                         where colmaster.DISPLAY_COLUMN_NAME == item.DispColName
                                         select colmaster.COLUMN_ID).FirstOrDefault();
                            DBOARD_COMPUTED_ADMIN_MAPPING_KPI_COLUMN addcol = new DBOARD_COMPUTED_ADMIN_MAPPING_KPI_COLUMN();
                            addcol.BASE_KPI_ID = kpiID.ToString();
                            addcol.COLUMN_ID = colid.ToString();
                            addcol.SORT_ORDER = i + 1;
                            addcol.IS_ACTIVE = "Y";
                            context.DBOARD_COMPUTED_ADMIN_MAPPING_KPI_COLUMN.Add(addcol);
                            context.SaveChanges();
                        }
                        i++;
                    }
                    foreach (var noarr in NoArray.Admin)
                    {
                        var NoColName = (from colarr in context.DBOARD_COMPUTED_ADMIN_MAPPING_KPI_COLUMN
                                         where colarr.BASE_KPI_ID == kpiID && colarr.COLUMN_ID == noarr.ColID && colarr.IS_ACTIVE == "Y"
                                         select colarr).ToList();
                        var NoColName1 = (from colarr in context.DBOARD_COMPUTED_ADMIN_MAPPING_KPI_COLUMN
                                          where colarr.BASE_KPI_ID == kpiID && colarr.COLUMN_ID == noarr.ColID && colarr.IS_ACTIVE == "N"
                                          select colarr).ToList();
                        if (NoColName.Count == 1)
                        {
                            DBOARD_COMPUTED_ADMIN_MAPPING_KPI_COLUMN addcol = new DBOARD_COMPUTED_ADMIN_MAPPING_KPI_COLUMN();
                            addcol = (from update in context.DBOARD_COMPUTED_ADMIN_MAPPING_KPI_COLUMN
                                      where update.BASE_KPI_ID == kpiID && update.COLUMN_ID == noarr.ColID
                                      select update).FirstOrDefault();
                            addcol.BASE_KPI_ID = kpiID.ToString();
                            addcol.COLUMN_ID = NoColName[0].COLUMN_ID;
                            addcol.SORT_ORDER = i + 1;
                            addcol.IS_ACTIVE = "N";
                            context.SaveChanges();
                            i++;
                        }
                        if (NoColName1.Count == 1)
                        {
                            DBOARD_COMPUTED_ADMIN_MAPPING_KPI_COLUMN addcol = new DBOARD_COMPUTED_ADMIN_MAPPING_KPI_COLUMN();
                            addcol = (from update in context.DBOARD_COMPUTED_ADMIN_MAPPING_KPI_COLUMN
                                      where update.BASE_KPI_ID == kpiID && update.COLUMN_ID == noarr.ColID
                                      select update).FirstOrDefault();
                            addcol.SORT_ORDER = i + 1;
                            context.SaveChanges();
                            i++;
                        }
                    }
                    returnValue = "Saved Successfully";
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return returnValue;
        }

		public virtual string SaveMasterColumnMapping(List<Admin> MasterColMapData)
        {
            string SaveData = "";
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    foreach (var item in MasterColMapData)
                    {
                        DBOARD_COMPUTED_ADMIN_COLUMN_SETTING ColSet = new DBOARD_COMPUTED_ADMIN_COLUMN_SETTING();
                        ColSet = (from x in context.DBOARD_COMPUTED_ADMIN_COLUMN_SETTING
                                  where x.COLUMN_NAME == item.ColName
                                  select x).FirstOrDefault();
                        ColSet.DISPLAY_COLUMN_NAME = item.DispColName;
                        ColSet.IS_IN_DISPLAYTABLE = item.IsInRequestMaster;
                        ColSet.IS_IN_SCOREBOARD = item.IsInScoreCard;
                        ColSet.IS_IN_FILTER = item.IsInFilter;
                        ColSet.IS_IN_PIVOT = item.IsInPivot;
                        context.SaveChanges();
                        SaveData = "Saved Successfully";
                    }
                }
            }
            catch (Exception e)
            {
                SaveData = "Error In saving";
                throw;
            }
            return SaveData;
        }


        public virtual List<Widget> GetKPIListDetail()
        {
            List<Widget> GetKPIlist = new List<Widget>();
            string Query = "";
            DataSet dsKpiDetailList = new DataSet();
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    Query = "EXEC USP_KPIDETAILLIST";
                    dsKpiDetailList = helper.GetResultReport(Query);


                    GetKPIlist = (from y in dsKpiDetailList.Tables[0].AsEnumerable()
                             select new Widget
                             {
                                 IdNum = y.Field<int?>("BASE_KPI_ID_NUMBER"),
                                 BaseKPIID = y.Field<string>("BASE_KPI_ID"),
                                 BaseKPIName = y.Field<string>("KPINAME"),
                                 BasKPIDesc = y.Field<string>("DESCRIPTION"),
                                 ticked = y.Field<string>("ISPOSIBLE")
                             }).ToList<Widget>();

                }
            }
            catch (Exception e)
            {
                throw;
            }
            return GetKPIlist;
        }

    }
}
