﻿using SeeIT_Common.Constant;
using SeeIT_Common.Helper;
using SeeIT_Common.Model;
using SeeIT_Data.EDMX;
using SeeIT_Data.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;



namespace SeeIT_Data.Repository
{
    public class UserSettingRepository
    {
        HelperFunctions helper = new HelperFunctions();
       
        string UserIdInfo = "";
        public UserValidation UserAuthentication(string DashBoardID)
        {
            string result = "";
            string UserID = "";
            UserValidation ValidateUser = new UserValidation();
            UserDetails ud = new UserDetails();
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = IsLoginForm();
                    UserID = helper.GetUserID(IsLogin);
                    //if (HttpContext.Current.Request.Cookies["UserId"].Value != null)
                    //{
                    //    UserID = HttpContext.Current.Request.Cookies["UserId"].Value.ToString();
                    //    string[] ArrayUserID = UserID.Split('=');
                    //    UserID = ArrayUserID[1];
                    //}
                  
                    var DashExists = (from x in context.DBOARD_COMPUTED_SYSTEM_MAPPING_DASHBOARD_USER
                                      where x.DASHBOARD_ID == DashBoardID
                                      select x).FirstOrDefault();
                    if (DashExists == null)
                    {
                        ValidateUser.IsValidUser = 0;
                    }
                    else
                    {
                        var UserDetail = (from x in context.DBOARD_COMPUTED_SYSTEM_MAPPING_DASHBOARD_USER
                                          where x.DASHBOARD_ID == DashBoardID && (x.USER_ID.Contains(UserID) || x.IS_PRIVACY_SETTING == 1)
                                          select x).FirstOrDefault();
                        if (UserDetail != null)
                        {
                            ValidateUser.IsValidUser = 1;
                        }
                        else
                        {
                            ValidateUser.IsValidUser = 2;
                            var GetUserID = (from x in context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL
                                             where x.DASHBOARD_ID == DashBoardID
                                             select new
                                             {
                                                 GetUserID = x.CREATED_BY
                                             }).FirstOrDefault();
                            ud = helper.GetUserDetailsWithID(GetUserID.GetUserID);
                            ValidateUser.CreatorName = ud.UserName;
                            ValidateUser.CreatorEmail = ud.UserEmailId;
                            ValidateUser.CreatorID = ud.UserID;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                result = "InValid";
                throw;
            }
            return ValidateUser;
        }
        public KPIUserSetting GetWidgetSettings(string KPIID, int ISKPI = 0)
        {
            KPIUserSetting UserSettingData = new KPIUserSetting();
            string Query = "";
            DataSet dsPivot = new DataSet();
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = IsLoginForm();
                    string UserID = helper.GetUserID(IsLogin);
                    //Vivek
                    //if (HttpContext.Current.Request.Cookies["UserId"].Value != null)
                    //{
                    //    UserID = HttpContext.Current.Request.Cookies["UserId"].Value.ToString();
                    //    string[] ArrayUserID = UserID.Split('=');
                    //    UserID = ArrayUserID[1];
                    //}
                    Query = "EXEC USP_GET_KPI_SETTING '" + UserID + "','" + KPIID + "','" + ISKPI + "'";
                    dsPivot = helper.GetResultReport(Query);
                    if (ISKPI == 1)
                    {
                        UserSettingData = (from x in dsPivot.Tables[9].AsEnumerable()
                                           select new KPIUserSetting
                                           {
                                               Request_Priority = (from y in dsPivot.Tables[1].AsEnumerable()
                                                                   select new CommonData
                                                                   {
                                                                       name = y.Field<string>("name"),
                                                                       ticked = Convert.ToBoolean(y.Field<string>("ticked"))
                                                                   }).ToList<CommonData>(),
                                               Request = (from y in dsPivot.Tables[0].AsEnumerable()
                                                          select new CommonData
                                                          {
                                                              name = y.Field<string>("name"),
                                                              ticked = Convert.ToBoolean(y.Field<string>("ticked"))
                                                          }).ToList<CommonData>(),
                                               Request_Status = (from y in dsPivot.Tables[2].AsEnumerable()
                                                                 select new CommonData
                                                                 {
                                                                     name = y.Field<string>("name"),
                                                                     ticked = Convert.ToBoolean(y.Field<string>("ticked"))
                                                                 }).ToList<CommonData>(),
                                               Criticality = (from y in dsPivot.Tables[3].AsEnumerable()
                                                              select new CommonData
                                                              {
                                                                  name = y.Field<string>("name"),
                                                                  ticked = Convert.ToBoolean(y.Field<string>("ticked"))
                                                              }).ToList<CommonData>(),
                                               BaseKPI = (from y in dsPivot.Tables[8].AsEnumerable()
                                                          select new BaseKPI
                                                          {
                                                              BaseKPIID = y.Field<string>("BASE_KPI_ID"),
                                                              BaseKPIName = y.Field<string>("BASE_KPI_NAME"),
                                                              ID = y.Field<int>("BASE_KPI_ID_NUMBER")
                                                          }).ToList<BaseKPI>(),
                                               Size = (from y in dsPivot.Tables[9].AsEnumerable()
                                                       select new Size
                                                       {
                                                           SizeXAxis = y.Field<int>("SIZE_XAXIS").ToString(),
                                                           SizeYAxis = y.Field<int>("SIZE_YAXIS").ToString()
                                                       }).ToList<Size>(),
                                               Position = (from y in dsPivot.Tables[9].AsEnumerable()
                                                           select new Position
                                                           {
                                                               PositionXAxis = y.Field<int?>("POSITION_X").ToString(),
                                                               PositionYAxis = y.Field<int?>("POSITION_Y").ToString()
                                                           }).ToList<Position>(),
                                               SLI = (from y in dsPivot.Tables[9].AsEnumerable()
                                                      select new SLICriticality
                                                      {
                                                          SCriticality = "SLICriticality",
                                                          Value = y.Field<string>("SLI_CRITICALITY")
                                                      }).ToList<SLICriticality>(),
                                               SLICri = (from y in dsPivot.Tables[9].AsEnumerable()
                                                         select new CommonData
                                                         {
                                                             name = y.Field<string>("SLI_CRITICALITY"),
                                                             ticked = true
                                                         }).ToList<CommonData>(),

                                               Plot = (from y in dsPivot.Tables[9].AsEnumerable()
                                                       select new ChartPlot
                                                       {
                                                           PlotType = y.Field<string>("CHART_BY_TYPE"),
                                                           PlotValue = y.Field<int?>("CHART_BY_VALUE").ToString()
                                                       }).ToList<ChartPlot>(),
                                               FilterObj = (from y in dsPivot.Tables[4].AsEnumerable()
                                                            select new CommonData
                                                            {
                                                                value = y.Field<string>("COLUMN_NAME"),
                                                                name = y.Field<string>("DISPLAY_COLUMN_NAME"),
                                                                ticked = false
                                                            }).ToList<CommonData>(),
                                               PivotObj = (from y in dsPivot.Tables[5].AsEnumerable()
                                                           select new CommonData
                                                           {
                                                               value = y.Field<string>("COLUMN_NAME"),
                                                               name = y.Field<string>("DISPLAY_COLUMN_NAME"),
                                                               ticked = false
                                                           }).ToList<CommonData>(),
                                               GetScoreboardObj = (from y in dsPivot.Tables[6].AsEnumerable()
                                                                   select new CommonData
                                                                   {
                                                                       value = y.Field<string>("COLUMN_NAME"),
                                                                       name = y.Field<string>("DISPLAY_COLUMN_NAME"),
                                                                       ticked = false
                                                                   }).ToList<CommonData>(),
                                               WidgetObj = (from y in dsPivot.Tables[7].AsEnumerable()
                                                            select new CommonData
                                                            {
                                                                value = y.Field<string>("BASE_KPI_ID"),
                                                                name = y.Field<string>("BASE_KPI_NAME"),
                                                                idNum = y.Field<int>("BASE_KPI_ID_NUMBER"),
                                                                ticked = false
                                                            }).ToList<CommonData>(),
                                               RemedyObj = (from y in dsPivot.Tables[12].AsEnumerable()
                                                            select new CommonData
                                                            {
                                                                value = y.Field<string>("COLUMN_NAME"),
                                                                name = y.Field<string>("DISPLAY_COLUMN_NAME"),
                                                                ticked = false
                                                            }).ToList<CommonData>(),
                                               //IsWithSLA = x.Field<int?>("IS_WITH_SLA"),
                                               IsGrouped = x.Field<int>("IS_GROUPED"),
                                               IsParent = x.Field<int>("IS_PARENT"),
                                               IsUseTheme = x.Field<int>("IS_USETHEME"),
                                               KPIName = x.Field<string>("KPI_NAME"),
                                               ChartColor = x.Field<string>("Chart_Color"),
                                               IsPeriodic = x.Field<string>("IS_PERIODIC"),
                                               PeriodicTime = x.Field<int?>("PERIODIC_TIME"),
                                               PeriodicTimeUnit = x.Field<string>("PERIODIC_TIME_UNIT"),
                                               
                                           }).FirstOrDefault();
                        if(UserSettingData.BaseKPI[0].BaseKPIID == "BKPI0009")
                        {
                            UserSettingData.SLIAgeQuery = (from y in dsPivot.Tables[11].AsEnumerable()
                                                           select new AgingReport
                                                           {
                                                               SLIAgeQuery = y.Field<string>("QUERY_JSON")
                                                           }).ToList<AgingReport>();
                            var x = (from y in dsPivot.Tables[11].AsEnumerable()
                                    select new
                                    {
                                        SLIAgeSelect = y.Field<string>("AGING_COMMON_VALUES"),
                                    }).ToList();
                            UserSettingData.SLIAgeSelect = x[0].SLIAgeSelect.ToString();
                        }
                    }
                    else
                    {
                        UserSettingData = (from x in dsPivot.Tables[0].AsEnumerable()
                                           select new KPIUserSetting
                                           {
                                               Request_Priority = (from y in dsPivot.Tables[1].AsEnumerable()
                                                                   select new CommonData
                                                                   {
                                                                       name = y.Field<string>("name"),
                                                                       ticked = Convert.ToBoolean(y.Field<string>("ticked"))
                                                                   }).ToList<CommonData>(),
                                               Request = (from y in dsPivot.Tables[0].AsEnumerable()
                                                          select new CommonData
                                                          {
                                                              name = y.Field<string>("name"),
                                                              ticked = Convert.ToBoolean(y.Field<string>("ticked"))
                                                          }).ToList<CommonData>(),
                                               Request_Status = (from y in dsPivot.Tables[2].AsEnumerable()
                                                                 select new CommonData
                                                                 {
                                                                     name = y.Field<string>("name"),
                                                                     ticked = Convert.ToBoolean(y.Field<string>("ticked"))
                                                                 }).ToList<CommonData>(),
                                               Criticality = (from y in dsPivot.Tables[3].AsEnumerable()
                                                              select new CommonData
                                                              {
                                                                  name = y.Field<string>("name"),
                                                                  ticked = Convert.ToBoolean(y.Field<string>("ticked"))
                                                              }).ToList<CommonData>(),
                                               BaseKPI = (from y in dsPivot.Tables[7].AsEnumerable()
                                                          select new BaseKPI
                                                          {
                                                              BaseKPIID = y.Field<string>("BASE_KPI_ID"),
                                                              BaseKPIName = y.Field<string>("BASE_KPI_NAME"),
                                                              ID = y.Field<int>("BASE_KPI_ID_NUMBER")
                                                          }).ToList<BaseKPI>(),
                                               Size = (from y in dsPivot.Tables[0].AsEnumerable()
                                                       select new Size
                                                       {
                                                           SizeXAxis = "",
                                                           SizeYAxis = ""
                                                       }).ToList<Size>(),
                                               FilterObj = (from y in dsPivot.Tables[4].AsEnumerable()
                                                            select new CommonData
                                                            {
                                                                value = y.Field<string>("COLUMN_NAME"),
                                                                name = y.Field<string>("DISPLAY_COLUMN_NAME"),
                                                                ticked = false
                                                            }).ToList<CommonData>(),
                                               PivotObj = (from y in dsPivot.Tables[5].AsEnumerable()
                                                           select new CommonData
                                                           {
                                                               value = y.Field<string>("COLUMN_NAME"),
                                                               name = y.Field<string>("DISPLAY_COLUMN_NAME"),
                                                               ticked = false
                                                           }).ToList<CommonData>(),
                                               GetScoreboardObj = (from y in dsPivot.Tables[6].AsEnumerable()
                                                                   select new CommonData
                                                                   {
                                                                       value = y.Field<string>("COLUMN_NAME"),
                                                                       name = y.Field<string>("DISPLAY_COLUMN_NAME"),
                                                                       ticked = false
                                                                   }).ToList<CommonData>(),
                                               WidgetObj = (from y in dsPivot.Tables[7].AsEnumerable()
                                                            select new CommonData
                                                            {
                                                                value = y.Field<string>("BASE_KPI_ID"),
                                                                name = y.Field<string>("BASE_KPI_NAME"),
                                                                idNum = y.Field<int>("BASE_KPI_ID_NUMBER"),
                                                                ticked = false
                                                            }).ToList<CommonData>(),

                                               RemedyObj = (from y in dsPivot.Tables[8].AsEnumerable()
                                                            select new CommonData
                                                            {
                                                                value = y.Field<string>("COLUMN_NAME"),
                                                                name = y.Field<string>("DISPLAY_COLUMN_NAME"),
                                                                ticked = false
                                                            }).ToList<CommonData>(),
                                               //IsWithSLA = 1,
                                               IsGrouped = 1,
                                               IsParent = 1,
                                               IsUseTheme = 1,
                                               KPIName = "",
                                               IsPeriodic="",
                                               PeriodicTime=0,
                                               PeriodicTimeUnit=""
                                           }).FirstOrDefault();

                    }

                    if (UserSettingData.BaseKPI[0].BaseKPIID == "BKPI0004" || UserSettingData.BaseKPI[0].BaseKPIID == "BKPI0012")
                    {
                        UserSettingData.GroupData = (from y in dsPivot.Tables[9].AsEnumerable()
                                                     select new CommonData
                                                     {
                                                         value = y.Field<string>("GROUP_DATA"),
                                                         ticked = true
                                                     }).ToList<CommonData>();
                        UserSettingData.SplitData = (from y in dsPivot.Tables[9].AsEnumerable()
                                                     select new CommonData
                                                     {
                                                         value = y.Field<string>("SPLIT_DATA"),
                                                         ticked = true
                                                     }).ToList<CommonData>();
                    }
                    else if (UserSettingData.BaseKPI[0].BaseKPIID == "BKPI0007")
                    {
                        UserSettingData.QueryData = (from y in dsPivot.Tables[10].AsEnumerable()
                                                     select new QueryData
                                                     {
                                                         QueryIcon = y.Field<string>("KPI_ICON"),
                                                         QueryJSON = y.Field<string>("QUERY_JSON"),
                                                         QueryValue = y.Field<string>("CONDITION"),
                                                         ScoreBoardType = y.Field<int?>("SCOREBOARD_TYPE")
                                                        
                                                      }).FirstOrDefault();
                        UserSettingData.ScoreBoardType = (from t in dsPivot.Tables[10].AsEnumerable()
                                                          select new CommonData
                                                          {
                                                              name = t.Field<int?>("SCOREBOARD_TYPE").ToString(),
                                                              ticked = true
                                                          }).FirstOrDefault();
                    }
                    else if (UserSettingData.BaseKPI[0].BaseKPIID == "BKPI0003")
                    {
                        UserSettingData.ParetoColumn = (from y in dsPivot.Tables[9].AsEnumerable()
                                                        select new CommonData
                                                        {
                                                            value = y.Field<string>("PARETO_BAR_COLUMN"),
                                                            ticked = true
                                                        }).ToList<CommonData>();
                        UserSettingData.IsLine = (from y in dsPivot.Tables[9].AsEnumerable()
                                                  select new CommonData
                                                  {
                                                      value = y.Field<string>("IS_PARETO_LINE_CHART"),
                                                      ticked = true
                                                  }).ToList<CommonData>();
                        UserSettingData.WhichUnit = (from y in dsPivot.Tables[9].AsEnumerable()
                                                     select new CommonData
                                                     {
                                                         value = y.Field<string>("PARETO_BAR_UNIT"),
                                                         ticked = true
                                                     }).ToList<CommonData>();
                    }
                    else if (UserSettingData.BaseKPI[0].BaseKPIID == "BKPI0011" || UserSettingData.BaseKPI[0].BaseKPIID == "BKPI0014" || UserSettingData.BaseKPI[0].BaseKPIID == "BKPI0013")
                    {
                        UserSettingData.ParetoColumn = (from y in dsPivot.Tables[9].AsEnumerable()
                                                        select new CommonData
                                                        {
                                                            value = y.Field<string>("PARETO_BAR_COLUMN"),
                                                            ticked = true
                                                        }).ToList<CommonData>();
                    }
                    else if (UserSettingData.BaseKPI[0].BaseKPIID == "BKPI0010")
                    {
                        UserSettingData.GroupData = (from y in dsPivot.Tables[9].AsEnumerable()
                                                     select new CommonData
                                                     {
                                                         value = y.Field<string>("GROUP_DATA"),
                                                         ticked = true
                                                     }).ToList<CommonData>();
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }
            return UserSettingData;
        }

        //public ResolutionViewMode GetKPICollections(string DashBoardID)
        //{
        //    ResolutionViewMode UserKPIData = new ResolutionViewMode();
        //    try
        //    {
        //        using (SeeITEntities context = new SeeITEntities())
        //        {
        //            string UserID = helper.GetCurrentUserId();
        //            DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL DashSetting = new DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL();
        //            var DashboardSettingDet = (from y in context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL
        //                                       where y.DASHBOARD_ID == DashBoardID
        //                                       select y).FirstOrDefault();

        //            if (DashboardSettingDet.IS_INDAYS == 1)
        //            {
        //                DateTime DStartDate = DateTime.Now.AddDays(-DashboardSettingDet.INDAYS);

        //                DashboardSettingDet.START_DATE = new DateTime(DStartDate.Year, DStartDate.Month, DStartDate.Day, 0, 0, 0);
        //                DashboardSettingDet.END_DATE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
        //                context.SaveChanges();
        //            }
        //            var UserDetail = (from y in context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL
        //                              join x in context.DBOARD_COMPUTED_SYSTEM_MAPPING_DASHBOARD_USER
        //                              on y.DASHBOARD_ID equals x.DASHBOARD_ID
        //                              where (x.USER_ID.Contains(y.CREATED_BY) || x.IS_PRIVACY_SETTING == 1) && y.DASHBOARD_ID == DashBoardID
        //                              select y).ToList();
        //            UserKPIData.High = (from UserKPI in UserDetail
        //                                from innerList in UserKPI.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE
        //                                where innerList.VIEW_MODE == "High"
        //                                select new UserViewMode
        //                                {
        //                                    KPI = (from x in UserDetail
        //                                           select new BaseKPIVal
        //                                           {
        //                                               Id = UserKPI.KPI_ID,
        //                                               baseKPIID = UserKPI.BASE_KPI_ID,
        //                                               idNum = Convert.ToInt32(UserKPI.BASE_KPI_ID.Substring(4)).ToString()
        //                                           }).FirstOrDefault(),
        //                                    sizeX = UserKPI.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE.Where(t => t.VIEW_MODE.ToUpper() == "HIGH").Select(e => e.SIZE_XAXIS).FirstOrDefault(),
        //                                    sizeY = UserKPI.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE.Where(t => t.VIEW_MODE.ToUpper() == "HIGH").Select(e => e.SIZE_YAXIS).FirstOrDefault(),
        //                                    row = UserKPI.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE.Where(t => t.VIEW_MODE.ToUpper() == "HIGH").Select(e => e.POSITION_X).FirstOrDefault(),
        //                                    col = UserKPI.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE.Where(t => t.VIEW_MODE.ToUpper() == "HIGH").Select(e => e.POSITION_Y).FirstOrDefault()
        //                                }).ToList<UserViewMode>();
        //            UserKPIData.Low = (from UserKPI in UserDetail
        //                               from innerList in UserKPI.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE
        //                               where innerList.VIEW_MODE == "Low"
        //                               select new UserViewMode
        //                               {
        //                                   KPI = (from x in UserDetail
        //                                          select new BaseKPIVal
        //                                          {
        //                                              Id = UserKPI.KPI_ID,
        //                                              baseKPIID = UserKPI.BASE_KPI_ID,
        //                                              idNum = Convert.ToInt32(UserKPI.BASE_KPI_ID.Substring(4)).ToString()
        //                                          }).FirstOrDefault(),
        //                                   sizeX = UserKPI.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE.Where(t => t.VIEW_MODE.ToUpper() == "LOW").Select(e => e.SIZE_XAXIS).FirstOrDefault(),
        //                                   sizeY = UserKPI.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE.Where(t => t.VIEW_MODE.ToUpper() == "LOW").Select(e => e.SIZE_YAXIS).FirstOrDefault(),
        //                                   row = UserKPI.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE.Where(t => t.VIEW_MODE.ToUpper() == "LOW").Select(e => e.POSITION_X).FirstOrDefault(),
        //                                   col = UserKPI.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE.Where(t => t.VIEW_MODE.ToUpper() == "LOW").Select(e => e.POSITION_Y).FirstOrDefault()
        //                               }).ToList<UserViewMode>();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        throw;
        //    }
        //    return UserKPIData;
        //}

        public UserDetails GetUserDetails(string UserID)
            {
            UserDetails UserSetting = new UserDetails();
            try
            {
                //using(MAPlatformEntities MAPlatform = new MAPlatformEntities())
                //{
                //    UserSetting = (from x in MAPlatform.MAP_SYSTEM_USER_SETTING
                //                   where x.USER_ID == UserID
                //                   select new UserDetails
                //                   {
                //                       UserID = x.USER_ID,
                //                       Base_Color = (x.BASE_THEME != null) ? x.BASE_THEME : "light",
                //                       Theme_Color = (x.THEME_COLOR != null) ? x.THEME_COLOR : "cyan"
                //                   }).FirstOrDefault();

                //    if (UserSetting == null)
                //    {
                //        UserSetting = new UserDetails();
                //        UserSetting.Base_Color = "light";
                //        UserSetting.Theme_Color = "cyan";
                //    }
                //}

                using (SeeITEntities context = new SeeITEntities())
                {
                    UserSetting = (from x in context.DBOARD_COMPUTED_SYSTEM_USER_SETTING
                                   where x.USER_ID == UserID
                                   select new UserDetails
                                   {
                                       UserID = x.USER_ID,
                                       Base_Color = (x.BASE_THEME != null) ? x.BASE_THEME : "light",
                                       Theme_Color = (x.THEME_COLOR != null) ? x.THEME_COLOR : "cyan"
                                   }).FirstOrDefault();

                    if (UserSetting == null)
                    {
                        UserSetting = new UserDetails();
                        UserSetting.Base_Color = "light";
                        UserSetting.Theme_Color = "cyan";
                    }
                    UserSetting.AdminDet = (from y in context.DBOARD_COMPUTED_SYSTEM_ADMIN_DETAIL
                                           where y.ADMIN_USER_ID == UserID
                                           select new AdminDetails {
                                               AdminID=y.ADMIN_USER_ID,
                                               AdminName=y.ADMIN_USER_NAME,
                                               IsAdmin=y.IS_ADMIN
                                           }).FirstOrDefault();
                    if (UserSetting.AdminDet == null)
                    {
                        AdminDetails AdminDet = new AdminDetails();
                        AdminDet.AdminID = "";
                        AdminDet.AdminName = "";
                        AdminDet.IsAdmin = 0;
                        UserSetting.AdminDet = AdminDet;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return UserSetting;
        }                
        public FieldList GetScoreboardColumnMap()
        {
            FieldList FieldsDetails = new FieldList();
            FieldList RemedyField = new FieldList();
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    FieldsDetails.FieldListVal = (from field in context.DBOARD_COMPUTED_ADMIN_COLUMN_SETTING
                                     where field.IS_IN_SCOREBOARD == 1
                                     orderby field.DISPLAY_COLUMN_NAME
                                     select new Fields
                                     {
                                         Column = field.COLUMN_NAME,
                                         name = field.DISPLAY_COLUMN_NAME,
                                         fieldType = field.DATATYPE
                                     }).ToList<Fields>();

                    FieldsDetails.RemedyFieldListVal = (from field in context.DBOARD_COMPUTED_ADMIN_COLUMN_SETTING
                                                        where field.IS_IN_REMEDY == 1
                                                        orderby field.DISPLAY_COLUMN_NAME
                                                        select new Fields
                                                        {
                                                            Column = field.COLUMN_NAME,
                                                            name = field.DISPLAY_COLUMN_NAME,
                                                            fieldType = field.DATATYPE
                                                        }).ToList<Fields>();

                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return FieldsDetails;
        }
        public string SaveUserTheme(BaseThemeColor userdata)
        {
            string ret = "";
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    var IsLogin = IsLoginForm();
                    string userid = helper.GetUserID(IsLogin);
                    //Vivek
                    //if (HttpContext.Current.Request.Cookies["UserId"].Value != null)
                    //{
                    //    userid = HttpContext.Current.Request.Cookies["UserId"].Value.ToString();
                    //    string[] ArrayUserID = userid.Split('=');
                    //    userid = ArrayUserID[1];
                    //}
                    DBOARD_COMPUTED_SYSTEM_USER_SETTING SaveTheme = context.DBOARD_COMPUTED_SYSTEM_USER_SETTING.SingleOrDefault(x => x.USER_ID == userid);
                    if (SaveTheme == null)
                    {
                        DBOARD_COMPUTED_SYSTEM_USER_SETTING InsertSaveTheme = new DBOARD_COMPUTED_SYSTEM_USER_SETTING();
                        InsertSaveTheme.USER_ID = userid;
                        InsertSaveTheme.BASE_THEME = userdata.Base_Color;
                        InsertSaveTheme.THEME_COLOR = userdata.Theme_Color;
                        context.DBOARD_COMPUTED_SYSTEM_USER_SETTING.Add(InsertSaveTheme);
                        context.SaveChanges();
                        ret = "Saved Successfully";
                    }
                    else
                    {
                        SaveTheme.USER_ID = userid;
                        SaveTheme.BASE_THEME = userdata.Base_Color;
                        SaveTheme.THEME_COLOR = userdata.Theme_Color;
                        context.SaveChanges();
                        ret = "Saved Successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return ret;
        }

        public string SaveWidgetSettings(User UserData)
        {
            string returnKPI = "";
            try
            {
                using(SeeITEntities context=new SeeITEntities())
                {
                    var IsLogin = IsLoginForm();
                    string UserID = helper.GetUserID(IsLogin);
                    
                    
                    //Vivek
                    //if (HttpContext.Current.Request.Cookies["UserId"].Value != null)
                    //{
                    //    UserID = HttpContext.Current.Request.Cookies["UserId"].Value.ToString();
                    //    string[] ArrayUserID = UserID.Split('=');
                    //    UserID = ArrayUserID[1];
                    //}

                    var UserSettingDetails=(from y in context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL
                                            where y.KPI_ID==UserData.KPIID
                                            select y).FirstOrDefault();
                    var ViewModeDetails=(from y in context.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE
                                         where y.KPIID==UserData.KPIID && y.VIEW_MODE==UserData.Resolution
                                         select y).FirstOrDefault();
                    var DashboardSettingDetails=(from y in context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL
                                                 where y.DASHBOARD_ID==UserData.DashBoardIID
                                                 select y).FirstOrDefault();
                    var ScoreBoardData=(from y in context.DBOARD_COMPUTED_SYSTEM_SCOREBOARD_KPI
                                        where y.KPI_ID==UserData.KPIID
                                        select y).FirstOrDefault();
                    var AgingData = (from y in context.DBOARD_COMPUTED_SYSTEM_AGING_KPI
                                     where y.KPI_ID == UserData.KPIID
                                     select y).FirstOrDefault();
                    if (UserData != null)
                    {
                        if (UserSettingDetails == null || (UserSettingDetails == null && UserSettingDetails.KPI_ID == null))
                        {
                            var GenerateIDDetails=(from y in context.DBOARD_CORE_GENERATE_ID
                                                   where y.ID!=null
                                                   select y).ToList();
                            var KPIinsert = GenerateIDDetails.Select(x => x.NEXT_KPI_ID).FirstOrDefault();
                            DBOARD_COMPUTED_SYSTEM_KPI_DETAIL usersetttingupdateinsert = new DBOARD_COMPUTED_SYSTEM_KPI_DETAIL();
                            DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE usersettingviewmode = new DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE();
                            if (UserData.IsParent == 1)
                            {
                                usersetttingupdateinsert.PRIORITY_CHOOSEN = DashboardSettingDetails.PRIORITY_CHOOSEN;
                                usersetttingupdateinsert.REQUEST_TYPE_CHOOSEN = DashboardSettingDetails.REQUEST_TYPE_CHOOSEN;
                                usersetttingupdateinsert.STATUS_CHOOSEN = DashboardSettingDetails.STATUS_CHOOSEN;
                                usersetttingupdateinsert.CRITICALITY = DashboardSettingDetails.CRITICALITY_CHOOSEN;
                            }
                            else
                            {
                                usersetttingupdateinsert.PRIORITY_CHOOSEN = UserData.Request_Priority;
                                usersetttingupdateinsert.REQUEST_TYPE_CHOOSEN = UserData.Request;
                                usersetttingupdateinsert.STATUS_CHOOSEN = UserData.Request_Status;
                                usersetttingupdateinsert.CRITICALITY = UserData.Criticality;
                            }
                            usersetttingupdateinsert.KPI_NAME = UserData.KPIName;
                            usersetttingupdateinsert.CHART_BY_TYPE = UserData.PlotType;
                            usersetttingupdateinsert.CHART_BY_VALUE = UserData.PlotValue;
                            usersetttingupdateinsert.BASE_KPI_ID = UserData.BaseKPIID;
                            usersetttingupdateinsert.DASHBOARD_ID = UserData.DashBoardIID;
                            usersetttingupdateinsert.SLI_CRITICALITY = UserData.SLICriticality;
                            usersetttingupdateinsert.IS_ACTIVE = "Y";
                            usersetttingupdateinsert.CREATED_DATE = DateTime.Now;
                            usersetttingupdateinsert.CREATED_BY = UserID;
                            usersetttingupdateinsert.KPI_ID = KPIinsert;
                            usersetttingupdateinsert.PARETO_BAR_COLUMN = UserData.ParetoLineUnit;
                            usersetttingupdateinsert.PARETO_BAR_COLUMN_NAME = UserData.ParetoBarColumnName;
                            usersetttingupdateinsert.PARETO_BAR_UNIT = UserData.ParetoBarUnit;
                            usersetttingupdateinsert.IS_PARETO_LINE_CHART = UserData.IsLine;
                            usersetttingupdateinsert.SPLIT_DATA = UserData.SplitData;
                            usersetttingupdateinsert.GROUP_DATA = UserData.GroupData;
                            usersetttingupdateinsert.IS_PARENT = UserData.IsParent;
                            usersetttingupdateinsert.Chart_Color = UserData.ChartColor;
                            usersetttingupdateinsert.IS_GROUPED = UserData.IsGrouped;
                            usersetttingupdateinsert.IS_USETHEME = UserData.IsUseTheme;
                            //vivek
                            usersetttingupdateinsert.KPI_TYPE = UserData.KpiType;
                            usersetttingupdateinsert.GROUP_NAME = UserData.GroupName;
                            usersetttingupdateinsert.GROUP_KEY = UserData.GroupKey;
                            usersetttingupdateinsert.IS_PERIODIC = UserData.IsPeriodic;
                            usersetttingupdateinsert.PERIODIC_TIME = UserData.PeriodicTime;
                            usersetttingupdateinsert.PERIODIC_TIME_UNIT = UserData.PeriodicTimeUnit;

                            //if (UserData.BaseKPIID != "BKPI0007")
                            //{
                            //    usersetttingupdateinsert.IS_PERIODIC = UserData.IsPeriodic;
                            //    usersetttingupdateinsert.PERIODIC_TIME = UserData.PeriodicTime;
                            //}
                            context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL.Add(usersetttingupdateinsert);
                            context.SaveChanges();
                            if (UserData.BaseKPIID == "BKPI0009")
                            {
                                DBOARD_COMPUTED_SYSTEM_AGING_KPI agingKPI = new DBOARD_COMPUTED_SYSTEM_AGING_KPI();
                                agingKPI.AGING_CONDITION = UserData.condition;
                                agingKPI.AGING_UNIT = UserData.unit;
                                agingKPI.AGING_VALUE = UserData.value;
                                agingKPI.KPI_ID = KPIinsert;
                                agingKPI.AGING_COMMON_VALUES = UserData.SLIAgeSelect;
                                agingKPI.QUERY_JSON = UserData.SLIAgeQuery;
                                context.DBOARD_COMPUTED_SYSTEM_AGING_KPI.Add(agingKPI);
                                usersetttingupdateinsert.GROUP_DATA = UserData.SLIAgeSelect;
                                context.SaveChanges();
                            }
                            if (UserData.BaseKPIID == "BKPI0007")
                            {
                                DBOARD_COMPUTED_SYSTEM_SCOREBOARD_KPI genericdatainssert = new DBOARD_COMPUTED_SYSTEM_SCOREBOARD_KPI();

                                genericdatainssert.KPI_ID = KPIinsert;
                                genericdatainssert.KPI_ICON = UserData.QueryIcon;
                                genericdatainssert.CONDITION = UserData.QueryValue;
                                genericdatainssert.QUERY_JSON = UserData.QueryJSON;
                                genericdatainssert.KPI_ICON = UserData.QueryIcon;
                                genericdatainssert.SCOREBOARD_TYPE = UserData.ScoreBoardType;
                                genericdatainssert.IS_ACTIVE = "Y";
                                context.DBOARD_COMPUTED_SYSTEM_SCOREBOARD_KPI.Add(genericdatainssert);
                                context.SaveChanges();

                                UserData.SizeXAxis = 2;
                                UserData.SizeYAxis = 2;
                            }
                            else
                            {
                                UserData.SizeXAxis = 4;
                                UserData.SizeYAxis = 2;
                            }
                            usersettingviewmode.POSITION_X = UserData.PositionXAxis;
                            usersettingviewmode.POSITION_Y = UserData.PositionYAxis;
                            usersettingviewmode.SIZE_XAXIS = UserData.SizeXAxis;
                            usersettingviewmode.SIZE_YAXIS = UserData.SizeYAxis;
                            usersettingviewmode.VIEW_MODE = UserData.Resolution;
                            usersettingviewmode.KPIID = KPIinsert;
                            context.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE.Add(usersettingviewmode);
                            context.SaveChanges();
                           
                            if (UserData.Resolution.ToLower() == "low")
                            {
                                usersettingviewmode.VIEW_MODE = "High";
                                var viewmode = "High";
                                List<ViewModeResol> res = new List<ViewModeResol>();
                                res = (from y in context.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE
                                       join t in context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL
                                       on y.KPIID equals t.KPI_ID
                                       where t.DASHBOARD_ID == UserData.DashBoardIID && y.VIEW_MODE == viewmode
                                       select new ViewModeResol
                                       {
                                           PositionX=y.POSITION_X,
                                           PositionY=y.POSITION_Y,
                                           SizeX=y.SIZE_XAXIS,
                                           SizeY=y.SIZE_YAXIS
                                       }).ToList<ViewModeResol>();
                                if (res != null)
                                {
                                    List<int?> PosX = res.Select(i => i.PositionX).Distinct().ToList();
                                    int? sizex = res.Where(y => y.PositionX == PosX[PosX.Count - 1]).Sum(y => y.SizeX);
                                    int? sizeTot = sizex + UserData.SizeXAxis;
                                    if (sizeTot <= 12)
                                    {
                                        int? Pos = res.Where(y => y.PositionX == PosX[PosX.Count - 1]).Max(y => y.SizeX);
                                        usersettingviewmode.POSITION_X = Pos;
                                        int? PosY = res.Max(y => y.SizeY);
                                        usersettingviewmode.POSITION_Y = sizex;
                                    }
                                    else
                                    {
                                        int? posy = res.Where(y => y.PositionX == PosX[PosX.Count - 1]).Max(y => y.SizeY);
                                        usersettingviewmode.POSITION_X = posy;
                                        usersettingviewmode.POSITION_Y = 0;
                                    }
                                }
                                else
                                {
                                    usersettingviewmode.POSITION_X = 0;
                                    usersettingviewmode.POSITION_Y = 0;
                                }

                                usersettingviewmode.SIZE_XAXIS = UserData.SizeXAxis;
                                usersettingviewmode.SIZE_YAXIS = UserData.SizeYAxis;
                            }
                            else
                            {
                                usersettingviewmode.VIEW_MODE = "Low";
                                var viewmode = "Low";
                                List<ViewModeResol> res = new List<ViewModeResol>();
                                res = (from y in context.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE
                                       join t in context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL
                                       on y.KPIID equals t.KPI_ID
                                       where t.DASHBOARD_ID == UserData.DashBoardIID && y.VIEW_MODE == viewmode
                                       select new ViewModeResol
                                       {
                                           PositionX = y.POSITION_X,
                                           PositionY = y.POSITION_Y,
                                           SizeX = y.SIZE_XAXIS,
                                           SizeY = y.SIZE_YAXIS
                                       }).ToList<ViewModeResol>();
                                if (res != null)
                                {
                                    int counter = 0;
                                    List<int?> PosX = res.Select(i => i.PositionX).Distinct().ToList();
                                    int? sizex = res.Where(y => y.PositionX == PosX[PosX.Count - 1]).Sum(y => y.SizeX);
                                    int? sizeTot = sizex + UserData.SizeXAxis;
                                    if (sizeTot <= 8)
                                    {
                                        int? Pos = res.Where(y => y.PositionX == PosX[PosX.Count - 1]).Max(y => y.SizeX);
                                        usersettingviewmode.POSITION_X = Pos;
                                        counter++;
                                        int? PosY = res.Max(y => y.SizeY);
                                        usersettingviewmode.POSITION_Y = sizex;
                                    }
                                    else
                                    {
                                        int? posy = res.Where(y => y.PositionX == PosX[PosX.Count - 1]).Max(y => y.SizeY);
                                        usersettingviewmode.POSITION_X = posy;
                                        usersettingviewmode.POSITION_Y = 0;
                                    }
                                }
                                else
                                {
                                    usersettingviewmode.POSITION_X = 0;
                                    usersettingviewmode.POSITION_Y = 0;
                                }
                            }
                            usersettingviewmode.KPIID = KPIinsert;
                            context.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE.Add(usersettingviewmode);
                            context.SaveChanges();
                            returnKPI = KPIinsert;
                        }
                        else
                        {
                            if (UserData.IsParent == 1)
                            {
                                UserSettingDetails.PRIORITY_CHOOSEN = DashboardSettingDetails.PRIORITY_CHOOSEN;
                                UserSettingDetails.REQUEST_TYPE_CHOOSEN = DashboardSettingDetails.REQUEST_TYPE_CHOOSEN;
                                UserSettingDetails.STATUS_CHOOSEN = DashboardSettingDetails.STATUS_CHOOSEN;
                                UserSettingDetails.CRITICALITY = DashboardSettingDetails.CRITICALITY_CHOOSEN;
                            }
                            else
                            {
                                UserSettingDetails.PRIORITY_CHOOSEN = UserData.Request_Priority;
                                UserSettingDetails.REQUEST_TYPE_CHOOSEN = UserData.Request;
                                UserSettingDetails.STATUS_CHOOSEN = UserData.Request_Status;
                                UserSettingDetails.CRITICALITY = UserData.Criticality;
                            }
                            if (UserData.BaseKPIID == "BKPI0009") 
                            {
                                AgingData.QUERY_JSON = UserData.SLIAgeQuery;
                                AgingData.AGING_UNIT = UserData.unit;
                                AgingData.AGING_VALUE = UserData.value;
                                AgingData.AGING_CONDITION = UserData.condition;
                                AgingData.AGING_COMMON_VALUES = UserData.SLIAgeSelect;
                                UserSettingDetails.GROUP_DATA = UserData.SLIAgeSelect;
                                context.SaveChanges();
                            }
                            if (UserData.BaseKPIID == "BKPI0007")
                            {
                                ScoreBoardData.KPI_ICON = UserData.QueryIcon;
                                ScoreBoardData.CONDITION = UserData.QueryValue;
                                ScoreBoardData.QUERY_JSON = UserData.QueryJSON;
                                ScoreBoardData.SCOREBOARD_TYPE = UserData.ScoreBoardType;
                                ScoreBoardData.KPI_ICON = UserData.QueryIcon;
                                context.SaveChanges();
                            }
                            UserSettingDetails.KPI_NAME = UserData.KPIName;
                            UserSettingDetails.CHART_BY_TYPE = UserData.PlotType;
                            UserSettingDetails.CHART_BY_VALUE = UserData.PlotValue;
                            UserSettingDetails.BASE_KPI_ID = UserData.BaseKPIID;
                            UserSettingDetails.DASHBOARD_ID = UserData.DashBoardIID;
                            UserSettingDetails.SLI_CRITICALITY = UserData.SLICriticality;
                            UserSettingDetails.IS_ACTIVE = "Y";
                            UserSettingDetails.KPI_NAME = UserData.KPIName;
                            UserSettingDetails.MODIFIED_DATE = DateTime.Now;
                            UserSettingDetails.PARETO_BAR_COLUMN = UserData.ParetoLineUnit;
                            UserSettingDetails.PARETO_BAR_COLUMN_NAME = UserData.ParetoBarColumnName;
                            UserSettingDetails.PARETO_BAR_UNIT = UserData.ParetoBarUnit;
                            UserSettingDetails.IS_PARETO_LINE_CHART = UserData.IsLine;
                            UserSettingDetails.SPLIT_DATA = UserData.SplitData;
                            UserSettingDetails.GROUP_DATA = UserData.GroupData;
                            UserSettingDetails.IS_PARENT = UserData.IsParent;
                            UserSettingDetails.Chart_Color = UserData.ChartColor;
                            UserSettingDetails.IS_GROUPED = UserData.IsGrouped;
                            UserSettingDetails.IS_USETHEME = UserData.IsUseTheme;
                            UserSettingDetails.KPI_TYPE = UserData.KpiType;
                            UserSettingDetails.GROUP_NAME = UserData.GroupName;
                            UserSettingDetails.GROUP_KEY = UserData.GroupKey;
                            UserSettingDetails.IS_PERIODIC = UserData.IsPeriodic;
                            UserSettingDetails.PERIODIC_TIME = UserData.PeriodicTime;
                            UserSettingDetails.PERIODIC_TIME_UNIT = UserData.PeriodicTimeUnit;
                            //if (UserData.BaseKPIID != "BKPI0007")
                            //{

                            //}
                            context.SaveChanges();
                            returnKPI = UserSettingDetails.KPI_ID;
                            context.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }
            return returnKPI;
        }

        public string SaveWigetPositionAndSize(User UserData, string resolution)
        {
            string returnKPI = "";
            try
            {
                using (SeeITEntities context = new SeeITEntities()) 
                {
                    var IsLogin = IsLoginForm();
                    string UserID = helper.GetUserID(IsLogin);
                   
                    //Vivek
                    //if (HttpContext.Current.Request.Cookies["UserId"].Value != null)
                    //{
                    //    UserID = HttpContext.Current.Request.Cookies["UserId"].Value.ToString();
                    //    string[] ArrayUserID = UserID.Split('=');
                    //    UserID = ArrayUserID[1];
                    //}

                    var UserSettingDetails = (from y in context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL
                                              where y.KPI_ID == UserData.KPIID
                                              select y).FirstOrDefault();
                    var ViewModeDetails = (from y in context.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE
                                           where y.KPIID == UserData.KPIID && y.VIEW_MODE == resolution
                                           select y).FirstOrDefault();
                    if (UserData != null)
                    {
                        if (UserSettingDetails != null)
                        {
                            UserSettingDetails.MODIFIED_DATE = DateTime.Now;
                            ViewModeDetails.SIZE_XAXIS = UserData.SizeXAxis;
                            ViewModeDetails.SIZE_YAXIS = UserData.SizeYAxis;
                            ViewModeDetails.POSITION_X = UserData.PositionXAxis;
                            ViewModeDetails.POSITION_Y = UserData.PositionYAxis;
                            UserSettingDetails.DBOARD_COMPUTED_SYSTEM_KPI_VIEWMODE.Add(ViewModeDetails);
                            context.SaveChanges();
                            returnKPI = UserSettingDetails.KPI_ID;
                        }
                    }
                }
            }
            catch (Exception e) 
            {
                throw;
            }
            return returnKPI;
        }
        //Function not used now.But Generic function to get user Details with AD
        //public UserDetails GetUserData()
        //{
        //    UserDetails ud = new UserDetails();
        //    string UserID;
        //    try
        //    {
        //        UserID =helper.GetCurrentUserId();
        //        ud = helper.GetUserDetailsWithID(UserID);
        //    }
        //    catch (Exception ex)
        //    {
        //        ud.UserID = "277070";
        //        ud.UserName = "Vijay Nair";
        //        ud.UserEmailId = "";
        //        ud.Location = "Chennai";
        //        ud.UserImage = "iVBORw0KGgoAAAANSUhEUgAAAJQAAACdCAYAAABSM1EcAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAA6SSURBVHhe7Z1/aJT3HcfNXe4uSBhZEmaTCOkwl4TMghtuS5lDxzomqFRxZf2jQ8ssWNatSoVNOnFDxI3+YQstCpPNUSGFtWhRWcv6x2RCCy1UqAQT42agxkBMNOgfyeUut/cn97lyudyP5+fd5/s8nxc8PN/vk8vdc8+9v5/v5/v5/lqlKIqiKIqiKIqiKIqiKIqiKIqiKIpIGvgcevr6+jYuLi72IElHH461DQ0NLbjWQn8vYA7XJ3G+h2MM6Vt4zfWFhYVr4+Pjc0uvCDGhFdTmzZsbJyYmtkAQTyO7E8fapT84BKJ6hPe6gOTlWCx2aXh4+FHuL+EiVILq7u5uSiQSW7PZ7G4cWyORSDv/yWvIUn2E430cF0ZHR8mahYJQCApCaonH4wcgolcgoma+XBNgudL4zHOwXn8cGRm5zZcDS5TPgYSqNVik30Sj0SH8oNtxxPlPNQOfGcFpA479bW1tLc3NzZ/Ozs4G1tcKrIXq7e19DqejOMjJlsQDWMrjqVTqzSA68YETVE9PzyCqmFNIklUQC6rC27jPg/CvyJEPDIGq8sgqoYp5D0lXLbZagPukcMSzra2tDTMzM//OXTWfwFgoiOkYTr/P5cwC1uoCGg2/CEKowXhBDQwMNMMfeRvVB8WSTOYarNYu01uCRgsK/tJaCOkikqL9JavAWZ+EqJ6BX3WVLxmHsYIiMeHhf4rjMb4UCDhutQOi+oAvGYWRgqJqLp1O/wfJQFimEjzA8SREdSOXNQcKuhkH+Uw4BVVMBHVKn6cIP+eNwThBUWsuAA54VfAd+9HyG6JoP18yAqPiUBz9fj2XCz7wD3sePnz4tZmZmQ/5kniMERRFwPGAKWhpVIl1C77zYGtr6zhEdY0vicYIp5zHLn2Oh7ueL4UNctKTJgyDMcKHgpioSyWsYiJastnsq5wWjXgLxYPibiIpvn/OZ+YymcwTt27dGuO8SMRbKLR0XsIp7GIimqLRqHgrJdpCURwG1ul/SBoXj/ELVH3fvnnzplgHXbSFgnX6LU4qpuWc4LNIxFoo7l6ZQrIpd0XJs7i4+OTY2NgnnBWFWAsFMVE0XMVUArR4aeqXSMQKCr6C2IdWbyCon3FSHCKrPAoVxGKxqUiNpzyZBJ7NEzdu3LjOWTGItFA0GVPFVJlMJiOyg1ykoOB0buOkUgZUe7s5KQpxguLhGoEfnuIBG2jUKqfFIE5Qd+/epXl1fq05EChgpcQVPHGCQnUX5k5gW0BQtOyQKMQJCtZpHSeVKmSzWWnT7EVaKHEPSSqwUOKsuThBgX4+K9VRp9wC2t1iA+rz5KQIJPpQoRoz7pZMJiOqRSzRQulgOoORKCjFYCQKimZ4KIYiTlCLi4sqKBtEo1FRyypqlWc4w8PDtAi/GMQJqqGhIfBLL3uIOGsuUVDGLWFTL+AeiJujJ05Q2Wz2FieVKkgsfBItlOiZscIY4bMYJDrlV2HK05xWKoDCJ245anGCohVGIpFIYNbt9gsUunsSF3eVaKHIj6JdnJTKiNyBQaqgArVdhR/AitPia+IQKaixsbEvcRI51VoCqO4ezc/Pi3QLRAqKgJW6zEmlCDjjF6TuZCVWUCiF73BSKQKCEruIq1hB0UptsFLa2lvJl6ju3uW0OMQKijnIZ4VBITsieeNG0YLildq06mMgpuudnZ3nOCsS6RaKxkwf0ch5DvhOR65cuSL6WYhf+P7+/fsz7e3t30Dye7kr4YT8SVjsw5wVi3gLRTQ2Nh6n2AtnQwlZJ06KxoitOaamph7BSmWRfCp3JVzAOp2DdXqDs6IxZq+X6enpq21tbTSrOFSLaZAjHovFdqFQpfiSaIyo8vKg6nsBVd9nnA08ENNkJBLZYdLm1kZsHlTIwMDAYwsLC7SRUKC2hi2GWrbRaPQnIyMjRgV3jbJQBM3yoFIbdCcd3/FF08REGCcoAg/6M1ioFzgbRN4cHR09w2mjMFJQBFo978DHeD6AQc/THR0dxnY5GedDFdPb27sJwvqH6T4VFQyq5ky1THmMFxRBq+Hix7iIpJE7pVNrDgXiGYljxO1ibJVXCI3wbGxs/CGSxnUkUxgEgvpuEMREGLUreiUo8Dc9Pf1ea2trA36gTSjxJhSWs7FY7OdoZIjfS9gqgajyilm3bl1PNBo9huSzuSuygFW6iir6cFCsUiGBFFSeZDJJPtVJWKstuSv1BUK6wUIK7KyeQAsqD4T1FET1GpJ1cdrZ6T7S0dFxVvp4JreEQlB50BrcDguxG5aCzn4vdkrDdD/C8f78/Pw5ycN2vSRUgspDGxRNTExQNbgNloM2M/RkoVgI9RHej6qzy3C2L5nUqesVoRRUMX19fRshhq0Qw7eQ7Ue6Bxas4vrfeA1VXbRSDPlFNEPnw1QqdTUslqgcKqgy0KiGTCbTlE6nG8GSBYPg7kE4S1YHTX1daU9RFEVRwktgul6CAvluq1evTs/OzhoZr1JBCaOlpeUUWo1/bWtr+057e3tizZo1/zVlggIRmFYe9d+hNZY2vfWVTCbvojVZOLZrDi3LSzj+3tXV9YH0SLuRgurp6RnEQ9+Ig/bcpRjSeo4bnR0dHX1+6UUG0t/fvx7f5QvOrgB/u43v/FYqlTozPj4ucgsTYwTFIzN344FWimyPQVBJThsHrNMhfD/qc6wGWa3TENhbtOwRXxOBaEHxaIE9VUS0DFR7HdL2P7EKvu8/8V23crYqFK2HZT6XyWSOSxGWSEGRkFACj+Jh7eRLdthl4vCQ7u7uplgsNsVVty3ywsL5CK9PWjdEjWokIcE/Oo9S+rlDMdFQkc2cNIqmpqZBJ2Ii8H+0re5enG/iGZ6EOFtyf6k9IgQF/6gdx5AbIeWBoDZx0ihgXX7KSTc04RkeSCQSX6BgkptQc+pe5fEXp9iLJ+OTyPzH4/GvOx06AmGTU2z7XuC7HXbju+FzP8ZpMJfzBhSud3EcrGU1WDdBkVlGSTqJ5N7cFe9AKf2R02ncEPh+iPsUZ61yCX7bDk7bhp/Ffc56zQOI6kWaGMt5X6lLlUchADLLSHouJgJWyvEY8q6urjP4Aa5ztipkEfF6V4uBwRn3c92rFhSwITzzU+T48zXfqLmg8MWew+lfOPzcTv/7fLYNR6ItTwWHNaPFwGhxWcfgPX7MST/Zj0L8MfUocN4XaiootED+gNPbOHwtKbAYrhxzCITGgl/K5SoyRzEgTjsGVs5y7MklG6jhg0Lt2+fVRFA0hhtfglpxR/mSr6DEN1MIgrOOgFDISlUczgvhnnYbUOzr63sc9/s4Z32Hng0EfBHPxxd3w3dB8YSAISRrPenSlZXinRxe52wpHqCAuLZOEG6trNNXQFSNuPe/cY3hKb4KKi8m3HzNYyL4zB9w0jGpVOrPEFXJUACuv4GWnesp5LjPWvhPJcFnH6VAKGc9wTdB1VNMjOsAJ/Xo4/5XtOBIZBDbnzjrCrx/XVc2xucf8FJUvghKgJiIteSfcNoxNNsXp+JW3HEvpkvRMByc6tZNksdLUfkiKIjpWJ3FtAScT9eRZw4j/DqXW2Kss7PzNKddUW/rVIhXovJcUBRnws39jrN1BffhSUcxfCVaJWUp0ozqzsv9VurmP5WCRXWIs47wVFA8A9dut4WfeNY3hodN+6x84lUXxsDAQDPe09O+Oy9AgTmB39FxT4NngoJlasfNnKc4B1+SwAavhnKgeU9WybNF99PpNDUafO8KsQuFFGAUhmj2DV+yhWeCwk38BSc/u1MckUgkPLECsCb78B33eiVQFD4vhqv4Ar4rbS4wRI0rvmQZTwQF67QPynY1jskv8MN54kfhffaQ9Y3H4/v4kltEb4QEUW2hxhVnLeNaUNQ0R8n1NDjmMa4tFPkUENNSCAIP+mUnJbcQqk7wPuI3QUIhOtTf32/rPl0Jih4sPnRImN+0DPxwg24FgALzS04Sa1FyXYVEUJ3UvLvFCeRPwdez1chyJag7d+6Q+RfXUimiCfe5kdO24dbYMgGhEL3CSUfg/YwZ9w5RbaJQEGer4lhQZLbxYSc4Kxrcp2PRp1IpepjLWmN4v414yI67diBIIyxUHtzva1YbI44FBbNNY6/r3m1gBVRZji0CxLOHk8vAezqyUuSTwEIZtY0I3S9ay69ytiKOBEVOKj7EshkUgCNrAitEO4iWs27bnYx+hBCN3OYW973fipVyJCiYQMmtuhXAytA0LRKHXQqd8WXgPRtx/IqzlsGzExt/qgS+azOs1H7OlsW2oJK5kX7GbdKDH9KWH8Ut2IpWGFbaVqCT39P1sJp6gXuvGjKxJSg8vCa8aU2G8XoNfnxbfhRahrQqcDVfh6Y/WR5KOzk5uYlKOmeNg57HxMRExUJmS1DxePwlPJCajX/2EvgAdlt6Zau7QvC+lgOdKIyiRhc4AaJ6mZMlsSwosk44uYq/1BMUhH74UZZmBPPrtudylaECBmtmtdvJSIe8CFp/omzhtCyoWCy210IVIB1L/gssCS08YTm6jtdWLWjka8GaOQ6wSgLfdxsnV2BJUGTSrTw0A7A6caFk7KkCg5VKLUGzg+2IVDhlrbclQbFJ93XGaY2oaqFokCAsse2OW/xPxQIHMRnvPxVA1V7JoUpWq7yKjpgpUJXDvmBZUN1ZcsaLwf/tpJEXnC1FEPynr0ABKdl9VFVQyWRyA/7Z2NhJIVTl0MJenF0Bi83RhFR6b2rxcXYZLLQgWPhCnubzMqoKCiXPdjRYMvg+ZQUVj8epanfcP4lqby9F5ElAhQc+U+RWtW5A4Sk57rzi+lDUMkkkEneRFDf22Sl4ELQQWbkZv+2wNMYGHmsNCtA3i9eFr2ihUGJXDN0wHRIMjqUFKkocKiYbZDKZFY2XioKCAh05qEo4gD5WdLiXFRQPzTCuE1ipHRAU7YC6jLKCgvkXOYtFkQP8UetVHlomJZuFilKAkzFmiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoihI2Vq36P24Mys1fe9BKAAAAAElFTkSuQmCC";
        //    }
        //    return ud;
        //}

        public UserDetails GetUserData(string UserId = null, string Password = null)
        {
            UserDetails ud = new UserDetails();
            string UserID;
            try
            {
                if (UserId != null && Password != null)
                {
                    bool IsAuth = IsAuthenticated(UserId, Password);
                    if (IsAuth)
                    {
                        ud = helper.GetUserDetailsWithID(UserId);

                        HttpCookie myCookie = new HttpCookie("UserID");
                        UserId = helper.Encrypt(UserId, "UserID");
                        myCookie.Values["UserID"] = UserId.ToString();
                        HttpContext.Current.Response.Cookies.Add(myCookie);

                        ud.IsAuth = true;
                    }
                    else
                    {
                        ud.IsAuth = false;
                       
                    }
                }
                else
                {
                    var IsLogin = IsLoginForm();
                    UserID = helper.GetUserID(IsLogin);
                    if (UserID != "")
                    {
                        ud = helper.GetUserDetailsWithID(UserID);
                        ud.IsAuth = true;
                    }
                    else
                    {
                        ud.IsAuth = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ud.UserID = "277070";
                ud.UserName = "Vijay Nair";
                ud.UserEmailId = "";
                ud.Location = "Chennai";
                ud.UserImage = "iVBORw0KGgoAAAANSUhEUgAAAJQAAACdCAYAAABSM1EcAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAA6SSURBVHhe7Z1/aJT3HcfNXe4uSBhZEmaTCOkwl4TMghtuS5lDxzomqFRxZf2jQ8ssWNatSoVNOnFDxI3+YQstCpPNUSGFtWhRWcv6x2RCCy1UqAQT42agxkBMNOgfyeUut/cn97lyudyP5+fd5/s8nxc8PN/vk8vdc8+9v5/v5/v5/lqlKIqiKIqiKIqiKIqiKIqiKIqiKIpIGvgcevr6+jYuLi72IElHH461DQ0NLbjWQn8vYA7XJ3G+h2MM6Vt4zfWFhYVr4+Pjc0uvCDGhFdTmzZsbJyYmtkAQTyO7E8fapT84BKJ6hPe6gOTlWCx2aXh4+FHuL+EiVILq7u5uSiQSW7PZ7G4cWyORSDv/yWvIUn2E430cF0ZHR8mahYJQCApCaonH4wcgolcgoma+XBNgudL4zHOwXn8cGRm5zZcDS5TPgYSqNVik30Sj0SH8oNtxxPlPNQOfGcFpA479bW1tLc3NzZ/Ozs4G1tcKrIXq7e19DqejOMjJlsQDWMrjqVTqzSA68YETVE9PzyCqmFNIklUQC6rC27jPg/CvyJEPDIGq8sgqoYp5D0lXLbZagPukcMSzra2tDTMzM//OXTWfwFgoiOkYTr/P5cwC1uoCGg2/CEKowXhBDQwMNMMfeRvVB8WSTOYarNYu01uCRgsK/tJaCOkikqL9JavAWZ+EqJ6BX3WVLxmHsYIiMeHhf4rjMb4UCDhutQOi+oAvGYWRgqJqLp1O/wfJQFimEjzA8SREdSOXNQcKuhkH+Uw4BVVMBHVKn6cIP+eNwThBUWsuAA54VfAd+9HyG6JoP18yAqPiUBz9fj2XCz7wD3sePnz4tZmZmQ/5kniMERRFwPGAKWhpVIl1C77zYGtr6zhEdY0vicYIp5zHLn2Oh7ueL4UNctKTJgyDMcKHgpioSyWsYiJastnsq5wWjXgLxYPibiIpvn/OZ+YymcwTt27dGuO8SMRbKLR0XsIp7GIimqLRqHgrJdpCURwG1ul/SBoXj/ELVH3fvnnzplgHXbSFgnX6LU4qpuWc4LNIxFoo7l6ZQrIpd0XJs7i4+OTY2NgnnBWFWAsFMVE0XMVUArR4aeqXSMQKCr6C2IdWbyCon3FSHCKrPAoVxGKxqUiNpzyZBJ7NEzdu3LjOWTGItFA0GVPFVJlMJiOyg1ykoOB0buOkUgZUe7s5KQpxguLhGoEfnuIBG2jUKqfFIE5Qd+/epXl1fq05EChgpcQVPHGCQnUX5k5gW0BQtOyQKMQJCtZpHSeVKmSzWWnT7EVaKHEPSSqwUOKsuThBgX4+K9VRp9wC2t1iA+rz5KQIJPpQoRoz7pZMJiOqRSzRQulgOoORKCjFYCQKimZ4KIYiTlCLi4sqKBtEo1FRyypqlWc4w8PDtAi/GMQJqqGhIfBLL3uIOGsuUVDGLWFTL+AeiJujJ05Q2Wz2FieVKkgsfBItlOiZscIY4bMYJDrlV2HK05xWKoDCJ245anGCohVGIpFIYNbt9gsUunsSF3eVaKHIj6JdnJTKiNyBQaqgArVdhR/AitPia+IQKaixsbEvcRI51VoCqO4ezc/Pi3QLRAqKgJW6zEmlCDjjF6TuZCVWUCiF73BSKQKCEruIq1hB0UptsFLa2lvJl6ju3uW0OMQKijnIZ4VBITsieeNG0YLildq06mMgpuudnZ3nOCsS6RaKxkwf0ch5DvhOR65cuSL6WYhf+P7+/fsz7e3t30Dye7kr4YT8SVjsw5wVi3gLRTQ2Nh6n2AtnQwlZJ06KxoitOaamph7BSmWRfCp3JVzAOp2DdXqDs6IxZq+X6enpq21tbTSrOFSLaZAjHovFdqFQpfiSaIyo8vKg6nsBVd9nnA08ENNkJBLZYdLm1kZsHlTIwMDAYwsLC7SRUKC2hi2GWrbRaPQnIyMjRgV3jbJQBM3yoFIbdCcd3/FF08REGCcoAg/6M1ioFzgbRN4cHR09w2mjMFJQBFo978DHeD6AQc/THR0dxnY5GedDFdPb27sJwvqH6T4VFQyq5ky1THmMFxRBq+Hix7iIpJE7pVNrDgXiGYljxO1ibJVXCI3wbGxs/CGSxnUkUxgEgvpuEMREGLUreiUo8Dc9Pf1ea2trA36gTSjxJhSWs7FY7OdoZIjfS9gqgajyilm3bl1PNBo9huSzuSuygFW6iir6cFCsUiGBFFSeZDJJPtVJWKstuSv1BUK6wUIK7KyeQAsqD4T1FET1GpJ1cdrZ6T7S0dFxVvp4JreEQlB50BrcDguxG5aCzn4vdkrDdD/C8f78/Pw5ycN2vSRUgspDGxRNTExQNbgNloM2M/RkoVgI9RHej6qzy3C2L5nUqesVoRRUMX19fRshhq0Qw7eQ7Ue6Bxas4vrfeA1VXbRSDPlFNEPnw1QqdTUslqgcKqgy0KiGTCbTlE6nG8GSBYPg7kE4S1YHTX1daU9RFEVRwktgul6CAvluq1evTs/OzhoZr1JBCaOlpeUUWo1/bWtr+057e3tizZo1/zVlggIRmFYe9d+hNZY2vfWVTCbvojVZOLZrDi3LSzj+3tXV9YH0SLuRgurp6RnEQ9+Ig/bcpRjSeo4bnR0dHX1+6UUG0t/fvx7f5QvOrgB/u43v/FYqlTozPj4ucgsTYwTFIzN344FWimyPQVBJThsHrNMhfD/qc6wGWa3TENhbtOwRXxOBaEHxaIE9VUS0DFR7HdL2P7EKvu8/8V23crYqFK2HZT6XyWSOSxGWSEGRkFACj+Jh7eRLdthl4vCQ7u7uplgsNsVVty3ywsL5CK9PWjdEjWokIcE/Oo9S+rlDMdFQkc2cNIqmpqZBJ2Ii8H+0re5enG/iGZ6EOFtyf6k9IgQF/6gdx5AbIeWBoDZx0ihgXX7KSTc04RkeSCQSX6BgkptQc+pe5fEXp9iLJ+OTyPzH4/GvOx06AmGTU2z7XuC7HXbju+FzP8ZpMJfzBhSud3EcrGU1WDdBkVlGSTqJ5N7cFe9AKf2R02ncEPh+iPsUZ61yCX7bDk7bhp/Ffc56zQOI6kWaGMt5X6lLlUchADLLSHouJgJWyvEY8q6urjP4Aa5ztipkEfF6V4uBwRn3c92rFhSwITzzU+T48zXfqLmg8MWew+lfOPzcTv/7fLYNR6ItTwWHNaPFwGhxWcfgPX7MST/Zj0L8MfUocN4XaiootED+gNPbOHwtKbAYrhxzCITGgl/K5SoyRzEgTjsGVs5y7MklG6jhg0Lt2+fVRFA0hhtfglpxR/mSr6DEN1MIgrOOgFDISlUczgvhnnYbUOzr63sc9/s4Z32Hng0EfBHPxxd3w3dB8YSAISRrPenSlZXinRxe52wpHqCAuLZOEG6trNNXQFSNuPe/cY3hKb4KKi8m3HzNYyL4zB9w0jGpVOrPEFXJUACuv4GWnesp5LjPWvhPJcFnH6VAKGc9wTdB1VNMjOsAJ/Xo4/5XtOBIZBDbnzjrCrx/XVc2xucf8FJUvghKgJiIteSfcNoxNNsXp+JW3HEvpkvRMByc6tZNksdLUfkiKIjpWJ3FtAScT9eRZw4j/DqXW2Kss7PzNKddUW/rVIhXovJcUBRnws39jrN1BffhSUcxfCVaJWUp0ozqzsv9VurmP5WCRXWIs47wVFA8A9dut4WfeNY3hodN+6x84lUXxsDAQDPe09O+Oy9AgTmB39FxT4NngoJlasfNnKc4B1+SwAavhnKgeU9WybNF99PpNDUafO8KsQuFFGAUhmj2DV+yhWeCwk38BSc/u1MckUgkPLECsCb78B33eiVQFD4vhqv4Ar4rbS4wRI0rvmQZTwQF67QPynY1jskv8MN54kfhffaQ9Y3H4/v4kltEb4QEUW2hxhVnLeNaUNQ0R8n1NDjmMa4tFPkUENNSCAIP+mUnJbcQqk7wPuI3QUIhOtTf32/rPl0Jih4sPnRImN+0DPxwg24FgALzS04Sa1FyXYVEUJ3UvLvFCeRPwdez1chyJag7d+6Q+RfXUimiCfe5kdO24dbYMgGhEL3CSUfg/YwZ9w5RbaJQEGer4lhQZLbxYSc4Kxrcp2PRp1IpepjLWmN4v414yI67diBIIyxUHtzva1YbI44FBbNNY6/r3m1gBVRZji0CxLOHk8vAezqyUuSTwEIZtY0I3S9ay69ytiKOBEVOKj7EshkUgCNrAitEO4iWs27bnYx+hBCN3OYW973fipVyJCiYQMmtuhXAytA0LRKHXQqd8WXgPRtx/IqzlsGzExt/qgS+azOs1H7OlsW2oJK5kX7GbdKDH9KWH8Ut2IpWGFbaVqCT39P1sJp6gXuvGjKxJSg8vCa8aU2G8XoNfnxbfhRahrQqcDVfh6Y/WR5KOzk5uYlKOmeNg57HxMRExUJmS1DxePwlPJCajX/2EvgAdlt6Zau7QvC+lgOdKIyiRhc4AaJ6mZMlsSwosk44uYq/1BMUhH74UZZmBPPrtudylaECBmtmtdvJSIe8CFp/omzhtCyoWCy210IVIB1L/gssCS08YTm6jtdWLWjka8GaOQ6wSgLfdxsnV2BJUGTSrTw0A7A6caFk7KkCg5VKLUGzg+2IVDhlrbclQbFJ93XGaY2oaqFokCAsse2OW/xPxQIHMRnvPxVA1V7JoUpWq7yKjpgpUJXDvmBZUN1ZcsaLwf/tpJEXnC1FEPynr0ABKdl9VFVQyWRyA/7Z2NhJIVTl0MJenF0Bi83RhFR6b2rxcXYZLLQgWPhCnubzMqoKCiXPdjRYMvg+ZQUVj8epanfcP4lqby9F5ElAhQc+U+RWtW5A4Sk57rzi+lDUMkkkEneRFDf22Sl4ELQQWbkZv+2wNMYGHmsNCtA3i9eFr2ihUGJXDN0wHRIMjqUFKkocKiYbZDKZFY2XioKCAh05qEo4gD5WdLiXFRQPzTCuE1ipHRAU7YC6jLKCgvkXOYtFkQP8UetVHlomJZuFilKAkzFmiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoihI2Vq36P24Mys1fe9BKAAAAAElFTkSuQmCC";
            }
            return ud;
        }

        public bool IsAuthenticated(string UserId, string Password)
        {
            string srvr = "chninmsadc01.chennaiodc.lntinfotech.com";
            // string srvr = "GC://dc=lntinfotech,dc=com";
            bool authenticated = false;
            try
            {
                PrincipalContext pc = new PrincipalContext(ContextType.Domain, srvr);
                authenticated = pc.ValidateCredentials(UserId, Password);

            }
            catch (Exception ex)
            {
                //not authenticated due to some other exception [this is optional]
            }
            return authenticated;
        }

        public KPICollections GetKPICollections(string DashBoardID)
        {
            KPICollections UserKPIData = new KPICollections();
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    //string UserID = helper.GetCurrentUserId();
                    var IsLogin = IsLoginForm();
                    string UserID = helper.GetUserID(IsLogin);
                    //string UserID = "";
                    ////Vivek
                    //if (HttpContext.Current.Request.Cookies["UserId"].Value != null)
                    //{
                    //    UserID = HttpContext.Current.Request.Cookies["UserId"].Value.ToString();
                    //    string[] ArrayUserID = UserID.Split('=');
                    //    UserID = ArrayUserID[1];
                    //}
                    DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL DashSetting = new DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL();
                    var DashboardSettingDet = (from y in context.DBOARD_COMPUTED_SYSTEM_DASHBOARD_DETAIL
                                               where y.DASHBOARD_ID == DashBoardID
                                               select y).FirstOrDefault();

                    if (DashboardSettingDet.IS_INDAYS == 1)
                    {
                        DateTime DStartDate = DateTime.Now.AddDays(-DashboardSettingDet.INDAYS);

                        DashboardSettingDet.START_DATE = new DateTime(DStartDate.Year, DStartDate.Month, DStartDate.Day, 0, 0, 0);
                        DashboardSettingDet.END_DATE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                        context.SaveChanges();
                    }
                    UserKPIData.DashBoardID = DashBoardID;
                    UserKPIData.IsCursol = DashboardSettingDet.IS_CURSOL;
                    UserKPIData.IsScoreCard = DashboardSettingDet.IS_SCORECARD;

                    UserKPIData.Cursol = (from y in context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL
                                          join x in context.DBOARD_COMPUTED_SYSTEM_MAPPING_DASHBOARD_USER
                                          on y.DASHBOARD_ID equals x.DASHBOARD_ID
                                          where y.DASHBOARD_ID == DashBoardID && y.KPI_TYPE == "Cursol" && (x.USER_ID.Contains(y.CREATED_BY) || x.IS_PRIVACY_SETTING == 1)
                                          select new BaseKPIVal
                                        {
                                            Id = y.KPI_ID,
                                            baseKPIID = y.BASE_KPI_ID,
                                            idNum = y.BASE_KPI_ID.Substring(4).ToString(),
                                            KPIName=y.KPI_NAME
                                        }).Distinct().ToList<BaseKPIVal>();

                    if (UserKPIData.Cursol != null)
                    {
                        foreach (var item in UserKPIData.Cursol)
                        {
                            item.idNum = Convert.ToInt32(item.idNum).ToString();
                        }
                    }

                    UserKPIData.ScoreCard = (from y in context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL
                                             join x in context.DBOARD_COMPUTED_SYSTEM_MAPPING_DASHBOARD_USER
                                             on y.DASHBOARD_ID equals x.DASHBOARD_ID
                                             where y.DASHBOARD_ID == DashBoardID && y.KPI_TYPE == "ScoreCard" && (x.USER_ID.Contains(y.CREATED_BY) || x.IS_PRIVACY_SETTING == 1)
                                             select new BaseKPIVal
                                             {
                                                 Id = y.KPI_ID,
                                                 baseKPIID = y.BASE_KPI_ID,
                                                 idNum = y.BASE_KPI_ID.Substring(4).ToString(),
                                                 KPIName = y.KPI_NAME
                                             }).Distinct().ToList<BaseKPIVal>();

                    if (UserKPIData.ScoreCard != null)
                    {
                        foreach (var item in UserKPIData.ScoreCard)
                        {
                            item.idNum = Convert.ToInt32(item.idNum).ToString();
                        }
                    }

                    UserKPIData.groups = (from grp in context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL
                                          where grp.DashBoardID == DashBoardID && grp.IS_ACTIVE == "Y" 
                                          orderby grp.GroupIndex ascending
                                          select new GroupDetail
                                          {
                                              KPI = (from kpi in context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL
                                                     where kpi.DASHBOARD_ID == DashBoardID && kpi.KPI_TYPE == "Group" && kpi.GROUP_KEY == grp.GROUP_KEY
                                                     select new BaseKPIVal
                                                     {
                                                         Id = kpi.KPI_ID,
                                                         baseKPIID = kpi.BASE_KPI_ID,
                                                         idNum = kpi.BASE_KPI_ID.Substring(4).ToString(),
                                                         KPIName = kpi.KPI_NAME
                                                     }).Distinct().ToList<BaseKPIVal>(),
                                              GroupView = grp.GroupView,
                                              GroupType = grp.GroupType,
                                              GroupName = grp.GroupName,
                                              GroupID=grp.GroupID,
                                              GroupIndex=grp.GroupIndex,
                                              GroupFilterLabel=(from colmaster in context.DBOARD_COMPUTED_ADMIN_COLUMN_SETTING
                                                           where colmaster.COLUMN_NAME == grp.GroupFilter
                                                           select colmaster.DISPLAY_COLUMN_NAME).FirstOrDefault(),
                                              GroupFilter = grp.GroupFilter,
                                              GroupFilterValue=grp.GroupFilterValue,
                                              IsFilter=grp.IS_FILTER,
                                              GroupKey=grp.GROUP_KEY
                                          }).ToList<GroupDetail>();

                    if (UserKPIData.groups.Count!= 0)
                    {
                        UserKPIData.groups = UserKPIData.groups.GroupBy(test => test.GroupName)
                                            .Select(grp => grp.First()).OrderBy(x=>x.GroupIndex)
                                            .ToList();

                        foreach (var item in UserKPIData.groups)
                        {
                            foreach (var kpiid in item.KPI)
                            {
                                kpiid.idNum = Convert.ToInt32(kpiid.idNum).ToString();
                            }
                        }
                    }
              

                }
            }
            catch (Exception e)
            {
                throw;
            }
            return UserKPIData;
        }

        public string LastRecordTimeStamp()
        {
            string lstmobtmpstmp = "";
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    lstmobtmpstmp = (from lstmod in context.DBOARD_DATA_DETAILS_REQUEST
                                     orderby lstmod.LAST_MODIFIED_TIMESTAMP descending
                                     select lstmod.LAST_MODIFIED_TIMESTAMP
                                     ).FirstOrDefault().ToString();
                 }
            }
            catch (Exception ex)
            {
                throw;
            }
            return lstmobtmpstmp;
        }

        public string StartRecordTimeStamp()
        {
            string strtmobtmpstmp = "";
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    strtmobtmpstmp = (from lstmod in context.DBOARD_DATA_DETAILS_REQUEST
                                     orderby lstmod.LAST_MODIFIED_TIMESTAMP ascending
                                     select lstmod.LAST_MODIFIED_TIMESTAMP
                                     ).FirstOrDefault().ToString();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return strtmobtmpstmp;
        }

        public UserDetails DemoUserData(string UserId = null, string Password = null)
        {
            UserDetails ud = new UserDetails();

            using (SeeITEntities context = new SeeITEntities())
           {
            try
            {
                if (UserId != null && Password != null)
                {
                    bool IsAuth =true;
                    if (IsAuth)
                    {
                        HttpCookie myCookie = new HttpCookie("UserId");
                        myCookie.Values["UserId"] = UserId.ToString();
                        HttpContext.Current.Response.Cookies.Add(myCookie);
                        ud.IsAuth = true;
                        ud.UserID = "demouser";
                        ud.UserName = "Demo User";
                        ud.UserEmailId = "demouser@lntinfotech.com";
                        ud.UserImage = "No_Image";
                        //ud.Country = "";
                        //ud.Location = "";
                        ud.Base_Color = "dark";
                        ud.Theme_Color = "teal";
                        //ud.Base_Color = (from lstmod in context.DBOARD_COMPUTED_SYSTEM_USER_SETTING
                        //                 where lstmod.USER_ID == "demouser"
                        //                 orderby lstmod.BASE_THEME descending
                        //                 select lstmod.BASE_THEME
                        //             ).FirstOrDefault().ToString();
                        //ud.Theme_Color = (from lstmod in context.DBOARD_COMPUTED_SYSTEM_USER_SETTING
                        //                  where lstmod.USER_ID == "demouser"
                        //                  orderby lstmod.THEME_COLOR descending
                        //                  select lstmod.THEME_COLOR
                        //             ).FirstOrDefault().ToString();
                    }
                    else
                    {
                        ud.IsAuth = false;

                    }
                }
              }
            
            catch (Exception ex)
            {
                ud.UserID = "277070";
                ud.UserName = "Vijay Nair";
                ud.UserEmailId = "";
                ud.Location = "Chennai";
                ud.UserImage = "iVBORw0KGgoAAAANSUhEUgAAAJQAAACdCAYAAABSM1EcAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAA6SSURBVHhe7Z1/aJT3HcfNXe4uSBhZEmaTCOkwl4TMghtuS5lDxzomqFRxZf2jQ8ssWNatSoVNOnFDxI3+YQstCpPNUSGFtWhRWcv6x2RCCy1UqAQT42agxkBMNOgfyeUut/cn97lyudyP5+fd5/s8nxc8PN/vk8vdc8+9v5/v5/v5/lqlKIqiKIqiKIqiKIqiKIqiKIqiKIpIGvgcevr6+jYuLi72IElHH461DQ0NLbjWQn8vYA7XJ3G+h2MM6Vt4zfWFhYVr4+Pjc0uvCDGhFdTmzZsbJyYmtkAQTyO7E8fapT84BKJ6hPe6gOTlWCx2aXh4+FHuL+EiVILq7u5uSiQSW7PZ7G4cWyORSDv/yWvIUn2E430cF0ZHR8mahYJQCApCaonH4wcgolcgoma+XBNgudL4zHOwXn8cGRm5zZcDS5TPgYSqNVik30Sj0SH8oNtxxPlPNQOfGcFpA479bW1tLc3NzZ/Ozs4G1tcKrIXq7e19DqejOMjJlsQDWMrjqVTqzSA68YETVE9PzyCqmFNIklUQC6rC27jPg/CvyJEPDIGq8sgqoYp5D0lXLbZagPukcMSzra2tDTMzM//OXTWfwFgoiOkYTr/P5cwC1uoCGg2/CEKowXhBDQwMNMMfeRvVB8WSTOYarNYu01uCRgsK/tJaCOkikqL9JavAWZ+EqJ6BX3WVLxmHsYIiMeHhf4rjMb4UCDhutQOi+oAvGYWRgqJqLp1O/wfJQFimEjzA8SREdSOXNQcKuhkH+Uw4BVVMBHVKn6cIP+eNwThBUWsuAA54VfAd+9HyG6JoP18yAqPiUBz9fj2XCz7wD3sePnz4tZmZmQ/5kniMERRFwPGAKWhpVIl1C77zYGtr6zhEdY0vicYIp5zHLn2Oh7ueL4UNctKTJgyDMcKHgpioSyWsYiJastnsq5wWjXgLxYPibiIpvn/OZ+YymcwTt27dGuO8SMRbKLR0XsIp7GIimqLRqHgrJdpCURwG1ul/SBoXj/ELVH3fvnnzplgHXbSFgnX6LU4qpuWc4LNIxFoo7l6ZQrIpd0XJs7i4+OTY2NgnnBWFWAsFMVE0XMVUArR4aeqXSMQKCr6C2IdWbyCon3FSHCKrPAoVxGKxqUiNpzyZBJ7NEzdu3LjOWTGItFA0GVPFVJlMJiOyg1ykoOB0buOkUgZUe7s5KQpxguLhGoEfnuIBG2jUKqfFIE5Qd+/epXl1fq05EChgpcQVPHGCQnUX5k5gW0BQtOyQKMQJCtZpHSeVKmSzWWnT7EVaKHEPSSqwUOKsuThBgX4+K9VRp9wC2t1iA+rz5KQIJPpQoRoz7pZMJiOqRSzRQulgOoORKCjFYCQKimZ4KIYiTlCLi4sqKBtEo1FRyypqlWc4w8PDtAi/GMQJqqGhIfBLL3uIOGsuUVDGLWFTL+AeiJujJ05Q2Wz2FieVKkgsfBItlOiZscIY4bMYJDrlV2HK05xWKoDCJ245anGCohVGIpFIYNbt9gsUunsSF3eVaKHIj6JdnJTKiNyBQaqgArVdhR/AitPia+IQKaixsbEvcRI51VoCqO4ezc/Pi3QLRAqKgJW6zEmlCDjjF6TuZCVWUCiF73BSKQKCEruIq1hB0UptsFLa2lvJl6ju3uW0OMQKijnIZ4VBITsieeNG0YLildq06mMgpuudnZ3nOCsS6RaKxkwf0ch5DvhOR65cuSL6WYhf+P7+/fsz7e3t30Dye7kr4YT8SVjsw5wVi3gLRTQ2Nh6n2AtnQwlZJ06KxoitOaamph7BSmWRfCp3JVzAOp2DdXqDs6IxZq+X6enpq21tbTSrOFSLaZAjHovFdqFQpfiSaIyo8vKg6nsBVd9nnA08ENNkJBLZYdLm1kZsHlTIwMDAYwsLC7SRUKC2hi2GWrbRaPQnIyMjRgV3jbJQBM3yoFIbdCcd3/FF08REGCcoAg/6M1ioFzgbRN4cHR09w2mjMFJQBFo978DHeD6AQc/THR0dxnY5GedDFdPb27sJwvqH6T4VFQyq5ky1THmMFxRBq+Hix7iIpJE7pVNrDgXiGYljxO1ibJVXCI3wbGxs/CGSxnUkUxgEgvpuEMREGLUreiUo8Dc9Pf1ea2trA36gTSjxJhSWs7FY7OdoZIjfS9gqgajyilm3bl1PNBo9huSzuSuygFW6iir6cFCsUiGBFFSeZDJJPtVJWKstuSv1BUK6wUIK7KyeQAsqD4T1FET1GpJ1cdrZ6T7S0dFxVvp4JreEQlB50BrcDguxG5aCzn4vdkrDdD/C8f78/Pw5ycN2vSRUgspDGxRNTExQNbgNloM2M/RkoVgI9RHej6qzy3C2L5nUqesVoRRUMX19fRshhq0Qw7eQ7Ue6Bxas4vrfeA1VXbRSDPlFNEPnw1QqdTUslqgcKqgy0KiGTCbTlE6nG8GSBYPg7kE4S1YHTX1daU9RFEVRwktgul6CAvluq1evTs/OzhoZr1JBCaOlpeUUWo1/bWtr+057e3tizZo1/zVlggIRmFYe9d+hNZY2vfWVTCbvojVZOLZrDi3LSzj+3tXV9YH0SLuRgurp6RnEQ9+Ig/bcpRjSeo4bnR0dHX1+6UUG0t/fvx7f5QvOrgB/u43v/FYqlTozPj4ucgsTYwTFIzN344FWimyPQVBJThsHrNMhfD/qc6wGWa3TENhbtOwRXxOBaEHxaIE9VUS0DFR7HdL2P7EKvu8/8V23crYqFK2HZT6XyWSOSxGWSEGRkFACj+Jh7eRLdthl4vCQ7u7uplgsNsVVty3ywsL5CK9PWjdEjWokIcE/Oo9S+rlDMdFQkc2cNIqmpqZBJ2Ii8H+0re5enG/iGZ6EOFtyf6k9IgQF/6gdx5AbIeWBoDZx0ihgXX7KSTc04RkeSCQSX6BgkptQc+pe5fEXp9iLJ+OTyPzH4/GvOx06AmGTU2z7XuC7HXbju+FzP8ZpMJfzBhSud3EcrGU1WDdBkVlGSTqJ5N7cFe9AKf2R02ncEPh+iPsUZ61yCX7bDk7bhp/Ffc56zQOI6kWaGMt5X6lLlUchADLLSHouJgJWyvEY8q6urjP4Aa5ztipkEfF6V4uBwRn3c92rFhSwITzzU+T48zXfqLmg8MWew+lfOPzcTv/7fLYNR6ItTwWHNaPFwGhxWcfgPX7MST/Zj0L8MfUocN4XaiootED+gNPbOHwtKbAYrhxzCITGgl/K5SoyRzEgTjsGVs5y7MklG6jhg0Lt2+fVRFA0hhtfglpxR/mSr6DEN1MIgrOOgFDISlUczgvhnnYbUOzr63sc9/s4Z32Hng0EfBHPxxd3w3dB8YSAISRrPenSlZXinRxe52wpHqCAuLZOEG6trNNXQFSNuPe/cY3hKb4KKi8m3HzNYyL4zB9w0jGpVOrPEFXJUACuv4GWnesp5LjPWvhPJcFnH6VAKGc9wTdB1VNMjOsAJ/Xo4/5XtOBIZBDbnzjrCrx/XVc2xucf8FJUvghKgJiIteSfcNoxNNsXp+JW3HEvpkvRMByc6tZNksdLUfkiKIjpWJ3FtAScT9eRZw4j/DqXW2Kss7PzNKddUW/rVIhXovJcUBRnws39jrN1BffhSUcxfCVaJWUp0ozqzsv9VurmP5WCRXWIs47wVFA8A9dut4WfeNY3hodN+6x84lUXxsDAQDPe09O+Oy9AgTmB39FxT4NngoJlasfNnKc4B1+SwAavhnKgeU9WybNF99PpNDUafO8KsQuFFGAUhmj2DV+yhWeCwk38BSc/u1MckUgkPLECsCb78B33eiVQFD4vhqv4Ar4rbS4wRI0rvmQZTwQF67QPynY1jskv8MN54kfhffaQ9Y3H4/v4kltEb4QEUW2hxhVnLeNaUNQ0R8n1NDjmMa4tFPkUENNSCAIP+mUnJbcQqk7wPuI3QUIhOtTf32/rPl0Jih4sPnRImN+0DPxwg24FgALzS04Sa1FyXYVEUJ3UvLvFCeRPwdez1chyJag7d+6Q+RfXUimiCfe5kdO24dbYMgGhEL3CSUfg/YwZ9w5RbaJQEGer4lhQZLbxYSc4Kxrcp2PRp1IpepjLWmN4v414yI67diBIIyxUHtzva1YbI44FBbNNY6/r3m1gBVRZji0CxLOHk8vAezqyUuSTwEIZtY0I3S9ay69ytiKOBEVOKj7EshkUgCNrAitEO4iWs27bnYx+hBCN3OYW973fipVyJCiYQMmtuhXAytA0LRKHXQqd8WXgPRtx/IqzlsGzExt/qgS+azOs1H7OlsW2oJK5kX7GbdKDH9KWH8Ut2IpWGFbaVqCT39P1sJp6gXuvGjKxJSg8vCa8aU2G8XoNfnxbfhRahrQqcDVfh6Y/WR5KOzk5uYlKOmeNg57HxMRExUJmS1DxePwlPJCajX/2EvgAdlt6Zau7QvC+lgOdKIyiRhc4AaJ6mZMlsSwosk44uYq/1BMUhH74UZZmBPPrtudylaECBmtmtdvJSIe8CFp/omzhtCyoWCy210IVIB1L/gssCS08YTm6jtdWLWjka8GaOQ6wSgLfdxsnV2BJUGTSrTw0A7A6caFk7KkCg5VKLUGzg+2IVDhlrbclQbFJ93XGaY2oaqFokCAsse2OW/xPxQIHMRnvPxVA1V7JoUpWq7yKjpgpUJXDvmBZUN1ZcsaLwf/tpJEXnC1FEPynr0ABKdl9VFVQyWRyA/7Z2NhJIVTl0MJenF0Bi83RhFR6b2rxcXYZLLQgWPhCnubzMqoKCiXPdjRYMvg+ZQUVj8epanfcP4lqby9F5ElAhQc+U+RWtW5A4Sk57rzi+lDUMkkkEneRFDf22Sl4ELQQWbkZv+2wNMYGHmsNCtA3i9eFr2ihUGJXDN0wHRIMjqUFKkocKiYbZDKZFY2XioKCAh05qEo4gD5WdLiXFRQPzTCuE1ipHRAU7YC6jLKCgvkXOYtFkQP8UetVHlomJZuFilKAkzFmiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoihI2Vq36P24Mys1fe9BKAAAAAElFTkSuQmCC";
            }
            }
            return ud;
        }

        public string SaveGrpFilterSetting(int GroupId, string SelectedValue)
        {
            string returnValue = "";

            List<GroupDetail> GetSearchGroupData = new List<GroupDetail>();
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    GetSearchGroupData = (from grpsett in context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL
                                 where grpsett.GroupID==GroupId
                                 select new GroupDetail
                                 {
                                     GroupID = grpsett.GroupID,
                                     GroupName=grpsett.GroupName,
                                     GroupIndex=grpsett.GroupIndex,
                                     GroupType=grpsett.GroupType,
                                     GroupFilter=grpsett.GroupFilter,
                                     GroupFilterValue=grpsett.GroupFilterValue,
                                     GroupView=grpsett.GroupView,
                                     DashBoardID=grpsett.DashBoardID    
                                 }).ToList<GroupDetail>();

                    //vivek Updating DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL table
                    if (GetSearchGroupData!= null)
                    {
                        foreach (var groupfilter in GetSearchGroupData)
                        {
                            DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL groupfiltersetting = context.DBOARD_COMPUTED_SYSTEM_GROUP_DETAIL.SingleOrDefault(x => x.GroupID == groupfilter.GroupID);
                            groupfiltersetting.GroupType = groupfilter.GroupType;
                            groupfiltersetting.GroupName = groupfilter.GroupName;
                            groupfiltersetting.GroupView = groupfilter.GroupView;
                            groupfiltersetting.DashBoardID = groupfilter.DashBoardID;
                            groupfiltersetting.IS_ACTIVE = "Y";
                            groupfiltersetting.GroupIndex = groupfilter.GroupIndex;
                            groupfiltersetting.GroupFilter = groupfilter.GroupFilter;
                            groupfiltersetting.GroupFilterValue = SelectedValue;
                            context.SaveChanges();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }
            returnValue = "Saved Successfully";
            return returnValue;
        }

        public string IsLoginForm()
        {
            var IsLogin = "";
            using(SeeITEntities context= new SeeITEntities())
            {
                IsLogin = (from x in context.DBOARD_COMPUTED_ADMIN_APPLICATION_SETTING
                           where x.Name == "app_login"
                           select x.Value).FirstOrDefault();
            }

            return IsLogin;
        }

        public string RedirectURL()
        {
            var RedirectUrl = "";
            using (SeeITEntities context = new SeeITEntities())
            {
                RedirectUrl = (from x in context.DBOARD_COMPUTED_ADMIN_APPLICATION_SETTING
                           where x.Name == "app_url"
                           select x.Value).FirstOrDefault();
            }

            return RedirectUrl;
        }

        public UserDetails GetCookiesUserID()
        {
            UserDetails ud = new UserDetails();
            string UserID;
            try
            {
                   var IsLogin = IsLoginForm();
                    UserID = helper.GetUserID(IsLogin);
                    if (IsLogin=="1")
                    {
                        if (UserID != "")
                        {
                            ud = helper.GetUserDetailsWithID(UserID);
                            ud.IsAuth = true;
                        }
                        else
                        {
                            ud.IsAuth = false;
                        }
                    }
                    else
                    {
                        if (UserID != "")
                        {
                            ud = helper.GetUserDetailsWithID(UserID);
                            ud.IsAuth = true;
                        }
                        else
                        {
                            ud.IsAuth = false;
                        }

                    }
                   
                
            }
            catch (Exception ex)
            {
                ud.UserID = "277070";
                ud.UserName = "Vijay Nair";
                ud.UserEmailId = "";
                ud.Location = "Chennai";
                ud.UserImage = "iVBORw0KGgoAAAANSUhEUgAAAJQAAACdCAYAAABSM1EcAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAA6SSURBVHhe7Z1/aJT3HcfNXe4uSBhZEmaTCOkwl4TMghtuS5lDxzomqFRxZf2jQ8ssWNatSoVNOnFDxI3+YQstCpPNUSGFtWhRWcv6x2RCCy1UqAQT42agxkBMNOgfyeUut/cn97lyudyP5+fd5/s8nxc8PN/vk8vdc8+9v5/v5/v5/lqlKIqiKIqiKIqiKIqiKIqiKIqiKIpIGvgcevr6+jYuLi72IElHH461DQ0NLbjWQn8vYA7XJ3G+h2MM6Vt4zfWFhYVr4+Pjc0uvCDGhFdTmzZsbJyYmtkAQTyO7E8fapT84BKJ6hPe6gOTlWCx2aXh4+FHuL+EiVILq7u5uSiQSW7PZ7G4cWyORSDv/yWvIUn2E430cF0ZHR8mahYJQCApCaonH4wcgolcgoma+XBNgudL4zHOwXn8cGRm5zZcDS5TPgYSqNVik30Sj0SH8oNtxxPlPNQOfGcFpA479bW1tLc3NzZ/Ozs4G1tcKrIXq7e19DqejOMjJlsQDWMrjqVTqzSA68YETVE9PzyCqmFNIklUQC6rC27jPg/CvyJEPDIGq8sgqoYp5D0lXLbZagPukcMSzra2tDTMzM//OXTWfwFgoiOkYTr/P5cwC1uoCGg2/CEKowXhBDQwMNMMfeRvVB8WSTOYarNYu01uCRgsK/tJaCOkikqL9JavAWZ+EqJ6BX3WVLxmHsYIiMeHhf4rjMb4UCDhutQOi+oAvGYWRgqJqLp1O/wfJQFimEjzA8SREdSOXNQcKuhkH+Uw4BVVMBHVKn6cIP+eNwThBUWsuAA54VfAd+9HyG6JoP18yAqPiUBz9fj2XCz7wD3sePnz4tZmZmQ/5kniMERRFwPGAKWhpVIl1C77zYGtr6zhEdY0vicYIp5zHLn2Oh7ueL4UNctKTJgyDMcKHgpioSyWsYiJastnsq5wWjXgLxYPibiIpvn/OZ+YymcwTt27dGuO8SMRbKLR0XsIp7GIimqLRqHgrJdpCURwG1ul/SBoXj/ELVH3fvnnzplgHXbSFgnX6LU4qpuWc4LNIxFoo7l6ZQrIpd0XJs7i4+OTY2NgnnBWFWAsFMVE0XMVUArR4aeqXSMQKCr6C2IdWbyCon3FSHCKrPAoVxGKxqUiNpzyZBJ7NEzdu3LjOWTGItFA0GVPFVJlMJiOyg1ykoOB0buOkUgZUe7s5KQpxguLhGoEfnuIBG2jUKqfFIE5Qd+/epXl1fq05EChgpcQVPHGCQnUX5k5gW0BQtOyQKMQJCtZpHSeVKmSzWWnT7EVaKHEPSSqwUOKsuThBgX4+K9VRp9wC2t1iA+rz5KQIJPpQoRoz7pZMJiOqRSzRQulgOoORKCjFYCQKimZ4KIYiTlCLi4sqKBtEo1FRyypqlWc4w8PDtAi/GMQJqqGhIfBLL3uIOGsuUVDGLWFTL+AeiJujJ05Q2Wz2FieVKkgsfBItlOiZscIY4bMYJDrlV2HK05xWKoDCJ245anGCohVGIpFIYNbt9gsUunsSF3eVaKHIj6JdnJTKiNyBQaqgArVdhR/AitPia+IQKaixsbEvcRI51VoCqO4ezc/Pi3QLRAqKgJW6zEmlCDjjF6TuZCVWUCiF73BSKQKCEruIq1hB0UptsFLa2lvJl6ju3uW0OMQKijnIZ4VBITsieeNG0YLildq06mMgpuudnZ3nOCsS6RaKxkwf0ch5DvhOR65cuSL6WYhf+P7+/fsz7e3t30Dye7kr4YT8SVjsw5wVi3gLRTQ2Nh6n2AtnQwlZJ06KxoitOaamph7BSmWRfCp3JVzAOp2DdXqDs6IxZq+X6enpq21tbTSrOFSLaZAjHovFdqFQpfiSaIyo8vKg6nsBVd9nnA08ENNkJBLZYdLm1kZsHlTIwMDAYwsLC7SRUKC2hi2GWrbRaPQnIyMjRgV3jbJQBM3yoFIbdCcd3/FF08REGCcoAg/6M1ioFzgbRN4cHR09w2mjMFJQBFo978DHeD6AQc/THR0dxnY5GedDFdPb27sJwvqH6T4VFQyq5ky1THmMFxRBq+Hix7iIpJE7pVNrDgXiGYljxO1ibJVXCI3wbGxs/CGSxnUkUxgEgvpuEMREGLUreiUo8Dc9Pf1ea2trA36gTSjxJhSWs7FY7OdoZIjfS9gqgajyilm3bl1PNBo9huSzuSuygFW6iir6cFCsUiGBFFSeZDJJPtVJWKstuSv1BUK6wUIK7KyeQAsqD4T1FET1GpJ1cdrZ6T7S0dFxVvp4JreEQlB50BrcDguxG5aCzn4vdkrDdD/C8f78/Pw5ycN2vSRUgspDGxRNTExQNbgNloM2M/RkoVgI9RHej6qzy3C2L5nUqesVoRRUMX19fRshhq0Qw7eQ7Ue6Bxas4vrfeA1VXbRSDPlFNEPnw1QqdTUslqgcKqgy0KiGTCbTlE6nG8GSBYPg7kE4S1YHTX1daU9RFEVRwktgul6CAvluq1evTs/OzhoZr1JBCaOlpeUUWo1/bWtr+057e3tizZo1/zVlggIRmFYe9d+hNZY2vfWVTCbvojVZOLZrDi3LSzj+3tXV9YH0SLuRgurp6RnEQ9+Ig/bcpRjSeo4bnR0dHX1+6UUG0t/fvx7f5QvOrgB/u43v/FYqlTozPj4ucgsTYwTFIzN344FWimyPQVBJThsHrNMhfD/qc6wGWa3TENhbtOwRXxOBaEHxaIE9VUS0DFR7HdL2P7EKvu8/8V23crYqFK2HZT6XyWSOSxGWSEGRkFACj+Jh7eRLdthl4vCQ7u7uplgsNsVVty3ywsL5CK9PWjdEjWokIcE/Oo9S+rlDMdFQkc2cNIqmpqZBJ2Ii8H+0re5enG/iGZ6EOFtyf6k9IgQF/6gdx5AbIeWBoDZx0ihgXX7KSTc04RkeSCQSX6BgkptQc+pe5fEXp9iLJ+OTyPzH4/GvOx06AmGTU2z7XuC7HXbju+FzP8ZpMJfzBhSud3EcrGU1WDdBkVlGSTqJ5N7cFe9AKf2R02ncEPh+iPsUZ61yCX7bDk7bhp/Ffc56zQOI6kWaGMt5X6lLlUchADLLSHouJgJWyvEY8q6urjP4Aa5ztipkEfF6V4uBwRn3c92rFhSwITzzU+T48zXfqLmg8MWew+lfOPzcTv/7fLYNR6ItTwWHNaPFwGhxWcfgPX7MST/Zj0L8MfUocN4XaiootED+gNPbOHwtKbAYrhxzCITGgl/K5SoyRzEgTjsGVs5y7MklG6jhg0Lt2+fVRFA0hhtfglpxR/mSr6DEN1MIgrOOgFDISlUczgvhnnYbUOzr63sc9/s4Z32Hng0EfBHPxxd3w3dB8YSAISRrPenSlZXinRxe52wpHqCAuLZOEG6trNNXQFSNuPe/cY3hKb4KKi8m3HzNYyL4zB9w0jGpVOrPEFXJUACuv4GWnesp5LjPWvhPJcFnH6VAKGc9wTdB1VNMjOsAJ/Xo4/5XtOBIZBDbnzjrCrx/XVc2xucf8FJUvghKgJiIteSfcNoxNNsXp+JW3HEvpkvRMByc6tZNksdLUfkiKIjpWJ3FtAScT9eRZw4j/DqXW2Kss7PzNKddUW/rVIhXovJcUBRnws39jrN1BffhSUcxfCVaJWUp0ozqzsv9VurmP5WCRXWIs47wVFA8A9dut4WfeNY3hodN+6x84lUXxsDAQDPe09O+Oy9AgTmB39FxT4NngoJlasfNnKc4B1+SwAavhnKgeU9WybNF99PpNDUafO8KsQuFFGAUhmj2DV+yhWeCwk38BSc/u1MckUgkPLECsCb78B33eiVQFD4vhqv4Ar4rbS4wRI0rvmQZTwQF67QPynY1jskv8MN54kfhffaQ9Y3H4/v4kltEb4QEUW2hxhVnLeNaUNQ0R8n1NDjmMa4tFPkUENNSCAIP+mUnJbcQqk7wPuI3QUIhOtTf32/rPl0Jih4sPnRImN+0DPxwg24FgALzS04Sa1FyXYVEUJ3UvLvFCeRPwdez1chyJag7d+6Q+RfXUimiCfe5kdO24dbYMgGhEL3CSUfg/YwZ9w5RbaJQEGer4lhQZLbxYSc4Kxrcp2PRp1IpepjLWmN4v414yI67diBIIyxUHtzva1YbI44FBbNNY6/r3m1gBVRZji0CxLOHk8vAezqyUuSTwEIZtY0I3S9ay69ytiKOBEVOKj7EshkUgCNrAitEO4iWs27bnYx+hBCN3OYW973fipVyJCiYQMmtuhXAytA0LRKHXQqd8WXgPRtx/IqzlsGzExt/qgS+azOs1H7OlsW2oJK5kX7GbdKDH9KWH8Ut2IpWGFbaVqCT39P1sJp6gXuvGjKxJSg8vCa8aU2G8XoNfnxbfhRahrQqcDVfh6Y/WR5KOzk5uYlKOmeNg57HxMRExUJmS1DxePwlPJCajX/2EvgAdlt6Zau7QvC+lgOdKIyiRhc4AaJ6mZMlsSwosk44uYq/1BMUhH74UZZmBPPrtudylaECBmtmtdvJSIe8CFp/omzhtCyoWCy210IVIB1L/gssCS08YTm6jtdWLWjka8GaOQ6wSgLfdxsnV2BJUGTSrTw0A7A6caFk7KkCg5VKLUGzg+2IVDhlrbclQbFJ93XGaY2oaqFokCAsse2OW/xPxQIHMRnvPxVA1V7JoUpWq7yKjpgpUJXDvmBZUN1ZcsaLwf/tpJEXnC1FEPynr0ABKdl9VFVQyWRyA/7Z2NhJIVTl0MJenF0Bi83RhFR6b2rxcXYZLLQgWPhCnubzMqoKCiXPdjRYMvg+ZQUVj8epanfcP4lqby9F5ElAhQc+U+RWtW5A4Sk57rzi+lDUMkkkEneRFDf22Sl4ELQQWbkZv+2wNMYGHmsNCtA3i9eFr2ihUGJXDN0wHRIMjqUFKkocKiYbZDKZFY2XioKCAh05qEo4gD5WdLiXFRQPzTCuE1ipHRAU7YC6jLKCgvkXOYtFkQP8UetVHlomJZuFilKAkzFmiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoihI2Vq36P24Mys1fe9BKAAAAAElFTkSuQmCC";
            }
            return ud;
        }

    }
}
