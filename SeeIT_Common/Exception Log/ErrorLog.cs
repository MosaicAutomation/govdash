﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SeeIT_Common.Exception_Log
{
    public class ErrorLog
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            XmlConfigurator.Configure(); //only once

            Log.Debug("Application is starting");
            Console.WriteLine("Test Line");

            var errorClass = new ErrorEntry();
            errorClass.LogSomething();

            Log.Debug("Application is ending");
            Console.Read();
        }

    }

    public class ErrorEntry
    {

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void LogSomething()
        {
            for (int i = 0; i < 100; i++)
            {
                Log.InfoFormat("CurrentTime is [{0}]", DateTime.Now.ToString("yyyy.MM.dd-hh.mm.ss~fff"));
            }

        }

        public void LogError(Exception ex)
        {
            Log.InfoFormat("CurrentTime is [{0}]", DateTime.Now.ToString("yyyy.MM.dd-hh.mm.ss~fff"));
            Log.ErrorFormat(ex.Message);
        }

        public void LogErrorMessage(string exMessage)
        {
            Log.InfoFormat("CurrentTime is [{0}]", DateTime.Now.ToString("yyyy.MM.dd-hh.mm.ss~fff"));
            Log.ErrorFormat(exMessage);
        }

    }
}
