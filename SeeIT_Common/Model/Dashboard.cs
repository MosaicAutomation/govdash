﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeIT_Common.Model
{
    public class UserDetail
    {
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string UserEmailId { get; set; }
        public string UserImage { get; set; }
    }

    public interface ICommon
    {
        string Request_Priority { get; set; }
    }

    public class Dashboard
    {
        public virtual string DashBoardIID { get; set; }
        public virtual string DashBoardName { get; set; }
        public virtual string DashboardIcon { get; set; }
        public virtual int? PrivacySetting { get; set; }
     

    }

    public class BaseKPI : Dashboard
    {
        public virtual int ID { get; set; }
        public virtual string BaseKPIID { get; set; }
        public virtual string BaseKPIName { get; set; }
        public virtual string KPIID { get; set; }
        public virtual string KPIName { get; set; }
        public virtual List<Category> Category { get; set; }
    }

    public class KPIAvgResponseResolution : UserKPIDashboard
    {
        public virtual List<KPIPriorityCount> SumTotal { get; set; }
        public virtual List<KPIPriorityCount> SumExceeding { get; set; }
        public virtual List<KPIPriorityCount> SumExclExceeding { get; set; }
        public virtual List<KPIPriorityMonthCount> MonthTotal { get; set; }
        public virtual List<KPIPriorityMonthCount> MonthExceeding { get; set; }
        public virtual List<KPIPriorityMonthCount> MonthExclExceeding { get; set; }
    }

    public class KPIGeneric : UserKPIDashboard
    {
        public virtual string GenericValue { get; set; }
        public virtual string GenericValueIcon { get; set; }
        public virtual string ScoreBoardType { get; set; }
        public virtual string IsPeriodic { get; set; }
        public virtual int? PeriodicTime { get; set; }
        public virtual string PeriodicTimeUnit { get; set; }
    }
    public class Category
    {
        public virtual string name { get; set; }
        public virtual bool ticked { get; set; }
    }
    public class KPIPriorityCount : ICommon
    {
        public virtual string Request_Priority { get; set; }
        public virtual double PriorityAvg { get; set; }
        public virtual double PriorityCount { get; set; }
        public virtual string Colour { get; set; }
        public virtual double SLI { get; set; }
        public virtual string SLIUnit { get; set; }
    }

    public class KPIPriorityMonthCount : KPIPriorityCount
    {
        public virtual string PriorityMonthDate { get; set; }
    }

    public class KPIIncomingVSCompleted : UserKPIDashboard
    {
        public virtual List<IncomingVSCompletedPriorityCount> IncomingVSCompleted { get; set; }
    }
    public class KPIReOpenCount : UserKPIDashboard
    {
        public virtual List<ReOpen_Count> IncomingVSCompleted { get; set; }
    }
    public class KPIHeatMapDay : UserKPIDashboard
    {
        public virtual List<HeatMapDay> HeatMapDay { get; set; }
    }
    public class KPIHeatMapHour : UserKPIDashboard
    {
        public virtual List<HeatMapHour> HeatMapDay { get; set; }
    }

    public class KPIExceeding : UserKPIDashboard
    {
        public virtual List<ExceedingCount> IncomingVSCompleted { get; set; }
    }

    public class KPITimeBookRequest : UserKPIDashboard
    {
        public virtual List<TimeBookRequestCount> TimeBook { get; set; }
    }

    public class KPIPareto : UserKPIDashboard
    {
        public virtual List<ParetoCount> Pareto { get; set; }
        public virtual string FirstSpinner { get; set; }
        public virtual string SecondSpinner { get; set; }
    }

    public class IncomingVSCompletedPriorityCount : ICommon
    {
        public virtual string Request_Priority { get; set; }
        public virtual double Incoming { get; set; }
        public virtual double Backlog { get; set; }
        public virtual double Completed { get; set; }
        public virtual string PriorityMonthDate { get; set; }
    }
    public class ReOpen_Count : ICommon
    {
        public virtual string Request_Priority { get; set; }
        public virtual double ReOpenCount { get; set; }
        public virtual string Pareto_Cat { get; set; }

    }
    public class HeatMapDay 
    {
        public virtual string Request_Priority { get; set; }
        public virtual int? date { get; set; }
        public virtual int? value { get; set; }
        public virtual string Category { get; set; }
    }
    public class HeatMapHour
    {
        public virtual string Request_Priority { get; set; }
        public virtual int? day { get; set; }
        public virtual int? hour { get; set; }
        public virtual int? value { get; set; }
        public virtual string Category { get; set; }
    }
    public class TimeBookRequestCount
    {
        public virtual int RequestCount { get; set; }
        public virtual double TimeBook { get; set; }
        public virtual string PriorityMonthDate { get; set; }
    }

    public class ExceedingCount : ICommon
    {
        public virtual string Request_Priority { get; set; }
        public virtual int Exceeding { get; set; }
        public virtual int NonExceeding { get; set; }
        public virtual double ExceedingPerc { get; set; }
        public virtual string PriorityMonthDate { get; set; }
    }

    public class ParetoCount
    {
        public virtual string BarName { get; set; }
        public virtual double BarValue { get; set; }
        public virtual double LineValue { get; set; }
        public virtual string Request_Priority { get; set; }
    }
    public class FieldList 
    {
        public virtual List<Fields> FieldListVal { get; set; }
        public virtual List<Fields> RemedyFieldListVal { get; set; }
    }
    public class Fields
    {
        public virtual string Column { get; set; }
        public virtual string name { get; set; }
        public virtual string fieldType { get; set; }
    }

    public class RequestInfo
    {
        public virtual string RequestID { get; set; }
        public virtual string RequestType { get; set; }
        public virtual string CurrentPriority { get; set; }
        public virtual string CurrentStatus { get; set; }
        public virtual string Summary { get; set; }
        public virtual string AssignedGroup { get; set; }
        public virtual string Assignee { get; set; }
        public virtual string Request_Priority { get; set; }
        public virtual string Request_Status { get; set; }
        public virtual string StatusState { get; set; }
        public virtual string TimeIn { get; set; }
        public virtual string TimeSpent { get; set; }
        public virtual string StartDate { get; set; }
        public virtual string EventType { get; set; }
        public virtual string Identifier { get; set; }

    }

    public class Search
    {
        public virtual string SearchID { get; set; }
        public virtual string SearchValue { get; set; }
    }

    public class DeleteDash
    {
        public virtual string DelDashboardId { get; set; }
        public virtual string DelKPIID { get; set; }
    }

    public class PriorityMonth : ICommon
    {
        public virtual string Request_Priority { get; set; }
        public virtual DateTime? MonthDate { get; set; }
    }

    public class AdminList
    {
        public List<Admin> Admin { get; set; }
    }

    public class Widget
    {
        public virtual string BaseKPIID { get; set; }
        public virtual string BaseKPIName { get; set; }
        public virtual string BasKPIDesc { get; set; }
        public virtual string ticked { get; set; }
        public virtual int? IdNum { get; set; }
    }
    public class Admin
    {
        public virtual string ColID { get; set; }
        public virtual string IsActive { get; set; }
        public virtual int? IsInFilter { get; set; }
        public virtual int? IsInScoreCard { get; set; }
        public virtual int? IsInRequestMaster { get; set; }
        public virtual int? IsInPivot { get; set; }
        public virtual string DispColName { get; set; }
        public virtual string ColName { get; set; }
        public virtual string KPIID { get; set; }
        public virtual string SortOrder { get; set; }
        public virtual bool ticked { get; set; }
    }

    public class KPIAgingReport : UserKPIDashboard
    {
        public virtual List<AgingReport> AgingReports { get; set; }
    }

    public class AgingReport : ICommon
    {
        public virtual string SLIAgeQuery { get; set; }
        public virtual List<CommonData> SLIAgeSelect { get; set; }
        public virtual string Request_Priority { get; set; }
        public virtual double ExceedSLA { get; set; }
        public virtual double NotExceedSLA { get; set; }
        public virtual double ClosingIntoSLA { get; set; }
        public virtual Int32? SLI { get; set; }
        public virtual string SLIUnit { get; set; }
    }

    public class Grpdetail
    {
        public virtual string KPIID { get; set; }
        public virtual string NewKPIID { get; set; }
        public virtual Int32? GroupID { get; set; }
        public virtual Int32? OldGroupID { get; set; }
    }

    public class SaveBaseKPIColumnMapping
    {
        public virtual string name { get; set; }
        public virtual string YesArray { get; set; }
        public virtual string NoArray { get; set; }
    }
    public class MasterDashColMapData
    {
        public virtual string MasterColMapData { get; set; }   
    }

}
