﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using SeeIT_Common;
using System.Data;
using System.Security.Principal;
using System.Web;
using SeeIT_Common.Model;
using System.DirectoryServices;
using System.IO;
using SeeIT_Common.Constant;
using System.Security.Cryptography;

namespace SeeIT_Common.Helper
{
    public class HelperFunctions
    {
        public DataSet GetResultReport(string Query)
        {
            DataSet retVal = new DataSet();
            string s = Constant.Constants.ConnectionString;
            System.Data.EntityClient.EntityConnectionStringBuilder e = new System.Data.EntityClient.EntityConnectionStringBuilder(s);
            string ProviderConnectionString = e.ProviderConnectionString;
            SqlConnection con = new SqlConnection(ProviderConnectionString);
            SqlCommand cmdReport = new SqlCommand(Query, con);
            SqlDataAdapter daReport = new SqlDataAdapter(cmdReport);
            using (cmdReport)
            {
                daReport.Fill(retVal);
            }
            return retVal;
        }

        public string GetCurrentUserId() {
            string UserID = HttpContext.Current.Request.LogonUserIdentity.Name;

           string[] userdata = UserID.Split('\\');
            if (userdata.Length > 1)
            {
                UserID = userdata[1];
            }
            else
            {
                UserID = userdata[0];
            }
            return UserID;
        }

        public string escapeQuote(string data)
        {
            data = data.Trim();
            data = data.Replace("\'", "&#39;");
            data = data.Replace("\"", "&#34;");
            data = data.Replace("\\", "&#92;");
            data = data.Replace("/", "&#47;");
            data = data.Replace(":", "&#58;");
            data = data.Replace(",", "&#44;");
            data = data.Replace("\\n", "");
            data = data.Replace('\n', ' ');
            data = data.Replace('\r', ' ');
            data = data.Replace('\t', ' ');
            data = data.Replace(Environment.NewLine, " ");
            Regex.Replace(data, @"\t|\n|\r", "");
            return data;
        }

        public System.Collections.IEnumerable Descend(IEnumerable<DataRow> data, int currentLevel, int maxLevel)
        {
            if (currentLevel > maxLevel)
            {
                return Enumerable.Empty<object>();
            }
            return from item in data
                   group item by new
                   {
                       name = item.Field<string>("Key_L" + currentLevel),
                   } into rowGroup
                   select new
                   {
                       name = rowGroup.Key.name,
                       value = rowGroup.Sum(i => i.Field<int>("Count")),
                       value2 = rowGroup.Sum(i => i.Field<int>("Support")),
                       children = (currentLevel > maxLevel) ? null : Descend(rowGroup, currentLevel + 1, maxLevel)
                   };
        }

        public UserDetails GetUserDetailsWithID(string UserID)
        {
            UserDetails ud = new UserDetails();
            DirectoryEntry objDir = null;
            DirectorySearcher deSearch;
            try
            {
                string ConnectionString = "GC://dc=lntinfotech,dc=com";
                objDir = new DirectoryEntry(ConnectionString);
                deSearch = new DirectorySearcher();
                deSearch.SearchRoot = objDir;
                deSearch.Filter = "(&(objectCategory=person)(objectClass=user) (SAMAccountName=" + UserID + "))";
                SearchResult mySearchResult = deSearch.FindOne();  // this is to get only 1 entry
                if (mySearchResult != null)
                {
                    DirectoryEntry myDirectoryEntry = mySearchResult.GetDirectoryEntry();
                    ud.UserID = UserID;
                    ud.UserName = mySearchResult.Properties["CN"][0].ToString();
                    ud.UserEmailId = mySearchResult.Properties["Mail"][0].ToString();
                    ud.Location = (mySearchResult.Properties["Extensionattribute11"].Count != 0) ? mySearchResult.Properties["Extensionattribute11"][0].ToString() : "";
                    ud.Country = (mySearchResult.Properties["c"].Count != 0) ? mySearchResult.Properties["c"][0].ToString() : "";
                    PropertyValueCollection collection = myDirectoryEntry.Properties["thumbnailPhoto"];
                    if (collection.Value != null && collection.Value is byte[])
                    {
                        byte[] thumbnailInBytes = (byte[])collection.Value;
                        MemoryStream s = new MemoryStream(thumbnailInBytes);
                        ud.UserImage = Convert.ToBase64String(thumbnailInBytes);
                    }
                    else
                    {
                        ud.UserImage = Constants.UserThumbNailError;
                    }
                }
            }
            catch (Exception e) 
            {
                ud.UserID = "277070";
                ud.UserName = "Vijay Nair";
                ud.UserEmailId = "";
                ud.Location = "Chennai";
                ud.UserImage = "iVBORw0KGgoAAAANSUhEUgAAAJQAAACdCAYAAABSM1EcAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAA6SSURBVHhe7Z1/aJT3HcfNXe4uSBhZEmaTCOkwl4TMghtuS5lDxzomqFRxZf2jQ8ssWNatSoVNOnFDxI3+YQstCpPNUSGFtWhRWcv6x2RCCy1UqAQT42agxkBMNOgfyeUut/cn97lyudyP5+fd5/s8nxc8PN/vk8vdc8+9v5/v5/v5/lqlKIqiKIqiKIqiKIqiKIqiKIqiKIpIGvgcevr6+jYuLi72IElHH461DQ0NLbjWQn8vYA7XJ3G+h2MM6Vt4zfWFhYVr4+Pjc0uvCDGhFdTmzZsbJyYmtkAQTyO7E8fapT84BKJ6hPe6gOTlWCx2aXh4+FHuL+EiVILq7u5uSiQSW7PZ7G4cWyORSDv/yWvIUn2E430cF0ZHR8mahYJQCApCaonH4wcgolcgoma+XBNgudL4zHOwXn8cGRm5zZcDS5TPgYSqNVik30Sj0SH8oNtxxPlPNQOfGcFpA479bW1tLc3NzZ/Ozs4G1tcKrIXq7e19DqejOMjJlsQDWMrjqVTqzSA68YETVE9PzyCqmFNIklUQC6rC27jPg/CvyJEPDIGq8sgqoYp5D0lXLbZagPukcMSzra2tDTMzM//OXTWfwFgoiOkYTr/P5cwC1uoCGg2/CEKowXhBDQwMNMMfeRvVB8WSTOYarNYu01uCRgsK/tJaCOkikqL9JavAWZ+EqJ6BX3WVLxmHsYIiMeHhf4rjMb4UCDhutQOi+oAvGYWRgqJqLp1O/wfJQFimEjzA8SREdSOXNQcKuhkH+Uw4BVVMBHVKn6cIP+eNwThBUWsuAA54VfAd+9HyG6JoP18yAqPiUBz9fj2XCz7wD3sePnz4tZmZmQ/5kniMERRFwPGAKWhpVIl1C77zYGtr6zhEdY0vicYIp5zHLn2Oh7ueL4UNctKTJgyDMcKHgpioSyWsYiJastnsq5wWjXgLxYPibiIpvn/OZ+YymcwTt27dGuO8SMRbKLR0XsIp7GIimqLRqHgrJdpCURwG1ul/SBoXj/ELVH3fvnnzplgHXbSFgnX6LU4qpuWc4LNIxFoo7l6ZQrIpd0XJs7i4+OTY2NgnnBWFWAsFMVE0XMVUArR4aeqXSMQKCr6C2IdWbyCon3FSHCKrPAoVxGKxqUiNpzyZBJ7NEzdu3LjOWTGItFA0GVPFVJlMJiOyg1ykoOB0buOkUgZUe7s5KQpxguLhGoEfnuIBG2jUKqfFIE5Qd+/epXl1fq05EChgpcQVPHGCQnUX5k5gW0BQtOyQKMQJCtZpHSeVKmSzWWnT7EVaKHEPSSqwUOKsuThBgX4+K9VRp9wC2t1iA+rz5KQIJPpQoRoz7pZMJiOqRSzRQulgOoORKCjFYCQKimZ4KIYiTlCLi4sqKBtEo1FRyypqlWc4w8PDtAi/GMQJqqGhIfBLL3uIOGsuUVDGLWFTL+AeiJujJ05Q2Wz2FieVKkgsfBItlOiZscIY4bMYJDrlV2HK05xWKoDCJ245anGCohVGIpFIYNbt9gsUunsSF3eVaKHIj6JdnJTKiNyBQaqgArVdhR/AitPia+IQKaixsbEvcRI51VoCqO4ezc/Pi3QLRAqKgJW6zEmlCDjjF6TuZCVWUCiF73BSKQKCEruIq1hB0UptsFLa2lvJl6ju3uW0OMQKijnIZ4VBITsieeNG0YLildq06mMgpuudnZ3nOCsS6RaKxkwf0ch5DvhOR65cuSL6WYhf+P7+/fsz7e3t30Dye7kr4YT8SVjsw5wVi3gLRTQ2Nh6n2AtnQwlZJ06KxoitOaamph7BSmWRfCp3JVzAOp2DdXqDs6IxZq+X6enpq21tbTSrOFSLaZAjHovFdqFQpfiSaIyo8vKg6nsBVd9nnA08ENNkJBLZYdLm1kZsHlTIwMDAYwsLC7SRUKC2hi2GWrbRaPQnIyMjRgV3jbJQBM3yoFIbdCcd3/FF08REGCcoAg/6M1ioFzgbRN4cHR09w2mjMFJQBFo978DHeD6AQc/THR0dxnY5GedDFdPb27sJwvqH6T4VFQyq5ky1THmMFxRBq+Hix7iIpJE7pVNrDgXiGYljxO1ibJVXCI3wbGxs/CGSxnUkUxgEgvpuEMREGLUreiUo8Dc9Pf1ea2trA36gTSjxJhSWs7FY7OdoZIjfS9gqgajyilm3bl1PNBo9huSzuSuygFW6iir6cFCsUiGBFFSeZDJJPtVJWKstuSv1BUK6wUIK7KyeQAsqD4T1FET1GpJ1cdrZ6T7S0dFxVvp4JreEQlB50BrcDguxG5aCzn4vdkrDdD/C8f78/Pw5ycN2vSRUgspDGxRNTExQNbgNloM2M/RkoVgI9RHej6qzy3C2L5nUqesVoRRUMX19fRshhq0Qw7eQ7Ue6Bxas4vrfeA1VXbRSDPlFNEPnw1QqdTUslqgcKqgy0KiGTCbTlE6nG8GSBYPg7kE4S1YHTX1daU9RFEVRwktgul6CAvluq1evTs/OzhoZr1JBCaOlpeUUWo1/bWtr+057e3tizZo1/zVlggIRmFYe9d+hNZY2vfWVTCbvojVZOLZrDi3LSzj+3tXV9YH0SLuRgurp6RnEQ9+Ig/bcpRjSeo4bnR0dHX1+6UUG0t/fvx7f5QvOrgB/u43v/FYqlTozPj4ucgsTYwTFIzN344FWimyPQVBJThsHrNMhfD/qc6wGWa3TENhbtOwRXxOBaEHxaIE9VUS0DFR7HdL2P7EKvu8/8V23crYqFK2HZT6XyWSOSxGWSEGRkFACj+Jh7eRLdthl4vCQ7u7uplgsNsVVty3ywsL5CK9PWjdEjWokIcE/Oo9S+rlDMdFQkc2cNIqmpqZBJ2Ii8H+0re5enG/iGZ6EOFtyf6k9IgQF/6gdx5AbIeWBoDZx0ihgXX7KSTc04RkeSCQSX6BgkptQc+pe5fEXp9iLJ+OTyPzH4/GvOx06AmGTU2z7XuC7HXbju+FzP8ZpMJfzBhSud3EcrGU1WDdBkVlGSTqJ5N7cFe9AKf2R02ncEPh+iPsUZ61yCX7bDk7bhp/Ffc56zQOI6kWaGMt5X6lLlUchADLLSHouJgJWyvEY8q6urjP4Aa5ztipkEfF6V4uBwRn3c92rFhSwITzzU+T48zXfqLmg8MWew+lfOPzcTv/7fLYNR6ItTwWHNaPFwGhxWcfgPX7MST/Zj0L8MfUocN4XaiootED+gNPbOHwtKbAYrhxzCITGgl/K5SoyRzEgTjsGVs5y7MklG6jhg0Lt2+fVRFA0hhtfglpxR/mSr6DEN1MIgrOOgFDISlUczgvhnnYbUOzr63sc9/s4Z32Hng0EfBHPxxd3w3dB8YSAISRrPenSlZXinRxe52wpHqCAuLZOEG6trNNXQFSNuPe/cY3hKb4KKi8m3HzNYyL4zB9w0jGpVOrPEFXJUACuv4GWnesp5LjPWvhPJcFnH6VAKGc9wTdB1VNMjOsAJ/Xo4/5XtOBIZBDbnzjrCrx/XVc2xucf8FJUvghKgJiIteSfcNoxNNsXp+JW3HEvpkvRMByc6tZNksdLUfkiKIjpWJ3FtAScT9eRZw4j/DqXW2Kss7PzNKddUW/rVIhXovJcUBRnws39jrN1BffhSUcxfCVaJWUp0ozqzsv9VurmP5WCRXWIs47wVFA8A9dut4WfeNY3hodN+6x84lUXxsDAQDPe09O+Oy9AgTmB39FxT4NngoJlasfNnKc4B1+SwAavhnKgeU9WybNF99PpNDUafO8KsQuFFGAUhmj2DV+yhWeCwk38BSc/u1MckUgkPLECsCb78B33eiVQFD4vhqv4Ar4rbS4wRI0rvmQZTwQF67QPynY1jskv8MN54kfhffaQ9Y3H4/v4kltEb4QEUW2hxhVnLeNaUNQ0R8n1NDjmMa4tFPkUENNSCAIP+mUnJbcQqk7wPuI3QUIhOtTf32/rPl0Jih4sPnRImN+0DPxwg24FgALzS04Sa1FyXYVEUJ3UvLvFCeRPwdez1chyJag7d+6Q+RfXUimiCfe5kdO24dbYMgGhEL3CSUfg/YwZ9w5RbaJQEGer4lhQZLbxYSc4Kxrcp2PRp1IpepjLWmN4v414yI67diBIIyxUHtzva1YbI44FBbNNY6/r3m1gBVRZji0CxLOHk8vAezqyUuSTwEIZtY0I3S9ay69ytiKOBEVOKj7EshkUgCNrAitEO4iWs27bnYx+hBCN3OYW973fipVyJCiYQMmtuhXAytA0LRKHXQqd8WXgPRtx/IqzlsGzExt/qgS+azOs1H7OlsW2oJK5kX7GbdKDH9KWH8Ut2IpWGFbaVqCT39P1sJp6gXuvGjKxJSg8vCa8aU2G8XoNfnxbfhRahrQqcDVfh6Y/WR5KOzk5uYlKOmeNg57HxMRExUJmS1DxePwlPJCajX/2EvgAdlt6Zau7QvC+lgOdKIyiRhc4AaJ6mZMlsSwosk44uYq/1BMUhH74UZZmBPPrtudylaECBmtmtdvJSIe8CFp/omzhtCyoWCy210IVIB1L/gssCS08YTm6jtdWLWjka8GaOQ6wSgLfdxsnV2BJUGTSrTw0A7A6caFk7KkCg5VKLUGzg+2IVDhlrbclQbFJ93XGaY2oaqFokCAsse2OW/xPxQIHMRnvPxVA1V7JoUpWq7yKjpgpUJXDvmBZUN1ZcsaLwf/tpJEXnC1FEPynr0ABKdl9VFVQyWRyA/7Z2NhJIVTl0MJenF0Bi83RhFR6b2rxcXYZLLQgWPhCnubzMqoKCiXPdjRYMvg+ZQUVj8epanfcP4lqby9F5ElAhQc+U+RWtW5A4Sk57rzi+lDUMkkkEneRFDf22Sl4ELQQWbkZv+2wNMYGHmsNCtA3i9eFr2ihUGJXDN0wHRIMjqUFKkocKiYbZDKZFY2XioKCAh05qEo4gD5WdLiXFRQPzTCuE1ipHRAU7YC6jLKCgvkXOYtFkQP8UetVHlomJZuFilKAkzFmiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoihI2Vq36P24Mys1fe9BKAAAAAElFTkSuQmCC";
            }
            return ud;
        }

        public string convert2Epoch(object dateinUTC)
        {
            string returnValue = "";
            if (dateinUTC != null)
            {
                var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                returnValue = Convert.ToInt64((DateTime.Parse(dateinUTC.ToString()) - epoch).TotalSeconds).ToString();
            }
            return returnValue;
        }

        public string GetUserID(string IsLogin)
        {
            string UserId = "";
            if(IsLogin=="1")
            {
                if (HttpContext.Current.Request.Cookies["app_id"] != null)
                {
                    UserId = HttpContext.Current.Request.Cookies["app_id"].Value.ToString();
                    string[] ArrayUserID = UserId.Split('=');
                    UserId = ArrayUserID[1];
                    UserId = Decrypt(UserId, "UserID");
                }
            }
            else
            {
                if (HttpContext.Current.Request.Cookies["UserID"] != null)
                {
                    UserId = HttpContext.Current.Request.Cookies["UserID"].Value.ToString();
                    string[] ArrayUserID = UserId.Split('=');
                    UserId = ArrayUserID[1];
                    UserId = Decrypt(UserId, "UserID");
                }
            }

            return UserId;
        }

        private const int Keysize = 256;
        private const int DerivationIterations = 1000;

        public string Encrypt(string plainText, string passPhrase)
        {
            var saltStringBytes = Generate256BitsOfRandomEntropy();
            var ivStringBytes = Generate256BitsOfRandomEntropy();
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
            {
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                var cipherTextBytes = saltStringBytes;
                                cipherTextBytes = cipherTextBytes.Concat(ivStringBytes).ToArray();
                                cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();
                                memoryStream.Close();
                                cryptoStream.Close();
                                return Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }
        }

        public string Decrypt(string cipherText, string passPhrase)
        {
            // Get the complete stream of bytes that represent:
            // [32 bytes of Salt] + [32 bytes of IV] + [n bytes of CipherText]
            var cipherTextBytesWithSaltAndIv = Convert.FromBase64String(cipherText);
            // Get the saltbytes by extracting the first 32 bytes from the supplied cipherText bytes.
            var saltStringBytes = cipherTextBytesWithSaltAndIv.Take(Keysize / 8).ToArray();
            // Get the IV bytes by extracting the next 32 bytes from the supplied cipherText bytes.
            var ivStringBytes = cipherTextBytesWithSaltAndIv.Skip(Keysize / 8).Take(Keysize / 8).ToArray();
            // Get the actual cipher text bytes by removing the first 64 bytes from the cipherText string.
            var cipherTextBytes = cipherTextBytesWithSaltAndIv.Skip((Keysize / 8) * 2).Take(cipherTextBytesWithSaltAndIv.Length - ((Keysize / 8) * 2)).ToArray();

            using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
            {
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new MemoryStream(cipherTextBytes))
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                            {
                                //cryptoStream.FlushFinalBlock();
                                var plainTextBytes = new byte[cipherTextBytes.Length];
                                var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                memoryStream.Close();
                                cryptoStream.Close();
                                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                            }
                        }
                    }
                }
            }
        }

        private byte[] Generate256BitsOfRandomEntropy()
        {
            var randomBytes = new byte[32]; // 32 Bytes will give us 256 bits.
            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                // Fill the array with cryptographically secure random bytes.
                rngCsp.GetBytes(randomBytes);
            }
            return randomBytes;
        }

    }
}
