appDashboard.controller('FilterRowController', function ($scope, $uibModal, $http) {
		$scope.Result = '';
		$scope.ResultJson = $scope.FilterValue;
		var counter = 0;

		function search(key, nameKey, myArray) {
			var pos = -1;
			for (var i = 0; i < myArray.length; i++) {
				if (myArray[i][key].toUpperCase() === nameKey.toUpperCase()) {
					pos = i;
					break;
				}
			}
			return pos;
		}
		$scope.requestObj = $scope.FilterCriteria;

		if ($scope.requestObjOp == undefined) {
			$scope.isdisableFilter = true;
		}
		$scope.initialdynamicData = [];
		$scope.switchSource = function (data) {
			if ($scope.oldChosenReqObj != undefined && $scope.oldChosenReqObj == data) {
				$scope.dynamicData = $scope.initialdynamicData;
			} else {
				$scope.dynamicData = [];
			}
		}
		$scope.selectDynamicData = function (data, selectedVal) {
			if (selectedVal != undefined) {
				var selectedValue = selectedVal[data];
				return selectedValue;
			}
		}

		$scope.selectRequestData = function (selectedValue) {
			var resultData = angular.copy($scope.requestObj);
			angular.forEach(resultData, function (item) {
				if (selectedValue != undefined && item.id.toUpperCase() == selectedValue.toUpperCase()) {
					item.ticked = true;
					$scope.isdisableFilter = false;
					counter++;
				}
			})
			return resultData;
		}
		$scope.selectRequestObj = function (selectedValue) {
			var resultData = angular.copy($scope.GroupObj);
			angular.forEach(resultData, function (item) {
				if (selectedValue != undefined && item.name.toUpperCase() == selectedValue.toUpperCase()) {
					item.Ticked = true;
				}
			})
			return resultData;
		}

		$scope.Inialize = function (key, selectedValue) {
			if (selectedValue != "" && selectedValue != undefined) {
				$scope.requestObj = $scope.selectRequestData(key.Val);
				$scope.oldChosenReqObj = key.Val;
				$scope.dynamicData = $scope.selectDynamicData(key.Val, selectedValue);
				$scope.initialdynamicData = $scope.dynamicData;
			} else {
				$scope.GroupObj = $scope.selectRequestObj(key.name);
			}
		}

		$scope.fClose = function () {
			if ($scope.requestObjOp[0] != undefined) {
				$scope.isdisableFilter = false;
				$scope.switchSource($scope.requestObjOp[0].id);
			} else {
				$scope.isdisableFilter = true;
			}
		}
		$scope.addRow = function (data) {
			$scope.TableQuery1.Keys.push({
				"name": "",
				"dynamicData": []
			});
		}
		$scope.addRow1 = function (data) {
			$scope.Ob1.push({
				"name": "",
				"value": "",
				"ticked": ""
			});
		}
		$scope.DeleteRow1 = function (data) {
			if (data != undefined) {
				$scope.Ob1.splice(data, 1);
			}
		}
		$scope.DeleteRow = function (data) {
			$scope.TableQuery1.Keys.splice(data, 1);
		}

		$scope.removeTokenize = function (data) {
			var pos = $scope.dynamicData.indexOf(data);
			$scope.dynamicData.splice(pos, 1);
			$scope.initialdynamicData = $scope.dynamicData;
		}

		$scope.$on('getValue', function (e) {
			$scope.$emit("pingBack", $scope.ReadVal(), $scope.comment);
		});

		$scope.$on('getSingleValue', function (e) {
			$scope.$emit("pingSingleBack", $scope.ReadSingleVal());
		});
		$scope.$on('checkFilterValue', function (e, data) {
			$scope.dynamicData = data;
		});

		$scope.ReadVal = function () {

			var genericValue = $scope.dynamicData;
			if (genericValue == "") {
				$scope.comment = "Please Select a Filter Value for the selected Generic Value"
			}
			var fff = "{\"GenericKey\":\"" + $scope.requestObjOp[0].id + "\",\"GenericValue\":\"" + genericValue.join(",") + "\"}";
			return JSON.parse(fff);
		}
		$scope.ReadSingleVal = function () {
			return $scope.GroupObjOP[0].value;
		}

		$scope.chooseFilterModal = function (size, whichGroup, FilterValues) {
			$scope.returnVal  = "check";
			
			if (whichGroup != undefined) {
				$scope.FilterType = whichGroup;
				$scope.chosenFilterValues = FilterValues;
				var modalInstance1 = $uibModal.open({
					animation: $scope.animationsEnabled,
					templateUrl: 'app/views/_FilterPopup.html',
					controller: 'FilterPopupController',
					size: size,
					scope: $scope
				});
			}
		};
	})
	.controller('FilterPopupController', function ($scope, $uibModalInstance, $http, mySharedService, $timeout, $filter) {
		$scope.isRetSearchData = false;
		var checkdata = angular.copy($scope.dynamicData);
		
		$scope.checkVal = checkdata;
		$scope.isChecked = false;
		if ($scope.checkVal == undefined) {
			$scope.checkVal = [];
		}


		/*url = url + "?UserID=" + userID + "&DashBoardID=" + $scope.dashBoardID + "&FilterType=" + $scope.FilterType + "&FilterValues=" + $scope.checkVal.join(",") + "";
		$http({
				method: 'GET',
				url: url,
				params: {version: getEpocStamp()},
				data: {
					'UserID': userID,
					'KPIID': $scope.KPIID,
					'ISKPI': 1
				}
			})*/

		var url = uriApiURL + "api/Dashboard/GetDashboardFilterValues";
		var userID = mySharedService.UserID;
		url = url + "?UserID=" + userID + "&DashBoardID=" + $scope.dashBoardID;

		var Userval = $scope.FilterType;
		var FilterValue = $scope.checkVal.join(",");
		var GetFilterData = { 'Userval': Userval, 'FilterValue': FilterValue };
		console.log("GetFilterData" + GetFilterData)
		$http({
		    method: 'POST',
		    url: url,
		    params: { version: getEpocStamp() },
		    data: GetFilterData,
		    contentType: "application/json",
		    dataType: "json"

		}).success(function (response) {
				$scope.isRetSearchData = true;
				$scope.ValueCollection = angular.fromJson(response);
				$scope.rowCount = $scope.ValueCollection.length;
			});

		$scope.ok = function () {
			if($scope.returnVal == 'radio'){
				$uibModalInstance.close($scope.checkVal);  
			}else{
				$uibModalInstance.dismiss('cancel');
				$scope.$emit("checkFilterValue", $scope.checkVal);
			}
			
		};
		$scope.cancelModal = function () {
			$uibModalInstance.dismiss('cancel');
		};
		$scope.changeFun = function (value) {
			var indexOfEle = $scope.checkVal.indexOf(value.GenericValue);
			if (indexOfEle == -1 && value.TickedVal == true) {
				$scope.BlurClass = "BlurClass";
				$scope.checkVal.push(value.GenericValue);
			} else if (value.TickedVal == false) {
				$scope.checkVal.splice(indexOfEle, 1);
			}
		};

	})