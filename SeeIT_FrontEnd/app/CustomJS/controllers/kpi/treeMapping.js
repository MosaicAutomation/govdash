var treeMapcolor;

appDashboard.controller('treeMappingController', function ($scope, $routeParams, $http, $timeout, $element, mySharedService, $window, Notification) {
    $scope.eleContr = $element;
    $scope.KPIID = $element.parent().parent().parent().attr("data-kpiid");
	$scope.KpiGroupType = $element.parent().parent().parent().attr("data-kpigroup");
	$scope.KpiGroupID = $element.parent().parent().parent().attr("data-kpigropid");
	$scope.KpiGroupName = $element.parent().parent().parent().attr("data-kpigropname");
	$scope.KpiGroupKey = $element.parent().parent().parent().attr("data-kpigroupkey");
    $scope.dashBoardID = $routeParams.dashboardId;
    $scope.TreeMapEl = "#scalablechart"+ $scope.KPIID;
    treeMapcolor = returnThemeColor($scope.ATClr);
	$scope.ColorArray = getColorArray(true, "", $scope.KPITClr, $scope.ATClr, 2);

	$scope.IsZoom = false;
    setTimeout(function () {
        //
        $scope.getWidgetSize = function () {
            var sizeval = getKPISize($scope.KPIID, $scope.KPICollections, $scope.SizeObjX, $scope.SizeObjY)
            $scope.SizeXVal = sizeval[0];
            $scope.SizeYVal = sizeval[1];
        }
        $scope.getWidgetSize();
	
        $scope.$on('resizeCharts', function () {
           
            $timeout(function () {
                setSize(svgTreeMap, el);
            }, 100);
           
        });
		$scope.$on('setBaseColor', function (e, Aclr, Bclr) {

           	$scope.ColorArray = getColorArray(true, "", $scope.KPITClr, $scope.ATClr, 2);
			console.log("CA " + $scope.ColorArray );
			console.log("CA " + $scope.ColorArray );
           $timeout(function () {
		    
			var neElTreMap = $element.find($scope.TreeMapEl).find('.blenderTree');
			
			$(neElTreMap).each(function (i) {
				var BlendVal = $(this).attr("blend");				
				var RetBlendVal= blend($scope.ColorArray[0],$scope.ColorArray[1],BlendVal);
				//console.log("BlendVal " + BlendVal + " r " + RetBlendVal)
              $(this).css({ fill: RetBlendVal });
            });
            }, 100);


        });
		$scope.openWidgetError = function (response) {		
			 Notification.error({
					verticalSpacing: 60,
					message: $scope.IsShowErrorMsg,
					title: '<i class="fa fa-warning"></i>&nbsp;Wrong Query or Insuffucuent Data'
			});	
		};
	
	
        var el = $element.find($scope.TreeMapEl);
        var margin = {
            top: 20,
            right: 0,
            bottom: 0,
            left: 0
        };
	
        var formatNumber = d3.format(',d');
        var transitioning;
        setSize(svgTreeMap, el);

        function setSize(child, parent) {
            //    console.log("CALL TREE " + parent.context.clientWidth + " "+  parent.context.clientHeight);
            $scope.TMwidth = parent.context.clientWidth;
            $scope.TMheight = parent.context.clientHeight - margin.top - margin.bottom -80,            
            $($scope.TreeMapEl).empty();
            dothis();
        }

        $(window).bind('resize', function (e) {
            window.resizeEvt;
            $(window).resize(function () {
                clearTimeout(window.resizeEvt);
                window.resizeEvt = setTimeout(function () {
                    setSize(svgTreeMap, el);

                }, 250);
            });
        });
        var svgTreeMap;
        function dothis() {
            var margin = {
                top: 20,
                right: 0,
                bottom: 0,
                left: 0
            },

                formatNumber = d3.format(',d'),
                transitioning;
            var xTreeMap = d3.scale.linear().domain([0, $scope.TMwidth]).range([0, $scope.TMwidth]);
            var yTreeMap = d3.scale.linear().domain([0, $scope.TMheight]).range([0, $scope.TMheight]);
            var treemap = d3.layout.treemap().children(function (d, depth) {
                return depth ? null : d._children;
            }).sort(function (a, b) {
                return a.value - b.value;
            }).ratio( $scope.TMheight / $scope.TMwidth * 0.5 * (1 + Math.sqrt(5))).round(false);
            $($scope.TreeMapEl).empty();
        
            var svgTreeMap = d3.select($scope.TreeMapEl).append('svg').attr('width', $scope.TMwidth + margin.left + margin.right).attr('height',$scope.TMheight + margin.bottom + margin.top).style('margin-left', -margin.left + 'px').style('margin.right', -margin.right + 'px').append('g').attr('transform', 'translate(' + margin.left + ',' + margin.top + ')').style('shape-rendering', 'crispEdges');
            var grandparent = svgTreeMap.append('g').attr('class', 'grandparent');
            grandparent.append('rect').attr('y', -margin.top).attr('width', $scope.TMwidth).attr('height', margin.top);
            grandparent.append('text').attr('x', 6).attr('y', 6 - margin.top).attr('dy', '.75em');
        

            var tooltipTreeMap = d3.select("#treeMapToolTip").text("a simple tooltip");
            var tooltipTreeMap = d3.select("body")
                .append("div")
                .attr('id', 'treeMapToolTip')
                .attr('class', 'ng-hide')
                .text("a simple tooltip");

            $http({
                method: 'GET',
				params: {version: getEpocStamp()},
                cache: mySharedService.KPIboardCache,
                url: uriApiURL + "api/KPI/GetTreeMapping?DashboardId=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID
            })
                .success(function (response) {

                
                    $scope.WholeData = angular.fromJson(response);
                    $scope.TreeMapDetails = $scope.WholeData[0].TreeMapDetails;
                    $scope.KPIDetails = $scope.WholeData[1].KPIDetails;
                    $scope.kpiname = $scope.KPIDetails.KPIName;
                    $scope.isLoadCmpltd = true;
                    var rootsimple = angular.copy($scope.TreeMapDetails);


                    function removeAllBlankOrNull(JsonObj) {
                        $.each(JsonObj, function (key, value) {
                            //console.log("key " + key + value);
                            if ((typeof value == 'object') && (value != null)) {
                                if (value.length == 0) {
                                    delete JsonObj[key];
                                } else if (typeof (value) === "object") {
                                    JsonObj[key] = removeAllBlankOrNull(value);
                                }
                            }

                        });
                        return JsonObj;
                    }             
					if($scope.TreeMapDetails.children.length > 0){	
                   
						var roots = removeAllBlankOrNull(rootsimple);
						
						initialize(roots);
						accumulate(roots);
						layout(roots);
						display(roots);
						
						
					
					}
					else{
						$scope.IsShowError= true;
						$scope.IsShowErrorMsg= 'Insufficient data';
					}

                    function initialize(root) {
                        root.x = root.y = 0;
                        root.dx = $scope.TMwidth;
                        root.dy = $scope.TMheight;
                        root.depth = 0;
                    }

                    function accumulate(d) {
                        return (d._children = d.children) ? d.value = d.children.reduce(function (p, v) {
                            return p + accumulate(v);
                        }, 0) : d.value;
                    }

                    function layout(d) {
                        if (d._children) {
                            treemap.nodes({
                                _children: d._children
                            });
                            d._children.forEach(function (c) {
                                c.x = d.x + c.x * d.dx;
                                c.y = d.y + c.y * d.dy;
                                c.dx *= d.dx;
                                c.dy *= d.dy;
                                c.parent = d;
                                layout(c);
                            });
                        }
                    }

                    function display(d) {
                        grandparent.datum(d.parent).on('click', transition).select('text').text(nameTreeMap(d));
                        var g1TreeMap = svgTreeMap.insert('g', '.grandparent').datum(d).attr('class', 'depth');
                        var gTreeMap = g1TreeMap.selectAll('g').data(d._children).enter().append('g');
                        gTreeMap.filter(function (d) {
                            return d._children;
                        }).classed('children', true).on('click', transition);

                        gTreeMap.selectAll('.child')
                            .data(function (d) {
                                return d._children || [d];
                            })
                            .enter()
                            .append('rect')
                            .attr('class', 'child')
                            .call(rectTreeMap);

                        gTreeMap.append('rect')
                            .attr('class', 'parent')

                        .on("mouseover", function () {

                            return tooltipTreeMap.attr('class', '')
                        })
                            .on("mousemove", function (d) {
                                distance = $($scope.TreeMapEl).offset().top;
                                return tooltipTreeMap
                                    .style("top", (d3.event.pageY) + "px")
                                    .style("left", (d3.event.pageX + 10) + "px")
                                    .html(d.name + ":" + d.value);
                            })
                            .on("mouseout", function () {
                                return tooltipTreeMap.attr('class', 'ng-hide')
                            })
                            .text(function (d) {
                                return formatNumber(d.value);
                            })
                            .call(rectTreeMap)
                        .on('click', function (d) {
                            stringval = d.parent ? nameTreeMap(d.parent) + ' » ' + d.name : d.name;
                            if (!d._children) {
                                var Variable_bname = new Array();
                                Variable_bname = stringval.split(' » ')
                                Variable_bname.splice(0,1)
                                stringval = Variable_bname.toString();
                                // window.open(d.url)
								$scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID
							   var obj = new Object();
							   obj.FirstGenericData = stringval;
							   var data = JSON.stringify(obj);
							   mySharedService.openGridModal('lg', $scope.url, data);
							    stringval = "";
                            }
                        });
                        setTimeout(function () {
						
                            gTreeMap.append('text')
                           .attr('dy', '.75em')
                           .html(function (d) {
                               var tempwidth = xTreeMap(d.x + d.dx) - xTreeMap(d.x);						     
                               if (d.area > 0.05) {
                                   var ret = d.name.split(" ");
                                   var retx= "";								
									
                                   if( d.name.length *6  < (tempwidth)) {										
                                       retx = d.name;
                                   }
                                   else{
										
                                       var maxch = Math.floor( (tempwidth-15)/6);
                                       retx= d.name.substring(0, maxch-3 ) + "..." ;
                                       //retx = d.name;
									
                                   }
                                   return retx;									
								
                               } else {
                                   return "";
                               }
                           }).call(textTreeMap);
                        }, 250);
                   

                        function transition(d) {
                            if (transitioning || !d)
                                return;
                            transitioning = true;
                            var g2TreeMap = display(d),
                                t1 = g1TreeMap.transition().duration(500),
                                t2 = g2TreeMap.transition().duration(500);
                            xTreeMap.domain([d.x, d.x + d.dx]);
                            yTreeMap.domain([d.y, d.y + d.dy]);
                            svgTreeMap.style('shape-rendering', null);
                            svgTreeMap.selectAll('.depth').sort(function (a, b) {
                                return a.depth - b.depth;
                            });
                            g2TreeMap.selectAll('text').style('fill-opacity', 0);
                            t1.selectAll('text').call(textTreeMap).style('fill-opacity', 0);
                            t2.selectAll('text').call(textTreeMap).style('fill-opacity', 1);
                            t1.selectAll('rect').call(rectTreeMap);
                            t2.selectAll('rect').call(rectTreeMap);
                            t1.remove().each('end', function () {
                                //    svgTreeMap.style('shape-rendering', 'crispEdges');
                                transitioning = false;
                            });
                        }
                        return gTreeMap;
                    }
				

                    function textTreeMap(textTreeMap) {
					
					

                        textTreeMap.attr('x', function (d) {
						
                            return xTreeMap(d.x) + 6;
                        }).attr('y', function (d) {
                            return yTreeMap(d.y) + 6;
                        });
                    }

                    function rectTreeMap(rectTreeMap) {
					
                        rectTreeMap.attr('x', function (d) {
                            return xTreeMap(d.x);
                        }).attr('y', function (d) {
                            return yTreeMap(d.y);
                        }).attr('width', function (d) {
						
                            return xTreeMap(d.x + d.dx) - xTreeMap(d.x);
                        }).attr('height', function (d) {
                            return yTreeMap(d.y + d.dy) - yTreeMap(d.y);
                        }).style("fill",function (d) {
                            return blend($scope.ColorArray[0],$scope.ColorArray[1],d.value2/d.value);
                        })
						.attr("class", "blenderTree")
						.attr("blend",function (d) {
                            return d.value2/d.value;
                        })
						;
                    }

                    function nameTreeMap(d) {
                        return d.parent ? nameTreeMap(d.parent) + ' » ' + d.name : d.name;
                    }

                    $scope.WholeData = angular.fromJson(response);
                    $scope.TreeMapDetails = $scope.WholeData[0].TreeMapDetails;
                    $scope.KPIDetails = $scope.WholeData[1].KPIDetails;
                    $scope.kpiname = $scope.KPIDetails.KPIName;

                    $scope.isLoadCmpltd = true;
                }).error(function (response) {
										
						$scope.IsShowError= true;
						$scope.IsShowErrorMsg= response;
						
					});
			





        }
	 
        //
    }, 300);
	
    
    

    




});