appDashboard.controller('IncomingVcCompletedController', function ($scope, $routeParams, $filter, $http, $uibModal, mySharedService, $element, $timeout, Notification) {
        $scope.isIncVsCompLoaded = false;
        $scope.toggle1 = false;
        $scope.datatoshow = true;
        $scope.dashBoardID = $routeParams.dashboardId;
        $scope.KPIIDs = $element.parent().parent().parent().attr("data-kpiid");
		
	$scope.KpiGroupType = $element.parent().parent().parent().attr("data-kpigroup");
	$scope.KpiGroupID = $element.parent().parent().parent().attr("data-kpigropid");
	$scope.KpiGroupName = $element.parent().parent().parent().attr("data-kpigropname");
	$scope.KpiGroupKey = $element.parent().parent().parent().attr("data-kpigroupkey");
	$scope.openWidgetError = function (response) {		
			 Notification.error({
					verticalSpacing: 60,
					message: $scope.IsShowErrorMsg,
					title: '<i class="fa fa-warning"></i>&nbsp;Wrong Query or Insuffucuent Data'
			});	
		};
	
        $scope.getWidgetSize = function () {
            var sizeval = getKPISize($scope.KPIIDs, $scope.KPICollections, $scope.SizeObjX, $scope.SizeObjY)
            $scope.SizeXVal = sizeval[0];
            $scope.SizeYVal = sizeval[1];
        }
        $scope.getWidgetSize();
        $scope.$on('resizeCharts', function () {
            if ($scope.chartInVsComp != null) {
                setTimeout(function () {
                    $scope.chartInVsComp.resize();
                }, 10);
            }
        });
        $scope.plotIncVsCompChart = function (type) {
            $scope.selectPriority = [type];
			 $scope.selectPriorityName = type;
            $scope.IncVsCompchartData = $filter('filter')($scope.IncVsCompData, type.toUpperCase());
            $scope.StackedData = [['Backlog', 'Incoming']];
            if ($scope.isGrouped) {
                $scope.StackedData = [];
            }
            $scope.ColorArray = getColorArray($scope.IsUseTheme, $scope.DBColorArray, $scope.KPITClr, $scope.ATClr, 3);
            $scope.chartInVsComp = c3.generate({
                bindto: "." + $scope.KPIIDZoom + "InVsComChrt",
                padding: {
                    top: 5,
                    bottom: 0
                },
                data: {
                    json: $scope.IncVsCompchartData,
                    keys: {
                        x: 'PriorityMonthDate',
                        value: ['Incoming', 'Backlog', 'Completed'],
                    },
                    type: 'bar',
                    types: {
                        'Completed': 'spline'
                    },
                    groups: $scope.StackedData,
                    onclick: function (e, event) {
                       // $scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID + "&IsMonth=" + "1" + "&Request_Priority=" + $scope.selectPriority + "&YearMonthWeek=" + $scope.IncVsCompchartData[e.x]['PriorityMonthDate'] + "&FirstGenericData=" + e.name
                       // mySharedService.openGridModal('lg', $scope.url);
						
						 $scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID
						   var obj = new Object();
						   obj.FirstGenericData = e.name;
						    obj.IsMonth = 1;
						    obj.Request_Priority = $scope.selectPriority[0];
						    obj.YearMonthWeek = $scope.IncVsCompchartData[e.x]['PriorityMonthDate'];
						   var data = JSON.stringify(obj);
						   mySharedService.openGridModal('lg', $scope.url, data);
                    },
                    names: {
                        Backlog: 'Backlog',
                        Incoming: 'New',
                        Completed: 'Completed',
                    },
                    colors: {
                        Incoming: $scope.ColorArray[0],
                        Backlog: $scope.ColorArray[1],
                        Completed: $scope.ColorArray[2]
                    }
                },
                tooltip: {
                    format: {
                        title: function (d, i) {
                            return $scope.chartInVsComp.categories()[d];
                        },
                        value: function (value, ratio, id) {
                            return value;
                        }
                    }
                },
                transition: {
                    duration: 10
                },
                axis: {
                    x: {
                        type: 'category',
                        tick: {
                            format: function (x) {
                                if ($scope.IncVsCompchartData[x] != undefined) {
                                    var barName = $scope.IncVsCompchartData[x].PriorityMonthDate
                                    if (barName.length > 7) {
                                        return barName.substring(0, 4) + "...";
                                    } else {
                                        return barName;
                                    }
                                } else {
                                    return "";
                                }
                            },
                        }
                    },

                    y: {
                        tick: {
                            format: function (x) {
                                return d3.round(x, 0)
                            },
							count: 5
                        },
                        padding: {
                            bottom: 0
                        },
                        min: 0
                    }
                }
            });
        };
        $scope.getIncVsCompData = function () {
            if (mySharedService.IsZoom) {
                $scope.isIncVsCompLoaded = true;
                $scope.KPIID = mySharedService.KPIID;
                $scope.IsZoom = mySharedService.IsZoom;
                $scope.KPIIDZoom = $scope.KPIID + "zoomed"
                $scope.toggle1 = false;
                $scope.datatoshow = true;
                $timeout(function () {
                    $scope.plotIncVsCompChart($scope.selectPriority[0])
                }, 10);
                $scope.$emit("ZoomDataDone");
            } else {
                $scope.KPIID = $element.parent().parent().parent().attr("data-kpiid");
				$scope.KpiGroupType = $element.parent().parent().parent().attr("data-kpigroup");
	$scope.KpiGroupID = $element.parent().parent().parent().attr("data-kpigropid");
	$scope.KpiGroupName = $element.parent().parent().parent().attr("data-kpigropname");
	$scope.KpiGroupKey = $element.parent().parent().parent().attr("data-kpigroupkey");
                $scope.IsZoom = false;
                $scope.KPIIDZoom = $scope.KPIID;

                $http({
                        method: 'GET',
						cache: mySharedService.KPIboardCache,
						params: {version: getEpocStamp()},
                        url: uriApiURL + "api/KPI/GetDataKPIIncomingVSCompleted?DashboardId=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID + "",
                        data: {
                            "kpiname": "test"
                        }
                    })
                    .success(function (response) {
                        $scope.isIncVsCompLoaded = true;
                        $scope.WholeData = angular.fromJson(response);
                        $scope.IncVsCompData = $scope.WholeData.IncomingVSCompleted;
                        $scope.prioritySwatch = [];
                        if ($scope.WholeData.Error == "False") {
                            if ($scope.WholeData.Request_Priority != null) {
                                $scope.toggle1 = false;
                                $scope.datatoshow = true;
                                $scope.prioritiesArr = $scope.WholeData.Request_Priority.split(",");
                                for (var i in $scope.prioritiesArr) {
                                    $scope.prioritySwatch.push({
                                        'name': $scope.prioritiesArr[i],
                                        'value': $scope.prioritiesArr[i],
                                        'opacity': 'theme-text'
                                    });
                                }
                                $scope.prioritySwatch[0].opacity = "";
                                $scope.kpiname = $scope.WholeData["KPIName"];
                                $scope.kpiType = "kpiType12";
                                $scope.period = "Jun-14 to Jun-15";
                                $scope.selectPriority = [$scope.prioritySwatch[0].value];
                                $scope.ColorArray = $scope.WholeData.ChartColor.split(",");
                                $scope.DBColorArray = $scope.ColorArray;
                                $scope.isGrouped = $scope.WholeData.IsGrouped;
                                $scope.IsUseTheme = $scope.WholeData.IsUseTheme;
                                $timeout(function () {
                                    $scope.plotIncVsCompChart($scope.selectPriority[0])
                                }, 10);
                                if (mySharedService.IsZoom) {
                                    $scope.$emit("ZoomDataDone");
                                }
                            } else {
                                $scope.toggle1 = true;
                                $scope.datatoshow = false;
                            }
                        } else {

                        }
                    }).error(function (response) {
										
						$scope.IsShowError= true;
						$scope.IsShowErrorMsg= response;
						
					});
            }
        }
        $scope.getIncVsCompData();
        $scope.$on('handleaaddUpdateWidgetDataBroadcast', function (kpiValue) {
            /*kpiValue is KPIID*/
            if (mySharedService.KPIID == 2) {
                $scope.getIncVsCompData();
            }
        });
        $scope.$on('setBaseColor', function (e, Aclr, Bclr) {
            $scope.ColorArray = getColorArray($scope.IsUseTheme, $scope.DBColorArray, Bclr, Aclr, 3);
            $scope.chartInVsComp.load({
                colors: {
                    Incoming: $scope.ColorArray[0],
                    Backlog: $scope.ColorArray[1],
                    Completed: $scope.ColorArray[2]
                }
            });
        });
        $scope.expandChart = function () {
            $scope.animationsEnabled = true;
            $scope.KPI = {
                idNum: 2,
                Id: $scope.KPIID
            }
            mySharedService.OpenZoomModal($scope);
        }
    })