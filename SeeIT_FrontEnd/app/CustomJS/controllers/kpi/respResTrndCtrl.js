 appDashboard.controller('ResponseResolutionTrendController', ['$rootScope', '$scope', '$routeParams', '$timeout', '$filter', '$http', '$uibModal', 'mySharedService', '$element', 'Notification', function ($rootScope, $scope, $routeParams, $timeout, $filter, $http, $uibModal, mySharedService, $element , Notification) {
        /*RespReslnCtrl Controller related to RespResln.html under "Views/KPI" */
        /*Variable Declaration for begins*/
        $scope.isRespReslnLoadCmpltd = false;
		$scope.IsShowError= false;
        $scope.dashBoardID = $routeParams.dashboardId;
        $scope.SLIsuffix = ($scope.SLIsuffix != undefined) ? $scope.SLIsuffix : "H"; /*Have to check why SLIsuffix is not changing*/
        $scope.KPIID = $element.parent().parent().parent().attr("data-kpiid");
		$scope.KpiGroupType = $element.parent().parent().parent().attr("data-kpigroup");
	$scope.KpiGroupID = $element.parent().parent().parent().attr("data-kpigropid");
	$scope.KpiGroupName = $element.parent().parent().parent().attr("data-kpigropname");
	$scope.KpiGroupKey = $element.parent().parent().parent().attr("data-kpigroupkey");
        $scope.criteriaswtch = [
            {
                'name': 'Total',
                'value': 'SumTotal',
                'monthName': 'MonthTotal',
                'opacity': ''
			},
            {
                'name': 'Exceeding SLA',
                'value': 'SumExceeding',
                'monthName': 'MonthExceeding',
                'opacity': 'theme-text'
			},
            {
                'name': 'Meeting SLA',
                'value': 'SumExclExceeding',
                'monthName': 'MonthExclExceeding',
                'opacity': 'theme-text'
			}
		];
        /*Variable declaration ends*/

        /*Function Declaration and definition begins*/
        /*Function called on click of the Request Count*/
		
		$scope.openWidgetError = function (response) {		
			 Notification.error({
					verticalSpacing: 60,
					message: $scope.IsShowErrorMsg,
					title: '<i class="fa fa-warning"></i>&nbsp;Wrong Query or Insuffucuent Data'
			});	
		};
		
		
        $scope.DrillDownRespResln = function (size, KPIID, Request_Priority, IsMonth, Exceeding) {
            if (Exceeding == "SumExceeding") {
                $scope.Exceeding = "Yes"
            } else if (Exceeding == "SumExclExceeding") {
                $scope.Exceeding = "No"
            } else if (Exceeding == "SumTotal") {
                $scope.Exceeding = "Total"
            }
            $scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + KPIID + "&IsMonth=" + IsMonth + "&Request_Priority=" + Request_Priority + "&Exceeding=" + $scope.Exceeding
            mySharedService.openGridModal('lg', $scope.url);
        }

        /*Function to fetch the Size details of the Response Resolution KPI in order to have the SizeXval and the SizeYVal */
        $scope.getWidgetSize = function () {
            var sizeval = getKPISize($scope.KPIID, $scope.KPICollections, $scope.SizeObjX, $scope.SizeObjY)
            $scope.SizeXVal = sizeval[0];
            $scope.SizeYVal = sizeval[1];
        }

        /*Listener Function to resize the RespResln Chart*/
        $scope.$on('resizeCharts', function () {
            if ($scope.chartRespResln != undefined) {
                setTimeout(function () {
                    $scope.chartRespResln.resize();
                }, 500);
            }
        });

        /*Main Function that fetches data from "api/Dashboard/GetDataKPIAgingReport" url */
        $scope.MFgetRespReslnData = function () {
            /*Checks whether the factory variable IsZoom is true or not
            	If IsZoom is true current RespReslnData is used to plot the chart.Doesnot hit server to retrieve data.
              If IsZoom is false fetches data from "api/Dashboard/GetDataAvgResponseResolution" url */
            if (mySharedService.IsZoom) {
                var kpiID = mySharedService.KPIID;
                $scope.IsZoom = mySharedService.IsZoom;
                $scope.KPIIDZoom = kpiID + "zoomed"
                $scope.SummaryChartToggle = false;
                $scope.$emit("ZoomDataDone");
                $scope.plotRespReslnChart();
            } else {
                $scope.IsZoom = false;
                $scope.KPIIDZoom = $scope.KPIID;
                var UserId = mySharedService.UserID;
                var PrivacySetting = "";
                PrivacySetting= mySharedService.Privacysetting;
                $scope.type = $element.parent().attr("type");
                var RespReslnUrlParam = {
                    KPIID: $scope.KPIID,
                    UserName: "",
                    DashBoardIID: $scope.dashBoardID,
                    DashBoardName: "",
                    DashboardIcon: ""
                };
                /*Type is to check whether the given KPI is Reponse or Resolution. 
                If IsResp is "Y" then given KPI is Response with BaseKPI Id='BKPI0001' otherwise KPI is Resolution KPI with BaseKPI Id='BKPI0008'*/
                if ($scope.type == "1") {
                    RespReslnUrlParam.isResp = "Y";
		    $scope.isResp = "Y"
                } else if ($scope.type == "8") {
                    RespReslnUrlParam.isResp = "N";
		    $scope.isResp = "N"
                }
                $http({
                        method: 'POST',cache: mySharedService.KPIboardCache,
						params: {version: getEpocStamp()},
                        url: uriApiURL + "api/KPI/GetDataAvgResponseResolution?DashboardId=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID + "&isResp=" + $scope.isResp,
//                        data: JSON.stringify(RespReslnUrlParam),
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .success(function (response) {
                        $scope.isRespReslnLoadCmpltd = true;
                        $scope.WholeData = angular.fromJson(response);
                        $scope.prioritySwatch = [];
                        $scope.KPISLIs = $scope.WholeData.KPISLIs;
                        $scope.StartDate = Date.parse($scope.WholeData.StartDate);
                        $scope.EndDate = Date.parse($scope.WholeData.EndDate);
                        $scope.Dateformat = "MMM-yy";
                        $scope.ColorArray = $scope.WholeData.ChartColor.split(",");
                        $scope.DBColorArray = $scope.ColorArray;
                        $scope.summaryTableToggle = true;
                        $scope.IsUseTheme = $scope.WholeData.IsUseTheme;
						$scope.IsWithSLA = $scope.WholeData.IsWithSLA;
                        $scope.prioritiesArr = $scope.WholeData.Request_Priority.split(",");
                        for (var i in $scope.prioritiesArr) {
                            $scope.prioritySwatch.push({
                                'name': $scope.prioritiesArr[i],
                                'value': $scope.prioritiesArr[i],
                                'opacity': 'theme-text'
                            });
                        }
                        $scope.prioritySwatch[0].opacity = "";
                        $scope.RespReslnTotalData = $scope.WholeData.MonthTotal;

                        /*Not Used variables begins. Need to confirm if we need these variables */
                        $scope.ChartDataExceed = $scope.WholeData.MonthExceeding;
                        $scope.ChartDataNotExceed = $scope.WholeData.MonthExclExceeding;
                        $scope.slaUnit = {
                            "High": "m.",
                            "Normal": "m."
                        };
                        $scope.slaValue = {
                            "High": "40",
                            "Normal": "40"
                        };
                        $scope.period = "Jun-14 to Jun-15";
                        /*Not used variables end*/

                        $scope.kpiname = $scope.WholeData.KPIName;
                        $scope.kpiType = "kpiType1";
                        $scope.selectCriteria = [$scope.criteriaswtch[0].value];
						$scope.selectPriorityName = $scope.criteriaswtch[0].name;
                        $scope.selectCriteriaMnthName = [$scope.criteriaswtch[0].monthName];
                        $scope.selectPriority = [$scope.prioritySwatch[0].value];
                        $scope.priorityData = $scope.WholeData[$scope.selectCriteria[0]];
                        $scope.RespReslnitems = $filter('filter')($scope.RespReslnTotalData, $scope.selectPriority[0]);

                        /*monthDAta is used to get the SLISuffix but SLISuffix is not proper.Have to check over it*/
                        $scope.monthData = $filter('filter')($scope.priorityData, $scope.selectPriority[0])[0];
                        $scope.SLIsuffix = $scope.monthData.SLIUnit.trim();

                        var priorityDatastr = JSON.stringify($scope.priorityData);
                        //console.log("priorityData " + priorityDatastr);

                        if (!mySharedService.IsZoom) {
                            if ($scope.prioritiesArr.length == 1) {
                                $scope.SummaryChartToggle = false;
                                $scope.showflipIcon = false;
                            } else {
                                $scope.SummaryChartToggle = true;
                                $scope.showflipIcon = true;
                            }
                        } else {
                            $scope.SummaryChartToggle = false;
                            $scope.$emit("ZoomDataDone");
                        }
                        $scope.plotRespReslnChart();
                    })
					.error(function (response) {
										
						$scope.IsShowError= true;
						$scope.IsShowErrorMsg= response;
						
					});
            }
        };

        /*Function for ploting the Response or Resolution KPI Chart*/
        $scope.plotRespReslnChart = function () {
            $scope.RespReslnchartData = makeRespReslnchartData($scope.RespReslnitems);
            $scope.ColorArray = getColorArray($scope.IsUseTheme, $scope.DBColorArray, $scope.KPITClr, $scope.ATClr, 2);
            $timeout(
                function () {
                    $scope.chartRespResln = c3.generate({
                        bindto: "." + $scope.KPIIDZoom + "RespReslnChrt",
                        padding: {
                            top: 5
                        },
                        axis: {
                            x: {
                                type: 'category',
                                tick: {
                                    format: function (x) {
                                        if ($scope.RespReslnitems[x] != undefined) {
                                            var barName = $scope.RespReslnitems[x].PriorityMonthDate
                                            if (barName.length > 7) {
                                                return barName.substring(0, 4) + "...";
                                            } else {
                                                return barName;
                                            }
                                        } else {
                                            return "";
                                        }
                                    },
                                } // this needed to load string x value

                            },
                            y: {
                                tick: {
                                    format: function (x) {
                                        return convert2ddhhmm(x, "M")
                                    },

                                    count: 5
                                },
                                padding: {
                                    bottom: 0
                                },
                                min: 0
                            },
                            y2: {
                                show: true,
                                tick: {
                                    format: function (x) {
                                        return d3.round(x);
                                    },
                                    culling: {
                                        max: 2 // the number of tick texts will be adjusted to less than this value
                                    },
                                    count: 5
                                },
                                padding: {
                                    bottom: 0
                                },
                                min: 0
                            }
                        },
                        transition: {
                            duration: 500
                        },
                        data: {
                            x: 'x',
                            columns: [$scope.RespReslnchartData[2], $scope.RespReslnchartData[1], $scope.RespReslnchartData[0]],
                            type: 'bar',
                            types: {
                                'line1': 'spline'
                            },
                            names: {
                                data1: 'No. of Requests',
                                line1: 'Average Response Time'
                            },
                            axes: {
                                'data1': 'y2'
                            },
                            onclick: function (e, event) {
                                if ($scope.selectCriteria == "SumExceeding") {
                                    $scope.Exceeding = "Yes"
                                } else if ($scope.selectCriteria == "SumExclExceeding") {
                                    $scope.Exceeding = "No"
                                } else if ($scope.selectCriteria == "SumTotal") {
                                    $scope.Exceeding = "Total"
                                }
                                $scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID + "&IsMonth=" + "1" + "&Request_Priority=" + $scope.selectPriority + "&YearMonthWeek=" + $scope.RespReslnitems[e.x]['PriorityMonthDate'] + "&Exceeding=" + $scope.Exceeding
                                mySharedService.openGridModal('lg', $scope.url);
                            },
                            colors: {
                                data1: ($scope.ColorArray != undefined ? $scope.ColorArray[0] : ""),
                                line1: ($scope.ColorArray != undefined ? $scope.ColorArray[1] : "")
                            }
                        },
                        tooltip: {
                            format: {
                                title: function (d, i) {
                                    return $scope.chartRespResln.categories()[d];
                                },
                                value: function (value, ratio, id) {
                                    return value;
                                }
                            }
                        },
                        tooltip: {
                            format: {
                                title: function (d, a, b) {
                                    return $scope.RespReslnchartData[2][d + 1];

                                },
                                value: function (value, ratio, id) {
                                    if (id === 'line1') {
                                        return convert2ddhhmm(value, "M");
                                    } else {
                                        return value;
                                    }
                                }
                            }
                        }
                    });
                }, 0);
        };

        /*Listener Function to Change the base Color whenever the Base Colors are changed*/
        $scope.$on('setBaseColor', function (e, Aclr, Bclr) {
            $scope.ColorArray = getColorArray($scope.IsUseTheme, $scope.DBColorArray, Bclr, Aclr, 2);
            $scope.chartRespResln.load({
                colors: {
                    data1: $scope.ColorArray[0],
                    line1: $scope.ColorArray[1]
                }
            });
        });

        /*Function Called on change of Swatches i.e., Priority swatch */
        $scope.SwatchChangeRespResln = function (whichPriority, whichName, type) {
			$scope.selectPSLAName = whichPriority;
			
			
			
			angular.forEach($scope.criteriaswtch, function (item) {
                if (item.monthName == whichName) {
                    $scope.selectPriorityName = item.name;
                }
            });
			
            if (type == "Request_Priority") {
                $scope.selectPriority = [whichPriority];
                $scope.prioritySwatch = $scope.changeOpacity($scope.prioritySwatch, "value", whichPriority);
            } else if (type == "Criteria") {
                $scope.selectCriteria = [whichPriority];
                $scope.selectCriteriaMnthName = [whichName];
                $scope.criteriaswtch = $scope.changeOpacity($scope.criteriaswtch, "value", whichPriority);
            }
            $scope.priorityData = $scope.WholeData[$scope.selectCriteria[0]];

            /*monthDAta is used to get the SLISuffix but SLISuffix is not proper.Have to check over it*/
            $scope.monthData = $filter('filter')($scope.priorityData, $scope.selectPriority[0])[0];
            $scope.SLIsuffix = $scope.monthData.SLIUnit.trim();

            if ($scope.SummaryChartToggle == false) {
                var dataToFilter = $scope.WholeData[$scope.selectCriteriaMnthName[0]];
                $scope.RespReslnitems = $filter('filter')(dataToFilter, $scope.selectPriority[0]);
                $scope.RespReslnchartData = makeRespReslnchartData($scope.RespReslnitems);
                var SummaryChartToggleValue = $scope.SummaryChartToggle;
                if (SummaryChartToggleValue == true) {
                    $scope.SummaryChartToggle = !SummaryChartToggleValue;
                }
                $timeout(function () {
                    $scope.plotRespReslnChart();
                    if (SummaryChartToggleValue == true)
                        $scope.SummaryChartToggle = true;
                }, 0);
            }
        };

        /*Function Called to flip from FrontPanel to BackPanel */
        $scope.makeFlip = function () {
            $scope.SummaryChartToggle = !$scope.SummaryChartToggle;
            if ($scope.SummaryChartToggle == false) {
                var dataToFilter = $scope.WholeData[$scope.selectCriteriaMnthName[0]];
				
                $scope.RespReslnitems = $filter('filter')(dataToFilter, $scope.selectPriority[0]);
				$scope.selectPSLAName = $scope.selectPriority[0];
                $scope.RespReslnchartData = makeRespReslnchartData($scope.RespReslnitems);
                var SummaryChartToggleValue = $scope.SummaryChartToggle;
                if (SummaryChartToggleValue == true)
                    $scope.SummaryChartToggle = !SummaryChartToggleValue;

                $timeout(function () {
                    $scope.chartRespResln.load({
                        columns: [
							 $scope.RespReslnchartData[2], $scope.RespReslnchartData[1], $scope.RespReslnchartData[0]
						]
                    });
                    if (SummaryChartToggleValue == true)
                        $scope.SummaryChartToggle = true;
                }, 0);
            }
        };

        /*Function Called On click of the Zoom button to have a magnified version of the charts*/
        $scope.expandChart = function () {
            $scope.animationsEnabled = true;
            if ($scope.type == "1") {
                $scope.KPI = {
                    idNum: 1,
                    Id: $scope.KPIID
                }
            } else if ($scope.type == "8") {
                $scope.KPI = {
                    idNum: 8,
                    Id: $scope.KPIID
                }
            }
            mySharedService.OpenZoomModal($scope);
        }

        /*Listener Function called when Response KPI with BaseKPI Id ='BKPI0001' or Resolution KPI with BaseKPI Id ='BKPI0008' is added or updated with the WidgetModal*/
        $scope.$on('handleaaddUpdateWidgetDataBroadcast', function () {
            /*mySharedService.KPIID is broadcated KPIID after saving Settings*/
            if (mySharedService.KPIID == 1) {
                $scope.MFgetRespReslnData();
            }
        });

        /*Function called to change the Opacity of the Swatches on change of those swatch values. Called by SwatchChangeRespResln function*/
        $scope.changeOpacity = function (swatchData, key, value) {
            angular.forEach(swatchData, function (item) {
                if (item[key] == value) {
                    item.opacity = "";
                } else {
                    item.opacity = "theme-text";
                }
            });
            return swatchData;
        };

        /*Function called to make the Required data to build the Response or Resolution charts*/
        function makeRespReslnchartData(jsonFilteredData) {
            var RespReslnchartDataArr1 = ['data1'],
                RespReslnchartDataArr2 = ['line1'],
                RespReslnchartDataArr3 = ['x'],
                combinedArr = [];
            for (var values in jsonFilteredData) {
                RespReslnchartDataArr1.push(jsonFilteredData[values].PriorityCount);
                RespReslnchartDataArr2.push(jsonFilteredData[values].PriorityAvg);
                RespReslnchartDataArr3.push(jsonFilteredData[values].PriorityMonthDate);
            }
            combinedArr.push(RespReslnchartDataArr1);
            combinedArr.push(RespReslnchartDataArr2);
            combinedArr.push(RespReslnchartDataArr3);
            return combinedArr;
        }
        /*Function Declaration and definition ends*/

        /*Function call begins*/
        /*Function to fetch size of KPI is called*/
        $scope.getWidgetSize();
        /*Main function to fetch data from database is called*/
        $scope.MFgetRespReslnData();
        /*Function call ends*/
	}])