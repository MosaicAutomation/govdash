appDashboard.controller('AgingReportController', function ($scope, $routeParams, $filter, $http, $uibModal, mySharedService, $element, $timeout, Notification) {
    /*AgingRepCtrl Controller related to AgingReport.html under "Views/KPI" */
    /*Variable Declaration begins*/
    $scope.isAgingReportLoadCmplt = false;
    $scope.dashBoardID = $routeParams.dashboardId;
    $scope.KPIID = $element.parent().parent().parent().attr("data-kpiid");
	$scope.KpiGroupType = $element.parent().parent().parent().attr("data-kpigroup");
	$scope.KpiGroupID = $element.parent().parent().parent().attr("data-kpigropid");
	$scope.KpiGroupName = $element.parent().parent().parent().attr("data-kpigropname");	
	$scope.KpiGroupKey = $element.parent().parent().parent().attr("data-kpigroupkey");
    $scope.getWidgetSize = function () {
        var sizeval = getKPISize($scope.KPIID, $scope.KPICollections, $scope.SizeObjX, $scope.SizeObjY);
        $scope.SizeXVal = sizeval[0];
        $scope.SizeYVal = sizeval[1];
    }
    $scope.getWidgetSize();
	$scope.openWidgetError = function (response) {		
			 Notification.error({
					verticalSpacing: 60,
					message: $scope.IsShowErrorMsg,
					title: '<i class="fa fa-warning"></i>&nbsp;Wrong Query or Insuffucuent Data'
			});	
		};

    $scope.MFgetAgingReportData = function (response) {

                setTimeout(function () {
                    
               
           

        $scope.isAgingReportLoadCmplt = true;
        $scope.WholeData = angular.fromJson(response);
        $scope.AgingChartData = $scope.WholeData.AgingChartData;
        $scope.GroupData = $scope.WholeData.GroupData;
        $scope.ColorArray = getColorArray(true, "", $scope.KPITClr, $scope.ATClr, $scope.WholeData.AgingChartData.length - 1);
        $scope.kpiname = $scope.WholeData.KPIName;
        $scope.drawChart = function () {
            $scope.AgingChart = c3.generate({
                data: {
                    x: "x",
                    columns: $scope.AgingChartData,
                    type: 'bar',
                    onclick: function (e, event) {
                        var barval = $scope.AgingChartData[0][e.x + 1].replace("BETWEEN", "<>")
			//$scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID + "&Exceeding=" + $scope.GroupData + "&FirstGenericData=" + barval + "&SecondGenericData=" + e.name
                        //mySharedService.openGridModal('lg', $scope.url);
			$scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID
			var obj = new Object();
			obj.Exceeding = $scope.GroupData;
			obj.FirstGenericData = barval;
			obj.SecondGenericData = e.name;
               
			var data = JSON.stringify(obj);
			mySharedService.openGridModal('lg', $scope.url, data);
                    },
                },
                color: {
                    pattern: $scope.ColorArray
                },
                bar: {
                    width: {
                        ratio: 0.8 // this makes bar width 50% of length between ticks
                    }
                },
                axis: {
                    x: {
                        type: 'category' // this is needed to load string x value
                    },
					 y: {
                        tick: {
                            
							format: function (d) {
								return (parseInt(d) == d) ? d : null;
							}
                        }
                    },
                },

                bindto: "." + $scope.KPIID + "AgingChart",
            });
            setTimeout(function () {
                $scope.AgingChart.resize();
            }, 100);
        }
        $scope.drawChart();
        $scope.$on('resizeCharts', function () {
            if ($scope.AgingChart != null) {
                setTimeout(function () {
                    $scope.AgingChart.resize();
                }, 100);
            }
        });
        $scope.$on('setBaseColor', function (e, Aclr, Bclr) {

            $scope.ColorArray = getColorArray(true, "", Bclr, Aclr, $scope.WholeData.AgingChartData.length - 1);            
            $scope.drawChart();


        });
        $scope.expandChart = function () {
                $scope.animationsEnabled = true;
                $scope.KPI = {
                    idNum: 9,
                    Id: $scope.KPIID
                }
                mySharedService.OpenZoomModal($scope);
            }
            //$scope.prioritySwatch = [];
            //$scope.ReportData = [];
            //$scope.WholeData = angular.fromJson(response);
            //$scope.kpiname = $scope.WholeData.KPIName;
            //$scope.prioritiesArr = $scope.WholeData.Request_Priority.split(",");
            //for (var i in $scope.prioritiesArr) {
            //	$scope.prioritySwatch.push({
            //		'name': $scope.prioritiesArr[i],
            //		'value': $scope.prioritiesArr[i],
            //		'opacity': 'theme-text'
            //	});
            //}
            //$scope.prioritySwatch[0].opacity = "";
            //$scope.kpiType = "kpiType1";
            //$scope.selectPriority = [$scope.prioritySwatch[0].value];
            //$scope.AgingReports = $scope.WholeData.AgingReports;
            //$scope.priorityData = $scope.selectPriority[0];
            //$scope.AgingReportData = $filter('filter')($scope.AgingReports, $scope.selectPriority[0]);

}, 100);
    }
	
	
        if (mySharedService.IsZoom) {
            $scope.isIncVsCompLoaded = true;
            $scope.KPIID = mySharedService.KPIID;
            $scope.IsZoom = mySharedService.IsZoom;
            $scope.KPIIDZoom = $scope.KPIID + "zoomed"
            $scope.toggle1 = false;
            $scope.datatoshow = true;
            $timeout(function () {
                $scope.MFgetAgingReportData($scope.wholeData)
            }, 10);
            $scope.$emit("ZoomDataDone");
        } else {
            $scope.KPIID = $element.parent().parent().parent().attr("data-kpiid");
			$scope.KpiGroupType = $element.parent().parent().parent().attr("data-kpigroup");
	$scope.KpiGroupID = $element.parent().parent().parent().attr("data-kpigropid");
	$scope.KpiGroupName = $element.parent().parent().parent().attr("data-kpigropname");
	$scope.KpiGroupKey = $element.parent().parent().parent().attr("data-kpigroupkey");
            $scope.IsZoom = false;
            $scope.KPIIDZoom = $scope.KPIID;

            $scope.type = $element.parent().attr("type");
            $http({
            method: 'GET',
			params: {version: getEpocStamp()},
			cache: mySharedService.KPIboardCache,
            url: uriApiURL + "api/KPI/GetDataKPIAgingReport?DashboardId=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID + ""
        })
        .success(function (response) {
            $scope.wholeData = response
            $scope.MFgetAgingReportData(response);
        }).error(function (response) {
										
						$scope.IsShowError= true;
						$scope.IsShowErrorMsg= response;
						
					});
        }
    

    /*Function Called on change of Swatches i.e., Priority swatch */
    $scope.SwatchChangeAgeReport = function (Request_Priority) {
        $scope.priorityData = Request_Priority;
		$scope.selectPriorityName= Request_Priority;
        $scope.AgingReportData = $filter('filter')($scope.AgingReports, Request_Priority);
    }

    /*Function called on click of the Request Count*/
    $scope.DrillDownAgeReport = function (exceedVal, FirstGen, SecondGen) {
        $scope.AgingReports[0].SLIUnit = $scope.AgingReports[1].SLIUnit;
        $scope.AgingReports[0].SLI = $scope.AgingReports[1].SLI;
        for (var i = 1; i < $scope.AgingReports.length; i++) {
            if ($scope.AgingReports[0].SLIUnit.replace(/[\s]/g, '') == "D" && $scope.AgingReports[i].SLIUnit.replace(/[\s]/g, '') == "D") {
                if ($scope.AgingReports[0].SLI < $scope.AgingReports[i].SLI) {
                    $scope.AgingReports[0].SLI = $scope.AgingReports[i].SLI;
                }
            }
            if ($scope.AgingReports[0].SLIUnit.replace(/[\s]/g, '') == "H" && $scope.AgingReports[i].SLIUnit.replace(/[\s]/g, '') == "H") {
                if ($scope.AgingReports[0].SLI < $scope.AgingReports[i].SLI) {
                    $scope.AgingReports[0].SLI = $scope.AgingReports[i].SLI;
                }
            }
            if ($scope.AgingReports[0].SLIUnit.replace(/[\s]/g, '') == "M" && $scope.AgingReports[i].SLIUnit.replace(/[\s]/g, '') == "M") {
                if ($scope.AgingReports[0].SLI < $scope.AgingReports[i].SLI) {
                    $scope.AgingReports[0].SLI = $scope.AgingReports[i].SLI;
                }
            }
            if ($scope.AgingReports[0].SLIUnit.replace(/[\s]/g, '') != $scope.AgingReports[i].SLIUnit.replace(/[\s]/g, '') && $scope.AgingReports[0].SLIUnit.replace(/[\s]/g, '') != 'D') {
                if ($scope.AgingReports[i].SLIUnit.replace(/[\s]/g, '') == "D") {
                    $scope.AgingReports[0].SLIUnit = "D"
                    $scope.AgingReports[0].SLI = $scope.AgingReports[i].SLI;
                } else if ($scope.AgingReports[i].SLIUnit.replace(/[\s]/g, '') == "H" && $scope.AgingReports[0].SLIUnit.replace(/[\s]/g, '') != "H") {
                    $scope.AgingReports[0].SLIUnit = "H"
                    $scope.AgingReports[0].SLI = $scope.AgingReports[i].SLI;
                } else {
                    $scope.AgingReports[0].SLIUnit = "M"
                }
            }

        }
        if ($scope.priorityData.toUpperCase() == "TOTAL") {
            //$scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID + "&Request_Priority=" + $scope.priorityData.toUpperCase() + "&Exceeding=" + exceedVal + "&FirstGenericData=" + $scope.AgingReports[0].SLIUnit + "&SecondGenericData=" + $scope.AgingReports[0].SLI
            $scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID
            var obj = new Object();
            obj.Request_Priority = $scope.priorityData.toUpperCase();
            obj.Exceeding = exceedVal;
            obj.FirstGenericData = $scope.AgingReports[0].SLIUnit;
            obj.SecondGenericData = $scope.AgingReports[0].SLI;

            var data = JSON.stringify(obj);
        } else {
            //$scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID + "&Request_Priority=" + $scope.priorityData.toUpperCase() + "&Exceeding=" + exceedVal + "&FirstGenericData=" + FirstGen + "&SecondGenericData=" + SecondGen
            $scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID
            var obj = new Object();
            obj.Request_Priority = $scope.priorityData.toUpperCase();
            obj.Exceeding = $scope.GroupData;
            obj.FirstGenericData = FirstGen;
            obj.SecondGenericData = SecondGen;

            var data = JSON.stringify(obj);
        }

        mySharedService.openGridModal('lg', $scope.url, data);

    }

    /*Listener Function called when Aging Report KPI with BaseKPI Id ='BKPI0009' is added or updated with the WidgetModal*/
    $scope.$on('handleaaddUpdateWidgetDataBroadcast', function (kpiValue) {
        /*kpiValue is KPIID*/
        if (mySharedService.KPIID == 9) {
            $scope.MFgetAgingReportData(response);
        }
    });
    /*Function Declaration and definition ends*/

    /*Function call begins*/
    /*Main function to fetch data from database is called*/
   
    /*Function call ends*/

})