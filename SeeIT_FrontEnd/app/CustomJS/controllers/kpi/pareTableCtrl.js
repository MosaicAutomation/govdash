appDashboard.controller('ParetoTableController', function ($scope, $timeout, $routeParams, $filter, $http, mySharedService, $element, Notification) {
    $scope.isLoadCmpltd = false;
    $scope.datatoshow = true;
    $scope.dashBoardID = $routeParams.dashboardId;
	$scope.IsZoom = false;
	$scope.type = $element.parent().attr("type");
	$scope.IsShowError= false;	
	$scope.KpiGroupType = $element.parent().parent().parent().attr("data-kpigroup");
	$scope.KpiGroupID = $element.parent().parent().parent().attr("data-kpigropid");
	$scope.KpiGroupName = $element.parent().parent().parent().attr("data-kpigropname");
	$scope.KpiGroupKey = $element.parent().parent().parent().attr("data-kpigroupkey");
	
    var recursiveAllName = function (params) {
        if (params.parent == null) {
            return params.key;
        } else {
            if (params.key == undefined) {
                return arguments.callee(params.parent) + "," + params.data[$scope.groupedName]; 
            } else {
                return arguments.callee(params.parent) + "," + params.key;
            }
        }
    }
	$scope.openWidgetError = function (response) {
		
		 Notification.error({
                verticalSpacing: 60,
                message: $scope.IsShowErrorMsg,
                title: '<i class="fa fa-warning"></i>&nbsp;Wrong Query or Insuffucuent Data'
            });
							
				
				};

    $scope.getWidgetSize = function () {
        var sizeval = getKPISize($scope.KPIIDs, $scope.KPICollections, $scope.SizeObjX, $scope.SizeObjY)
        $scope.SizeXVal = sizeval[0];
        $scope.SizeYVal = sizeval[1];
    }
    $scope.getWidgetSize();
    $scope.$on('resizeCharts', function () {
        if ($scope.chartInVsComp != null) {
            setTimeout(function () {
                $scope.chartInVsComp.resize();
            }, 10);
        }
    });
    $scope.plotIncVsCompChart = function (response) {
        if ($scope.type == "4") {
		    $timeout(function () {
               
            
            $scope.KPI = {
                idNum: 4,
                Id: $scope.KPIID
            }
            $scope.isAngularGrid = true;
            $scope.isPivotChart = false;
            $scope.responseData = angular.fromJson(response.m_StringValue);
            $scope.kpiname = $scope.responseData.KPIHeaderName[0]['KPI_NAME'];
			var gridQuerySel =  '#myGrid_'+$scope.KPIID
			// var gridDiv = angular.element($element[0].querySelector());
            gridDiv = document.querySelector(gridQuerySel);			
            $scope.groupBylength = $scope.responseData.groupBy.length;
            $scope.gridOptions = {
                columnDefs: $scope.responseData.gridHeader,
                rowData: null,
                enableFilter: true,
                enableSorting: true,
                pinnedColumnCount: 1,
                groupColumnDef: {
                    headerName: $scope.responseData.groupBy[$scope.groupBylength - 1],
                    field: $scope.responseData.groupBy[$scope.groupBylength - 1],
                    width: 200,
                    comparator: agGrid.defaultGroupComparator,
                    cellRenderer: {
                        renderer: 'group',
                        padding: 5
                    }
                },
                sizeColumnsToFit: true,
                rowHeight: 40,
                headerHeight: 40
            };
            $scope.groupedName = $scope.gridOptions.groupColumnDef.headerName;

            new agGrid.Grid(gridDiv, $scope.gridOptions);
            $scope.gridOptions.api.setRowData($scope.responseData.gridData);
            $scope.gridOptions.api.addEventListener('cellClicked', function (params) {
                if (params.value) {
                    var counter = 0;
                    for (var i = 0; i < $scope.responseData.groupBy.length; i++) {
                        if ($scope.responseData.groupBy[i].toUpperCase() == params.colDef.headerName.toUpperCase()) {
                            counter++;
                        }
                    }
                    if (counter != 1 && params.value != 0) {
                        /*$scope.FirstGenericData = recursiveAllName(params.node);
                        $scope.SecondGenericData = params.colDef.field;
                        $scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID + "&IsMonth=" + "1" + "&FirstGenericData=" + encodeURIComponent($scope.FirstGenericData) + "&SecondGenericData=" + encodeURIComponent($scope.SecondGenericData)
                        mySharedService.openGridModal('lg', $scope.url);
						*/
						   $scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID
						   var obj = new Object();
						   obj.FirstGenericData = recursiveAllName(params.node);
						   obj.SecondGenericData = params.colDef.field;
						   var data = JSON.stringify(obj);
						   mySharedService.openGridModal('lg', $scope.url, data);
                    }
                }
            });
			}, 100);
        } else {
            $scope.KPI = {
                idNum: 4,
                Id: $scope.KPIID
            }
            $scope.isAngularGrid = false;
            $scope.isPivotChart = true;
            $scope.AllData = angular.fromJson(response);
			//$scope.fielddataJson = angular.toJson($scope.AllData, true);		
			
			
			$scope.WholeData = $scope.AllData.PivotChartData[0];
			$scope.ColorofArray = $scope.AllData.ChartColor.split(",");
			$scope.ColorOfChart = $scope.ColorofArray[0];
			$scope.IsUseTheme = $scope.AllData.IsUseTheme;
            $scope.kpiname = $scope.AllData["KPIName"];
             //$scope.ColorArray = getColorArray($scope.IsUseTheme, $scope.DBColorArray, $scope.KPITClr, $scope.ATClr, $scope.WholeData.PivotValue.length);
			 $scope.ColorArray = getColorArray(true, "", $scope.KPITClr, $scope.ATClr, $scope.WholeData.PivotValue.length - 1);
            if($scope.IsUseTheme == 1){
				//$scope.ColorArray = getColorArray(true, "", $scope.KPITClr, $scope.ATClr, $scope.WholeData.PivotValue.length - 1);
			}else{
				//$scope.ColorArray = generateColorGradientFromBaseTheme($scope.KPITClr, $scope.ColorOfChart, $scope.WholeData.PivotValue.length - 1);
				//$scope.ColorArray = ColorLuminance("6699CC", 0.2)
				
			}
			
           
			
			$scope.PivotChatDispTable = [];
			$scope.PivotChatDispFinTable = [];
			$scope.prioritySwatch = [];
			$scope.AllTemCar = angular.fromJson(response);
			$scope.tempPriorArray = [];
			$scope.tempPriorArrayIns = 0;
			$scope.tempPriorArray = $scope.AllTemCar.Priority.split(',');
			//console.log("$scope.AllTemCar.Priority" + $scope.AllTemCar.Priority +  " "+$scope.tempArr0505.length)
			
			for( var pril = 0; pril < $scope.AllData.PivotChartData.length; pril++ ){				
				if ($scope.AllData.PivotChartData[pril].Priority == $scope.tempPriorArray[0]){
				$scope.tempPriorArrayIns = pril;
				}
			}
			 $scope.PivotChartData = $scope.AllData.PivotChartData[$scope.tempPriorArrayIns];
			
			for( var pri = 0; pri < $scope.tempPriorArray.length; pri++ ){
				
				$scope.prioritySwatch.push({
                                        'name': $scope.tempPriorArray[pri],
                                        'value': $scope.tempPriorArray[pri],
                                        'opacity': 'theme-text'
                });

			}
			// $scope.prioritySwatch[0].opacity = "";
			 $scope.selectPriorityName =  $scope.prioritySwatch[0].name;
			
			/*console.log("$scope.prioritySwatch[0].name; " +$scope.tempPriorArrayIns+ " - " + $scope.prioritySwatch[$scope.tempPriorArrayIns].name)
            // $scope.selectPriority = [$scope.prioritySwatch[0].value];
			$scope.fielddataJson = angular.toJson($scope.prioritySwatch, true);					
			//console.log("prioritySwatch " +$scope.fielddataJson)	
			$scope.fielddataJson = angular.toJson($scope.PivotChatDispFinTable, true);
			//console.log("fielddataJson " +$scope.fielddataJson)
           */
            $scope.drawChart();




            $scope.$on('resizeCharts', function () {
                if ($scope.chartPivotChart != null) {
                    setTimeout(function () {
                        $scope.chartPivotChart.resize();
                    }, 10);
                }
            });
            $scope.$on('setBaseColor', function (e, Aclr, Bclr) {

                $scope.ColorArray = getColorArray(true, "", Bclr, Aclr, $scope.WholeData.PivotValue.length - 1);
                console.log("$scope.ColorArray" + $scope.ColorArray)
                $scope.drawChart();


            });
            //$scope.ChartPivot($scope.PivotChartData);
        }
        $scope.isLoadCmpltd = true;
    };
    $scope.getParaTabData = function () {
	
        if (mySharedService.IsZoom) {
            $scope.isIncVsCompLoaded = true;
            $scope.KPIID = mySharedService.KPIID;
            $scope.IsZoom = mySharedService.IsZoom;
            $scope.KPIIDZoom = $scope.KPIID + "zoomed"
            $scope.toggle1 = false;
            $scope.datatoshow = true;
            $timeout(function () {
                $scope.plotIncVsCompChart($scope.AllData)
            }, 10);
            $scope.$emit("ZoomDataDone");
        } else {
            $scope.KPIID = $element.parent().parent().parent().attr("data-kpiid");
            $scope.IsZoom = false;
            $scope.KPIIDZoom = $scope.KPIID;

            $scope.type = $element.parent().attr("type");
            if ($scope.type == "4") {
                $scope.url = uriApiURL + "api/KPI/GetDataKPIPivot?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID + ""
            } else if ($scope.type == "12") {
                $scope.url = uriApiURL + "api/KPI/GetDataKPIPivotChart?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID + ""
            }

            $http({
                    method: 'GET',
					cache: mySharedService.KPIboardCache,
                    url: $scope.url
                })
                .success(function (response) {
				 $scope.AllData= response;
                    $scope.plotIncVsCompChart(response)
                }).error(function (response) {
										
						$scope.IsShowError = true;
						$scope.IsShowErrorMsg= response;
						
					});;
        }
    }
    $scope.getParaTabData();
	
    $scope.$on('handleaaddUpdateWidgetDataBroadcast', function (kpiValue) {
        /*kpiValue is KPIID*/
        if (mySharedService.KPIID == 2) {
            $scope.getParaTabData();
        }
    });
    $scope.$on('setBaseColor', function (e, Aclr, Bclr) {
        $scope.ColorArray = getColorArray($scope.IsUseTheme, $scope.DBColorArray, Bclr, Aclr, 3);
        $scope.chartInVsComp.load({
            colors: {
                Incoming: $scope.ColorArray[0],
                Backlog: $scope.ColorArray[1],
                Completed: $scope.ColorArray[2]
            }
        });
    });
	 $scope.drawChart = function () {
	 var tempArrIn = $scope.PivotChartData.PivotValue
	 for( var pi = 0; pi < tempArrIn.length; pi++ ){
				var tempary=[];
				var tempar = tempArrIn[pi];
				
				for( var pc = 0; pc < tempar[0].length; pc++ ){
					var tempch =  { pid : pc, value : tempArrIn[pi]};
					
					tempary.push(tempch);
				}
				$scope.PivotChatDispTable.push(tempch);
				$scope.PivotChatDispFinTable.push( { pid : pi, value :""});
			}
			for( var pi = 0; pi < $scope.PivotChatDispTable.length; pi++ ){		
					var temparyx=[];
					for( var pc = 0; pc < $scope.PivotChatDispTable[pi].value.length; pc++ ){
						var tempch =  { cid : pc, value :$scope.PivotChatDispTable[pi].value[pc]};
						temparyx.push(tempch);
					}
					$scope.PivotChatDispFinTable[pi].value =temparyx;
				
			}
	 
                $scope.chartPivotChart = c3.generate({
                    data: {
                        x: "x",
                        columns: $scope.PivotChartData.PivotValue,
                        type: 'bar',

                    },
                    color: {
                        pattern: $scope.ColorArray
                    },
                    bar: {
                        width: {
                            ratio: 0.8 // this makes bar width 50% of length between ticks
                        }
                    },
                    axis: {
                        x: {
                            type: 'category' // this is needed to load string x value
                        },
						y: {
                        tick: {
                            format: function (x) {
                                return d3.round(x)
                            },
                            count: 3
                        }
                    }
                    },
                    bindto: "." + $scope.KPIID + "PivotChart",
                });
                setTimeout(function () {
                    $scope.chartPivotChart.resize();
                }, 100);
            }
	$scope.changePriority = function (Request_Priority) {
		var Requesting_Priority = 0;
		for( var pril = 0; pril < $scope.AllData.PivotChartData.length; pril++ ){		
				
				if ( $scope.AllData.PivotChartData[pril].Priority == $scope.tempPriorArray[Request_Priority]){
					$scope.selectPriorityName = $scope.AllData.PivotChartData[pril].Priority ;
					Requesting_Priority = pril;
					
				}
			}
	
	//$scope.selectPriorityName =  $scope.prioritySwatch[Request_Priority].name;
	
	$scope.PivotChatDispTable= [];
	$scope.PivotChatDispFinTable = [];
            $scope.WholeData = $scope.AllData.PivotChartData[Requesting_Priority];
			$scope.PivotChartData = $scope.AllData.PivotChartData[Requesting_Priority];
			console.log("Request_Priority " + Requesting_Priority)
			$timeout(function () {
             $scope.drawChart($scope.wholeData )
                 }, 100);
        }
    $scope.expandChart = function () {
        $scope.animationsEnabled = true;


        $scope.KPI = {
            idNum: $scope.type,
            Id: $scope.KPIID
        }

        mySharedService.OpenZoomModal($scope);
    }
})