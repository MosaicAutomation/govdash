appDashboard.controller('ScoreboardController', function ($scope, $routeParams, $filter, $http, $uibModal, mySharedService, $element, $timeout, Notification , $interval) {
        $scope.isGeneralLoad = false;
        $scope.isCircle = false;
		$scope.isrecursive = false;	
		$scope.intervalTime = 0;		
        $scope.dashBoardID = $routeParams.dashboardId;
        $scope.testJSON = {
            "GenericValue": 10,
            "KPIName": "Generic Test",
            "IconValue": "fa fa-users"
        }
		/*TIMER COND STARTS*/
		$scope.ShowTimer = false;
		var intervalId;		
		$scope.counter = 0;
		$scope.countdown = $scope.intervalTime;		
		$scope.SCTimer = function(){
			
			var startTime = new Date();
			intervalId = $interval(function(){
				if($scope.countdown > 0){
				 $scope.countdown--;
				}
				else if($scope.countdown <= 0){
					$scope.ShowTimer = false;
					$scope.stopSCTimer();
					$scope.isGeneralLoad = false;
					mySharedService.KPIboardCache= false;
					$scope.countdown = $scope.intervalTime;							
					
					console.log("INITIATE RELOAD" + $scope.KPIID + " ");
					$scope.getData();
					
				}	
						
			}, 1000);
		  };
		  
		  $scope.$watch('countdown', function(countdown){
			if (countdown < 0){
				//countdown = intervalTime
			}
		  
			
		  });

			/*TIMER COND STARTS*/
					$scope.startSCTimer = function(){	
							$scope.SCTimer();
						};
					  
					  $scope.stopSCTimer = function(){
						$interval.cancel(intervalId);
					  };
					  /*TIMER COND STARTS*/
			
		
		
		/*TIMER ends STARTS*/
		
        $scope.getData = function () {
            $scope.KPIID = $element.parent().parent().parent().attr("data-kpiid");
			$scope.KpiGroupType = $element.parent().parent().parent().attr("data-kpigroup");
	$scope.KpiGroupID = $element.parent().parent().parent().attr("data-kpigropid");
	$scope.KpiGroupName = $element.parent().parent().parent().attr("data-kpigropname");
	$scope.KpiGroupKey = $element.parent().parent().parent().attr("data-kpigroupkey");
	var PieContainer = ".RequestInfoOpenTime_"+$scope.KPIID;
	
	$scope.openWidgetError = function (response) {		
			 Notification.error({
					verticalSpacing: 60,
					message: $scope.IsShowErrorMsg,
					title: '<i class="fa fa-warning"></i>&nbsp;Wrong Query or Insuffucuent Data'
			});	
		};
			
            $http({
                    method: 'GET',
					cache: mySharedService.KPIboardCache,
					params: {version: getEpocStamp()},
                    url: uriApiURL + "api/KPI/GetDataKPIScoreboard?DashboardId=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID + "",
                    data: {
                        "kpiname": "test"
                    }
                })
                .success(function (response) {
                    $scope.isGeneralLoad = true;
                    $scope.WholeData = angular.fromJson(response);
                    $scope.IsUseTheme = $scope.WholeData.IsUseTheme;
                    $scope.GenericValue = $scope.WholeData.GenericValue;
                    $scope.kpiname = $scope.WholeData.KPIName;
                    $scope.IconValue = $scope.WholeData.GenericValueIcon;
                    $scope.ScoreBoardType = $scope.WholeData.ScoreBoardType;
					$scope.IS_PeriodicCheck = $scope.WholeData.IsPeriodic;
					$scope.PeriodicTimeUnit = $scope.WholeData.PeriodicTimeUnit;
					$scope.intervalTime = Number($scope.WholeData.PeriodicTime)
					if ( ($scope.WholeData.IsPeriodic == "Y") || ($scope.WholeData.IsPeriodic == "y")){
						if ($scope.PeriodicTimeUnit == "MM"){
							$scope.intervalTime = $scope.intervalTime * 60;
						}else if  ($scope.PeriodicTimeUnit == "HH"){
							$scope.intervalTime = $scope.intervalTime * 3600;
						}
						
						console.log("SC TIMER CK FOR " + $scope.KPIID + " ");
						$scope.isrecursive = true;	
						$scope.ShowTimer = true;
						$scope.initialCountdown = $scope.intervalTime ;
						$scope.countdown = $scope.intervalTime ;
						$scope.SCTimer();
					}	
                    if ($scope.ScoreBoardType == "1") {
                        $scope.isCircle = true;
                        $timeout(
                            function () {
                                var jsonValueData = [];
                                $scope.jsonKeyValue = [];
                                $scope.ColorArray = getColorArray(true, "", $scope.KPITClr, $scope.ATClr, 2);
                                $scope.colorObj = {};
                                $scope.Pie = [{
                                    "label": $scope.WholeData.KPIName,
                                    "value": parseInt(($scope.WholeData.GenericValue))
							 }, {
                                    "label": "",
                                    "value": 100 -  parseInt(($scope.WholeData.GenericValue))
							 }];
                                $.each($scope.Pie, function (i, item) {
                                    var obj = {};
                                    obj[item.label] = item.value;
                                    jsonValueData.push(obj);
                                    $scope.jsonKeyValue.push(item.label);
                                    if (i == 0) {
                                        $scope.colorObj[item.label] = $scope.ColorArray[0];
                                    } else if (i == 1) {
                                        $scope.colorObj[item.label] = 'rgba(125, 125, 125, 0.4)';
                                    }
                                });
                                $scope.donutChart = c3.generate({
                                    bindto: PieContainer,
                                    data: {
                                        json: jsonValueData,
                                        keys: {
                                            value: $scope.jsonKeyValue
                                        },
                                        type: 'donut',
                                        colors: $scope.colorObj
                                    },
                                    donut: {
                                        title: $scope.WholeData.GenericValue + '%'
                                    }
                                });

                            }, 0);
                    }
                }).error(function (response) {
										
						$scope.IsShowError= true;
						$scope.IsShowErrorMsg= response;
						
					});
        }
        $scope.getData();
        $scope.KPIModalFun = function () {
			if(  $scope.GenericValue > 0){
			    $scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID
			    var obj = new Object();
			    obj.FirstGenericData = "";
			    var data = JSON.stringify(obj);
               //var data = "";
		
            mySharedService.openGridModal('lg', $scope.url,data);
			}
        }
        $scope.$on('handleaaddUpdateWidgetDataBroadcast', function (kpiValue) {
            /*kpiValue is KPIID*/
            if (mySharedService.KPIID == 7) {
				scope.isCache = true;
                $scope.getData();
            }
        });
        $scope.$on('setBaseColor', function (e, Aclr, Bclr) {
            if ($scope.Pie != undefined) {
                $scope.colorObj = {}
                $scope.ColorArray = getColorArray($scope.IsUseTheme, $scope.DBColorArray, Bclr, Aclr, 1)
                $.each($scope.Pie, function (i, item) {
                    if (i == 0) {
                        $scope.colorObj[item.label] = $scope.ColorArray[0];
                    } else if (i == 1) {
                        $scope.colorObj[item.label] = 'rgba(255, 255, 255, 0)';
                    }
                });
                if ($scope.donutChart != undefined) {
                    $scope.donutChart.load({
                        colors: $scope.colorObj
                    });
                };
            }
        });
    })