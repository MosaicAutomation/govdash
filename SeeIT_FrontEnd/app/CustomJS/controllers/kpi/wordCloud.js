appDashboard.controller('wordCloudController', function ($scope, $routeParams, $http, $timeout, $element, mySharedService, Notification) {
    var steps = 6;
$scope.IsZoom = false;
    var stringval;
    $scope.KPIID = $element.parent().parent().parent().attr("data-kpiid");
	$scope.KpiGroupType = $element.parent().parent().parent().attr("data-kpigroup");
	$scope.KpiGroupID = $element.parent().parent().parent().attr("data-kpigropid");
	$scope.KpiGroupName = $element.parent().parent().parent().attr("data-kpigropname");
	$scope.KpiGroupKey = $element.parent().parent().parent().attr("data-kpigroupkey");
    $scope.dashBoardID = $routeParams.dashboardId;
    $scope.colors = generateColorGradient('', $scope.ATClr, steps);
    $scope.colors.reverse();
    $scope.steps = steps;
	var CloudContainer = $element.find('#cloud-dynamic');

    $scope.getWidgetSize = function () {
        var sizeval = getKPISize($scope.KPIID, $scope.KPICollections, $scope.SizeObjX, $scope.SizeObjY)
        $scope.SizeXVal = sizeval[0];
        $scope.SizeYVal = sizeval[1];
    }

    $scope.getWidgetSize();
	
	$scope.openWidgetError = function (response) {		
		 Notification.error({
                verticalSpacing: 60,
                message: $scope.IsShowErrorMsg,
                title: '<i class="fa fa-warning"></i>&nbsp;Wrong Query or Insuffucuent Data'
            });						
				
				};

    $scope.$on('resizeCharts', function () {

        $timeout(function () {
            $scope.drawWordCloud();
        }, 100);

    });
    $http({
        method: 'GET',
        cache: mySharedService.KPIboardCache,
		params: {version: getEpocStamp()},
        url: uriApiURL + "api/KPI/GetWordCloud?DashboardId=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID
    })
        .success(function (response) {
            $scope.kpiname = "";
            $scope.words = "";
            var wordCloudData = angular.fromJson(response);

            var tag_list = new Array();
			if(wordCloudData.KPIwords.length > 0){
				for (var i = 0; i < wordCloudData.KPIwords.length; ++i) {
					var x = wordCloudData.KPIwords[i];
					tag_list.push({
						text: x.text,
						weight: x.weight,
						handlers: {
							click: function () {
								// alert(jQuery(this).html());
								stringval = jQuery(this).text();
								//$scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID + "&FirstGenericData=" + stringval
								//mySharedService.openGridModal('lg', $scope.url);
							    //stringval = "";
								$scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID
							   	var obj = new Object();
								obj.FirstGenericData = stringval;
								var data = JSON.stringify(obj);
								mySharedService.openGridModal('lg', $scope.url, data);
								stringval = "";

							}
						},
						html: { title: x.text + " - " + x.weight + " Items." },
					});
				}
				// 
				$scope.isLoadCmpltd = true;
				$timeout(function () {
					$scope.drawWordCloud();

				}, 100);
			}
			else{
				console.log("ERROR")
				$scope.IsShowError= true;
				$scope.IsShowErrorMsg= 'Data Not Available !';
			}

            $scope.drawWordCloud = function () {

                var getelementsetW = $('#cloud-dynamic').parent().width() - 60;
                var getelementsetH = $('#cloud-dynamic').parent().height() - 20;
                $('#cloud-dynamic').css("width", getelementsetW);
                $('#cloud-dynamic').css("min-width", getelementsetW);
                $('#cloud-dynamic').css("min-height", getelementsetH);
                $scope.kpiname = wordCloudData.KPIDetails.KPIName;
                $scope.words = wordCloudData.KPIwords;
                // $("#cloud-dynamic").jQCloud($scope.words);
                $(CloudContainer).jQCloud(tag_list, {
                    colors: $scope.colors,
                    autoResize: true
                });

                $timeout(function () {
                    $scope.applycloudcolors = generateColorGradient('', $scope.ATClr, steps);
                    //console.log("colors " + $scope.applycloudcolors);
                    $('#cloud-dynamic > span').each(function () { /* ... */ });
                }, 100);
            };

        }).error(function (response) {
										
						$scope.IsShowError= true;
						$scope.IsShowErrorMsg= response;
						
					})

    /*Listener Function to Change the base Color whenever the Base Colors are changed*/
    $scope.$on('setBaseColor', function (e, Aclr, Bclr) {
        //$scope.ColorArray = getColorArray($scope.IsUseTheme, $scope.DBColorArray, Bclr, Aclr, 2);
        //$scope.colors = generateColorGradient('', Aclr, 7);
    });
})