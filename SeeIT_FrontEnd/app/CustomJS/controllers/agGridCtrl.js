appDashboard.controller('AngularGridController', function ($scope, $window, $location, $uibModalInstance, $http, $routeParams) {
    $scope.isRetDatasGrid = false;
	$scope.filename = "ReportSheet";
    $scope.dashBoardID = $routeParams.dashboardId;
    $scope.ok = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.DropValues = [{
        name: "Detailed Analysis"
		}];
    $scope.DropVal = $scope.DropValues[0];
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
	var UserDashboardData = { Userval: $scope.JsonData };
       $http({
        method: 'POST',
        data: UserDashboardData,
        contentType: "application/json",
        dataType: "json",
        params: { version: getEpocStamp() },
            //method: 'GET',
            url: uriApiURL + $scope.url,
              
        }).success(function (response) {
            $scope.isRetDatasGrid = true;
            var textData = "";
            $scope.responseData = angular.fromJson(response.m_StringValue);
			$scope.SendGetGridHead =   $scope.responseData.gridData[0];
			$scope.SendGridContent = $scope.responseData.gridData;
			$scope.SendSetGridHead = {};
			$.each( $scope.SendGetGridHead, function( key, value ) {
				value = key;
				$scope.SendSetGridHead[key] = key
			});
			$scope.seedre = angular.toJson($scope.SendSetGridHead, true);
			
			$scope.SendGridData = [];
			$scope.SendGridData.push($scope.SendSetGridHead);
			angular.forEach($scope.SendGridContent, function (item, index) {
				$scope.SendGridData.push(item);				               
            });
			
            var gridDiv = document.querySelector('#requestList');
            $scope.gridOptions = {
                rowData: null,
                enableFilter: true,
                enableSorting: true,
                columnDefs: $scope.responseData.gridHeader,
                pinnedColumnCount: 2,
                //showToolPanel: false,
                //icons: {
                //	// use font awesome for menu icons
                //	menu: '<i class="fa fa-bars" {{ATClr}}/>',
                //	filter: '<i class="fa fa-filter {{ATClr}}"/>',
                //	sortAscending: '<i class="fa fa-long-arrow-down {{ATClr}}"/>',
                //	sortDescending: '<i class="fa fa-long-arrow-up {{ATClr}}"/>',
                //},
                //cellClicked: function (params) {
                //	if (params.colDef.headerName.toUpperCase() == "REQUEST ID" && $scope.DropVal.name == "Detailed Analysis") {
                //		$window.open('#/RequestDrillDown/' + params.value + '');
                //		$uibModalInstance.dismiss('cancel');
                //		sessionStorage.RequestId = params.value;
                //	}
                //}
                //angularCompileHeaders: true,
                //headerCellRenderer: headerCellRendererFunc,
                onModelUpdated: modelUpdated,
                rowHeight: 40,
                headerHeight: 40
            };
            new agGrid.Grid(gridDiv, $scope.gridOptions);
            $scope.gridOptions.api.setRowData($scope.responseData.gridData);
            $scope.gridOptions.api.addEventListener('cellClicked', function (params) {
                if (params.colDef.headerName.toUpperCase() == "REQUEST ID" && $scope.DropVal.name == "Detailed Analysis") {
                    $window.open('#/RequestDrillDown/' + params.value + '');
                    $uibModalInstance.dismiss('cancel');
                    sessionStorage.RequestId = params.value;
                }
            });
            $scope.isLoadCmpltd = true;
        });

    function modelUpdated() {
        var model = $scope.gridOptions.api.getModel();
        if ($scope.gridOptions.rowData != null)
            var totalRows = $scope.gridOptions.rowData.length;
        var processedRows = model.getVirtualRowCount();
        $scope.rowCount = processedRows.toLocaleString() + ' / ' + ((totalRows != undefined) ? totalRows.toLocaleString() : 0)
    }

    function headerCellRendererFunc(params) {
        var eHeader = document.createElement('span');
        var eTitle = document.createTextNode(params.colDef.headerName);
        //eHeader.setAttribute("class", "{{ATClr}} theme-text");
        //eHeader.appendChild(eTitle);
        return eHeader;
    }
})