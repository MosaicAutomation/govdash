appDashboard.controller('MasterController', function ($scope, translationService, $timeout, $window, $rootScope, $http, $routeParams, $uibModal, mySharedService, $location, Notification) {
    /*MasterController Controller related to index.html under "Views" is the master controller */
    /*Variable Declaration begins*/
console.log("MASTER WAKES..");
 $scope.scopeMenu = false; 
    //Run translation if selected language changes
    $scope.translate = function () {
        translationService.getTranslation($scope, $scope.selectedLanguage);
    };

    //Init
    $scope.selectedLanguage = 'en';
    $scope.translate();

    $scope.oldWidth = $window.innerWidth;
    $scope.oldHeight = $window.innerHeight;
    $scope.TClr = "light";
    $scope.TIClr = "dark";
    $scope.dashBoardID = $routeParams.dashboardId;
    $scope.isFetchingData = false;
    $scope.BlurClass = "";
    $scope.animationsEnabled = true;
    $scope.themeApplycolors = [
        'amber',
        'brown',
        'cobalt',
        'crimson',
        'cyan',
        'magenta',
        'lime',
        'indigo',
        'green',
        'emerald',
        'mauve',
        'olive',
        'orange',
        'pink',
        'red',
        'sienna',
        'steel',
        'teal',
        'violet',
        'yellow'
    ];
    /*Variable declaration ends*/

    /*Function Declaration and definition begins*/
    /*Function used to set the Base Theme Color and the City Climate information along with the URL for the Feeds*/
    $scope.GetUserCompleteUserData = function (UserInfo) {
            $scope.ATClr = UserInfo.theme_Color;
            $scope.KPITClr = UserInfo.base_Color;
            if ($scope.KPITClr == "light") {
                $scope.KClr = "white";
                $scope.TClr = "light";
                $scope.TClrs = "lightTheme";
                $scope.TIClr = "darkITheme";
            } else {
                $scope.KClr = "dark";
                $scope.TClr = "dark";
                $scope.TClrs = "darkTheme";
                $scope.TIClr = "lightITheme";
            }
        }
        /*Function called on click of the Accent Colors under the Appearance dropdown and to save the colors using the "api/UserSetting/SaveUserTheme"*/
    $scope.AccentColorChange = function (event) {
        var self = {};
        $scope.ATClr = event.currentTarget.title;
        var ATClkObj = {
            "Theme_Color": event.currentTarget.title,
            "Base_Color": $scope.KPITClr
        };
        if ($scope.KPITClr == "light") {
            $scope.KClr = "white";
        } else {
            $scope.KClr = "dark";
        }
        $scope.$broadcast('setBaseColor', $scope.ATClr, $scope.KPITClr);
        $http({
                method: 'POST',
                url: uriApiURL + "api/UserSetting/SaveUserTheme?userData=" + JSON.stringify(ATClkObj) + "",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
            .success(function (response) {
                Notification.success({
                    verticalSpacing: 60,
                    message: 'Theme saved successfully.'
                });
            var UserVal = JSON.parse(sessionStorage.UserInfo)
            UserVal.theme_Color = $scope.ATClr;
            UserVal.base_Color = $scope.KPITClr;
            sessionStorage.UserInfo = JSON.stringify(UserVal)
            });
    };
	
	 $scope.SwapSideMenu = function (event) {
		console.log("PSO");
		 $scope.scopeMenu = $scope.scopeMenu === false ? true: false;
		
	};
    /*Function called on click of the Base theme Colors under the Appearance dropdown and to save the colors using the "api/UserSetting/SaveUserTheme"*/
    $scope.BaseThemeChange = function (event) {
	
		
        var self = {};
        if (event.currentTarget.title == 'white') {
            $scope.TClr = "light";
            $scope.TClrs = "lightTheme";
            $scope.TIClr = "darkITheme";
            $scope.KPITClr = "light";
        } else {
            $scope.TClr = "dark";
            $scope.TClrs = "darkTheme";
            $scope.TIClr = "lightITheme";
            $scope.KPITClr = "dark";
        }
        var BTClkObj = {
            "Theme_Color": $scope.ATClr,
            "Base_Color": $scope.KPITClr
        };
        var BodyEl = angular.element(document).find('body');
       // BodyEl.css('background', 'url(../Media/' + $scope.TClr + '/1.jpg)  center center no-repeat fixed');
        $http({
                method: 'POST',
				params: {version: getEpocStamp()},
                url: uriApiURL + "api/UserSetting/SaveUserTheme?userData=" + JSON.stringify(BTClkObj) + "",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
            .success(function (response) {
                Notification.success({
                    verticalSpacing: 60,
                    message: 'Theme saved successfully.'
                });
            var UserVal = JSON.parse(sessionStorage.UserInfo)
            UserVal.theme_Color = $scope.ATClr;
            UserVal.base_Color = $scope.KPITClr;
            sessionStorage.UserInfo = JSON.stringify(UserVal)
            });

    };

    /*Function called on click of the Log out button under the User Dropdown menu. Used to navigate to the Login page*/
    $scope.logOut = function () {
        //authService.logOut();
		$scope.UserInfo = "";
        $scope.IsAdmin = "";
		 $scope.scopeMenu = false; 
		sessionStorage.UserInfo = "";
        $location.path('/login');
		 $scope.TClr = "light";
    $scope.TIClr = "dark";
    };
	
	$scope.$on('$routeChangeStart', function(){
        $scope.scopeMenu = false; 
    });
    /*Function called on click of the Admin button under the User Dropdown menu. Used to navigate to the Admin page*/


    /*Function to invoke the System Walkthrough.*/
    $scope.systemWalkThroughFn = function () {
		$location.path('/');
        $timeout(function () {
            $scope.$broadcast('walkthrough')
        }, 0);


    };
    /*Function called to delete either the dashboard or the widget. When isDeleteDashboard parameter is "0" the Widget is deleted, otherwise the Dashboard is deleted using the "api/Dashboard/DeleteDashboardOrWidgetKPI" url*/
    $scope.deleteDashboard = function (size, isDeleteDashboard, dashboardId, kpiID) {
        console.log("DEL DASH/KPI" + isDeleteDashboard + " " + dashboardId + " " +kpiID)
		if (isDeleteDashboard == 0) {
            $scope.delval = "kpidel";
            $scope.HeaderName = "Delete Widget"
            $scope.broadVal = 'handleKPIdel';
            $scope.DeleteVar = " Widget";
            $scope.DelKPIID = kpiID;
			$scope.DelDASHID = dashboardId;
            $scope.Deleteurl = uriApiURL + "api/Dashboard/DeleteDashboardOrWidgetKPI?DashboardID=" + dashboardId + "&IsDelDash=" + 0 + "&KPIID=" + kpiID;
        }
		else if (isDeleteDashboard == 1) {
            $scope.delval = "dashdel";
            $scope.HeaderName = "Delete Dashboard"
            $scope.broadVal = "handleDashboarddel"
            $scope.DeleteVar = " Dashboard";
			 $scope.DelKPIID = dashboardId;
			 $scope.DelDASHID = dashboardId;
            $scope.Deleteurl = uriApiURL + "api/Dashboard/DeleteDashboardOrWidgetKPI?DashboardID=" + dashboardId + "&IsDelDash=" + 1;
        } 
		else if (isDeleteDashboard == 2) {
            $scope.delval = "grpdel";
            $scope.HeaderName = "Delete Grpup"
            $scope.broadVal = 'handleGrpdel';
            $scope.DeleteVar = " Group";
            $scope.DelDASHID = kpiID;
            $scope.Deleteurl = uriApiURL + "api/Dashboard/DeleteDashboardOrWidgetKPI?DashboardID=" + dashboardId + "&IsDelDash=" + 2 + "&GroupKey=" + kpiID;
        }
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/views/_DeleteModal.html',
            controller: 'DeleteModalController',
            size: 'md',//size
            scope: $scope
        })
    };
	
	
	$scope.loginDash = function () {
		$location.path('/login');
        /*
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/views/_Login.html',
            controller: 'LoginModalController',
            size: 'xs loginModal',//size
            scope: $scope
        })*/
    };

    /*Function to invoke the KPI Walkthrough*/
    $scope.KPIWalkThroughFn = function (widget) {
        $location.path('/login')
        alert("Implement the Widget walk-through!");
    };
	
	$scope.videoWalkThrough = function () {
	$scope.scopeMenu = $scope.scopeMenu === false ? true: false;
	
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'app/views/videoPlayer.html',
                controller: 'videoPlayController',
                size: 'lg md-video',
                scope: $scope,
                backdrop: 'static'
            }).result.finally(function () {
                if (!mySharedService.IsZoom) {
                    $scope.BlurClass = "";
                }
            });
      


    };

    /*Function called to open the Widget setting modal either to Add a new widget or to Edit an existing widget*/

	$scope.openWidgetModal = function (size, isSetting, KPIID,grpType, grpID, grpKey) {
		console.log("KPI SET OPN : " + size +" " + isSetting +" " + KPIID +" " + grpType +" " + grpID + " grpKey  " + grpKey) ;
		mySharedService.KPIboardCache = false;
		mySharedService.carouselLock = true;
		$rootScope.$broadcast('carouselLockChange');
		$scope.grpType = grpType;
		$scope.grpID = grpID;
		$scope.grpKey = grpKey;
		mySharedService.KPIGroupUpdate = { 'grpType' : grpType , 'grpID' : grpID, 'grpKey' : grpKey};
	mySharedService.KPIboardCache = false;
        $scope.ChildValue = mySharedService.SlaChoose;
        $scope.DashBoardID = $routeParams.dashboardId;
        $scope.KPIVal = $scope.$$childTail.KPICollections;
        /*isSetting=0 means EVent is triggered from Add widget*/
        if ($scope.$$childTail.isReorderData == undefined || $scope.$$childTail.isReorderData == false) {
            $scope.SettingValue = isSetting;
            $scope.KPIID = KPIID;
            if (isSetting != 0) {
                $scope.isKPISetting = true;
                $scope.HeaderName ="Widget Settings";
            }
            else {
                $scope.isKPISetting = false;
                $scope.HeaderName ="Add Widget";
            }
            $scope.BlurClass ="BlurClass";
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'app/views/_WidgetSettings.html',
                controller: 'WidgetModalController',
                size: size,
                scope: $scope
            }).result.finally(function () {
                $scope.BlurClass ="";
				mySharedService.carouselLock = false;
				$rootScope.$broadcast('carouselLockChange');
            });
        } else {
            Notification.error({
                verticalSpacing: 60,
                message: 'You cannot change the settings when you are reordering the widgets!',
                title: '<i class="fa fa-warning"></i>&nbsp;Operation not allowed'
            });
        }
    };

    /*Function called to open the Dashboard setting modal either to Add a new dashboard or to Edit an existing dashboard*/
    $scope.openDashboardSettings = function (size, isSetting, dashBoardID, isDMVActive ) {
			
        $scope.dashBoardID = dashBoardID;
        $scope.isSetting = isSetting;
        $scope.BlurClass ="BlurClass";
		$scope.isDMVActive  = isDMVActive;
		mySharedService.carouselLock = true;
		$rootScope.$broadcast('carouselLockChange');
		
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/views/_DashboardModal.html',
            controller: 'DashboardModalController',
            size: size,
            scope: $scope
        }).result.finally(function () {
            $scope.BlurClass ="";
			mySharedService.carouselLock = false;
			$rootScope.$broadcast('carouselLockChange');
			
        });
    };
	
	/*Function Clone Dashboard*/
    $scope.cloneDashboard = function (size,  dashID, dashName ) {
			
        $scope.dashBoardID = dashID;
		$scope.dashName = dashName;
		
        $scope.BlurClass ="BlurClass";
		mySharedService.carouselLock = true;
		$rootScope.$broadcast('carouselLockChange');
		
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/views/_DashboardCloneModal.html',
            controller: 'DashboardCloneModalController',
            size: size,
            scope: $scope
        }).result.finally(function () {
            $scope.BlurClass ="";
			mySharedService.carouselLock = false;
			$rootScope.$broadcast('carouselLockChange');
			
        });
    };

    /*Function to inverse the animationsEnabled variable*/
    $scope.toggleAnimation = function () {
        $scope.animationsEnabled = !$scope.animationsEnabled;
    };

    /*Function used to call the Modal that shows the drilled down data on click of any request count*/
    $scope.openDrilldownGrid = function (size, url, data) {
        if ($scope.$$childTail.isReorderData == undefined || $scope.$$childTail.isReorderData == false) {
            $scope.url = url;
            $scope.HeaderName = "Request(s) List";
            $scope.BlurClass = "BlurClass";
            $scope.JsonData = data;
            mySharedService.carouselLock = true;
            $rootScope.$broadcast('carouselLockChange');
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'app/views/_AngularGridData.html',
                controller: 'AngularGridController',
                size: size.ModalSize,
                scope: $scope,
                backdrop: 'static'
            }).result.finally(function () {
                if (!mySharedService.IsZoom) {
                    $scope.BlurClass = "";
                    mySharedService.carouselLock = false;
                    $rootScope.$broadcast('carouselLockChange');
                }
            });
        } else {
            Notification.error({
                verticalSpacing: 60,
                message: 'Click functionality of the chart is disabled when you are reordering the charts!',
                title: '<i class="fa fa-warning"></i>&nbsp;Operation not allowed'
            });
        }
    };


    /*Listener function to invoke the DrillDown data grid*/
    $scope.$on('handleGridModalbroadcast', function () {
        		$scope.openDrilldownGrid(mySharedService.GridMethodParams, mySharedService.GridMethodParams.urlData,mySharedService.GridMethodParams.JsonData);
    });

    /*Listener function to handle the Loading message*/
    $scope.$on('handleshowBgLoadbroadcast', function () {
        $scope.isFetchingData = mySharedService.isBgShow;
        if (mySharedService.isBgShow == true) {
            $scope.BlurClass ="BlurClass";
        }
        else {
            $scope.BlurClass ="";
        }
    });
	
	 $scope.$on('GotUserDetails', function () {
        $scope.UserInfo = JSON.parse(sessionStorage.UserInfo);
            $scope.IsAdmin = $scope.UserInfo.adminDet.isAdmin;
            
            $scope.GetUserCompleteUserData($scope.UserInfo);
    });

    /*Main Function that fetches data from "api/Dashboard/GetUserDetails" url*/
    $scope.MFGetUserSettingsDetails = function () {
//	console.log("sessionStorage.UserInfo" + sessionStorage.UserInfo)
	if (sessionStorage.UserInfo) {
		$scope.UserInfo = JSON.parse(sessionStorage.UserInfo);
		console.log("SESSION VALIDATIES USER " + $scope.UserInfo.userID );
		if($scope.UserInfo.userID == "null"){
			sessionStorage.UserInfo = "";
			 $scope.UserInfo= "";
		}
	}
	/*if (!sessionStorage.UserInfo) {		
		var temLogUserStr = localStorage.getItem(appLocStrUsrVar);
		if(temLogUserStr){
			sessionStorage.UserInfo = temLogUserStr;
			$scope.UserInfo = JSON.parse(temLogUserStr);			
		}
	}*/
        if (!sessionStorage.UserInfo) {
			var epoTsmp = getEpocStamp();
			console.log("AD USER VAILDATION ");
            $http({ method: 'GET', url: uriApiURL + "api/Dashboard/GetUserDetails?version="+ epoTsmp })
			.success(function (response) {
           
				
                $scope.UserInfo = angular.fromJson(response);
				if($scope.UserInfo.userID = "null"){
						$location.path('/login');
				}else{
				$scope.IsAdmin = $scope.UserInfo.adminDet.isAdmin;
				 sessionStorage.UserInfo = JSON.stringify($scope.UserInfo);
				 mySharedService.StartDateAvailable = $scope.UserInfo.StartRecordTimeStamp;
				 mySharedService.EndDateAvailable = $scope.UserInfo.LastModifiedDate;
                 mySharedService.SetUserDetail(sessionStorage.UserInfo);
                  $scope.GetUserCompleteUserData($scope.UserInfo);
				}
				
            }).error(function (response) {
				console.log("FAIL LO")	;
				$scope.UserInfo = {userID : "dbError"}
				
			});
        }
        else {
            $scope.UserInfo = JSON.parse(sessionStorage.UserInfo);
            $scope.IsAdmin = $scope.UserInfo.adminDet.isAdmin;
            mySharedService.SetUserDetail(sessionStorage.UserInfo);
            $scope.GetUserCompleteUserData($scope.UserInfo);
			
			
        }
		//$scope.UserInfoJS = angular.toJson($scope.UserInfo, true);
		//console.log("US IN" + $scope.UserInfoJS);
		
		
    }
    /*Function Declaration and definition ends*/

    /*Function call begins*/
    /*Main function to fetch data from database is called*/
    $scope.MFGetUserSettingsDetails();
    /*Function call ends*/
});



appDashboard.controller('videoPlayController', function ($scope, $window, $uibModalInstance ) {

$scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
   
})