 appDashboard.controller('AdminController', function ($scope, $http, $location, limitToFilter, Notification, $window) {
     /*AdminController related to the _Admin.html for Admin activities */
     /*Variable Declaration begins*/
     $scope.AdminGridItems = [
         {
             id: 1,
             Icon: "",
             Header: "Base KPI Column Mapping",
             sizeX: 1,
             sizeY: 1,
             row: 0,
             col: 0
            },
         {
             id: 2,
             Icon: "",
             Header: "Master Column Mapping",
             sizeX: 1,
             sizeY: 1,
             row: 0,
             col: 1
            }
		];
     /*Variable Declaration ends*/
     /*Function Declaration and definition begins*/
     /*Main Function to Navigate either to BaseKPI Column Mapping section or to the Maseter Column Mapping section*/
     $scope.navToMasterColMap = function (id) {
             if (id == 1) {
                 $location.path("/BaseKPIColMap");
             } else {
                 $location.path("/MasterColMap");
             }
         }
         /*Function Declaration and definition begins*/
 })