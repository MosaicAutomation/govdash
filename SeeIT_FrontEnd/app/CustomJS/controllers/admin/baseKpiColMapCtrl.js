appDashboard.controller('BaseKPIColMapController', function ($scope, $http, limitToFilter, Notification, $window) {
    /*BaseKPIColMapController related to the _BaseKPIColMap.html for mapping the columns related to each Base KPI*/
    /*Variable Declaration begins*/
    $scope.showsort = false;
    $scope.showsave = false;
    $scope.list1 = {};
    $scope.list2 = {};
    $scope.LoadingData = false;
    $scope.noResults = false;
    $scope.isFetchingData = false;
	$scope.onSelectedName ="";
    $scope.sortableOptions = {
        placeholder: "app",
        connectWith: ".apps-container"
    };
	
	

    $scope.showsort = false;
	
	/*Variable Declaration begins*/
	$scope.WidgetObj = [
        	{"name":"Average Response Time Analysis","avaial" : "success", value:"BKPI0001" , "space" : 3},
			{"name":"Average Resolution Time Analysis","avaial" : "warning", value:"BKPI0008" , "space" : 3},
			{"name":"Response Violation Trend","avaial" : "success", value:"BKPI0005"  , "space" : 3},
        	{"name":"Resolution Violation Trend","avaial" : "warning", value:"BKPI0006"  , "space" : 3},
        	{"name":"Incoming vs Completed Trend","avaial" : "success", value:"BKPI0002" , "space" : 3},
        	{"name":"Aging Report","avaial" : "success", value:"BKPI0009"  , "space" : 3},
        	{"name":"Word Cloud","avaial" : "success", value:"BKPI0009"  , "space" : 3},
			{"name":"Day HeatMap","avaial" : "success", value:"BKPI0009"  , "space" : 3},
			{"name":"Week HeatMap","avaial" : "success", value:"BKPI0009"  , "space" : 6},
        	{"name":"Pareto Analysis","avaial" : "success", value:"BKPI0003" , "space" : 6},
        	{"name":"Pivot Table","avaial" : "success", value:"BKPI0004" , "space" : 6},
        	{"name":"Scoreboard/Count","avaial" : "success", value:"BKPI0007"  , "space" : 6},
        ];
		
		/*Variable Declaration begins*/
    /*Variable Declaration begins*/
    /*Function Declaration and definition begins*/
    /*Main Function to get the Base KPI Column data*/
    $scope.GetBaseKPI = function () {
            var epoTsmp = "version=" + getEpocStamp();
            var url = uriApiURL + 'api/Administrator/GetBaseKPICollection?' + epoTsmp + '&Keyword=';
            $scope.getsuggesstion = function (sug) {
                return $http.get(url + sug, {
                        SearchJson: sug
                    })
                    .then(function (response) {
                        var WholeData = angular.fromJson(response.data.m_StringValue);
                        return limitToFilter(WholeData.BKPIDetails, 10);
                    });
            };
        }
        /*Function called on select of the given Base KPI value from autocomplete*/
    $scope.onSelect = function (item) {
		$scope.onSelectedName  = item;
        var epoTsmp = "&version=" + getEpocStamp();
        $http({
                method: 'GET',
                url: uriApiURL + 'api/Administrator/GetBaseKPIColumnMapping?Keyword=' + item + epoTsmp,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
            .success(function (response) {
                $scope.showsort = true;
                $scope.showsave = true;
                $scope.WholeData = angular.fromJson(response.m_StringValue);
                $scope.YesJSON = $scope.WholeData.ColumnData[0].YesJSON;
                $scope.NoJSON = $scope.WholeData.ColumnData[1].NoJSON;
                $scope.oldYes = JSON.stringify($scope.WholeData.ColumnData[0].YesJSON)
                $scope.oldNo = JSON.stringify($scope.WholeData.ColumnData[1].NoJSON)
                $scope.list1 = $scope.YesJSON;
                $scope.list2 = $scope.NoJSON;
            });
    };
	$scope.onSelectedName  = $scope.WidgetObj[0].name;
	$scope.onSelect($scope.WidgetObj[0].name); 
    /*Function called to reset the columns*/
    $scope.reset = function () {
            $scope.list1 = JSON.parse($scope.oldYes);
            $scope.list2 = JSON.parse($scope.oldNo);
        }
        /*Function to save the column mapping settings*/
    $scope.save = function () {
        $scope.isFetchingData = true;
        var arr = [];
        for (var i = 0; i < $scope.list1.length; i++) {
            $scope.list1[i].SortOrder = i + 1;
            arr.push($scope.AdminDetails($scope.list1[i]));
        }

        var NoArr = [];
        for (var i = 0; i < $scope.list2.length; i++) {

            NoArr.push($scope.AdminDetails($scope.list2[i]));
        }
        var obj1 = {};
        obj1 = {
            "Admin": NoArr
        };
        var obj = {};
        obj = {
            "Admin": arr
        };

        //$http({
        //        method: 'POST',
        //        //url: uriApiURL + 'api/Administrator/SaveBaseKPIColumnMapping?name=' + name + '&YesArray=' + (JSON.stringify(obj)) + epoTsmp + '&NoArray=' + (JSON.stringify(obj1)),
        //        url:url,
        //        headers: {
        //            'Content-Type': 'application/x-www-form-urlencoded'
        //        }
        //    })
        var name = $scope.onSelectedName;
        var epoTsmp = "&version=" + getEpocStamp();
        var url = uriApiURL + "api/Administrator/SaveBaseKPIColumnMapping";
     
        var SaveColumnMapping = { 'name': name, 'YesArray': JSON.stringify(obj), 'NoArray': JSON.stringify(obj1) };

        $http({
            method: 'POST',
            url: url,
            params: { version: getEpocStamp() },
            data: SaveColumnMapping,
            contentType: "application/json",
            dataType: "json"

        }).success(function (response) {
                $scope.isFetchingData = false;
                Notification.success({
                    message: 'Changes saved successfully.'
                });

            });
    };
    /*Function to validate given values*/
    $scope.AdminDetails = function (list) {
            var self = {};
            self.ColID = list.ColID;
            self.DispColName = list.DispColName;
            self.IsActive = list.IsActive;
            self.ColName = list.ColName;
            self.SortOrder = list.SortOrder;
            return self;
        }
        /*Function Declaration and definition ends*/
        /*Function call begins*/
    $scope.GetBaseKPI();
    /*Function call ends*/
})