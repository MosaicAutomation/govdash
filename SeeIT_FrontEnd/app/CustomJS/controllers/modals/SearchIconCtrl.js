appDashboard.controller('SearchIconController', function ($scope, $uibModalInstance, $http, mySharedService) {
        $scope.cancelModal = function () {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.chosenIconName = "";
        $scope.chosenIconfunc = function (value) {
            $scope.chosenIconName = "fa-" + value;
            mySharedService.chosenIconfunc($scope.chosenIconName);
            $uibModalInstance.dismiss('cancel');
        }
        $scope.iconList = [{
                "Tag": "glass|martini|drink|bar|alcohol|liquor",
                "Icon": "glass"
			},
            {
                "Tag": "music|note|sound",
                "Icon": "music"
			},
            {
                "Tag": "search|magnify|zoom|enlarge|bigger",
                "Icon": "search"
			},
            {
                "Tag": "envelope-o|email|support|e-mail|letter|mail|notification",
                "Icon": "envelope-o"
			},
            {
                "Tag": "heart|love|like|favorite",
                "Icon": "heart"
			},
            {
                "Tag": "star|award|achievement|night|rating|score",
                "Icon": "star"
			},
            {
                "Tag": "star-o|award|achievement|night|rating|score",
                "Icon": "star-o"
			},
            {
                "Tag": "user|person|man|head|profile",
                "Icon": "user"
			},
            {
                "Tag": "film|movie",
                "Icon": "film"
			},
            {
                "Tag": "th-large|blocks|squares|boxes",
                "Icon": "th-large"
			},
            {
                "Tag": "th|blocks|squares|boxes",
                "Icon": "th"
			},
            {
                "Tag": "th-list|ul|ol|checklist|finished|completed|done|todo",
                "Icon": "th-list"
			},
            {
                "Tag": "check|checkmark|done|todo|agree|accept|confirm",
                "Icon": "check"
			},
            {
                "Tag": "times|remove|close|close|exit|x",
                "Icon": "times"
			},
            {
                "Tag": "search-plus|magnify|zoom|enlarge|bigger",
                "Icon": "search-plus"
			},
            {
                "Tag": "search-minus|magnify|minify|zoom|smaller",
                "Icon": "search-minus"
			},
            {
                "Tag": "power-off|on",
                "Icon": "power-off"
			},
            {
                "Tag": "signal",
                "Icon": "signal"
			},
            {
                "Tag": "cog|gear|settings",
                "Icon": "cog"
			},
            {
                "Tag": "trash-o|garbage|delete|remove|trash|hide",
                "Icon": "trash-o"
			},
            {
                "Tag": "home|main|house",
                "Icon": "home"
			},
            {
                "Tag": "file-o|new|page|pdf|document",
                "Icon": "file-o"
			},
            {
                "Tag": "clock-o|watch|timer|late|timestamp",
                "Icon": "clock-o"
			},
            {
                "Tag": "road|street",
                "Icon": "road"
			},
            {
                "Tag": "download|import",
                "Icon": "download"
			},
            {
                "Tag": "arrow-circle-o-down|download",
                "Icon": "arrow-circle-o-down"
			},
            {
                "Tag": "arrow-circle-o-up",
                "Icon": "arrow-circle-o-up"
			},
            {
                "Tag": "inbox",
                "Icon": "inbox"
			},
            {
                "Tag": "play-circle-o",
                "Icon": "play-circle-o"
			},
            {
                "Tag": "repeat|rotate-right|redo|forward",
                "Icon": "repeat"
			},
            {
                "Tag": "refresh|reload",
                "Icon": "refresh"
			},
            {
                "Tag": "list-alt|ul|ol|checklist|finished|completed|done|todo",
                "Icon": "list-alt"
			},
            {
                "Tag": "lock|protect|admin",
                "Icon": "lock"
			},
            {
                "Tag": "flag|report|notification|notify",
                "Icon": "flag"
			},
            {
                "Tag": "headphones|sound|listen|music",
                "Icon": "headphones"
			},
            {
                "Tag": "volume-off|mute|sound|music",
                "Icon": "volume-off"
			},
            {
                "Tag": "volume-down|lower|quieter|sound|music",
                "Icon": "volume-down"
			},
            {
                "Tag": "volume-up|higher|louder|sound|music",
                "Icon": "volume-up"
			},
            {
                "Tag": "qrcode|scan",
                "Icon": "qrcode"
			},
            {
                "Tag": "barcode|scan",
                "Icon": "barcode"
			},
            {
                "Tag": "tag|label",
                "Icon": "tag"
			},
            {
                "Tag": "tags|labels",
                "Icon": "tags"
			},
            {
                "Tag": "book|read|documentation",
                "Icon": "book"
			},
            {
                "Tag": "bookmark|save",
                "Icon": "bookmark"
			},
            {
                "Tag": "print",
                "Icon": "print"
			},
            {
                "Tag": "camera|photo|picture|record",
                "Icon": "camera"
			},
            {
                "Tag": "font|text",
                "Icon": "font"
			},
            {
                "Tag": "bold",
                "Icon": "bold"
			},
            {
                "Tag": "italic|italics",
                "Icon": "italic"
			},
            {
                "Tag": "text-height",
                "Icon": "text-height"
			},
            {
                "Tag": "text-width",
                "Icon": "text-width"
			},
            {
                "Tag": "align-left|text",
                "Icon": "align-left"
			},
            {
                "Tag": "align-center|middle|text",
                "Icon": "align-center"
			},
            {
                "Tag": "align-right|text",
                "Icon": "align-right"
			},
            {
                "Tag": "align-justify|text",
                "Icon": "align-justify"
			},
            {
                "Tag": "list|ul|ol|checklist|finished|completed|done|todo",
                "Icon": "list"
			},
            {
                "Tag": "outdent|dedent",
                "Icon": "outdent"
			},
            {
                "Tag": "indent",
                "Icon": "indent"
			},
            {
                "Tag": "video-camera|film|movie|record",
                "Icon": "video-camera"
			},
            {
                "Tag": "picture-o|photo|image",
                "Icon": "picture-o"
			},
            {
                "Tag": "pencil|write|edit|update",
                "Icon": "pencil"
			},
            {
                "Tag": "map-marker|map|pin|location|coordinates|localize|address|travel|where|place",
                "Icon": "map-marker"
			},
            {
                "Tag": "adjust|contrast",
                "Icon": "adjust"
			},
            {
                "Tag": "tint|raindrop",
                "Icon": "tint"
			},
            {
                "Tag": "pencil-square-o|edit|write|edit|update",
                "Icon": "pencil-square-o"
			},
            {
                "Tag": "share-square-o|social|send",
                "Icon": "share-square-o"
			},
            {
                "Tag": "check-square-o|todo|done|agree|accept|confirm",
                "Icon": "check-square-o"
			},
            {
                "Tag": "arrows|move|reorder|resize",
                "Icon": "arrows"
			},
            {
                "Tag": "step-backward|rewind|previous|beginning|start|first",
                "Icon": "step-backward"
			},
            {
                "Tag": "fast-backward|rewind|previous|beginning|start|first",
                "Icon": "fast-backward"
			},
            {
                "Tag": "backward|rewind|previous",
                "Icon": "backward"
			},
            {
                "Tag": "play|start|playing|music|sound",
                "Icon": "play"
			},
            {
                "Tag": "pause|wait",
                "Icon": "pause"
			},
            {
                "Tag": "stop|block|box|square",
                "Icon": "stop"
			},
            {
                "Tag": "forward|forward|next",
                "Icon": "forward"
			},
            {
                "Tag": "fast-forward|next|end|last",
                "Icon": "fast-forward"
			},
            {
                "Tag": "step-forward|next|end|last",
                "Icon": "step-forward"
			},
            {
                "Tag": "eject",
                "Icon": "eject"
			},
            {
                "Tag": "chevron-left|bracket|previous|back",
                "Icon": "chevron-left"
			},
            {
                "Tag": "chevron-right|bracket|next|forward",
                "Icon": "chevron-right"
			},
            {
                "Tag": "plus-circle|add|new|create|expand",
                "Icon": "plus-circle"
			},
            {
                "Tag": "minus-circle|delete|remove|trash|hide",
                "Icon": "minus-circle"
			},
            {
                "Tag": "times-circle|close|exit|x",
                "Icon": "times-circle"
			},
            {
                "Tag": "check-circle|todo|done|agree|accept|confirm",
                "Icon": "check-circle"
			},
            {
                "Tag": "question-circle|help|information|unknown|support",
                "Icon": "question-circle"
			},
            {
                "Tag": "info-circle|help|information|more|details",
                "Icon": "info-circle"
			},
            {
                "Tag": "crosshairs|picker",
                "Icon": "crosshairs"
			},
            {
                "Tag": "times-circle-o|close|exit|x",
                "Icon": "times-circle-o"
			},
            {
                "Tag": "check-circle-o|todo|done|agree|accept|confirm",
                "Icon": "check-circle-o"
			},
            {
                "Tag": "ban|block|abort",
                "Icon": "ban"
			},
            {
                "Tag": "arrow-left|previous|back",
                "Icon": "arrow-left"
			},
            {
                "Tag": "arrow-right|next|forward",
                "Icon": "arrow-right"
			},
            {
                "Tag": "arrow-up",
                "Icon": "arrow-up"
			},
            {
                "Tag": "arrow-down|download",
                "Icon": "arrow-down"
			},
            {
                "Tag": "share|mail-forward",
                "Icon": "share"
			},
            {
                "Tag": "expand|enlarge|bigger|resize",
                "Icon": "expand"
			},
            {
                "Tag": "compress|combine|merge|smaller",
                "Icon": "compress"
			},
            {
                "Tag": "plus|add|new|create|expand",
                "Icon": "plus"
			},
            {
                "Tag": "minus|hide|minify|delete|remove|trash|hide|collapse",
                "Icon": "minus"
			},
            {
                "Tag": "asterisk|details",
                "Icon": "asterisk"
			},
            {
                "Tag": "exclamation-circle|warning|error|problem|notification|alert",
                "Icon": "exclamation-circle"
			},
            {
                "Tag": "gift|present",
                "Icon": "gift"
			},
            {
                "Tag": "leaf|eco|nature",
                "Icon": "leaf"
			},
            {
                "Tag": "fire|flame|hot|popular",
                "Icon": "fire"
			},
            {
                "Tag": "eye|show|visible|views",
                "Icon": "eye"
			},
            {
                "Tag": "eye-slash|toggle|show|hide|visible|visiblity|views",
                "Icon": "eye-slash"
			},
            {
                "Tag": "exclamation-triangle|warning|warning|error|problem|notification|alert",
                "Icon": "exclamation-triangle"
			},
            {
                "Tag": "plane|travel|trip|location|destination|airplane|fly|mode",
                "Icon": "plane"
			},
            {
                "Tag": "calendar|date|time|when",
                "Icon": "calendar"
			},
            {
                "Tag": "random|sort",
                "Icon": "random"
			},
            {
                "Tag": "comment|speech|notification|note",
                "Icon": "comment"
			},
            {
                "Tag": "magnet",
                "Icon": "magnet"
			},
            {
                "Tag": "chevron-up",
                "Icon": "chevron-up"
			},
            {
                "Tag": "chevron-down",
                "Icon": "chevron-down"
			},
            {
                "Tag": "retweet|refresh|reload|share",
                "Icon": "retweet"
			},
            {
                "Tag": "shopping-cart|checkout|buy|purchase|payment",
                "Icon": "shopping-cart"
			},
            {
                "Tag": "folder",
                "Icon": "folder"
			},
            {
                "Tag": "folder-open",
                "Icon": "folder-open"
			},
            {
                "Tag": "arrows-v|resize",
                "Icon": "arrows-v"
			},
            {
                "Tag": "arrows-h|resize",
                "Icon": "arrows-h"
			},
            {
                "Tag": "bar-chart|bar-chart-o|graph",
                "Icon": "bar-chart"
			},
            {
                "Tag": "camera-retro|photo|picture|record",
                "Icon": "camera-retro"
			},
            {
                "Tag": "key|unlock|password",
                "Icon": "key"
			},
            {
                "Tag": "cogs|gears|settings",
                "Icon": "cogs"
			},
            {
                "Tag": "comments|conversation|notification|notes",
                "Icon": "comments"
			},
            {
                "Tag": "thumbs-o-up|like|approve|favorite|agree",
                "Icon": "thumbs-o-up"
			},
            {
                "Tag": "thumbs-o-down|dislike|disapprove|disagree",
                "Icon": "thumbs-o-down"
			},
            {
                "Tag": "star-half|award|achievement|rating|score",
                "Icon": "star-half"
			},
            {
                "Tag": "heart-o|love|like|favorite",
                "Icon": "heart-o"
			},
            {
                "Tag": "sign-out|log out|logout|leave|exit|arrow",
                "Icon": "sign-out"
			},
            {
                "Tag": "linkedin-square",
                "Icon": "linkedin-square"
			},
            {
                "Tag": "thumb-tack|marker|pin|location|coordinates",
                "Icon": "thumb-tack"
			},
            {
                "Tag": "external-link|open|new",
                "Icon": "external-link"
			},
            {
                "Tag": "sign-in|enter|join|sign up|sign in|signin|signup|arrow",
                "Icon": "sign-in"
			},
            {
                "Tag": "trophy|award|achievement|winner|game",
                "Icon": "trophy"
			},
            {
                "Tag": "upload|import",
                "Icon": "upload"
			},
            {
                "Tag": "lemon-o",
                "Icon": "lemon-o"
			},
            {
                "Tag": "phone|call|voice|number|support",
                "Icon": "phone"
			},
            {
                "Tag": "square-o|block|square|box",
                "Icon": "square-o"
			},
            {
                "Tag": "bookmark-o|save",
                "Icon": "bookmark-o"
			},
            {
                "Tag": "phone-square|call|voice|number|support",
                "Icon": "phone-square"
			},
            {
                "Tag": "unlock|protect|admin|password",
                "Icon": "unlock"
			},
            {
                "Tag": "credit-card|money|buy|debit|checkout|purchase|payment",
                "Icon": "credit-card"
			},
            {
                "Tag": "rss|feed|blog",
                "Icon": "rss"
			},
            {
                "Tag": "hdd-o|harddrive|hard drive|storage|save",
                "Icon": "hdd-o"
			},
            {
                "Tag": "bullhorn|announcement|share|broadcast|louder",
                "Icon": "bullhorn"
			},
            {
                "Tag": "bell|alert|reminder|notification",
                "Icon": "bell"
			},
            {
                "Tag": "certificate|badge|star",
                "Icon": "certificate"
			},
            {
                "Tag": "hand-o-right|point|right|next|forward",
                "Icon": "hand-o-right"
			},
            {
                "Tag": "hand-o-left|point|left|previous|back",
                "Icon": "hand-o-left"
			},
            {
                "Tag": "hand-o-up|point",
                "Icon": "hand-o-up"
			},
            {
                "Tag": "hand-o-down|point",
                "Icon": "hand-o-down"
			},
            {
                "Tag": "arrow-circle-left|previous|back",
                "Icon": "arrow-circle-left"
			},
            {
                "Tag": "arrow-circle-right|next|forward",
                "Icon": "arrow-circle-right"
			},
            {
                "Tag": "arrow-circle-up",
                "Icon": "arrow-circle-up"
			},
            {
                "Tag": "arrow-circle-down|download",
                "Icon": "arrow-circle-down"
			},
            {
                "Tag": "globe|world|planet|map|place|travel|earth|global|translate|all|language|localize|location|coordinates|country",
                "Icon": "globe"
			},
            {
                "Tag": "wrench|settings|fix|update",
                "Icon": "wrench"
			},
            {
                "Tag": "tasks|progress|loading|downloading|downloads|settings",
                "Icon": "tasks"
			},
            {
                "Tag": "filter|funnel|options",
                "Icon": "filter"
			},
            {
                "Tag": "briefcase|work|business|office|luggage|bag",
                "Icon": "briefcase"
			},
            {
                "Tag": "arrows-alt|expand|enlarge|bigger|move|reorder|resize",
                "Icon": "arrows-alt"
			},
            {
                "Tag": "users|group|people|profiles|persons",
                "Icon": "users"
			},
            {
                "Tag": "link|chain|chain",
                "Icon": "link"
			},
            {
                "Tag": "cloud|save",
                "Icon": "cloud"
			},
            {
                "Tag": "flask|science|beaker|experimental|labs",
                "Icon": "flask"
			},
            {
                "Tag": "scissors|cut",
                "Icon": "scissors"
			},
            {
                "Tag": "files-o|copy|duplicate",
                "Icon": "files-o"
			},
            {
                "Tag": "paperclip|attachment",
                "Icon": "paperclip"
			},
            {
                "Tag": "floppy-o|save",
                "Icon": "floppy-o"
			},
            {
                "Tag": "square|block|box",
                "Icon": "square"
			},
            {
                "Tag": "bars|navicon|reorder|menu|drag|reorder|settings|list|ul|ol|checklist|todo|list",
                "Icon": "bars"
			},
            {
                "Tag": "list-ul|ul|ol|checklist|todo|list",
                "Icon": "list-ul"
			},
            {
                "Tag": "list-ol|ul|ol|checklist|list|todo|list|numbers",
                "Icon": "list-ol"
			},
            {
                "Tag": "strikethrough",
                "Icon": "strikethrough"
			},
            {
                "Tag": "underline",
                "Icon": "underline"
			},
            {
                "Tag": "table|data|excel|spreadsheet",
                "Icon": "table"
			},
            {
                "Tag": "magic|wizard|automatic|autocomplete",
                "Icon": "magic"
			},
            {
                "Tag": "truck|shipping",
                "Icon": "truck"
			},
            {
                "Tag": "pinterest",
                "Icon": "pinterest"
			},
            {
                "Tag": "pinterest-square",
                "Icon": "pinterest-square"
			},
            {
                "Tag": "google-plus-square|social network",
                "Icon": "google-plus-square"
			},
            {
                "Tag": "google-plus|social network",
                "Icon": "google-plus"
			},
            {
                "Tag": "money|cash|money|buy|checkout|purchase|payment",
                "Icon": "money"
			},
            {
                "Tag": "caret-down|more|dropdown|menu",
                "Icon": "caret-down"
			},
            {
                "Tag": "caret-up",
                "Icon": "caret-up"
			},
            {
                "Tag": "caret-left|previous|back",
                "Icon": "caret-left"
			},
            {
                "Tag": "caret-right|next|forward",
                "Icon": "caret-right"
			},
            {
                "Tag": "columns|split|panes",
                "Icon": "columns"
			},
            {
                "Tag": "sort|unsorted|order",
                "Icon": "sort"
			},
            {
                "Tag": "sort-desc|sort-down|dropdown|more|menu",
                "Icon": "sort-desc"
			},
            {
                "Tag": "sort-asc|sort-up",
                "Icon": "sort-asc"
			},
            {
                "Tag": "envelope",
                "Icon": "envelope"
			},
            {
                "Tag": "linkedin",
                "Icon": "linkedin"
			},
            {
                "Tag": "undo|rotate-left|back",
                "Icon": "undo"
			},
            {
                "Tag": "gavel|legal",
                "Icon": "gavel"
			},
            {
                "Tag": "tachometer|dashboard",
                "Icon": "tachometer"
			},
            {
                "Tag": "comment-o|notification|note",
                "Icon": "comment-o"
			},
            {
                "Tag": "comments-o|conversation|notification|notes",
                "Icon": "comments-o"
			},
            {
                "Tag": "bolt|flash|lightning|weather",
                "Icon": "bolt"
			},
            {
                "Tag": "sitemap|directory|hierarchy|organization",
                "Icon": "sitemap"
			},
            {
                "Tag": "umbrella",
                "Icon": "umbrella"
			},
            {
                "Tag": "clipboard|paste|copy",
                "Icon": "clipboard"
			},
            {
                "Tag": "lightbulb-o|idea|inspiration",
                "Icon": "lightbulb-o"
			},
            {
                "Tag": "exchange",
                "Icon": "exchange"
			},
            {
                "Tag": "cloud-download|import",
                "Icon": "cloud-download"
			},
            {
                "Tag": "cloud-upload|import",
                "Icon": "cloud-upload"
			},
            {
                "Tag": "user-md|doctor|profile|medical|nurse",
                "Icon": "user-md"
			},
            {
                "Tag": "stethoscope",
                "Icon": "stethoscope"
			},
            {
                "Tag": "suitcase|trip|luggage|travel|move|baggage",
                "Icon": "suitcase"
			},
            {
                "Tag": "bell-o|alert|reminder|notification",
                "Icon": "bell-o"
			},
            {
                "Tag": "coffee|morning|mug|breakfast|tea|drink|cafe",
                "Icon": "coffee"
			},
            {
                "Tag": "cutlery|food|restaurant|spoon|knife|dinner|eat",
                "Icon": "cutlery"
			},
            {
                "Tag": "file-text-o|new|page|pdf|document",
                "Icon": "file-text-o"
			},
            {
                "Tag": "building-o|work|business|apartment|office",
                "Icon": "building-o"
			},
            {
                "Tag": "hospital-o|building",
                "Icon": "hospital-o"
			},
            {
                "Tag": "ambulance|support|help",
                "Icon": "ambulance"
			},
            {
                "Tag": "medkit|first aid|firstaid|help|support",
                "Icon": "medkit"
			},
            {
                "Tag": "fighter-jet|fly|plane|airplane|quick|fast|travel",
                "Icon": "fighter-jet"
			},
            {
                "Tag": "beer|alcohol|stein|drink|mug|bar|liquor",
                "Icon": "beer"
			},
            {
                "Tag": "h-square|hospital|hotel",
                "Icon": "h-square"
			},
            {
                "Tag": "plus-square|add|new|create|expand",
                "Icon": "plus-square"
			},
            {
                "Tag": "angle-double-left|laquo|quote|previous|back",
                "Icon": "angle-double-left"
			},
            {
                "Tag": "angle-double-right|raquo|quote|next|forward",
                "Icon": "angle-double-right"
			},
            {
                "Tag": "angle-double-up",
                "Icon": "angle-double-up"
			},
            {
                "Tag": "angle-double-down",
                "Icon": "angle-double-down"
			},
            {
                "Tag": "angle-left|previous|back",
                "Icon": "angle-left"
			},
            {
                "Tag": "angle-right|next|forward",
                "Icon": "angle-right"
			},
            {
                "Tag": "angle-up",
                "Icon": "angle-up"
			},
            {
                "Tag": "angle-down",
                "Icon": "angle-down"
			},
            {
                "Tag": "desktop|monitor|screen|desktop|computer|demo|device",
                "Icon": "desktop"
			},
            {
                "Tag": "laptop|demo|computer|device",
                "Icon": "laptop"
			},
            {
                "Tag": "tablet|ipad|device",
                "Icon": "tablet"
			},
            {
                "Tag": "mobile|mobile-phone|cell phone|cellphone|text|call|iphone|number",
                "Icon": "mobile"
			},
            {
                "Tag": "circle-o",
                "Icon": "circle-o"
			},
            {
                "Tag": "quote-left",
                "Icon": "quote-left"
			},
            {
                "Tag": "quote-right",
                "Icon": "quote-right"
			},
            {
                "Tag": "spinner|loading|progress",
                "Icon": "spinner"
			},
            {
                "Tag": "circle|dot|notification",
                "Icon": "circle"
			},
            {
                "Tag": "reply|mail-reply",
                "Icon": "reply"
			},
            {
                "Tag": "github-alt|octocat",
                "Icon": "github-alt"
			},
            {
                "Tag": "folder-o",
                "Icon": "folder-o"
			},
            {
                "Tag": "folder-open-o",
                "Icon": "folder-open-o"
			},
            {
                "Tag": "smile-o|emoticon|happy|approve|satisfied|rating",
                "Icon": "smile-o"
			},
            {
                "Tag": "frown-o|emoticon|sad|disapprove|rating",
                "Icon": "frown-o"
			},
            {
                "Tag": "meh-o|emoticon|rating|neutral",
                "Icon": "meh-o"
			},
            {
                "Tag": "gamepad|controller",
                "Icon": "gamepad"
			},
            {
                "Tag": "keyboard-o|type|input",
                "Icon": "keyboard-o"
			},
            {
                "Tag": "flag-o|report|notification",
                "Icon": "flag-o"
			},
            {
                "Tag": "flag-checkered|report|notification|notify",
                "Icon": "flag-checkered"
			},
            {
                "Tag": "terminal|command|prompt|code",
                "Icon": "terminal"
			},
            {
                "Tag": "code|html|brackets",
                "Icon": "code"
			},
            {
                "Tag": "reply-all|mail-reply-all",
                "Icon": "reply-all"
			},
            {
                "Tag": "star-half-o|star-half-empty|star-half-full|award|achievement|rating|score",
                "Icon": "star-half-o"
			},
            {
                "Tag": "location-arrow|map|coordinates|location|address|place|where",
                "Icon": "location-arrow"
			},
            {
                "Tag": "crop",
                "Icon": "crop"
			},
            {
                "Tag": "code-fork|git|fork|vcs|svn|github|rebase|version|merge",
                "Icon": "code-fork"
			},
            {
                "Tag": "chain-broken|unlink|remove",
                "Icon": "chain-broken"
			},
            {
                "Tag": "question|help|information|unknown|support",
                "Icon": "question"
			},
            {
                "Tag": "info|help|information|more|details",
                "Icon": "info"
			},
            {
                "Tag": "exclamation|warning|error|problem|notification|notify|alert",
                "Icon": "exclamation"
			},
            {
                "Tag": "superscript|exponential",
                "Icon": "superscript"
			},
            {
                "Tag": "subscript",
                "Icon": "subscript"
			},
            {
                "Tag": "eraser",
                "Icon": "eraser"
			},
            {
                "Tag": "puzzle-piece|addon|add-on|section",
                "Icon": "puzzle-piece"
			},
            {
                "Tag": "microphone|record|voice|sound",
                "Icon": "microphone"
			},
            {
                "Tag": "microphone-slash|record|voice|sound|mute",
                "Icon": "microphone-slash"
			},
            {
                "Tag": "shield|award|achievement|winner",
                "Icon": "shield"
			},
            {
                "Tag": "calendar-o|date|time|when",
                "Icon": "calendar-o"
			},
            {
                "Tag": "fire-extinguisher",
                "Icon": "fire-extinguisher"
			},
            {
                "Tag": "rocket|app",
                "Icon": "rocket"
			},
            {
                "Tag": "maxcdn",
                "Icon": "maxcdn"
			},
            {
                "Tag": "chevron-circle-left|previous|back",
                "Icon": "chevron-circle-left"
			},
            {
                "Tag": "chevron-circle-right|next|forward",
                "Icon": "chevron-circle-right"
			},
            {
                "Tag": "chevron-circle-up",
                "Icon": "chevron-circle-up"
			},
            {
                "Tag": "chevron-circle-down|more|dropdown|menu",
                "Icon": "chevron-circle-down"
			},
            {
                "Tag": "html5",
                "Icon": "html5"
			},
            {
                "Tag": "css3|code",
                "Icon": "css3"
			},
            {
                "Tag": "anchor|link",
                "Icon": "anchor"
			},
            {
                "Tag": "unlock-alt|protect|admin|password",
                "Icon": "unlock-alt"
			},
            {
                "Tag": "bullseye|target",
                "Icon": "bullseye"
			},
            {
                "Tag": "ellipsis-h|dots",
                "Icon": "ellipsis-h"
			},
            {
                "Tag": "ellipsis-v|dots",
                "Icon": "ellipsis-v"
			},
            {
                "Tag": "rss-square|feed|blog",
                "Icon": "rss-square"
			},
            {
                "Tag": "play-circle|start|playing",
                "Icon": "play-circle"
			},
            {
                "Tag": "ticket|movie|pass|support",
                "Icon": "ticket"
			},
            {
                "Tag": "minus-square|hide|minify|delete|remove|trash|hide|collapse",
                "Icon": "minus-square"
			},
            {
                "Tag": "minus-square-o|hide|minify|delete|remove|trash|hide|collapse",
                "Icon": "minus-square-o"
			},
            {
                "Tag": "level-up",
                "Icon": "level-up"
			},
            {
                "Tag": "level-down",
                "Icon": "level-down"
			},
            {
                "Tag": "check-square|checkmark|done|todo|agree|accept|confirm",
                "Icon": "check-square"
			},
            {
                "Tag": "pencil-square|write|edit|update",
                "Icon": "pencil-square"
			},
            {
                "Tag": "external-link-square|open|new",
                "Icon": "external-link-square"
			},
            {
                "Tag": "share-square|social|send",
                "Icon": "share-square"
			},
            {
                "Tag": "compass|safari|directory|menu|location",
                "Icon": "compass"
			},
            {
                "Tag": "caret-square-o-down|toggle-down|more|dropdown|menu",
                "Icon": "caret-square-o-down"
			},
            {
                "Tag": "caret-square-o-up|toggle-up",
                "Icon": "caret-square-o-up"
			},
            {
                "Tag": "caret-square-o-right|toggle-right|next|forward",
                "Icon": "caret-square-o-right"
			},
            {
                "Tag": "eur|euro",
                "Icon": "eur"
			},
            {
                "Tag": "gbp",
                "Icon": "gbp"
			},
            {
                "Tag": "usd|dollar",
                "Icon": "usd"
			},
            {
                "Tag": "inr|rupee",
                "Icon": "inr"
			},
            {
                "Tag": "jpy|cny|rmb|yen",
                "Icon": "jpy"
			},
            {
                "Tag": "rub|ruble|rouble",
                "Icon": "rub"
			},
            {
                "Tag": "krw|won",
                "Icon": "krw"
			},
            {
                "Tag": "btc|bitcoin",
                "Icon": "btc"
			},
            {
                "Tag": "file|new|page|pdf|document",
                "Icon": "file"
			},
            {
                "Tag": "file-text|new|page|pdf|document",
                "Icon": "file-text"
			},
            {
                "Tag": "sort-alpha-asc",
                "Icon": "sort-alpha-asc"
			},
            {
                "Tag": "sort-alpha-desc",
                "Icon": "sort-alpha-desc"
			},
            {
                "Tag": "sort-amount-asc",
                "Icon": "sort-amount-asc"
			},
            {
                "Tag": "sort-amount-desc",
                "Icon": "sort-amount-desc"
			},
            {
                "Tag": "sort-numeric-asc|numbers",
                "Icon": "sort-numeric-asc"
			},
            {
                "Tag": "sort-numeric-desc|numbers",
                "Icon": "sort-numeric-desc"
			},
            {
                "Tag": "thumbs-up|like|favorite|approve|agree",
                "Icon": "thumbs-up"
			},
            {
                "Tag": "thumbs-down|dislike|disapprove|disagree",
                "Icon": "thumbs-down"
			},
            {
                "Tag": "long-arrow-down",
                "Icon": "long-arrow-down"
			},
            {
                "Tag": "long-arrow-up",
                "Icon": "long-arrow-up"
			},
            {
                "Tag": "long-arrow-left|previous|back",
                "Icon": "long-arrow-left"
			},
            {
                "Tag": "long-arrow-right",
                "Icon": "long-arrow-right"
			},
            {
                "Tag": "apple|osx",
                "Icon": "apple"
			},
            {
                "Tag": "windows",
                "Icon": "windows"
			},
            {
                "Tag": "android",
                "Icon": "android"
			},
            {
                "Tag": "linux|tux",
                "Icon": "linux"
			},
            {
                "Tag": "trello",
                "Icon": "trello"
			},
            {
                "Tag": "female|woman|user|person|profile",
                "Icon": "female"
			},
            {
                "Tag": "male|man|user|person|profile",
                "Icon": "male"
			},
            {
                "Tag": "gratipay|gittip|heart|like|favorite|love",
                "Icon": "gratipay"
			},
            {
                "Tag": "sun-o|weather|contrast|lighter|brighten|day",
                "Icon": "sun-o"
			},
            {
                "Tag": "moon-o|night|darker|contrast",
                "Icon": "moon-o"
			},
            {
                "Tag": "archive|box|storage",
                "Icon": "archive"
			},
            {
                "Tag": "bug|report",
                "Icon": "bug"
			},
            {
                "Tag": "vk",
                "Icon": "vk"
			},
            {
                "Tag": "pagelines|leaf|leaves|tree|plant|eco|nature",
                "Icon": "pagelines"
			},
            {
                "Tag": "stack-exchange",
                "Icon": "stack-exchange"
			},
            {
                "Tag": "arrow-circle-o-right|next|forward",
                "Icon": "arrow-circle-o-right"
			},
            {
                "Tag": "arrow-circle-o-left|previous|back",
                "Icon": "arrow-circle-o-left"
			},
            {
                "Tag": "caret-square-o-left|toggle-left|previous|back",
                "Icon": "caret-square-o-left"
			},
            {
                "Tag": "dot-circle-o|target|bullseye|notification",
                "Icon": "dot-circle-o"
			},
            {
                "Tag": "wheelchair|handicap|person|accessibility|accessibile",
                "Icon": "wheelchair"
			},
            {
                "Tag": "try|turkish-lira",
                "Icon": "try"
			},
            {
                "Tag": "plus-square-o|add|new|create|expand",
                "Icon": "plus-square-o"
			},
            {
                "Tag": "space-shuttle",
                "Icon": "space-shuttle"
			},
            {
                "Tag": "slack",
                "Icon": "slack"
			},
            {
                "Tag": "envelope-square",
                "Icon": "envelope-square"
			},
            {
                "Tag": "openid",
                "Icon": "openid"
			},
            {
                "Tag": "university|institution|bank",
                "Icon": "university"
			},
            {
                "Tag": "graduation-cap|mortar-board",
                "Icon": "graduation-cap"
			},
            {
                "Tag": "delicious",
                "Icon": "delicious"
			},
            {
                "Tag": "pied-piper",
                "Icon": "pied-piper"
			},
            {
                "Tag": "pied-piper-alt",
                "Icon": "pied-piper-alt"
			},
            {
                "Tag": "language",
                "Icon": "language"
			},
            {
                "Tag": "fax",
                "Icon": "fax"
			},
            {
                "Tag": "building",
                "Icon": "building"
			},
            {
                "Tag": "child",
                "Icon": "child"
			},
            {
                "Tag": "paw",
                "Icon": "paw"
			},
            {
                "Tag": "spoon",
                "Icon": "spoon"
			},
            {
                "Tag": "cube",
                "Icon": "cube"
			},
            {
                "Tag": "cubes",
                "Icon": "cubes"
			},
            {
                "Tag": "behance",
                "Icon": "behance"
			},
            {
                "Tag": "behance-square",
                "Icon": "behance-square"
			},
            {
                "Tag": "steam",
                "Icon": "steam"
			},
            {
                "Tag": "steam-square",
                "Icon": "steam-square"
			},
            {
                "Tag": "recycle",
                "Icon": "recycle"
			},
            {
                "Tag": "car|automobile|vehicle",
                "Icon": "car"
			},
            {
                "Tag": "taxi|cab|vehicle",
                "Icon": "taxi"
			},
            {
                "Tag": "tree",
                "Icon": "tree"
			},
            {
                "Tag": "database",
                "Icon": "database"
			},
            {
                "Tag": "file-pdf-o",
                "Icon": "file-pdf-o"
			},
            {
                "Tag": "file-word-o",
                "Icon": "file-word-o"
			},
            {
                "Tag": "file-excel-o",
                "Icon": "file-excel-o"
			},
            {
                "Tag": "file-powerpoint-o",
                "Icon": "file-powerpoint-o"
			},
            {
                "Tag": "file-image-o|file-photo-o|file-picture-o",
                "Icon": "file-image-o"
			},
            {
                "Tag": "file-archive-o|file-zip-o",
                "Icon": "file-archive-o"
			},
            {
                "Tag": "file-audio-o|file-sound-o",
                "Icon": "file-audio-o"
			},
            {
                "Tag": "file-video-o|file-movie-o",
                "Icon": "file-video-o"
			},
            {
                "Tag": "file-code-o",
                "Icon": "file-code-o"
			},
            {
                "Tag": "vine",
                "Icon": "vine"
			},
            {
                "Tag": "codepen",
                "Icon": "codepen"
			},
            {
                "Tag": "jsfiddle",
                "Icon": "jsfiddle"
			},
            {
                "Tag": "life-ring|life-bouy|life-buoy|life-saver|support",
                "Icon": "life-ring"
			},
            {
                "Tag": "circle-o-notch",
                "Icon": "circle-o-notch"
			},
            {
                "Tag": "rebel|ra",
                "Icon": "rebel"
			},
            {
                "Tag": "empire|ge",
                "Icon": "empire"
			},
            {
                "Tag": "hacker-news",
                "Icon": "hacker-news"
			},
            {
                "Tag": "tencent-weibo",
                "Icon": "tencent-weibo"
			},
            {
                "Tag": "qq",
                "Icon": "qq"
			},
            {
                "Tag": "weixin|wechat",
                "Icon": "weixin"
			},
            {
                "Tag": "paper-plane|send",
                "Icon": "paper-plane"
			},
            {
                "Tag": "paper-plane-o|send-o",
                "Icon": "paper-plane-o"
			},
            {
                "Tag": "history",
                "Icon": "history"
			},
            {
                "Tag": "circle-thin|genderless",
                "Icon": "circle-thin"
			},
            {
                "Tag": "header",
                "Icon": "header"
			},
            {
                "Tag": "paragraph",
                "Icon": "paragraph"
			},
            {
                "Tag": "sliders",
                "Icon": "sliders"
			},
            {
                "Tag": "share-alt",
                "Icon": "share-alt"
			},
            {
                "Tag": "share-alt-square",
                "Icon": "share-alt-square"
			},
            {
                "Tag": "bomb",
                "Icon": "bomb"
			},
            {
                "Tag": "futbol-o|soccer-ball-o",
                "Icon": "futbol-o"
			},
            {
                "Tag": "tty",
                "Icon": "tty"
			},
            {
                "Tag": "binoculars",
                "Icon": "binoculars"
			},
            {
                "Tag": "plug",
                "Icon": "plug"
			},
            {
                "Tag": "twitch",
                "Icon": "twitch"
			},
            {
                "Tag": "yelp",
                "Icon": "yelp"
			},
            {
                "Tag": "newspaper-o",
                "Icon": "newspaper-o"
			},
            {
                "Tag": "wifi",
                "Icon": "wifi"
			},
            {
                "Tag": "calculator",
                "Icon": "calculator"
			},
            {
                "Tag": "bell-slash",
                "Icon": "bell-slash"
			},
            {
                "Tag": "bell-slash-o",
                "Icon": "bell-slash-o"
			},
            {
                "Tag": "trash",
                "Icon": "trash"
			},
            {
                "Tag": "copyright",
                "Icon": "copyright"
			},
            {
                "Tag": "at",
                "Icon": "at"
			},
            {
                "Tag": "eyedropper",
                "Icon": "eyedropper"
			},
            {
                "Tag": "paint-brush",
                "Icon": "paint-brush"
			},
            {
                "Tag": "birthday-cake",
                "Icon": "birthday-cake"
			},
            {
                "Tag": "area-chart",
                "Icon": "area-chart"
			},
            {
                "Tag": "pie-chart",
                "Icon": "pie-chart"
			},
            {
                "Tag": "line-chart",
                "Icon": "line-chart"
			},
            {
                "Tag": "lastfm",
                "Icon": "lastfm"
			},
            {
                "Tag": "lastfm-square",
                "Icon": "lastfm-square"
			},
            {
                "Tag": "toggle-off",
                "Icon": "toggle-off"
			},
            {
                "Tag": "toggle-on",
                "Icon": "toggle-on"
			},
            {
                "Tag": "bicycle|vehicle|bike",
                "Icon": "bicycle"
			},
            {
                "Tag": "bus|vehicle",
                "Icon": "bus"
			},
            {
                "Tag": "ioxhost",
                "Icon": "ioxhost"
			},
            {
                "Tag": "angellist",
                "Icon": "angellist"
			},
            {
                "Tag": "cc",
                "Icon": "cc"
			},
            {
                "Tag": "ils|shekel|sheqel",
                "Icon": "ils"
			},
            {
                "Tag": "meanpath",
                "Icon": "meanpath"
			},
            {
                "Tag": "buysellads",
                "Icon": "buysellads"
			},
            {
                "Tag": "connectdevelop",
                "Icon": "connectdevelop"
			},
            {
                "Tag": "dashcube",
                "Icon": "dashcube"
			},
            {
                "Tag": "forumbee",
                "Icon": "forumbee"
			},
            {
                "Tag": "leanpub",
                "Icon": "leanpub"
			},
            {
                "Tag": "sellsy",
                "Icon": "sellsy"
			},
            {
                "Tag": "shirtsinbulk",
                "Icon": "shirtsinbulk"
			},
            {
                "Tag": "simplybuilt",
                "Icon": "simplybuilt"
			},
            {
                "Tag": "skyatlas",
                "Icon": "skyatlas"
			},
            {
                "Tag": "cart-plus|add|shopping",
                "Icon": "cart-plus"
			},
            {
                "Tag": "cart-arrow-down|shopping",
                "Icon": "cart-arrow-down"
			},
            {
                "Tag": "diamond|gem|gemstone",
                "Icon": "diamond"
			},
            {
                "Tag": "ship|boat|sea",
                "Icon": "ship"
			},
            {
                "Tag": "user-secret|whisper|spy|incognito",
                "Icon": "user-secret"
			},
            {
                "Tag": "motorcycle|vehicle|bike",
                "Icon": "motorcycle"
			},
            {
                "Tag": "street-view|map",
                "Icon": "street-view"
			},
            {
                "Tag": "heartbeat|ekg",
                "Icon": "heartbeat"
			},
            {
                "Tag": "venus|female",
                "Icon": "venus"
			},
            {
                "Tag": "mars|male",
                "Icon": "mars"
			},
            {
                "Tag": "mercury|transgender",
                "Icon": "mercury"
			},
            {
                "Tag": "transgender",
                "Icon": "transgender"
			},
            {
                "Tag": "transgender-alt",
                "Icon": "transgender-alt"
			},
            {
                "Tag": "venus-double",
                "Icon": "venus-double"
			},
            {
                "Tag": "mars-double",
                "Icon": "mars-double"
			},
            {
                "Tag": "venus-mars",
                "Icon": "venus-mars"
			},
            {
                "Tag": "mars-stroke",
                "Icon": "mars-stroke"
			},
            {
                "Tag": "mars-stroke-v",
                "Icon": "mars-stroke-v"
			},
            {
                "Tag": "mars-stroke-h",
                "Icon": "mars-stroke-h"
			},
            {
                "Tag": "neuter",
                "Icon": "neuter"
			},
            {
                "Tag": "server",
                "Icon": "server"
			},
            {
                "Tag": "user-plus",
                "Icon": "user-plus"
			},
            {
                "Tag": "user-times",
                "Icon": "user-times"
			},
            {
                "Tag": "bed|hotel|travel",
                "Icon": "bed"
			},
            {
                "Tag": "train",
                "Icon": "train"
			},
            {
                "Tag": "subway",
                "Icon": "subway"
			},
            {
                "Tag": "medium",
                "Icon": "medium"
			}];
    })