appDashboard.controller('DashboardModalController', function ($scope, $uibModalInstance,limitToFilter, $http, $routeParams, mySharedService, $uibModal, $timeout, $filter, Notification, $window) {
    $scope.chosenIconText = "fa-picture-o";
    $scope.SharedUser = [];
	$scope.IsCursol = true;
	$scope.IsScoreCard  = true;
	$scope.GroupDetailFilter = [];
	$scope.dtmax = new Date();
	$scope.isDMVActive = Number($scope.isDMVActive);
	$scope.StartDateAvailable =  mySharedService.StartDateAvailable;
	$scope.EndDateAvailable =  mySharedService.EndDateAvailable;			
	
	$scope.WalkThroughDashSet = function () {
        $scope.isDMVActive = 1;
		console.log("DAA")
		$scope.WalkThroughDashSetTrig();
    };
	
	if( isNaN($scope.isDMVActive) == true){
		$scope.isDMVActive = 1;
	}
	
	 $scope.grpViewType = [
		{'name': 'Slide','value':0},
		{'name': 'List','value':1}
	];
	$scope.grpViewcount = [
		{'name': 'Two','value':2},
		{'name': 'Three','value':3}
	];
	
	
	var sectionItem = 999999;
	$scope.togCursol = function () {
        $scope.IsCursol = $scope.IsCursol === true ? false : true;		
    };
	$scope.togScoreCard = function () {
       $scope.IsScoreCard = $scope.IsScoreCard === true ? false : true;	   
    };
	$scope.AddNewSect = function () {
        $scope.sectionItem--;
		var grpplen = "Section " + ($scope.GroupDetailFilter.length+ 1 );
		newtempSecAdd = {			
			"GroupID": "",
			"GroupType": 0,
			"GroupName": "",
			"GroupView": 2,
			"GroupIndex": 2,
			"GroupFilter": "",
			"GroupFilterValue": "",
			"IsFilter" : "False"
			
		};
		$scope.GroupDetailFilter.push(newtempSecAdd)
        
    };
	$scope.changeGrpSubFilter = function (id) {
		console.log( "SDSDSD " + id)
        if($scope.GroupDetailFilter[id].IsFilter == "True"){
			$scope.GroupDetailFilter[id].IsFilter = "False"
		}else{
			$scope.GroupDetailFilter[id].IsFilter = "True"
		}
		
     };
	$scope.deleteFldItem = function (id,pid) {
        $scope.GroupDetailFilter.splice(id,1);                    
     };
	 
	 $scope.chooseSubFilterModal = function (GrpInd, size, whichGroup, FilterValues) {
	 
			$scope.dynamicData = [];
			if(FilterValues !=""){
				$scope.dynamicData = FilterValues.split(',');
			}			
			if (whichGroup != undefined) {
				$scope.FilterType = whichGroup;
				$scope.chosenFilterValues = FilterValues.split(',');	
				$scope.returnVal  = "radio";
				$scope.checkVal = [];
				var modalInstanceRep = $uibModal.open({
				  animation: $scope.animationsEnabled,
				  templateUrl: 'app/views/_FilterPopup.html',
					controller: 'FilterPopupController',
					size: size,
					scope: $scope,
				  resolve: {
					checkVal: function () {
					  return $scope.checkVal;
					}
				  }
				})
				.result.then(function (selectedItem) {
				console.log("selectedItem" + selectedItem + "dd" + $scope.checkVal);
				 $scope.GroupDetailFilter[GrpInd].GroupFilterValue = String(selectedItem);
				  $scope.selected = selectedItem;
				}, function () {
				  console.log('modal-component dismissed at: ' + $scope.selected  + "dd" + $scope.checkVal);
				 
				  
				});
				
			}
		};
	 
	 $scope.updateGroupSubFilter = function (getIndex) {
			console.log("HAHAGA")
		};
	
	
		$scope.GetDashboardSetting = function () {
			if ($scope.isSetting != 0) {
				$scope.isKPISetting = true;
				$scope.HeaderName = "MOD_DASH";
			} else {
				$scope.isKPISetting = false;
				$scope.HeaderName = "ADD_DASH";
			}
			var url = uriApiURL + "api/Dashboard/GetDashboardSettings";
			var userID = mySharedService.UserID;
			url = url + "?DashBoardID=" + $scope.dashBoardID + "&isSaved=" + $scope.isSetting + "";
			$http({
					method: 'GET',
					url: url,
					data: {
						'UserID': userID,
						'KPIID': $scope.KPIID,
						'ISKPI': 1
					}
				})
				.success(function (response) {
					$scope.isRetDatas = true;
					$scope.disableSize = true;
					$scope.PriorityObjOP;
					$scope.DbhSettingData = angular.fromJson(response)[0];
					
					if ($scope.isSetting == 1) {
					    $scope.DbSettingChoosenFilter = angular.fromJson(response)[1]
						$scope.dashBoardName = $scope.DbhSettingData["DashBoardName"];
						$scope.chosenIconText = $scope.DbhSettingData["DashboardIcon"];
						$scope.startdt = $scope.DbhSettingData["DashboardStartDate"];
						$scope.enddt = $scope.DbhSettingData["DashboardEndDate"];
						$scope.IsCreator = $scope.DbhSettingData["IsCreator"];
						var startdt = $scope.startdt.split('-');
						var enddt = $scope.enddt.split('-');
						$scope.startdt = new Date(startdt[0], startdt[1] - 1, startdt[2]);
						$scope.enddt = new Date(enddt[0], enddt[1] - 1, enddt[2]);
						$scope.DashBoardID = $scope.DbhSettingData["DashBoardIID"];
						$scope.TableQuery1 = $scope.DbSettingChoosenFilter["ChosenFilter"];
						$scope.FilterValue = $scope.DbhSettingData["Filter"];
						$scope.IsIndays = $scope.DbhSettingData.IsInDays;
						$scope.PrivacySetting = $scope.DbhSettingData.IsPrivacySetting;
						$scope.IsWithSLA = $scope.DbhSettingData.IsWithSLA;
						$scope.ProvideAccessSettings = false;
						
						
						if( ($scope.DbhSettingData.IsCursol == "true") || ($scope.DbhSettingData.IsCursol == "True")){
							$scope.IsCursol = true;
						}else{
							$scope.IsCursol = false;
						}
						if( ($scope.DbhSettingData.IsScoreCard == "true") || ($scope.DbhSettingData.IsScoreCard == "True")){
							$scope.IsScoreCard = true;
						}else{
							$scope.IsScoreCard = false;
						}					
						// JSON.parse('[1, 5, "false"]'); angular.fromJson($scope.DbhSettingData.GroupDetailFilter)
						
						$scope.GroupDetailFilter = $scope.DbhSettingData.GroupDetailFilter; 
						
						if ($scope.GroupDetailFilter.length == 0) {
							//sectionItem  = sectionItem -1;
							 $scope.sectionItem--;
							var grpplen = "Section " + ($scope.GroupDetailFilter.length+ 1 );
							var snewtempSecAdd = {
								"GroupID": "",
								"GroupType": 0,
								"GroupName": "",
								"GroupView": 2,
								"GroupIndex": 2,
								"GroupFilter": "",
								"GroupFilterValue": "",
								"IsFilter" : "False"
								};
							$scope.GroupDetailFilter.push(snewtempSecAdd)
						}
						
						$scope.NumbOfDays = $scope.DbhSettingData.InDays;
					} else {
					    $scope.PrivacySetting = 1;
						$scope.TableQuery1.Keys.push({
							"name": "",
							"dynamicData": []
						});
					}
					if ($scope.PrivacySetting == 0) {
					    $scope.PrivacySetting = "IsPrivate"
					    $scope.IsInPrivate = true;
					    $scope.IsInPublic = false;
					    $scope.IsInShared = false;
					} else if ($scope.PrivacySetting == 1) {
					    $scope.PrivacySetting = "IsPublic"
					    $scope.IsInPublic = true;
					    $scope.IsInPrivate = false;
					    $scope.IsInShared = false;
					}
					else if ($scope.PrivacySetting == 2) {
					    $scope.PrivacySetting = "IsShared"
					    $scope.IsInShared = true;
					    $scope.IsInPublic = false;
					    $scope.IsInPrivate = false;
					    $scope.ShareUserId = ($scope.DbhSettingData.SharedUserId != null) ? $scope.DbhSettingData.SharedUserId.split(',') : "";
					    $scope.ShareUserName = ($scope.DbhSettingData.SharedUserName != null) ? $scope.DbhSettingData.SharedUserName.split(',') : "";
					    $scope.SharedUser = [];
					    if ($scope.ShareUserId != undefined || $scope.ShareUserId != null) {
					        for (var i = 0; i < $scope.ShareUserId.length; i++) {
					            $scope.SharedUser.push({ id: $scope.ShareUserId[i], name: $scope.ShareUserName[i] })
					        }
					    }
					}
					if ($scope.IsIndays == 1) {
						$scope.IsRadio = true;
						$scope.IsRadios = false
						$scope.IsIndays = "IsIndays"
						$scope.BlurClas = "BlurClass"
						$scope.BlurClass = "";
					} else {
						$scope.IsRadio = false;
						$scope.IsRadios = true;
						$scope.IsIndays = "IsNotIndays"
						$scope.BlurClass = "BlurClass"
						$scope.BlurClas = "";
					}
					$scope.FilterCriteria = $scope.DbhSettingData["Filter"];
					$scope.PriorityObj = $scope.DbhSettingData.AllPriority;
					$scope.PriorityOutPutObj = $scope.PriorityObj;
					$scope.RequestObj = $scope.DbhSettingData.AllRequest;
					$scope.StatusObj = $scope.DbhSettingData.AllStatus;
					$scope.CriticObj = $scope.DbhSettingData.AllCriticality;
				});
		}
		$scope.GetDashboardSetting();
		$scope.isRetDatas = false;
		$scope.selectedVal = {};
		$scope.selectedKeyVal = [];
		$scope.ChangeRadiobtn = function (inday, days) {
			if (inday == days) {
				$scope.IsRadio = true;
				$scope.IsRadios = false;
				$scope.BlurClas = "BlurClass"
				$scope.BlurClass = "";
			} else {
				$scope.IsRadio = false;
				$scope.IsRadios = true;
				$scope.BlurClass = "BlurClass"
				$scope.BlurClas = "";
			}
		}
		$scope.TableQuery1 = {
			"keys": [
				{
					"name": "Company"
				},
				{
					"name": ""
				}
			],
			"values": {
				"Company": [
					"Brazil",
					"Canada",
					"Denmark",
					"Finland"
				],
				"Competency": [
					"BRM",
					"BSD",
					"DigitalICDesign(ICD)"
				]
			}
		}
		$scope.dateOptions = {
			formatYear: 'yy',
			maxDate: new Date(2020, 5, 22),
			minDate: new Date(),
			startingDay: 1
		};
		$scope.TableQuery = [
			{
				"requestObj": [
					{
						"id": "Company",
						"ticked": true,
						"name": "Company"
						},
					{
						"id": "Competency",
						"ticked": false,
						"name": "Competency"
						}
					],
				"dynamicData": [
					{
						"id": "Company",
						"ticked": true,
						"name": "Company"
						},
					{
						"id": "Competency",
						"ticked": false,
						"name": "Competency"
						}
					]
				}
		];
		var todayDate = new Date();
		$scope.enddt = new Date();
		todayDate.setMonth(todayDate.getMonth() - 1);
		$scope.startdt = todayDate;
		$scope.dtmax = todayDate;
		$scope.startOpened = false;
		$scope.openCalendarStart = function () {
			$timeout(function () {
				$scope.startOpened = true;
			}, 0);
		};
		$scope.todaydate = todayDate;
		$scope.endOpened = false;
		$scope.openCalendarEnd = function () {
			$timeout(function () {
				$scope.endOpened = true;
			});
		};

		$scope.animationsEnabled = true;

		$scope.loadTags = function ($query) {
		    var url = uriApiURL + 'api/Dashboard/GetUserDataToMapData?Keyword=';
		    return $http({
		        method: 'GET',
		        url: url + $query
		    })
                    .then(function (response) {
                        var WholeData = angular.fromJson(response.data.m_StringValue);
                        $scope.UserDetails = WholeData[0].SearchUser;
                        return $scope.UserDetails.filter(function (item) {
                            if (item.name.toLowerCase().indexOf($query.toLowerCase()) != -1)
                            return item;
                        });
                    });
		};
	
		$scope.ChangePrivacySetting=function(privacy,type){
		    if (type == "IsPrivate") {
		        $scope.IsInPrivate = true;
		        $scope.IsInPublic = false;
		        $scope.IsInShared = false;
		    } else if (type == "IsPublic") {
		        $scope.IsInPrivate = false;
		        $scope.IsInPublic = true;
		        $scope.IsInShared = false;
		    } else {
		        $scope.IsInPrivate = false;
		        $scope.IsInPublic = false;
		        $scope.IsInShared = true;
		        //$scope.GetUserDataToMapData();
		    }
		}
		$scope.chooseIconModal = function (size) {
			mySharedService.searchicon($scope, size)
		};
		$scope.toggleAnimation = function () {
			$scope.animationsEnabled = !$scope.animationsEnabled;
		};
		$scope.GetValues2Plot = function () {
			$scope.selectedVal = [];
			$scope.$broadcast('getValue');
		}
		
		
		
		$scope.$on('pingBack', function (e, data, comnt) {
			$scope.filtercomment = "";
			$scope.filtercomment = comnt;
			$scope.selectedVal.push(data);
			$scope.selectedKeyVal.push(data.GenericKey);
		});
		$scope.FilterCriteria = "";
		$scope.FilterValue = "";
		$scope.TableQuery1 = {
			Keys: []
		};
		$scope.ok = function () {
			angular.forEach($scope.GroupDetailFilter, function (item, index) {
				var indexAdv = index+1;	
				$scope.GroupDetailFilter[index].GroupIndex	= indexAdv;		
				               
            });
		
		
			mySharedService.DashboardCache= false;
			var dashdata = $scope.DashboardDetailModel();
			if (dashdata != undefined) {
				mySharedService.addUpdateDbSetting(dashdata);
				$uibModalInstance.dismiss('cancel');
				mySharedService.changeDashboardSettingsFunc($scope.DashboardsDetailModel());
				$uibModalInstance.dismiss('cancel');
			}
		};
		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};

		function search(key, nameKey, myArray) {
			var pos = -1;
			for (var i = 0; i < myArray.length; i++) {
				if (myArray[i][key].toUpperCase() === nameKey.toUpperCase()) {
					pos = i;
					break;
				}
			}
			return pos;
		}
		$scope.DashboardsDetailModel = function () {
			var self = {};
			self.DashboardIcon = $scope.chosenIconText;
			self.Dashboard = {};
			self.ChosenFilter = {
				"Keys": [],
				"values": {}
			};
			self.Dashboard.DashboardName = $scope.dashBoardName;
			self.Dashboard.Id = $scope.dashBoardID;
			if ($scope.IsIndays == "IsIndays") {
				var temp = new Date(),
					today = new Date();
				temp.setDate(temp.getDate() - $scope.NumbOfDays)
				self.Dashboard.DashboardStartDate = $filter('date')(temp, "dd-MMM-yyyy");
				self.Dashboard.DashboardEndDate = $filter('date')(today, "dd-MMM-yyyy");
			} else {
				self.Dashboard.DashboardStartDate = $filter('date')($scope.startdt, "dd-MMM-yyyy");;
				self.Dashboard.DashboardEndDate = $filter('date')($scope.enddt, "dd-MMM-yyyy");
			}
			$scope.GetValues2Plot();
			self.Dashboard.HiercharchyFilter = $scope.selectedVal;
			for (var x = 0; x < self.Dashboard.HiercharchyFilter.length; x++) {
				var pos = search("id", self.Dashboard.HiercharchyFilter[x].GenericKey, $scope.FilterCriteria);
				self.ChosenFilter.Keys.push({
					"Name": $scope.FilterCriteria[pos].name,
					"Val": self.Dashboard.HiercharchyFilter[x].GenericKey
				});
				self.ChosenFilter.values[self.Dashboard.HiercharchyFilter[x].GenericKey] = [];
				self.ChosenFilter.values[self.Dashboard.HiercharchyFilter[x].GenericKey].push(self.Dashboard.HiercharchyFilter[x].GenericValue);
			}
			self.UserID = mySharedService.UserID;
			self.UserName = mySharedService.UserName;
			self.IsWithSLA =  $scope.IsWithSLA  ;
			self.IsCursol  = $scope.IsCursol  ;
			self.IsScoreCard =  $scope.IsScoreCard  ;
			self.GroupDetailFilter = [];
			self.GroupDetailFilter = $scope.GroupDetailFilter;
			
			var  rrr1 =angular.toJson(self.GroupDetailFilter, true)
			//console.log("rrr 1 " + rrr1);
		
			return self;
		};
		$scope.DashboardDetailModel = function () {
			var self = {},
				cmt = "";
			$scope.GetValues2Plot();

			if ($scope.selectedVal == "") {
				cmt += "\nPlease Select Filter!"
			}
			if ($scope.dashBoardName == undefined) {
				cmt += "\nPlease Enter Dashboard Name!"
			}
			if ($scope.RequestObjOP == "") {
				cmt += "\nPlease Select a Request Type!"
			}
			if ($scope.StatusObjOP == "") {
				cmt += "\nPlease Select a Request Status!"
			}
			if ($scope.CriticObjOP == "") {
				cmt += "\nPlease Select Criticality!"
			}
			if ($scope.PriorityObjOP == "") {
			    cmt += "\nPlease Select Request Priority!"
			}
			if (duplicateVal($scope.selectedKeyVal).length > 0 && $scope.RequestObjOP == "" && $scope.StatusObjOP == "" && $scope.CriticObjOP == "" && $scope.PriorityObjOP == "" && $scope.dashBoardName == "") {
				cmt += "\nPlease choose unique filter criterias!"
			}
			if ($scope.IsIndays == "IsIndays") {
				if ($scope.NumbOfDays == undefined || $scope.NumbOfDays == "") {
					cmt += "\nPlease Enter the number of day(s)!"
				}
			}
			if ($scope.filtercomment != "" && $scope.filtercomment != undefined) {
				cmt += $scope.filtercomment;
			}
			if ($scope.GroupDetailFilter.length> 0) {
				var groupnamelen = 0;
				var groupfilterlen = 0;
				var groupfiltervallen = 0;
				angular.forEach($scope.GroupDetailFilter, function (item, index) {
					if (( item.GroupName == "") ||  ( item.GroupName == " ")){
						groupnamelen ++;						
					}
					if   ( item.IsFilter == "True"){
						if (( item.GroupFilter == "") ||  ( item.GroupFilterValue == "")){
							groupfilterlen ++;						
						}						
					}
					
					
										
				});
				if(groupnamelen > 0){
							cmt += "Please Provide valid name to all Sections";
					}
					if(groupfilterlen){
							cmt += "Please Provide valid Filters to all Sections";
					}
			
				
			}
			if (cmt != "") {
				Notification.error({
					verticalSpacing: 60,
					message: "" + cmt + "",
					title: '<i class="fa fa-warning"></i>&nbsp;Operation not allowed!'
				});
			} else {
				self.DashboardIcon = $scope.chosenIconText;
				self.DashBoardName = $scope.dashBoardName;
				if ($scope.IsIndays == "IsIndays") {
					self.IsIndays = 1;
					self.InDays = $scope.NumbOfDays;
				} else {
					self.IsIndays = 0;
				} if ($scope.PrivacySetting == "IsPrivate") {
				    self.IsPrivacySetting = 0;
				    self.SharedUserId = mySharedService.UserID;
				    self.SharedUserName = mySharedService.UserName + "(" + mySharedService.UserID + ")";
				} else if ($scope.PrivacySetting == "IsPublic") {
				    self.IsPrivacySetting = 1;
				    self.SharedUserId = mySharedService.UserID;
				    self.SharedUserName = mySharedService.UserName + "(" + mySharedService.UserID + ")";
				}else if ($scope.PrivacySetting == "IsShared") {
				    self.IsPrivacySetting = 2;
				    var ShareUserId = [], ShareUserName = [];
				    for (var i = 0; i < $scope.SharedUser.length; i++) {
				        ShareUserId.push($scope.SharedUser[i].id);
				        ShareUserName.push($scope.SharedUser[i].name);
				    }
				    ShareUserId.push(mySharedService.UserID);
				    ShareUserName.push(mySharedService.UserName + "(" + mySharedService.UserID + ")");
				    self.SharedUserId = ShareUserId.toString();
				    self.SharedUserName = ShareUserName.toString();
				}				
				self.DashboardStartDate = $filter('date')($scope.startdt, "dd-MMM-yyyy");
				self.DashboardEndDate = $filter('date')($scope.enddt, "dd-MMM-yyyy");
				self.Request = $scope.GetName($scope.RequestObjOP).toString();
				self.HiercharchyFilter = $scope.selectedVal;
				self.Request_Status = $scope.GetName($scope.StatusObjOP).toString();
				self.Criticality = $scope.GetName($scope.CriticObjOP).toString();
				self.Request_Priority = $scope.GetName($scope.PriorityObjOP).toString();
				self.DashBoardID = $scope.dashBoardID;
				self.UserID = mySharedService.UserID;
				self.UserName = mySharedService.UserName;
				self.IsWithSLA =  $scope.IsWithSLA  ;
				 
				self.IsCursol =  $scope.IsCursol  ;
				self.IsScoreCard =  $scope.IsScoreCard  ;
				self.GroupDetailFilter = [];
				self.GroupDetailFilter = $scope.GroupDetailFilter;
				
				var  rrr2 =angular.toJson(self.GroupDetailFilter, true)
	//		console.log("rrr 2 " + rrr2);
		
				return self;
			}
		};
		$scope.GetName = function (array) {
			var dataarray = [];
			for (var i = 0; i < array.length; i++) {
				dataarray.push(array[i].name);
			}
			return dataarray;
		}
		$scope.$on('handlecloseIconModal', function () {
			$scope.chosenIconText = mySharedService.chosenIcon;
		});
		$scope.IntroOptions = {
			steps: [
				{
					element: '#IntroTitle',
					position: 'bottom',
					intro: "We have defined Dashboard as a collection of widget(s) that have a common date range and parameters(filters). For example we can create a dashboard to shows some information for the last 3 months for a particular assignment group. <br/><br/>This introduction will explain to you how you can create a dashboard."
			},
				{
					element: '#IntroDI',
					position: 'bottom',
					intro: "Click on the picture to choose the Icon you want to use to represent the dashboard."
			},
				{
					element: '#IntroDN',
					position: 'top',
					intro: "Give the name of the dashboard."
			},
				{
					element: '#IntroF',
					position: 'top',
					intro: 'Use this to set the parameters/filters for a dashboard. This is where you can set the assignment group mentioned in the above step.'
			},
				{
					element: '#IntroP',
					position: 'top',
					intro: 'Use this to set the date range for the widget(s) in the dashboard. One dashboard can have only single date range for all the widget(s) and this can\'t be over ridden at the widget level.'
			},
				{
					element: '#IntroDKPI',
					position: 'top',
					intro: "Some common parameters used in the widgets are placed here. You can set it here to avoid setting it for each widget. You can override this at a widget level."
			}
			],
			showStepNumbers: false,
			exitOnOverlayClick: true,
			exitOnEsc: true,
			nextLabel: '<span class="btn btn-sm">NEXT!</span>',
			prevLabel: '<span class="btn btn-sm">Previous</span>',
			doneLabel: 'Thanks'
		};
		$scope.ShouldAutoStart = false;
	});
	


	
