 appDashboard.controller('DeleteModalController', function ($scope, $http, $uibModalInstance, mySharedService, Notification) {
        $scope.ok = function () {
            $http({
                    method: 'POST',
					params: {version: getEpocStamp()},
                    url: $scope.Deleteurl,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                .success(function (response) {
                   console.log("DelDASHID" +$scope.DelDASHID );
					var delteIte= '';
					if ($scope.delval == "dashdel") {
						 mySharedService.updateKPIdel($scope.delval, $scope.DelDASHID);
						delteIte = " Dashboard " + $scope.DelDASHID;
 					} else if ($scope.delval == "kpidel") {
						 mySharedService.updateKPIdel($scope.delval, $scope.DelKPIID);
						delteIte = " Widget " + $scope.DelKPIID;
					}else if ($scope.delval == "grpdel") {
							delteIte = " Group " + $scope.DelKPIID;
					}
                    Notification.success({
                        message: 'Selected "' + delteIte + '" deleted successfully.'
                    });
                    $uibModalInstance.dismiss('cancel');
                });
        };


        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    })