 appDashboard.controller('LoginModalController', function ($scope, $http, $uibModalInstance, mySharedService, Notification) {
        $scope.loginUserName = "";
		$scope.loginUserPass = "";
		
 $scope.LoginFn = function () {
 //api/Dashboard/GetUserDetails?UserId=10615217&Password=newuser1234#
		
		
		
		$http({
                method: 'GET',
				params: {version: getEpocStamp()},
                cache: false,
                url: uriApiURL + "api/Dashboard/GetUserDetails?UserId=" + $scope.loginUserName + "&Password=" + $scope.loginUserPass
            })
            .success(function (response) {
				var userIndo = 	response;
                 console.log("userIndo IS " + response );
				 if((userIndo.userID == 'null')  || (userIndo.userID == null) || (userIndo.userID == '')  ){
					Notification.error({
						verticalSpacing: 60,
						message: "Invalid Username and/or password!" ,
						title: '<i class="fa fa-warning"></i>&nbsp; Unable to Login.<br> '
					});
				 }else{
					Notification.info({
						verticalSpacing: 60,
						message: "Logged In Succesfully" ,
						title: '<i class="fa fa-info"></i>&nbsp; Hi '+ userIndo.userName +' <br> '
					});
					 
					 $uibModalInstance.dismiss('cancel');
					
				}
				//console.log("userlogged IS " + $rootScope.userlogged.id );
				


            })
		
		
		
		
			};


        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    })
	
appDashboard.controller('ApLoginController', function ($scope, $http, mySharedService, Notification,$location) {
        $scope.loginUserName = "";
		$scope.loginUserPass = "";
		$scope.loginProcess = false;
		
 $scope.LoginFn = function () {
 //api/Dashboard/GetUserDetails?UserId=10615217&Password=newuser1234#
		$scope.loginProcess = true;
		
		
		$http({
                method: 'GET',
                cache: false,
				params: {version: getEpocStamp()},
                url: uriApiURL + "api/Dashboard/GetUserDetails?UserId=" + $scope.loginUserName + "&Password=" + encodeURIComponent($scope.loginUserPass)
            })
            .success(function (response) {
				$scope.loginProcess = false;
				var userIndo = 	response;
                 $scope.UserInfolJS = angular.toJson(response, true);
					//console.log("US IN" + $scope.UserInfolJS);
				 if((userIndo.userID == 'null')  || (userIndo.userID == null) || (userIndo.userID == '')  ){
					Notification.error({
						verticalSpacing: 60,
						message: "Invalid Username and/or password!" ,
						title: '<i class="fa fa-warning"></i>&nbsp; Unable to Login.<br> '
					});
				 }else{
					Notification.info({
						verticalSpacing: 60,
						message: "Logged In Succesfully" ,
						title: '<i class="fa fa-info"></i>&nbsp; Hi '+ userIndo.userName +' <br> '
					});
				 //   $http({ method: 'GET', url: "http://localhost:60553/api/UserSetting/GetUserThemeSettings" })
                 //  .success(function (res) {
                 //    $scope.UserInfo.base_Color = res.userThemeSetting[0].baseTheme;
                 //    $scope.UserInfo.theme_Color = res.userThemeSetting[0].themeColor;
                 //    sessionStorage.UserInfo.base_Color = $scope.UserInfo.base_Color;
                 //    sessionStorage.UserInfo.theme_Color = $scope.UserInfo.theme_Color;
                 //    sessionStorage.UserInfo = JSON.stringify($scope.UserInfo);
                 //    var userresponse = JSON.stringify($scope.UserInfo);
                 //    localStorage.setItem(appLocStrUsrVar, userresponse);
                 //    mySharedService.SetUserDetail(sessionStorage.UserInfo);
                 //});

					$scope.UserInfo = angular.fromJson(response);
					$scope.IsAdmin = $scope.UserInfo.adminDet.isAdmin;
					sessionStorage.UserInfo = JSON.stringify($scope.UserInfo);
					var userresponse =   JSON.stringify($scope.UserInfo);
					//localStorage.setItem( appLocStrUsrVar, userresponse);	
					mySharedService.SetUserDetail(sessionStorage.UserInfo);
					mySharedService.StartDateAvailable = $scope.UserInfo.startRecordTimeStamp;
					mySharedService.EndDateAvailable = $scope.UserInfo.lastModifiedDate;
					
					$location.path('/');
					/*
					
					$scope.UserInfo = angular.fromJson(response.data);
                $scope.IsAdmin = $scope.UserInfo.adminDet.isAdmin;
                sessionStorage.UserInfo = JSON.stringify($scope.UserInfo);
                mySharedService.SetUserDetail(sessionStorage.UserInfo);
                $scope.GetUserCompleteUserData($scope.UserInfo);
					*/
					 
					 
					
				}
				//console.log("userlogged IS " + $rootScope.userlogged.id );
				


            })
		
		
		
		
			};


        
    })