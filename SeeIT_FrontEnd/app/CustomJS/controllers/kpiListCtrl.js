appDashboard.controller('KPIListController', ['$scope', '$rootScope', '$window', '$http', 'mySharedService', '$timeout', '$routeParams', 'Notification' , '$uibModal' , '$location', function ($scope, $rootScope, $window, $http, mySharedService, $timeout, $routeParams, Notification,  $uibModal, $location) {

	
	$scope.isDboardTitleCollapsed = true;
	$scope.DisplayGrpLst = [];
	$scope.slickConfigGroups = [];
	$scope.slickConfigShow = [];
	mySharedService.GroupDetailFilter=  []; 
	mySharedService.carouselLock = false;
	$scope.carouselLock = false;
	$scope.carouselIndex = 0;
	
	 $scope.$on('carouselLockChange', function () {
        $scope.carouselLock  = mySharedService.carouselLock ;
    });
    // Slick 3
    //====================================
	
	$scope.slickConfig = {
       
    }
	$scope.scoreCardCSS = function() {
			
			var SCC = "";
			if( ($scope.KPICollections.IsCursol == 'true') ||  ($scope.KPICollections.IsCursol == 'True') ||  ($scope.KPICollections.IsCursol == true)){
				SCC = SCC + "col-md-3";
			}
			else{
				SCC = SCC + "col-md-12 full-list";
			}
			return SCC;
		}
	$scope.carsoulCSS = function() {
			
			var SCC = "";
			if( ($scope.KPICollections.IsScoreCard == 'true') ||  ($scope.KPICollections.IsScoreCard == 'True') ||  ($scope.KPICollections.IsScoreCard == true)){
				SCC = SCC + "col-md-9";
			}
			else{
				SCC = SCC + "col-md-12 ";
			}
			return SCC;
		}	
	

    $scope.toggleSlick = function() {
	
      $scope.slickConfig3Loaded.enabled = !$scope.slickConfig.enabled;
	   $timeout(function () {
							$scope.slickConfig3Loaded = !$scope.slickConfig3Loaded ;
		}, 5);
    }
	$scope.changeSlideInDelay = function( ind, len) {
	
		$timeout(function () {
		var newValToComp =  angular.element('#carouselInd').val();
		var newValToNum =Number(newValToComp);
		//	console.log( "MOLE IN " +  newValToNum + " " + ind )
		if(newValToNum == ind){
			//$scope.carouselIndex ++;
			if ( $scope.carouselIndex == len ){
				//$scope.carouselIndex = 0;
			}
		}				
		}, 11000);
		
		
      
	   
    }
	

    $scope.slickConfig3Loaded = true;
    
	

    //====================================

    $scope.tempKPIL = {};
    //////////////////
    $scope.myInterval = 500000;
    $scope.noWrapSlides = false;
    $scope.active = 0;
    var slides = $scope.slides = [];
    var currIndex = 0;

    

    $scope.randomize = function () {
        var indexes = generateIndexesArray();
        assignNewIndexesToSlides(indexes);
    };

    

    // Randomize logic below

    function assignNewIndexesToSlides(indexes) {
        for (var i = 0, l = slides.length; i < l; i++) {
            slides[i].id = indexes.pop();
        }
    }

    function generateIndexesArray() {
        var indexes = [];
        for (var i = 0; i < currIndex; ++i) {
            indexes[i] = i;
        }
        return shuffle(indexes);
    }


    function shuffle(array) {
        var tmp, current, top = array.length;

        if (top) {
            while (--top) {
                current = Math.floor(Math.random() * (top + 1));
                tmp = array[current];
                array[current] = array[top];
                array[top] = tmp;
            }
        }

        return array;
    }

    //////////////////
    mySharedService.KPIboardCache = true;

    $scope.Resolution = sessionStorage.getItem("Resolution");
    if (!$scope.Resolution) {
        if ($window.innerWidth > 1440) {
            sessionStorage.setItem("Resolution", "High");
        } else {
            sessionStorage.setItem("Resolution", "Low");
        }
        $scope.Resolution = sessionStorage.getItem("Resolution");
    }

    if ($scope.Resolution == "High") {
        $scope.columnValue = 12;
        $scope.opacityValCom = "";
        $scope.opacityValue = "theme-text";
        $scope.SizeObjX = [
            {
                name: "2"
				},
            {
                name: "3"
				},
            {
                name: "4"
				},
            {
                name: "5"
				},
            {
                name: "6"
				},
            {
                name: "7"
				},
            {
                name: "8"
				},
            {
                name: "9"
				},
            {
                name: "10"
				},
            {
                name: "11"
				},
            {
                name: "12"
				}];
        $scope.SizeObjY = [
            {
                name: "2"
				},
            {
                name: "3"
				},
            {
                name: "4"
				},
            {
                name: "5"
				},
            {
                name: "6"
				},
            {
                name: "7"
				},
            {
                name: "8"
				},
            {
                name: "9"
				},
            {
                name: "10"
				},
            {
                name: "11"
				},
            {
                name: "12"
				}];
    } else {
        $scope.columnValue = 8;
        $scope.opacityValCom = "theme-text";
        $scope.opacityValue = "";
        $scope.SizeObjX = [
            {
                name: "2"
				},
            {
                name: "3"
				},
            {
                name: "4"
				},
            {
                name: "5"
				},
            {
                name: "6"
				},
            {
                name: "7"
				},
            {
                name: "8"
				}];
        $scope.SizeObjY = [
            {
                name: "2"
				},
            {
                name: "3"
				},
            {
                name: "4"
				},
            {
                name: "5"
				},
            {
                name: "6"
				},
            {
                name: "7"
				},
            {
                name: "8"
				}];
    }


    /*Variable Declaration and initialization Starts*/
    $scope.KPICollections = [];
    $scope.isPositionChanged = false;
    $scope.dashBoardID = $routeParams.dashboardId;
    $scope.gridsterOpts = {
        mobileBreakPoint: 800,
        columns: $scope.columnValue,
        margins: [15, 15],
        outerMargin: true,
        pushing: true,
        floating: true,
        draggable: {
            enabled: false,
            stop: function (event, $element, widget) {
                $scope.isPositionChanged = true;
            }
        },
        resizable: {
            enabled: false,
            handles: ['n', 'e', 's', 'w', 'se', 'sw']
        }
    };

    $scope.CheckUserAuthentication = function () {
        $scope.urlGetUserDashboardSetting = uriApiURL + "api/UserSetting/UserAuthentication?DashBoardID=" + $scope.dashBoardID;

        $http({
                method: 'GET',
				params: {version: getEpocStamp()},
                url: $scope.urlGetUserDashboardSetting
            })
            .success(function (response) {
                $scope.AuthUser = angular.fromJson(response);
                $scope.IsValidUser = $scope.AuthUser.IsValidUser;
                if ($scope.IsValidUser == 1) {
                    $scope.UserInfo = mySharedService.UserInfo;
                    $scope.UserID = $scope.UserInfo.UserID;
                    getKAPIValues($scope.columnValue);
                    $scope.GetDashboardSetting();
                } else {
                    mySharedService.makeBgLoad(false);
                }
            }).error(function (response) {
                
				$location.path('/login');
				 mySharedService.makeBgLoad(false);
            });
    }

    /*Variable Declaration and initialization Ends*/
    $scope.$on('walkthroughKpi', function () {

        $timeout(function () {
            $scope.WalkThroughCall(5)
        }, 0);

        $scope.IntroOptions = {
            steps: [
                {
                    element: '#IntroDashDetails',
                    position: 'bottom',
                    intro: "We have defined Dashboard as a collection of widget(s) that have a common date range and parameters(filters). For example we can create a dashboard to shows some information for the last 3 months for a particular assignment group. <br/><br/>This introduction will explain to you how you can create a dashboard."
				},
                {
                    element: '#IntroViewMode',
                    position: 'right',
                    intro: "Click on the picture to choose the Icon you want to use to represent the dashboard."
				},
                {
                    element: '#IntroSaveReorder',
                    position: 'bottom',
                    intro: 'Use this to set the parameters/filters for a dashboard. This is where you can set the assignment group mentioned in the above step.'
				},
                {
                    element: '#IntroKPI',
                    position: 'bottom',
                    intro: 'Use this to set the date range for the widget(s) in the dashboard. One dashboard can have only single date range for all the widget(s) and this can\'t be over ridden at the widget level.'
				}
				],
            showStepNumbers: false,
            exitOnOverlayClick: true,
            exitOnEsc: true,
            nextLabel: '<span class="btn btn-sm">NEXT!</span>',
            prevLabel: '<span class="btn btn-sm">Previous</span>',
        };
        $scope.ShouldAutoStar = true;
    })

    /*Angular Function Definition Starts*/
    $scope.ChangeViewMode = function (columnValue) {
        mySharedService.GetchangeViewMode($scope, columnValue, getKPIValues);
    };
	$scope.chooseSubFilterModalDash = function (GrpIndex,size, whichGroup, FilterValues) {
		
			 $scope.GrpCrrIndex  =GrpIndex;
			 
			$scope.dynamicData = [];
			//console.log("FilterValues " + FilterValues + " " +  typeof(FilterValues))
			if(FilterValues !=""){
				$scope.dynamicData = FilterValues.split(',');
			}	
			
			
				$scope.FilterType = whichGroup;
				$scope.chosenFilterValues = FilterValues;
				$scope.returnVal  = "radio";
				
				
				
				var modalInstanceRep = $uibModal.open({
				  animation: $scope.animationsEnabled,
				  templateUrl: '_FilterPopup.html',
					controller: 'FilterPopupController',
					size: size,
					scope: $scope,
				  resolve: {
					checkVal: function () {
					  return $scope.checkVal;
					}
				  }
				})
				.result.then(function (selectedItem) {
					//console.log("Selected Group " + selectedItem + " " + $scope.GrpCrrIndex);
					$scope.KPICollections.groups[$scope.GrpCrrIndex].GroupFilterValue =  String(selectedItem);
					/** Only for Dashboard Page Group Filter Change **/
					$scope.returnGroupId   =  $scope.KPICollections.groups[$scope.GrpCrrIndex].GroupID;
					$scope.returnGroupVal = String(selectedItem);

					var url = uriApiURL + "api/UserSetting/SaveGroupFilterSettings";
					url = url + "?GroupId=" + $scope.returnGroupId;

					var Userval = $scope.returnGroupVal;
					var FilterGrpSelectedValue = { 'Userval': Userval };

					$http({
					    method: 'POST',
					    url: url,
					    params: { version: getEpocStamp() },
					    data: FilterGrpSelectedValue,
					    contentType: "application/json",
					    dataType: "json"

					})
					//$http({
					//	method: 'POST',
					//	params: {version: getEpocStamp()},
					//	url: uriApiURL + "api/UserSetting/SaveGroupFilterSettings?GroupId="+ $scope.returnGroupId + "&FilterGrpSelectedValue="+ $scope.returnGroupVal,
					//	headers: {
					//		'Content-Type': 'application/x-www-form-urlencoded'
					//	}
					//})
					.success(function (response) {
						//console.log("DICW");
						Notification.success({
							message: 'Group Filter Changes Saved Succesfully!'
						});
					});
				/** Only for Dashboard Page Group Filter Change **/
				mySharedService.KPIboardCache = false;
					$scope.KPICollections.groups[$scope.GrpCrrIndex].GroupType  = $scope.KPICollections.groups[$scope.GrpCrrIndex].GroupType + 100;
					$timeout(function () {	
						$scope.KPICollections.groups[$scope.GrpCrrIndex].GroupType  = $scope.KPICollections.groups[$scope.GrpCrrIndex].GroupType - 100;	
						
					},500);
				});
			  
				
				
				
				
				
			
		};

    $scope.GetDashboardSetting = function () {
        $scope.urlGetUserDashboardSetting = uriApiURL + "api/Dashboard/GetDashboardSettings";

        $scope.urlGetUserDashboardSetting = $scope.urlGetUserDashboardSetting + "?DashBoardID=" + $scope.dashBoardID + "&isSaved=1";

        $http({
                method: 'GET',
				params: {version: getEpocStamp()},
                url: $scope.urlGetUserDashboardSetting
            })
            .success(function (response) {
                $scope.Dateformat = "dd-MMM-yyyy";
                $scope.DbHData = angular.fromJson(response)[0];
                $scope.DbHData.ChosenFilter = angular.fromJson(response)[1].ChosenFilter;
                mySharedService.SlaChoose = $scope.DbHData.AllCriticality;
                $scope.DashboardCollection = angular.fromJson(response)[0];
                $scope.Dashboard = {};
                $scope.DashboardIcon = $scope.DbHData.DashboardIcon;
                $scope.dashboardid = $scope.DbHData.DashBoardIID;
                $scope.Dashboard.DashboardName = $scope.DbHData.DashBoardName
                $scope.dashname = $scope.DbHData.DashBoardName;
                $scope.dashid = $scope.DbHData.DashBoardIID;
                $scope.dashicon = $scope.DbHData.DashboardIcon;
                $scope.strtdt = $scope.DbHData.DashboardStartDate;
                $scope.IsCreator = $scope.DbHData.IsCreator;
                mySharedService.DBstrtdt = $scope.strtdt;
                $scope.enddt = $scope.DbHData.DashboardEndDate;
                mySharedService.DBenddt = $scope.enddt;
                $scope.ChosenFilter = $scope.DbHData.ChosenFilter;
                $scope.Privacysetting = $scope.DbHData.IsPrivacySetting;
                mySharedService.Privacysetting = $scope.Privacysetting;
				
            });
    }

    $scope.$on("handleKPIdel", function (e, DelKPIID) {
        var position;
		console.log( "DelKPIID" + DelKPIID);
        
		for (var j = 0; j < $scope.KPICollections.groups.length; j++) {
			for (var i = 0; i < $scope.KPICollections.groups[j].KPI.length; i++) {
				console.log("groups DEL" + $scope.KPICollections.groups[j].Id  + " " +DelKPIID );
				if ($scope.KPICollections.groups[j].KPI[i].Id == DelKPIID) {
					var tempj = j;	
												
						$scope.KPICollections.groups[j].KPI.splice(i, 1);
						if( $scope.KPICollections.groups[j].GroupType == 0){
							$scope.KPICollections.groups[j].GroupType  = $scope.KPICollections.groups[j].GroupType + 100;
							$timeout(function () {	
							$scope.KPICollections.groups[tempj].GroupType  = $scope.KPICollections.groups[tempj].GroupType - 100;							
							},500);
						}
						
			}
			
			
			
			
        }
				
			}
		for (var i = 0; i < $scope.KPICollections.ScoreCard.length; i++) {
			console.log("SC DEL" + $scope.KPICollections.ScoreCard[i].Id  + " " +DelKPIID );
            if ($scope.KPICollections.ScoreCard[i].Id == DelKPIID) {
                $scope.KPICollections.ScoreCard.splice(i, 1)
            }
        }
		for (var i = 0; i < $scope.KPICollections.Cursol.length; i++) {
			console.log("SC DEL" + $scope.KPICollections.Cursol[i].Id  + " " +DelKPIID );
            if ($scope.KPICollections.Cursol[i].Id == DelKPIID) {
                $scope.KPICollections.Cursol.splice(i, 1)
            }
        }
		
		
    })
	
	$scope.$on("handleGRPdel", function (e, DelGRPID) {
       
		console.log( "Delete Group" + DelGRPID);        
		for (var j = 0; j < $scope.KPICollections.groups.length; j++) {
			if ($scope.KPICollections.groups[j].GroupID == DelGRPID) {
						$scope.KPICollections.groups.splice(j, 1);
			}				
		}
		
		
		
		
    })

    $scope.savePos = function (savewidget) {
        var counter = 0;
        angular.forEach($scope.KPICollections, function (item) {
            for (var i = 0; i < mySharedService.KPIPositionArr.length; i++) {
                if (mySharedService.KPIPositionArr[i].KPIID == item.KPI.Id) {
                    if (mySharedService.KPIPositionArr[i].SizeXVal.name != item.sizeX) {
                        counter++;
                    }
                    if (mySharedService.KPIPositionArr[i].SizeYVal.name != item.sizeY) {
                        counter++;
                    }
                }
            }
        })
        if (counter != 0) {
            Notification.error({
                message: 'Changes cannot be saved!'
            });
        } else {
            $scope.gridsterOpts.draggable.enabled = false;
            $scope.isReorderData = false;
            $scope.reOrderCls = "";
            $scope.isPositionChanged = false;
            var userDataArr = [];
            for (var i = 0; i < $scope.KPICollections.length; i++) {
                userDataArr.push($scope.UserKPIDetailModel($scope.KPICollections[i]));
            }
           
               
        }
    };

    $scope.saveDashboardSetting = function () {
		console.log("CALL DB SETT");
        var url = uriApiURL + "api/Dashboard/SaveDashboardSettings";
        var userID = mySharedService.UserID;
        url = url + "?DashboardID=" + $scope.dashBoardID;
		
    	var Userval = "" + JSON.stringify(mySharedService.DbSettingJSON) + "";
        var UserDashboardData = { 'Userval': Userval };

        $http({
		method: 'POST',
               url: url,
			   params: {version: getEpocStamp()},
			   data: UserDashboardData,
			   contentType: "application/json",
			   dataType: "json"

            })
            .success(function (response) {
                //$scope.Dateformat = "mediumDate";
				 $scope.KPICollections = [];
                $scope.WholeData = angular.fromJson(response);
                mySharedService.DashboardDetailsJSON.Dashboard.Id = $scope.WholeData.Dashboard.Id;
                mySharedService.DashboardDetailsJSON.Dashboard.DashboardName = $scope.WholeData.Dashboard.DashboardName;
                mySharedService.DashboardDetailsJSON.DashboardIcon = $scope.WholeData.DashBoardIcon;
                Notification.success({
                    verticalSpacing: 60,
                    message: 'Settings saved successfully.'
                });
                $scope.DashboardIcon = $scope.WholeData.DashBoardIcon;
                $scope.Dashboard.DashboardName = $scope.WholeData.Dashboard.DashboardName;
                $scope.strtdt = mySharedService.DashboardDetailsJSON.Dashboard.DashboardStartDate;
                mySharedService.DBstrtdt = $scope.strtdt;
                $scope.enddt = mySharedService.DashboardDetailsJSON.Dashboard.DashboardEndDate;
                mySharedService.DBenddt = $scope.enddt;
                $scope.ChosenFilter = mySharedService.DashboardDetailsJSON.ChosenFilter;
                getKAPIValues($scope.columnValue);
            });
    };

    $scope.reorderKPI = function () {
        if ($scope.KPICollections.length == 0) {
            $scope.isDisabled = true;
        } else {
            $scope.gridsterOpts.draggable.enabled = true;
            $scope.isReorderData = true;
            $scope.$parent.BlurTop = "BlurClass";
            $scope.reOrderCls = "reorderCls";
            $scope.oldKPICollection = angular.copy($scope.KPICollections);
        }
    };

    $scope.hideBg = function () {
        $scope.gridsterOpts.draggable.enabled = false;
        $scope.isReorderData = false;
        $scope.$parent.BlurTop = "";
        $scope.reOrderCls = "";
        for (var i = 0; i < $scope.oldKPICollection.length; i++) {
            $scope.KPICollections[i].col = $scope.oldKPICollection[i].col;
            $scope.KPICollections[i].row = $scope.oldKPICollection[i].row;
            $scope.KPICollections[i].sizeX = $scope.oldKPICollection[i].sizeX;
            $scope.KPICollections[i].sizeY = $scope.oldKPICollection[i].sizeY;
        }
        mySharedService.KPIPositionArr = [];
        $scope.$broadcast('resizeCharts');
    };

    $scope.addWidget = function () {
        $scope.KPICollections.push({
            sizeX: 2,
            sizeY: 1,
            KPI: {
                idNum: "1",
                Id: "KPI000001",
                baseKPIID: "BKPI0001"
            }
        });
    };

    $scope.clear = function () {
        $scope.KPICollections = [];
    };

    $scope.applyWidgetSize = function (KPIID, SizeObjXval, SizeObjYval) {
        $scope.gridsterOpts.draggable.enabled = true;
        $scope.sizexval = SizeObjXval.name;
        $scope.sizeyval = SizeObjYval.name;
        $scope.$parent.BlurTop = "";
        $scope.kpiidn = KPIID;
        if ($scope.sizexval != undefined && $scope.sizeyval != undefined) {
            for (var i = 0; i < $scope.KPICollections.length; i++) {
                if ($scope.KPICollections[i].KPI.Id == $scope.kpiidn) {
                    $scope.KPICollections[i].sizeX = $scope.sizexval
                    $scope.KPICollections[i].sizeY = $scope.sizeyval
                }
            }
        }
        $scope.$broadcast('resizeCharts');
    }

    $scope.changeWidgetSize = function (KPIID, sizeX, sizeY) {
        if (mySharedService.KPIPositionArr.length != 0) {
            for (var i = 0; i < mySharedService.KPIPositionArr.length; i++) {
                if (mySharedService.KPIPositionArr[i].KPIID == KPIID) {
                    mySharedService.KPIPositionArr[i].SizeXVal = sizeX;
                    mySharedService.KPIPositionArr[i].SizeYVal = sizeY;
                } else {
                    mySharedService.KPIPositionArr.push({
                        KPIID: KPIID,
                        SizeXVal: sizeX,
                        SizeYVal: sizeY
                    })
                }
            }
        } else {
            mySharedService.KPIPositionArr.push({
                KPIID: KPIID,
                SizeXVal: sizeX,
                SizeYVal: sizeY
            })
        }
    }

    $scope.UserKPIDetailModel = function (kpiData) {
        var self = {};
        self.SizeXAxis = kpiData.sizeX;
        self.SizeYAxis = kpiData.sizeY;
        self.PositionXAxis = kpiData.row;
        self.PositionYAxis = kpiData.col;
        self.KPIID = kpiData.KPI.Id;
        return self;
    };
    /*Angular Function Definition Starts*/

    /*Calling Function Definition Starts*/
    if (!sessionStorage.UserInfo) {
        $scope.$on('GotUserDetails', function () {
            /*mySharedService.KPIID is broadcasted KPIID after adding widgets*/
            $scope.CheckUserAuthentication();
        });
    } else {
        mySharedService.makeBgLoad(true);
        $scope.CheckUserAuthentication();

    }
	$scope.$on("handleDashboardSettingUpdate", function () {
			
		 getKAPIValues($scope.columnValue);
	})

    $scope.$on('handleDashboardbroadcast', function () {
        /*mySharedService.KPIID is broadcasted KPIID after adding widgets*/
        $scope.saveDashboardSetting();
    });

    $scope.$on('handleaddWidgetroadcast', function () {
        /*mySharedService.KPIID is broadcasted KPIID after adding widgets*/
        saveKPISetting();
    });

    $scope.$watch('KPICollections', function (items) {
        //console.log(items);
    }, true);

    function saveKPISetting() {

        var url = uriApiURL + "api/UserSetting/SaveWidgetSettings";
        var userID = mySharedService.UserID;

        var Userval = "" + JSON.stringify(mySharedService.WidgetSettingJSON) + "";
        var UserDashboardData = { 'Userval': Userval };


        $http({
            method: 'POST',
            data: UserDashboardData,
            contentType: "application/json",
            dataType: "json",
            params: { version: getEpocStamp() },
            //url: uriApiURL + "api/UserSetting/SaveWidgetSettings?userData=" + encodeURIComponent(JSON.stringify(mySharedService.WidgetSettingJSON)) + "",
            url: url,
            //headers: {
            //    'Content-Type': 'application/x-www-form-urlencoded'
            //}
        })
            .success(function (response) {
                Notification.success({
                    verticalSpacing: 60,
                    message: 'Your widget settings saved successfully.'
                });
				
				mySharedService.WidgetSetupData.KPI.Id = response;
				mySharedService.WidgetSetupData.KPI.baseKPIID = mySharedService.WidgetSettingJSON.BaseKPIID;
                var pos = search(response, $scope.KPICollections);
				
				
                
				if(mySharedService.KPIGroupUpdate.grpType == "ScoreCard"){
					var posSC = searchNoGrpKPI(response, $scope.KPICollections.ScoreCard);
					if (posSC == -1) {						
							$scope.KPICollections.ScoreCard.push(mySharedService.WidgetSetupData.KPI);
							$scope.isDisabled = false;
					} else {
							$scope.KPICollections.ScoreCard[posSC] = mySharedService.WidgetSetupData.KPI;
					}
					
					
				
				}else if(mySharedService.KPIGroupUpdate.grpType == "Cursol"){
					var posSC = searchNoGrpKPI(response, $scope.KPICollections.Cursol);
					console.log("posSC " + posSC );
					if (posSC == -1) {						
							$scope.KPICollections.Cursol.push(mySharedService.WidgetSetupData.KPI);
							$scope.isDisabled = false;
					} else {
							$scope.KPICollections.Cursol[posSC] = mySharedService.WidgetSetupData.KPI;
					}
				
				}else {
				
					for (var i = 0; i < $scope.KPICollections.groups.length; i++) {
						if(mySharedService.KPIGroupUpdate.grpID == $scope.KPICollections.groups[i].GroupID ){
							var tempI = i;	
							var posSC = searchNoGrpKPI(response, $scope.KPICollections.groups[i].KPI);
							
							if (posSC == -1) {						
									$scope.KPICollections.groups[i].KPI.push(mySharedService.WidgetSetupData.KPI);
									$scope.isDisabled = false;
							} else {
									$scope.KPICollections.groups[i].KPI[posSC] = mySharedService.WidgetSetupData.KPI;

																		
							}
							if( $scope.KPICollections.groups[i].GroupType == 0){
							$scope.KPICollections.groups[i].GroupType = $scope.KPICollections.groups[i].GroupType + 5;
							$timeout(function () {							
								$scope.KPICollections.groups[tempI].GroupType  = $scope.KPICollections.groups[tempI].GroupType - 5;
							},800);
							
							}
							
						
						}else{
							/*
											
							var tempI = i;
							var posSC = searchNoGrpKPI(response, $scope.KPICollections.groups[i].KPI);
							
							if (posSC != -1) {	
							$scope.KPICollections.groups[i].GroupType = $scope.KPICollections.groups[i].GroupType + 5;
							$scope.KPICollections.groups[i].KPI.splice(posSC,1);
								
									$timeout(function () {									
							//	$scope.slickConfigGroups[tempI].slidesToShow  = $scope.KPICollections.groups[tempI].GroupView;								
								$scope.KPICollections.groups[tempI].GroupType = $scope.KPICollections.groups[tempI].GroupType - 5;
								},800);
							} */
							
							
						}
						

					
						
						
					}
					
				
				}
				$scope.WholeDatar = angular.toJson( $scope.KPICollections , true);
				//console.log("d" + $scope.WholeDatar);
                
                $timeout(function () {
                    $scope.savePos("savewidgetsetting");
                }, 200);
            });
    }

    function searchs(nameKey, myArray) {
        var pos = -1;
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i].Dashboard.Id === nameKey) {
                pos = i;
                break;
            }
        }
        return pos;
    }

    function search1(key, nameKey, myArray) {
        var pos = -1;
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i][key].toUpperCase() === nameKey.toUpperCase()) {
                pos = i;
                break;
            }
        }
        return pos;
    }
	function searchNoGrpKPI(nameKey, myArray) {
        var pos = -1;
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i].Id === nameKey) {
                pos = i;
                break;
            }
        }
        return pos;
    }
    function search(nameKey, myArray) {
        var pos = -1;
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i].KPI.Id === nameKey) {
                pos = i;
                break;
            }
        }
        return pos;
    }

    function getKAPIValues(columnValues) {
        $http({
                method: 'GET',
				cache: false,
				params: {version: getEpocStamp()},
                url: uriApiURL + "api/UserSetting/GetKPICollections?PrivacySetting=" + mySharedService.Privacysetting + "&DashBoardID=" + $scope.dashBoardID + "&Resolution=" + $scope.Resolution
            })
            .success(function (response) {

                $scope.KPICollectionsAll = angular.fromJson(response);

                //$scope.WholeDatar = angular.toJson($scope.KPICollectionsAll, true);
               console.log("DASHBOARD LOAD/RELOAD");

                $scope.KPICollectionsHighRaw = $scope.KPICollectionsAll.High;
                $scope.KPICollectionsLowRaw = $scope.KPICollectionsAll.Low
				
				mySharedService.GroupDetailFilter =  $scope.KPICollectionsAll.groups; 
				//console.log("PI" + mySharedService.GroupDetailFilter.length);
                //$scope.KPICollectionsHigh  = $scope.KPICollectionsHighRaw.sort(sortFunc);	
                //$scope.KPICollectionsLow  = $scope.KPICollectionsLowRaw.sort(sortFunc);	
                $scope.tempKPIL = $scope.KPICollectionsAll;
				for (var i = 0; i < $scope.tempKPIL.groups.length;  i++) {
					var tempConf = { 'config' : true };
					var slickcon1 = {
					method: {},
					dots: true,
					infinite: false,
					speed: 300,
					slidesToShow: $scope.KPICollectionsAll.groups[i].GroupView,
					slidesToScroll: 1,
					adaptiveHeight: true,
					enabled: true,
					responsive: [
						
						{
							breakpoint: 900,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1
							}
					}
				  ]
				};
						$scope.slickConfigGroups.push(slickcon1);
						$scope.slickConfigGroups[i].slidesToShow =  Number($scope.KPICollectionsAll.groups[i].GroupView	);	
				}
				
				
				
				
                $scope.KPICollectionsHigh = $scope.tempKPIL;
                $scope.KPICollectionsLow = $scope.tempKPIL;

                getKPIValues($scope.columnValue);
            });
    }
	$scope.changeGroupView = function( index, value )
	{
		$scope.KPICollectionsAll.groups[index].GroupType = value;
	}
	$scope.changeGroupList = function( index, value )
	{
		$scope.slickConfigGroups[index].slidesToShow = value;
		$scope.KPICollectionsAll.groups[index].GroupView = Number(value);	
		
		var rtimeleft = $scope.KPICollectionsAll.groups[index].GroupType;
		if( $scope.KPICollections.groups[index].GroupType == 0){
		
		$scope.KPICollectionsAll.groups[index].GroupType = -1;
		$timeout(function () {
				$scope.KPICollectionsAll.groups[index].GroupType = rtimeleft;
			});
		}
		
		
		$scope.WholeDatar = angular.toJson($scope.KPICollectionsAll.groups, true);
		
	}

    function getKPIValues(columnValues) {
        $scope.gridsterOpts.columns = columnValues;
        if (columnValues == 12) {
            $scope.KPICollections = $scope.tempKPIL
        } else {
            $scope.KPICollections = $scope.tempKPIL;
        }
		
		

        if ($scope.KPICollections.length == 0) {
            $scope.isDisabled = true;
        } else {
            $scope.isDisabled = false;
        }
        mySharedService.makeBgLoad(false);


    }
    $(window).unbind('resize');
    $(window).bind('resize', function (e) {
        var newWidth;
        newWidth = $window.innerWidth;

        if (window.RT) {
            clearTimeout(window.RT);
        }

        if ($window.innerWidth > 1200) {
            sessionStorage.setItem("Resolution", "High");
            $scope.Resolution = "High";
            $scope.columnValue = 12;
            $scope.opacityValCom = "";
            $scope.opacityValue = "theme-text";
        } else {
            sessionStorage.setItem("Resolution", "Low");
            $scope.Resolution = "Low";
            $scope.columnValue = 8;
            $scope.opacityValCom = "theme-text";
            $scope.opacityValue = "";
        }
        window.RT = setTimeout(function () {

            mySharedService.GetchangeViewMode($scope, $scope.columnValue, getKPIValues);
        }, 200);

        $scope.oldWidth = $window.innerWidth;
    });
	}])