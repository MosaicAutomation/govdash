appDashboard.service('translationService', function ($http) {
    this.getTranslation = function ($scope, language) {
       var languageFilePath = 'app/CustomJS/Language/' + language + '.js';
	   //var languageFilePath = sHrefIs +'//'+hostIs+'/' +pathAbs0+'/SliderDev/CustomJS/Language/' + language + '.js';
        //var languageFilePath = sHrefIs +'//'+hostIs+'/' +pathAbs0+'/CustomJS/Language/' + language + '.js';

        $http({
            method: 'GET',
			params: {version: getEpocStamp()},
            url: languageFilePath,
            cache: true
        }).success(function (data, status) {
            $scope.translation = data;
        }).error(function (data, status) {
            console.log("JSON DATA NOT FOUND")
        });
    };
});


appDashboard.factory('authInterceptorService', ['$q', '$injector', '$location', 'localStorageService', function ($q, $injector, $location, localStorageService) {

	        var authInterceptorServiceFactory = {};

	        var _request = function (config) {

	            config.headers = config.headers || {};

	            var authData = localStorageService.get('authorizationData');
	            if (authData) {
	                config.headers.Authorization = 'Bearer ' + authData.token;
	            }

	            return config;
	        }

	        var _responseError = function (rejection) {
	            if (rejection.status === 401) {
	                var authService = $injector.get('authService');
	                var authData = localStorageService.get('authorizationData');

	                if (authData) {
	                    if (authData.useRefreshTokens) {
	                        $location.path('/refresh');
	                        return $q.reject(rejection);
	                    }
	                }
	                authService.logOut();
	                $location.path('/login');
	            }
	            return $q.reject(rejection);
	        }

	        authInterceptorServiceFactory.request = _request;
	        authInterceptorServiceFactory.responseError = _responseError;

	        return authInterceptorServiceFactory;
	}])
	    .factory('mySharedService', function ($rootScope, $uibModal, Notification) {
	        var sharedService = {};
	        sharedService.LoginDetails = {};
	        sharedService.KPIID = '';
	        sharedService.IsZoom = false;
	        sharedService.idNum = '';
	        sharedService.KPIPositionArr = [];
	        sharedService.WidgetSetUpData = {};
	        sharedService.WidgetSettingJSON = {};
	        sharedService.DashboardDetailsJSON = {};
	        sharedService.DashboardDetailsJSON.Dashboard = {};
	        sharedService.SlaChoose = {};
	        sharedService.KPIPositionJSON = {};
	        sharedService.GridMethodParams = {};
	        sharedService.isBgShow = false;
	        sharedService.UserCity = {};
	        sharedService.modalSize = 'lg';
	        sharedService.PrivacySetting = 0;
	        sharedService.chosenIcon = "";
	        sharedService.DbSettingJSON = {};
	        sharedService.DBstrtdt = '';
	        sharedService.DBenddt = '';

	        /*Function is triggered on click of zoom button. control is passed over to the ZoomChartModalController*/
	        sharedService.OpenZoomModal = function (scopeVal) {
	            if ($rootScope.$$childTail.isReorderData == undefined || $rootScope.$$childTail.isReorderData == false) {
	                $rootScope.$$childTail.BlurClass = "BlurClass";
	                var modalInstance = $uibModal.open({
	                    animation: scopeVal.animationsEnabled,
	                    templateUrl: '_ZoomChart.html',
	                    controller: 'ZoomChartModalController',
	                    size: 'lg',
	                    scope: scopeVal
	                }).result.finally(function () {
	                    $rootScope.$$childHead.BlurClass = "";
	                    sharedService.IsZoom = false;
	                });
	            } else {
	                Notification.error({
	                    verticalSpacing: 60,
	                    message: 'You cannot zoom chart when you are reordering the widgets.',
	                    title: '<i class="fa fa-warning"></i>&nbsp;Operation not allowed.'
	                });
	            }
	        };

	        sharedService.SetUserDetail = function (UserInfo) {
	            sharedService.UserInfo = JSON.parse(UserInfo);
	            sharedService.UserID = sharedService.UserInfo.userID;
	            sharedService.UserName = sharedService.UserInfo.userName;
	            sharedService.UserImage = sharedService.UserInfo.userImage;
	            $rootScope.$broadcast("GotUserDetails");
				
	        };

	        sharedService.updateKPIdel = function (delval, KpiId) {
	            if (delval == "dashdel") {
	                $rootScope.$broadcast("handleDashboarddel");
	            } else if (delval == "kpidel") {
	                $rootScope.$broadcast("handleKPIdel", KpiId);
	            }else if (delval == "grpdel") {
	                $rootScope.$broadcast("handleGRPdel", KpiId);
	            }

	        }

	        sharedService.prepForBroadcast = function (kpiValue) {
	            this.KPIID = kpiValue;
	            this.broadcastItem();
	        };

	        sharedService.addWidgetFun = function (WidgetSetupVal, WidgetSettingVal) {
	            this.WidgetSetupData = WidgetSetupVal;
	            this.WidgetSettingJSON = WidgetSettingVal;
	            this.addWidget();

	        };

	        sharedService.changeDashboardSettingsFunc = function (DashboardDetails) {
	            this.DashboardDetailsJSON = DashboardDetails;
	            this.DashBoardSetting();

	        };

	        sharedService.openGridModal = function (size, url,data) {
	            this.GridMethodParams.ModalSize = size;
	            this.GridMethodParams.urlData = url;
	            this.GridMethodParams.JsonData = data;
	            $rootScope.$broadcast('handleGridModalbroadcast');
	        };

	        sharedService.updateWidgetPos = function (KPIData) {
	            sharedService.KPIPositionJSON.PosX = KPIData.row;
	            sharedService.KPIPositionJSON.PosY = KPIData.col;
	        };

	        sharedService.UpdateWidgetDataFunc = function (kpiValue) {
	            /*kpiValue is KPIID*/
	            this.idNum = kpiValue;
	            this.addUpdateWidgetData();

	        };

	        sharedService.makeBgLoad = function (kpiValue) {
	            /*kpiValue is KPIID*/
	            this.isBgShow = kpiValue;
	            this.showBgLoad();
	        };

	        sharedService.chosenIconfunc = function (chosenIconValue) {
	            /*kpiValue is KPIID*/
	            this.chosenIcon = chosenIconValue;
	            this.closeIconModal();
	        };

	        sharedService.closeIconModal = function () {
	            $rootScope.$broadcast('handlecloseIconModal');
	        };

	        sharedService.searchicon = function (scope, size) {
	            var modalInstance = $uibModal.open({
	                animation: scope.animationsEnabled,
	                templateUrl: '_DashboardIcon.html',
	                controller: 'SearchIconController',
	                size: size,
	                scope: scope
	            });
	        }

	        sharedService.GetchangeViewMode = function (scope, columnValue, functionname) {
	            if (scope.gridsterOpts.columns != columnValue) {
	                scope.$broadcast('resizeCharts');
	                if (columnValue == 8) {
	                    sessionStorage.setItem("Resolution", "Low")
	                    scope.Resolution = "Low";
	                    scope.opacityValCom = "theme-text";
	                    scope.opacityValue = "";
	                } else {
	                    sessionStorage.setItem("Resolution", "High")
	                    scope.Resolution = "High";
	                    scope.opacityValue = "theme-text";
	                    scope.opacityValCom = "";
	                }
	                scope.columnValue = columnValue;
	                sharedService.makeBgLoad(true);
	                functionname(columnValue);
	            }
	        }

	        sharedService.addWidget = function () {
	            $rootScope.$broadcast('handleaddWidgetroadcast');
	        };
	        sharedService.DashBoardSetting = function () {
	            $rootScope.$broadcast('handleDashboardbroadcast');
				 $rootScope.$broadcast('handleDashboardSettingUpdate');
	        };
	        sharedService.widgetPos = function () {
	            $rootScope.$broadcast('handlewidgetPosbroadcast');
	        };
	        sharedService.UpdateWidgetSetting = function () {
	            $rootScope.$broadcast('handleaUpdateWidgetSettingBroadcast');
	        };
	        sharedService.addUpdateWidgetData = function () {
	            $rootScope.$broadcast('handleaaddUpdateWidgetDataBroadcast');
	        };
	        sharedService.showBgLoad = function () {
	            $rootScope.$broadcast('handleshowBgLoadbroadcast');
	        };
	        sharedService.addUpdateDbSetting = function (DbSettingVal) {
	            this.DbSettingJSON = DbSettingVal;
	            $rootScope.$broadcast('handleaddUpdateDbSettingbroadcast');
	        }
	        return sharedService;
	    })