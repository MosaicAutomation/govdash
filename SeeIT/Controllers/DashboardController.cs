﻿using log4net;
using Newtonsoft.Json;
using SeeIT_Common.Exception_Log;
using SeeIT_Common.Helper;
using SeeIT_Common.Model;
using SeeIT_Data.EDMX;
using SeeIT_Data.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.DirectoryServices;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace SeeIT_API.Controllers
{
    
    public class DashboardController : ApiController
    {
        string constring = System.Configuration.ConfigurationManager.ConnectionStrings["SeeITEntities"].ConnectionString;
        DashboardRepository DashboardRepositoryInstance = new DashboardRepository();
        ErrorEntry errorLoger = new ErrorEntry();
        private static readonly log4net.ILog log = LogManager.GetLogger(typeof(AdministratorController));
        HelperFunctions helper = new HelperFunctions();

        //Function called to get the List of all the Dashboards associated with the given User Id
        [Route("api/Dashboard/GetDashBoardCollections")]
        [HttpGet]
        public HttpResponseMessage GetDashBoardCollections()
        {
            List<Dashboard> UserDashboardData = new List<Dashboard>();
            StringBuilder sbJSON = new StringBuilder();
            try
            {

                UserDashboardData = DashboardRepositoryInstance.GetDashBoardCollections();
                sbJSON.Append("{\"DashBoardDetails\":[{\"sizeX\":2,\"sizeY\":2,\"DashboardIcon\":\"fa-search\",\"Dashboard\":{\"Id\":\"DASH000000\",\"DashboardName\":\"Request Analysis\",\"idNum\":\"0\"}},");
                foreach (var item in UserDashboardData)
                {
                    sbJSON.Append("{\"sizeX\":2,\"sizeY\":2,\"DashboardIcon\":\"" + item.DashboardIcon + "\",\"Dashboard\":{\"Id\":\"" + item.DashBoardIID + "\",\"DashboardName\":\"" + item.DashBoardName + "\",\"idNum\":\"" + Convert.ToInt32(item.DashBoardIID.Substring(5)).ToString() + "\"}" + "},");
                }
                sbJSON.Remove(sbJSON.Length - 1, 1);
                sbJSON.Append("]");
                sbJSON.Append("}");
                return Request.CreateResponse(HttpStatusCode.OK, sbJSON);
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        //Function called to get the Dashboard Settings on click of either the Add Dashboard button or Settings button on each Dashboard
        [HttpGet]
        [Route("api/Dashboard/GetDashboardSettings")]
        public HttpResponseMessage GetDashboardSettings(string DashBoardID = null, int ISSaved = 0)
        {
            string output = "";
            try
            {
                string CheckFilter = "";
                UserDashboard UserDashboardData = new UserDashboard();
                UserDashboardData = DashboardRepositoryInstance.GetDashboardSettings(DashBoardID, ISSaved);
                StringBuilder sbJSON = new StringBuilder();
                var FilterAll = new List<KeyValuePair<string, string>>();
                output = new JavaScriptSerializer().Serialize(UserDashboardData);
                if (ISSaved == 1)
                {
                    output = output + ",{" + "\"ChosenFilter\":" + "{\"values\":";
                    foreach (var elementfilter in UserDashboardData.HiercharchyFilter)
                    {
                        if (CheckFilter == "")
                        {
                            CheckFilter = elementfilter.GenericKey;
                            sbJSON.Append("{\"" + CheckFilter + "\":[");
                            FilterAll.Add(new KeyValuePair<string, string>(CheckFilter, elementfilter.FilterName));
                        }
                        if (CheckFilter != elementfilter.GenericKey)
                        {
                            CheckFilter = elementfilter.GenericKey;
                            FilterAll.Add(new KeyValuePair<string, string>(CheckFilter, elementfilter.FilterName));
                            sbJSON = sbJSON.Remove(sbJSON.Length - 1, 1);
                            sbJSON.Append("],\"" + CheckFilter + "\":[");
                        }
                        if (CheckFilter == elementfilter.GenericKey)
                        {
                            string[] GenericKeyValueArr = elementfilter.GenericValue.Split(',');
                            foreach (var genericVal in GenericKeyValueArr)
                            {
                                sbJSON.Append("\"" + genericVal + "\",");
                            }
                        }
                    }
                    sbJSON = sbJSON.Remove(sbJSON.Length - 1, 1);
                    sbJSON.Append("]");
                    sbJSON.Append("}," + "\"Keys\":[");
                    foreach (var filterdata in FilterAll)
                    {
                        sbJSON.Append("{\"Name\":\"" + filterdata.Value + "\",\"Val\":\"" + filterdata.Key + "\"},");
                    }
                    sbJSON = sbJSON.Remove(sbJSON.Length - 1, 1);
                    sbJSON.Append("]}}]");
                    output = "[" + output + sbJSON;
                }
                else {
                    output = "[" + output + "]";
                }
                return Request.CreateResponse(HttpStatusCode.OK, output);
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        
        //Function called to get the Details for the selected Filter value in the Dashboard Modal
        [Route("api/Dashboard/GetDashboardFilterValues")]
        [HttpGet,HttpPost]
        public HttpResponseMessage GetDashboardFilterValues(GetFilteValue GetFilterData,string DashboardID = null)
        {
            string output = "";
            try
            {
                string FilterTypeData = GetFilterData.Userval;
                string FilterValuesData = GetFilterData.FilterValue;
                UserDashboard UserDashboardData = new UserDashboard();
                StringBuilder sbJSON = new StringBuilder();
                var FilterAll = new List<KeyValuePair<string, string>>();
                output = new JavaScriptSerializer().Serialize(DashboardRepositoryInstance.GetDashboardFilterValues(FilterTypeData, DashboardID, FilterValuesData));
                return Request.CreateResponse(HttpStatusCode.OK, output);
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        //Function called to get the Request Collections in the Request Info page
        [Route("api/Dashboard/GetRequestIDCollections")]
        [HttpGet]
        public HttpResponseMessage GetRequestIDCollections(string Keyword)
        {
            StringBuilder SearchJson = new StringBuilder();
            List<Search> GeRequestData = new List<Search>();
            try
            {
                GeRequestData = DashboardRepositoryInstance.GetRequestIDCollections(Keyword);
                SearchJson.Append("{\"SearchData\":[");
                foreach (var Fields in GeRequestData)
                {
                    SearchJson.Append("\"" + Fields.SearchValue + "\",");
                }
                SearchJson = SearchJson.Remove(SearchJson.Length - 1, 1);
                SearchJson.Append("]}");
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, SearchJson);
        }

        //Function called to get all the neccessary data to build the Request Info Charts 
        [Route("api/Dashboard/GetRequestDeepDive")]
        [HttpPost]
        public HttpResponseMessage GetRequestDeepDive(string Keyword)
        {
            DataSet dsPivot = new DataSet();
            StringBuilder sbJSON = new StringBuilder();
            string jsonObj = "";
            try
            {
                dsPivot = DashboardRepositoryInstance.GetRequestDeepDive(Keyword);
                if (dsPivot.Tables[0].Rows.Count > 0)
                {
                    jsonObj = "{\"BasicInfo\": [{\"REQUEST_ID\": \"" + dsPivot.Tables[0].Rows[0][0].ToString() + "\",\"REQUEST_TYPE\":\"" + dsPivot.Tables[0].Rows[0][1].ToString() + "\",\"REQUEST_PRIORITY\":\"" + dsPivot.Tables[0].Rows[0][2].ToString() + "\",\"STATUS\":\"" + dsPivot.Tables[0].Rows[0][3].ToString() + "\",\"STATUS_STATE\":\"" + dsPivot.Tables[0].Rows[0][4].ToString() + "\",\"SUMMARY\":\"" + helper.escapeQuote(dsPivot.Tables[0].Rows[0][5].ToString()) + "\"}],";
                }
                DataTable dtTimeLineMajor = new DataTable();
                dtTimeLineMajor = dsPivot.Tables[1];
                string TimeLineMajor = "";
                for (int i = 0; i < dtTimeLineMajor.Rows.Count; i++)
                {
                    TimeLineMajor = TimeLineMajor + "{ \"start\": \"" + Convert.ToDateTime(dtTimeLineMajor.Rows[i][1]).ToString("yyyy-MM-dd HH:mm:ss") + "\",\"className\":\"" + dtTimeLineMajor.Rows[i][3].ToString() + "\",\"myType\":\"" + dtTimeLineMajor.Rows[i][5].ToString() + "\",\"content\":\"" + dtTimeLineMajor.Rows[i][2].ToString() + "\"},";
                }
                if (dtTimeLineMajor.Rows.Count > 0)
                {
                    TimeLineMajor = TimeLineMajor.Remove(TimeLineMajor.Length - 1);
                }
                jsonObj = jsonObj + "\"TimeLineMajor\":[" + TimeLineMajor + "],";
                DataTable dtTimeLineMinor = new DataTable();
                dtTimeLineMinor = dsPivot.Tables[2];
                string TimeLineMinor = "";
                for (int i = 0; i < dtTimeLineMinor.Rows.Count; i++)
                {
                    TimeLineMinor = TimeLineMinor + "{ \"start\": \"" + Convert.ToDateTime(dtTimeLineMinor.Rows[i][1]).ToString("yyyy-MM-dd HH:mm:ss") + "\",\"className\":\"" + dtTimeLineMinor.Rows[i][3].ToString() + "\",\"myType\":\"" + dtTimeLineMinor.Rows[i][5].ToString() + "\",\"idenitifer\":\"" + dtTimeLineMinor.Rows[i][4].ToString() + "\",\"content\":\"" + dtTimeLineMinor.Rows[i][2].ToString() + "\"},";
                }
                if (dtTimeLineMinor.Rows.Count > 0)
                {
                    TimeLineMinor = TimeLineMinor.Remove(TimeLineMinor.Length - 1);
                }
                jsonObj = jsonObj + "\"TimeLineMinor\":[" + TimeLineMinor + "],";
                DataTable dtBarTeam = new DataTable();
                dtBarTeam = dsPivot.Tables[3];
                string strBarTeam = "";
                for (int i = 0; i < dtBarTeam.Rows.Count; i++)
                {
                    strBarTeam = strBarTeam + "{ \"x\": \"" + dtBarTeam.Rows[i][0].ToString() + "\",\"y\":" + dtBarTeam.Rows[i][1].ToString() + "},";
                }
                if (dtBarTeam.Rows.Count > 0)
                {
                    strBarTeam = strBarTeam.Remove(strBarTeam.Length - 1);
                }
                jsonObj = jsonObj + "\"BarTeam\":[" + strBarTeam + "],";
                DataTable dtBarStatus = new DataTable();
                dtBarStatus = dsPivot.Tables[4];
                string strBarStatus = "";
                string statusState = "";
                for (int i = 0; i < dtBarStatus.Rows.Count; i++)
                {
                    if (dtBarStatus.Rows[i][1].ToString().Equals(String.Empty))
                    {
                        statusState = dtBarStatus.Rows[i][0].ToString();
                    }
                    else
                    {
                        statusState = dtBarStatus.Rows[i][0].ToString() + '(' + dtBarStatus.Rows[i][1].ToString() + ')'; ;
                    }
                    strBarStatus = strBarStatus + "{\"x\": \"" + statusState + "\",\"y\":" + dtBarStatus.Rows[i][2].ToString() + "},";
                }
                if (dtBarStatus.Rows.Count > 0)
                {
                    strBarStatus = strBarStatus.Remove(strBarStatus.Length - 1);
                }
                jsonObj = jsonObj + "\"BarStatus\":[" + strBarStatus + "],";
                DataTable dtBarAllinOne = new DataTable();
                dtBarAllinOne = dsPivot.Tables[5];
                string strBarAllinOne = "";
                for (int i = 0; i < dtBarAllinOne.Rows.Count; i++)
                {
                    if (dtBarAllinOne.Rows[i][2].ToString().Equals(String.Empty))
                    {
                        statusState = dtBarAllinOne.Rows[i][1].ToString();
                    }
                    else
                    {
                        statusState = dtBarAllinOne.Rows[i][1].ToString() + '(' + dtBarAllinOne.Rows[i][2].ToString() + ')'; ;
                    }
                    strBarAllinOne = strBarAllinOne + "{\"team\": \"" + dtBarAllinOne.Rows[i][0].ToString() + "\",\"Request_Status\": \"" + statusState + "\",\"value\":" + dtBarAllinOne.Rows[i][3].ToString() + "},";
                }
                if (dtBarAllinOne.Rows.Count > 0)
                {
                    strBarAllinOne = strBarAllinOne.Remove(strBarAllinOne.Length - 1);
                }
                jsonObj = jsonObj + "\"BarAllinOne\":[" + strBarAllinOne + "]";
                DataTable dtPie = new DataTable();
                dtPie = dsPivot.Tables[6];
                string strPie = "";
                string classNames = "";
                float timebooked = 0;
                for (int i = 0; i < dtPie.Rows.Count; i++)
                {
                    float value = 0;
                    object value1 = dtPie.Rows[i][2];
                    if (value1 != DBNull.Value)
                    {
                        value = float.Parse(dtPie.Rows[i][2].ToString());
                        if (i == 3)
                        {
                            value -= timebooked;
                        }
                    }

                    if (i == 0)
                    {
                        timebooked = value;
                    }

                    if (value != 0.0)
                    {
                        strPie = strPie + "{ \"label\": \"" + dtPie.Rows[i][0].ToString() + "\",\"class\": \"" + dtPie.Rows[i][1].ToString() + "\",\"value\":" + value + "},";
                        classNames += "\"" + dtPie.Rows[i][1].ToString() + "\",";
                    }
                }
                if (strPie.Length > 0)
                {
                    strPie = strPie.Remove(strPie.Length - 1);
                    classNames = classNames.Remove(classNames.Length - 1);
                }
                jsonObj = jsonObj + ",\"Pie\":[" + strPie + "],\"PieClass\":[" + classNames + "],";

                DataTable dtBarTimeBar = new DataTable();
                dtBarTimeBar = dsPivot.Tables[7];
                string strBarTimeBar = "";
                string TimeBarState = "";
                for (int i = 0; i < dtBarTimeBar.Rows.Count; i++)
                {
                    if (dtBarTimeBar.Rows[i][0].ToString().Equals(String.Empty))
                    {
                        TimeBarState = "Unknown?";
                    }
                    else
                    {
                        TimeBarState = dtBarTimeBar.Rows[i][0].ToString();
                    }
                    strBarTimeBar = strBarTimeBar + "{ \"x\": \"" + TimeBarState + "\",\"y\":\"" + dtBarTimeBar.Rows[i][1].ToString() + "\"},";
                }
                if (dtBarTimeBar.Rows.Count > 0)
                {
                    strBarTimeBar = strBarTimeBar.Remove(strBarTimeBar.Length - 1);
                }
                jsonObj = jsonObj + "\"BarTimeBar\":[" + strBarTimeBar + "],";

                DataTable dtBarTime = new DataTable();
                dtBarTime = dsPivot.Tables[8];
                string strBarTime = "";
                for (int i = 0; i < dtBarTime.Rows.Count; i++)
                {
                    strBarTime = strBarTime + "{ \"x\": \"" + dtBarTime.Rows[i][0].ToString() + "\",\"y\":\"" + dtBarTime.Rows[i][1].ToString() + "\"},";
                }
                if (dtBarTime.Rows.Count > 0)
                {
                    strBarTime = strBarTime.Remove(strBarTime.Length - 1);
                }
                jsonObj = jsonObj + "\"BarTime\":[" + strBarTime + "]";
                jsonObj = jsonObj + "}";
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, jsonObj);
        }

        //Function to save dashboard settings from the dashboard modal
        [Route("api/Dashboard/SaveDashboardSettings")]
        [HttpPost,HttpGet]
        public HttpResponseMessage SaveDashboardSettings(UserDashboarddetail UserDashboardData, string DashboardID = null)
        {
            try
            {
                // JsonData = "{\"DashboardIcon\":\"fa-picture-o\",\"DashBoardName\":\"test_vivek\",\"IsIndays\":0,\"IsPrivacySetting\":1,\"SharedUserId\":\"10615260\",\"SharedUserName\":\"Vivek Kumar Maurya(10615260)\",\"DashboardStartDate\":\"26-Jul-2016\",\"DashboardEndDate\":\"26-Aug-2016\",\"Request\":\"Business Critical,Change Request,Incident,Project Task,Service Request,Task\",\"HiercharchyFilter\":[{\"GenericKey\":\"ASSIGNEE\",\"GenericValue\":\"Thomas Kronstrøm\"}],\"Request_Status\":\"Assigned,Closed,Completed,In Progress,New,Pending\",\"Criticality\":\"Bronze,Gold,Platinum,Silver\",\"Request_Priority\":\"BUSINESS CRITICAL,HIGH,NORMAL,LOW\",\"DashBoardID\":\"DASH00087\",\"UserID\":\"10615260\",\"UserName\":\"Vivek Kumar Maurya\",\"GroupDetailFilter\":[{\"GroupID\":\"99\",\"GroupType\":\"0\",\"GroupView\":\"2\",\"GroupName\":\"Group 1_\",\"GroupIndex\":\"2\"},{\"GroupID\":\"100\",\"GroupType\":\"0\",\"GroupView\":\"2\",\"GroupName\":\"Group 2 _\",\"GroupIndex\":\"1\"}],\"IsCursol\":\"True\",\"IsScoreCard\":\"False\",\"IsWithSLA\":\"2\"}";
                //JsonData = "{\"DashboardIcon\":\"fa-picture-o\",\"DashBoardName\":\"vivek_private\",\"IsIndays\":0,\"IsPrivacySetting\":0,\"SharedUserId\":\"10615260\",\"SharedUserName\":\"Vivek Kumar Maurya(10615260)\",\"DashboardStartDate\":\"01-Jan-2016\",\"DashboardEndDate\":\"30-Sep-2016\",\"Request\":\"Business Critical,Change Request,Incident,Project Task,Service Request,Task\",\"HiercharchyFilter\":[{\"GenericKey\":\"ASSIGNED_GROUP\",\"GenericValue\":\"KBN-Enovia\"}],\"Request_Status\":\"Assigned,Closed,Completed,In Progress,New,Pending\",\"Criticality\":\"Bronze,Gold,Platinum,Silver\",\"Request_Priority\":\"BUSINESS CRITICAL,HIGH,NORMAL,LOW\",\"DashBoardID\":\"DASH00097\",\"UserID\":\"10615260\",\"UserName\":\"Vivek Kumar Maurya\",\"IsWithSLA\":0,\"IsCursol\":true,\"IsScoreCard\":true,\"GroupDetailFilter\":[{\"GroupID\":124,\"GroupType\":0,\"GroupName\":\"g1\",\"GroupView\":2,\"KPI\":null,\"GroupIndex\":1,\"GroupFilter\":\"REGION\",\"GroupFilterValue\":\"AMERICAS\",\"$$hashKey\":\"object:969\"},{\"GroupID\":125,\"GroupType\":0,\"GroupName\":\"g2\",\"GroupView\":2,\"KPI\":null,\"GroupIndex\":2,\"GroupFilter\":\"REGION\",\"GroupFilterValue\":\"AMERICAS\",\"$$hashKey\":\"object:970\"},{\"GroupID\":126,\"GroupType\":0,\"GroupName\":\"g3\",\"GroupView\":2,\"KPI\":null,\"GroupIndex\":3,\"GroupFilter\":\"REGION\",\"GroupFilterValue\":\"EMEA\",\"$$hashKey\":\"object:971\"},{\"GroupID\":127,\"GroupType\":0,\"GroupName\":\"g4\",\"GroupView\":2,\"KPI\":null,\"GroupIndex\":4,\"GroupFilter\":\"REGION\",\"GroupFilterValue\":\"AMERICAS\",\"$$hashKey\":\"object:972\"}]}";
                //JsonData = "{\"DashboardIcon\":\"fa-birthday-cake\",\"DashBoardName\":\"testing demo\",\"IsIndays\":0,\"IsPrivacySetting\":1,\"SharedUserId\":\"10615260\",\"SharedUserName\":\"Vivek Kumar Maurya(10615260)\",\"DashboardStartDate\":\"01-Jan-2014\",\"DashboardEndDate\":\"25-Oct-2016\",\"Request\":\"Business Critical,Change Request,Incident,Project Task,Service Request,Task\",\"HiercharchyFilter\":[{\"GenericKey\":\"ASSIGNED_GROUP\",\"GenericValue\":\"#Project Assigned,Atlas-Operations,KBN-eCAD Applications,KBN-Enovia,KBN-Enterprise Architecture,KBN-EPDS,KBN-Exchange,KBN-Feedback Management\"}],\"Request_Status\":\"Assigned,Closed,Completed,In Progress,New,Pending\",\"Criticality\":\"Bronze,Gold,Platinum,Silver\",\"Request_Priority\":\"BUSINESS CRITICAL,HIGH,NORMAL,LOW\",\"DashBoardID\":\"DASH00111\",\"UserID\":\"10615260\",\"UserName\":\"Vivek Kumar Maurya\",\"IsWithSLA\":0,\"IsCursol\":true,\"IsScoreCard\":true,\"GroupDetailFilter\":[{\"GroupID\":142,\"GroupType\":0,\"GroupName\":\"g1\",\"GroupView\":2,\"KPI\":null,\"GroupIndex\":1,\"GroupFilter\":\"REGION\",\"GroupFilterValue\":\"EMEA,AMERICAS\",\"DashBoardID\":null,\"IsFilter\":\"True\",\"$$hashKey\":\"object:877\"},{\"GroupID\":147,\"GroupType\":0,\"GroupName\":\"g2\",\"GroupView\":2,\"KPI\":null,\"GroupIndex\":2,\"GroupFilter\":\"ROOT_CATEGORY_TIER_1\",\"GroupFilterValue\":\"Cloud,Commercial\",\"DashBoardID\":null,\"IsFilter\":\"True\",\"$$hashKey\":\"object:878\"},{\"GroupID\":\"\",\"GroupType\":0,\"GroupName\":\"g6\",\"GroupView\":2,\"KPI\":null,\"GroupIndex\":3,\"GroupFilter\":\"\",\"GroupFilterValue\":\"\",\"DashBoardID\":null,\"IsFilter\":false,\"$$hashKey\":\"object:1121\"}]}";
                string Jsonval = UserDashboardData.Userval;
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                UserDashboard UserDashboardDt = (UserDashboard)serializer.Deserialize(Jsonval, typeof(UserDashboard));
                return Request.CreateResponse(HttpStatusCode.OK, DashboardRepositoryInstance.SaveDashboardSettings(DashboardID, UserDashboardDt));
               
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        //Fnction called to delete either a Dashboard or a Widget KPI
        //vivek Modified: Added HttpGet and the Parameter GRPID
        [Route("api/Dashboard/DeleteDashboardOrWidgetKPI")]
        [HttpPost, HttpGet]
        public HttpResponseMessage DeleteDashboardOrWidgetKPI(string DashboardID = null, string IsDelDash = null, string KPIID = null, string GroupKey=null)
        {
            string delete = "";
            try
            {
                delete = DashboardRepositoryInstance.DeleteDashboardOrWidgetKPI(DashboardID, IsDelDash, KPIID, GroupKey);
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, delete);
        }

        [Route("api/Dashboard/GetUserDataToMapData")]
        [HttpGet]
        public HttpResponseMessage GetUserDataToMapData(string Keyword)
        {
            var hiddenIDs = new List<object>();
            
            StringBuilder SearchData=new StringBuilder();
            DirectoryEntry directory = new DirectoryEntry();
            directory.Path = "GC://dc=lntinfotech,dc=com";
            List<string> items = new List<string>();
            string strPsno = "";
            string strName = "";
            string strEmail = "";
            int value;
            
            try
            {
                byte[] asciiBytes = Encoding.ASCII.GetBytes(Keyword);
                StringBuilder SearchJSON = new StringBuilder();
                if (int.TryParse(Keyword, out value))
                {
                    int counter = 0;
                    string filter = "(&(sAMAccountName=" + Keyword + "*))";
                    string[] strCats = { "sAMAccountName" };
                    DirectorySearcher dirComp = new DirectorySearcher(directory, filter, strCats, SearchScope.Subtree);
                    SearchResultCollection results = dirComp.FindAll();

                    foreach (SearchResult result in results)
                    {
                        if (counter != 5)
                        {
                            strPsno = result.Properties["sAMAccountName"][0].ToString();
                            strName = "";
                            DirectorySearcher deSearch = new DirectorySearcher();
                            DirectoryEntry objDir = new DirectoryEntry(directory.Path);
                            deSearch.SearchRoot = objDir;
                            deSearch.Filter = "(&(objectCategory=person)(objectClass=user) (SAMAccountName=" + strPsno + "))";
                            SearchResultCollection resultsName = deSearch.FindAll();
                            foreach (SearchResult searchResult in resultsName)
                            {

                                strName = searchResult.Properties["CN"][0].ToString();
                                strEmail = searchResult.Properties["Mail"][0].ToString();

                            }
                            hiddenIDs.Add(new { _id = strPsno, name = strName + "(" + strPsno + ")" });
                            SearchData.Append("\"" + strName + "(" + strPsno + ")" + "\",");
                            counter++;
                        }
                    }
                    SearchData = SearchData.Remove(SearchData.Length - 1, 1);
                }
                else
                {
                    int counter = 0;
                    string filter = "(&(CN=" + Keyword + "*))";
                    string[] strCats = { "CN" };
                    DirectorySearcher dirComp = new DirectorySearcher(directory, filter, strCats, SearchScope.Subtree);
                    SearchResultCollection results = dirComp.FindAll();
                    foreach (SearchResult result in results)
                    {
                        if (counter != 5)
                        {
                            strName = result.Properties["CN"][0].ToString();
                            strPsno = "";
                            DirectorySearcher deSearch = new DirectorySearcher();
                            DirectoryEntry objDir = new DirectoryEntry(directory.Path);
                            deSearch.SearchRoot = objDir;
                            deSearch.Filter = "(&(objectCategory=person)(objectClass=user) (CN=" + strName + "))";
                            SearchResultCollection resultsName = deSearch.FindAll();
                            foreach (SearchResult searchResult in resultsName)
                            {

                                strPsno = searchResult.Properties["sAMAccountName"][0].ToString();
                                strEmail = searchResult.Properties["Mail"][0].ToString();
                            }
                            hiddenIDs.Add(new { id = strPsno, name = strName + "(" + strPsno + ")" });
                            SearchData.Append("\"" + strName + "(" + strPsno + ")" + "\",");
                            counter++;
                        }
                    }
                    SearchData = SearchData.Remove(SearchData.Length - 1, 1);
                }
                var SearchUser=JsonConvert.SerializeObject(hiddenIDs);
                SearchJSON.Append("[{\"SearchUser\":" + SearchUser + "},");
                SearchJSON.Append("{\"UserData\":["+SearchData+"]}]");
                return Request.CreateResponse(HttpStatusCode.OK, SearchJSON);
            }
            catch(Exception ex)
            {
                string SearchJSON = "";
                SearchJSON = "[{\"SearchUser\":[{\"id\":\"10606473\",\"name\":\"Manivannan Kaluvan(10606473)\"}," +
                "{\"id\":\"10607401\",\"name\":\"Karthikeyan Elango(10607401)\"}," +
                "{\"id\":\"10615217\",\"name\":\"HemalathaS Sundaravadivelu(10615217)\"}," +
                "{\"id\":\"277070\",\"name\":\"Vijay Nair(277070)\"}," +
                "{\"id\":\"10617374\",\"name\":\"Padamatinti Archana(10617374)\"}," +
                "{\"id\":\"10609979\",\"name\":\"Vigneshwaran Saravanan(10609979)\"}," +
                "{\"id\":\"10607086\",\"name\":\"Ashmitha Srinivas(10607086)\"}]}," +
                "{\"UserData\":[\"Manivannan Kaluvan(10606473)\",\"Vijay Nair(277070)\",\"HemalathaS Sundaravadivelu(10615217)\",\"Karthikeyan Elango(10607401)\",\"Padamatinti Archana(10617374)\",\"Vigneshwaran Saravanan(10609979)\",\"Ashmitha Srinivas(10607086)\"]}]";
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.OK, SearchJSON);
            }
        }

        //needs to verify
        [Route("api/Dashboard/GetUserID")]
        public IHttpActionResult Get()
        {
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            var userName = principal.Claims.Where(c => c.Type == "sub").Single().Value;
            return Ok("You are allowed to request data");
        }


        //Function to clone dashboard settings from the existing dashboard
        [Route("api/Dashboard/CloneDashboardSettings")]
        [HttpPost, HttpGet]
        public HttpResponseMessage CloneDashboardSettings(CloneDashboard CloneDboardDetail, string DashboardID = null)
        {
            //DashboardID = "DASH00136";
            //string Jsonval = "{\"Icon\":\"fa-picture-o\",\"NewDbName\":\"Clone_test1\"}";
            try
            {
                string Jsonval = CloneDboardDetail.CloneDb;
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                CloneDashboarddetail UserDashboardDt = (CloneDashboarddetail)serializer.Deserialize(Jsonval, typeof(CloneDashboarddetail));
                return Request.CreateResponse(HttpStatusCode.OK, DashboardRepositoryInstance.CloneDashboardSetting(DashboardID, UserDashboardDt));

            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

    [Route("api/Dashboard/GetAppInfoDetail")]
    [HttpPost,HttpGet]
    public HttpResponseMessage GetAppInfoDetail()
        {
            string output = "";
            try
            {
                AdminDetailCollection AppDetailInfo = new AdminDetailCollection();
                AppDetailInfo = DashboardRepositoryInstance.AppInfoDetail();
                StringBuilder sbJSON = new StringBuilder();
                output = new JavaScriptSerializer().Serialize(AppDetailInfo);
                return Request.CreateResponse(HttpStatusCode.OK, output);
                
              //  return Request.CreateResponse(HttpStatusCode.OK, DashboardRepositoryInstance.AppInfoDetail());

            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

        }
    }
}