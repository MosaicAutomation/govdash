﻿using log4net;
using Newtonsoft.Json;
using SeeIT_Common.Exception_Log;
using SeeIT_Common.Helper;
using SeeIT_Common.Model;
using SeeIT_Data.EDMX;
using SeeIT_Data.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace SeeIT_API.Controllers
{
    public class KPIController : ApiController
    {

        KPIRepository KPIRepositoryInstance = new KPIRepository();
        ErrorEntry errorLoger = new ErrorEntry();
        private static readonly log4net.ILog log = LogManager.GetLogger(typeof(AdministratorController));
        UserSettingRepository UserSettingRepository = new UserSettingRepository();

        HelperFunctions helper = new HelperFunctions();

        [Route("api/KPI/GetDataKPIAgingReport")]
        [HttpGet]
        public HttpResponseMessage GetDataKPIAgingReport(string DashboardId, string KPIID)
        {
            DataSet dsPivot = new DataSet();

            var IsLogin = UserSettingRepository.IsLoginForm();
            string UserId = helper.GetUserID(IsLogin);
            //Vivek
            //if (HttpContext.Current.Request.Cookies["UserId"].Value != null)
            //{
            //    UserId = HttpContext.Current.Request.Cookies["UserId"].Value.ToString();
            //    string[] ArrayUserID = UserId.Split('=');
            //    UserId = ArrayUserID[1];
            //}
            dsPivot = KPIRepositoryInstance.GetDataKPIAgingReport(UserId, DashboardId, KPIID);
            StringBuilder sbJSON = new StringBuilder();
            UserKPIDashboard UserKPIDashboardDetails = new UserKPIDashboard();
            using (SeeITEntities context = new SeeITEntities())
            {
                UserKPIDashboardDetails = (from userkpimapping in context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL
                                           join userAging in context.DBOARD_COMPUTED_SYSTEM_AGING_KPI
                                           on userkpimapping.KPI_ID equals userAging.KPI_ID
                                           where userkpimapping.KPI_ID == KPIID && userkpimapping.DASHBOARD_ID == DashboardId
                                           select new UserKPIDashboard
                                           {
                                               KPIName = userkpimapping.KPI_NAME,
                                               ChartColor = userkpimapping.Chart_Color,
                                               GroupData = userAging.AGING_COMMON_VALUES
                                           }).FirstOrDefault();
            }
            sbJSON.Append("{\"KPIName\":\"" + UserKPIDashboardDetails.KPIName + "\",\"ChartColor\":\"" + UserKPIDashboardDetails.ChartColor + "\",");
            sbJSON.Append("\"GroupData\":\"" + UserKPIDashboardDetails.GroupData + "\",");
            sbJSON.Append("\"AgingChartData\":[[\"x\",");
            for (var k = 1; k < dsPivot.Tables[0].Columns.Count; k++)
            {
                sbJSON.Append("\"" + dsPivot.Tables[0].Columns[k].ColumnName + "\",");
            }
            sbJSON = sbJSON.Remove(sbJSON.Length - 1, 1);
            sbJSON.Append("],");
            for (var i = 0; i < dsPivot.Tables[0].Rows.Count; i++)
            {
                sbJSON.Append("[");
                for (var j = 0; j < dsPivot.Tables[0].Columns.Count; j++)
                {
                    var rowval = (dsPivot.Tables[0].Rows[i][j].ToString() != "") ? dsPivot.Tables[0].Rows[i][j].ToString() : "0";
                    sbJSON.Append("\"" + rowval + "\",");
                }
                sbJSON = sbJSON.Remove(sbJSON.Length - 1, 1);
                sbJSON.Append("],");
            }
            sbJSON = sbJSON.Remove(sbJSON.Length - 1, 1);
            sbJSON.Append("]}");
            return Request.CreateResponse(HttpStatusCode.OK, sbJSON.ToString());
        }

        [Route("api/KPI/GetDataAvgResponseResolution")]
        [HttpGet, HttpOptions, HttpPost]
        public HttpResponseMessage GetDataAvgResponseResolution(string DashboardId,string KPIID,string isResp)
        {
            KPIAvgResponseResolution AvgResponseResolution = new KPIAvgResponseResolution();
            try
            {
                AvgResponseResolution = KPIRepositoryInstance.GetDataAvgResponseResolution(DashboardId,KPIID,isResp);
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(AvgResponseResolution));
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/KPI/GetDataKPIIncomingVSCompleted")]
        [HttpGet]
        public HttpResponseMessage GetDataKPIIncomingVSCompleted( string DashboardId, string KPIID)
        {
            KPIIncomingVSCompleted IncomingVSCompleted = new KPIIncomingVSCompleted();
            try
            {
                IncomingVSCompleted = KPIRepositoryInstance.GetDataKPIIncomingVSCompleted( DashboardId, KPIID);
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(IncomingVSCompleted));
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/KPI/GetDataKPIResponseExceeding")]
        [HttpGet]
        public HttpResponseMessage GetDataKPIResponseExceeding(string DashboardId, string KPIID)
        {
            KPIExceeding ResponseExceedingCount = new KPIExceeding();
            try
            {
                ResponseExceedingCount = KPIRepositoryInstance.GetDataKPIResponseExceeding( DashboardId, KPIID);
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(ResponseExceedingCount));
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/KPI/GetDataKPIResolveExceeding")]
        [HttpGet]
        public HttpResponseMessage GetDataKPIResolveExceeding(string DashboardId, string KPIID)
        {
            KPIExceeding ResolveExceedingCount = new KPIExceeding();
            try
            {
                ResolveExceedingCount = KPIRepositoryInstance.GetDataKPIResolveExceeding( DashboardId, KPIID);
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(ResolveExceedingCount));
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/KPI/GetDataKPIScoreboard")]
        [HttpGet]
        public HttpResponseMessage GetDataKPIScoreboard( string DashboardId, string KPIID)
        {
            KPIGeneric GenericDataCount = new KPIGeneric();
            try
            {
                GenericDataCount = KPIRepositoryInstance.GetDataKPIScoreboard( DashboardId, KPIID);
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(GenericDataCount));
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/KPI/GetDataKPIPareto")]
        [HttpGet,HttpPost]
        public HttpResponseMessage GetDataKPIPareto(string DashboardID, string KPIID, GetPartoValue GetParetoData)
        {
            UserKPIDashboard UserKPIDashboardDetails = new UserKPIDashboard();

            string SpinnerData = GetParetoData.SpinnerData;
            string PrioritySelected = GetParetoData.PrioritySelected;
            try
            {
                using (SeeITEntities context = new SeeITEntities())
                {
                    UserKPIDashboardDetails = (from userkpimapping in context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL
                                               where userkpimapping.KPI_ID == KPIID && userkpimapping.DASHBOARD_ID == DashboardID
                                               select new UserKPIDashboard
                                               {
                                                   ParetoBarUnit = userkpimapping.PARETO_BAR_UNIT
                                               }).FirstOrDefault();

                }

                if (UserKPIDashboardDetails.ParetoBarUnit == "INCOMING_REQUEST" || UserKPIDashboardDetails.ParetoBarUnit == "COMPLETE_REQUEST" || UserKPIDashboardDetails.ParetoBarUnit == "BACKLOG_REQUEST")
                {
                    KPIIncomingVSCompleted IncomingVSCompleted = new KPIIncomingVSCompleted();
                    IncomingVSCompleted = KPIRepositoryInstance.GetDataKPIParetoInc( DashboardID, KPIID, SpinnerData, PrioritySelected);
                    var json = JsonConvert.SerializeObject(IncomingVSCompleted);
                    return Request.CreateResponse(HttpStatusCode.OK, json.Replace("IncomingVSCompleted", "Pareto"));
                }
                if (UserKPIDashboardDetails.ParetoBarUnit == "RESOLVE_EXCEEDING_PERC" || UserKPIDashboardDetails.ParetoBarUnit == "RESPONSE_EXCEEDING_PERC" || UserKPIDashboardDetails.ParetoBarUnit == "EXCEEDING_RESP_KPI" || UserKPIDashboardDetails.ParetoBarUnit == "EXCEEDING_RESLN_KPI")
                {
                    KPIExceeding ParetoExceedingCount = new KPIExceeding();
                    ParetoExceedingCount = KPIRepositoryInstance.GetDataKPIParetoResolveResolveExceeding( DashboardID, KPIID, SpinnerData, PrioritySelected);
                    var json = JsonConvert.SerializeObject(ParetoExceedingCount);
                    return Request.CreateResponse(HttpStatusCode.OK, json.Replace("IncomingVSCompleted", "Pareto"));
                }
                if (UserKPIDashboardDetails.ParetoBarUnit == "REOPEN_COUNT")
                {
                    KPIReOpenCount ParetoExceedingCount = new KPIReOpenCount();
                    ParetoExceedingCount = KPIRepositoryInstance.GetDataKPIParetoReOpen(DashboardID, KPIID, SpinnerData, PrioritySelected);
                    var json = JsonConvert.SerializeObject(ParetoExceedingCount);
                    return Request.CreateResponse(HttpStatusCode.OK, json.Replace("IncomingVSCompleted", "Pareto"));
                }
                else
                {
                    KPIPareto ParetoData = new KPIPareto();
                    ParetoData = KPIRepositoryInstance.GetDataKPIPareto( DashboardID, KPIID, SpinnerData, PrioritySelected);
                    var json = JsonConvert.SerializeObject(ParetoData);
                    return Request.CreateResponse(HttpStatusCode.OK, json);
                }
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        //[Authorize]
        [Route("api/KPI/GetDataKPIPivotChart")]
        [HttpGet]
        public HttpResponseMessage GetDataKPIPivotChart( string DashboardID, string KPIID)
        {
            try
            {
                DataSet dsPivot = new DataSet();
                dsPivot = KPIRepositoryInstance.GetDataKPIPivot( DashboardID, KPIID);
                StringBuilder sbJSON = new StringBuilder();
                UserKPIDashboard UserKPIDashboardDetails = new UserKPIDashboard();
                using (SeeITEntities context = new SeeITEntities())
                {
                    UserKPIDashboardDetails = (from userkpimapping in context.DBOARD_COMPUTED_SYSTEM_KPI_DETAIL
                                               where userkpimapping.KPI_ID == KPIID && userkpimapping.DASHBOARD_ID == DashboardID
                                               select new UserKPIDashboard
                                               {
                                                   KPIName = userkpimapping.KPI_NAME,
                                                   ChartColor = userkpimapping.Chart_Color,
                                                   Request_Priority = "TOTAL," + userkpimapping.PRIORITY_CHOOSEN,
                                                   GroupData=userkpimapping.GROUP_DATA,
                                                   SplitData=userkpimapping.SPLIT_DATA
                                               }).FirstOrDefault();
                }
                sbJSON.Append("{\"KPIName\":\"" + UserKPIDashboardDetails.KPIName + "\",\"ChartColor\":\"" + UserKPIDashboardDetails.ChartColor + "\",");
                sbJSON.Append("\"GroupData\":\"" + UserKPIDashboardDetails.GroupData + "\",\"SplitData\":\"" + UserKPIDashboardDetails.SplitData + "\",");
                sbJSON.Append("\"Priority\":\"" + UserKPIDashboardDetails.Request_Priority + "\",");
                var query = (from t in dsPivot.Tables[0].AsEnumerable()
                             select t["PRIORITY"]).Distinct().ToArray();
                int colcount = dsPivot.Tables[0].Columns.Count;
                sbJSON.Append("\"PivotChartData\":[");
                string FirstColName = dsPivot.Tables[0].Columns[0].ColumnName.ToString();
                foreach (var item in query)
                {
                    sbJSON.Append("{\"Priority\":\"" + item.ToString() + "\",\"PivotValue\":[[\"x\",");
                    var ReqData = (from t in dsPivot.Tables[0].AsEnumerable()
                                   where t.Field<string>("PRIORITY") == item.ToString()
                                   select t.Field<string>(FirstColName)).Distinct().ToArray();

                    for (var kl = 0; kl < ReqData.Length; kl++)
                    {
                        sbJSON.Append("\"" + ReqData[kl] + "\",");
                    }
                    sbJSON = sbJSON.Remove(sbJSON.Length - 1, 1);
                    sbJSON.Append("],");
                    for (var k = 1; k < dsPivot.Tables[0].Columns.Count - 1; k++)
                    {
                        sbJSON.Append("[\"" + dsPivot.Tables[0].Columns[k].ColumnName + "\",");
                        for (var i = 0; i < dsPivot.Tables[0].Rows.Count; i++)
                        {
                            if (item.ToString() == dsPivot.Tables[0].Rows[i][colcount - 1].ToString())
                            {
                                sbJSON.Append("\"" + dsPivot.Tables[0].Rows[i][k] + "\",");
                            }
                        }
                        sbJSON = sbJSON.Remove(sbJSON.Length - 1, 1);
                        sbJSON.Append("],");
                    }
                    sbJSON = sbJSON.Remove(sbJSON.Length - 1, 1);
                    sbJSON.Append("]},");
                }
                sbJSON = sbJSON.Remove(sbJSON.Length - 1, 1);
                sbJSON.Append("]}");
                return Request.CreateResponse(HttpStatusCode.OK, sbJSON.ToString());
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/KPI/GetDataKPIPivot")]
        [HttpGet]
        public HttpResponseMessage GetDataKPIPivot( string DashboardID, string KPIID)
        {
            try
            {
                DataSet dsPivot = new DataSet();
                StringBuilder sbJSON = new StringBuilder();
                string GroupBy = "";
                string SplitBy = "";
                dsPivot = KPIRepositoryInstance.GetDataKPIPivot( DashboardID, KPIID);
                sbJSON.Append("{\"gridHeader\":[");
                GroupBy = dsPivot.Tables[1].Rows[0][0].ToString();
                SplitBy = dsPivot.Tables[1].Rows[0][1].ToString();
                int groupByLength = GroupBy.Split(',').Length - 1;
                int counter = 0;
                int rowGroupIndex = 0;
                foreach (DataColumn column in dsPivot.Tables[0].Columns)
                {
                    if (counter < groupByLength)
                    {
                        string ColumnName = column.ColumnName;
                        sbJSON.Append("{\"headerName\"" + ":" + "\"" + ColumnName + "\"" + ",\"field\":\"" + ColumnName + "\", \"rowGroupIndex\":" + rowGroupIndex + ",\"width\":" + "120" + "},");
                        rowGroupIndex++;
                    }
                    else if (counter > groupByLength)
                    {
                        string ColumnName = column.ColumnName;
                        sbJSON.Append("{\"headerName\"" + ":" + "\"" + ColumnName + "\"" + ",\"field\":\"" + ColumnName + "\",\"aggFunc\":\"sum\",\"width\":" + "120" + "},");
                    }
                    counter++;
                }
                sbJSON = sbJSON.Remove(sbJSON.Length - 1, 1);
                sbJSON.Append("],\"gridData\": [");
                foreach (DataRow row1 in dsPivot.Tables[0].Rows)
                {
                    sbJSON.Append("{");
                    foreach (DataColumn column in dsPivot.Tables[0].Columns)
                    {
                        string ColumnName = column.ColumnName;
                        string ColumnData = row1[column].ToString();
                        int num;
                        bool isNum = Int32.TryParse(ColumnData, out num);
                        if (isNum)
                        {
                            sbJSON.Append("\"" + ColumnName + "\":" + ColumnData + ",");
                        }
                        else
                        {
                            sbJSON.Append("\"" + ColumnName + "\":\"" + ColumnData + "\",");
                        }
                    }
                    sbJSON = sbJSON.Remove(sbJSON.Length - 1, 1);
                    sbJSON.Append("},");
                }

                sbJSON = sbJSON.Remove(sbJSON.Length - 1, 1);
                sbJSON.Append("],\"groupBy\":[" + GroupBy + "],\"splitBy\": [" + SplitBy);
                sbJSON.Append("],\"KPIHeaderName\": ");
                sbJSON = sbJSON.Append(JsonConvert.SerializeObject(dsPivot.Tables[2], Formatting.Indented));
                sbJSON.Append("}");
                return Request.CreateResponse(HttpStatusCode.OK, sbJSON);
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

        }

        [Route("api/KPI/GetDrillDownData")]
        [HttpGet,HttpPost]
       // public HttpResponseMessage GetDrillDownData(string DashboardID, string KPIID, int? ISMonth = null, string Request_Priority = null, string YearMonthWeek = null, string Exceeding = null, string FirstGenericData = null, string SecondGenericData = null)
        public HttpResponseMessage GetDrillDownData(string DashboardID, string KPIID,DrilldownData DrillDownValues)
        {
            StringBuilder sbJSON = new StringBuilder();
            string json = DrillDownValues.Userval;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DrilldownData userval = (DrilldownData)serializer.Deserialize(json, typeof(DrilldownData));
            
            DataSet dsPivot = new DataSet();
            try
            {
                string IsPeriodic = KPIRepositoryInstance.GetKPISettingInfo(KPIID, DashboardID);

                if (IsPeriodic=="Y")
                {
                     dsPivot = KPIRepositoryInstance.GetRemedyData(KPIID, DashboardID);
                }
                else
                {

                    dsPivot = KPIRepositoryInstance.GetDrillDownData(DashboardID, KPIID, userval.Request_Priority, userval.ISMonth, userval.YearMonthWeek, userval.Exceeding, userval.FirstGenericData, userval.SecondGenericData,userval.ThirdGenericData);
                }

                sbJSON.Append("{\"gridHeader\":[");
                foreach (DataColumn column in dsPivot.Tables[0].Columns)
                {
                    string ColumnName = column.ColumnName;
                    sbJSON.Append("{\"headerName\"" + ":" + "\"" + ColumnName + "\"" + ",\"field\":\"" + ColumnName + "\",\"width\":" + 200 + ",\"cellTemplate\":\"" + "" + "\"},");
                }
                sbJSON = sbJSON.Remove(sbJSON.Length - 1, 1);
                sbJSON.Append("],\"gridData\": ");
                sbJSON = sbJSON.Append(JsonConvert.SerializeObject(dsPivot.Tables[0], Formatting.Indented));
                sbJSON.Append("}");
             
                
                return Request.CreateResponse(HttpStatusCode.OK, sbJSON);
             
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/KPI/GetTreeMapping")]
        [HttpGet]
        public HttpResponseMessage GetTreeMapping(string DashboardId, string KPIID)
        {
            try
            {
                DataSet dsPivot = new DataSet();
                StringBuilder sbJSON = new StringBuilder();
                dsPivot = KPIRepositoryInstance.GetTreeMapping( DashboardId, KPIID);
                DataTable dt = dsPivot.Tables[0];
                DataTable dtTree = dsPivot.Tables[1];
                string rootName = dtTree.Rows[0][4].ToString();
                for (var i = 0; i < dt.Columns.Count - 2; i++)
                {
                    dt.Columns[i].ColumnName = "Key_L" + i;
                }
                var tree = helper.Descend(dt.AsEnumerable(), 0, dt.Columns.Count - 3);
                var serializer = new JavaScriptSerializer();
                var x = serializer.Serialize(tree);
                string myString = x.ToString().Substring(1, x.Length - 1);
                myString = "[{\"TreeMapDetails\":{\"name\": \""+ rootName + "\",\"children\":[" + myString + "}},";
                myString = myString + "{\"KPIDetails\": {\"KPIName\":\"" + dtTree.Rows[0][0] + "\",\"ChartColor\":\"" + dtTree.Rows[0][1] + "\"}}]";
                return Request.CreateResponse(HttpStatusCode.OK, myString);
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/KPI/GetWordCloud")]
        [HttpGet]
        public HttpResponseMessage GetWordCloud( string DashboardId, string KPIID)
        {
            try
            {
                DataSet dsPivot = new DataSet();
                StringBuilder sbJSON = new StringBuilder();
                dsPivot = KPIRepositoryInstance.GetWordCloud( DashboardId, KPIID);
                DataTable dt = dsPivot.Tables[1];
                string myString = "";
                myString = "{\"KPIDetails\": {\"KPIName\":\"" + dt.Rows[0][0] + "\",\"ChartColor\":\"" + dt.Rows[0][1] + "\"},\"KPIwords\":"+ JsonConvert.SerializeObject(dsPivot.Tables[0], Formatting.Indented) + "}";
                return Request.CreateResponse(HttpStatusCode.OK, myString);
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/KPI/GetHourHeatmap")]
        [HttpGet]
        public HttpResponseMessage GetHourHeatmap( string DashboardId, string KPIID)
        {
            try
            {
                KPIHeatMapHour HeatMapHour = new KPIHeatMapHour();
                HeatMapHour = KPIRepositoryInstance.GetHourHeatmap( DashboardId, KPIID);
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(HeatMapHour));
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/KPI/GetDayHeatmap")]
        [HttpGet]
        public HttpResponseMessage GetDayHeatmap(string DashboardId, string KPIID)
        {
            try
            {
                KPIHeatMapDay HeatMapDay = new KPIHeatMapDay();
                HeatMapDay = KPIRepositoryInstance.GetDayHeatmap( DashboardId, KPIID);
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(HeatMapDay));
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
